<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => 'Case Vacanze Budoni Sardegna-Solaria Vacanze Immobiliare',
            'description'  => 'Solaria Vacanze opera da anni principalmente nel campo degli affitti, il nostro obiettivo è quello di offrire le migliori residenze estive sul mercato.', // set false to total remove
            'separator'    => ' ',
            'keywords'     => ['Case', 'Vacanze', 'Budoni', 'Sardegna-Solaria', 'Vacanze', 'Immobiliare',],
            'canonical'    => env('APP_URL'),
            'author'    => env('APP_NAME'),
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Case Vacanze Budoni Sardegna-Solaria Vacanze Immobiliare', // set false to total remove
            'description' => 'Solaria Vacanze opera da anni principalmente nel campo degli affitti, il nostro obiettivo è quello di offrire le migliori residenze estive sul mercato.', // set false to total remo', // set false to total remove
            'url'         => env('APP_URL'), // Set null for using Url::current(), set false to total remove
            'type'        => 'website',
            'site_name'   => env('APP_NAME'),
            'images'      => [],
            'locale' => 'it_IT',
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          //'card'        => 'summary',
          'site'        => '@solariahomeholiday',
        ],
    ],
];
