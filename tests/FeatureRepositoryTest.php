<?php

use App\Models\Admin\Feature;
use App\Repositories\Admin\FeatureRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FeatureRepositoryTest extends TestCase
{
    use MakeFeatureTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var FeatureRepository
     */
    protected $featureRepo;

    public function setUp()
    {
        parent::setUp();
        $this->featureRepo = App::make(FeatureRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateFeature()
    {
        $feature = $this->fakeFeatureData();
        $createdFeature = $this->featureRepo->create($feature);
        $createdFeature = $createdFeature->toArray();
        $this->assertArrayHasKey('id', $createdFeature);
        $this->assertNotNull($createdFeature['id'], 'Created Feature must have id specified');
        $this->assertNotNull(Feature::find($createdFeature['id']), 'Feature with given id must be in DB');
        $this->assertModelData($feature, $createdFeature);
    }

    /**
     * @test read
     */
    public function testReadFeature()
    {
        $feature = $this->makeFeature();
        $dbFeature = $this->featureRepo->find($feature->id);
        $dbFeature = $dbFeature->toArray();
        $this->assertModelData($feature->toArray(), $dbFeature);
    }

    /**
     * @test update
     */
    public function testUpdateFeature()
    {
        $feature = $this->makeFeature();
        $fakeFeature = $this->fakeFeatureData();
        $updatedFeature = $this->featureRepo->update($fakeFeature, $feature->id);
        $this->assertModelData($fakeFeature, $updatedFeature->toArray());
        $dbFeature = $this->featureRepo->find($feature->id);
        $this->assertModelData($fakeFeature, $dbFeature->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteFeature()
    {
        $feature = $this->makeFeature();
        $resp = $this->featureRepo->delete($feature->id);
        $this->assertTrue($resp);
        $this->assertNull(Feature::find($feature->id), 'Feature should not exist in DB');
    }
}
