<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GeneralInformationApiTest extends TestCase
{
    use MakeGeneralInformationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateGeneralInformation()
    {
        $generalInformation = $this->fakeGeneralInformationData();
        $this->json('POST', '/api/v1/generalInformations', $generalInformation);

        $this->assertApiResponse($generalInformation);
    }

    /**
     * @test
     */
    public function testReadGeneralInformation()
    {
        $generalInformation = $this->makeGeneralInformation();
        $this->json('GET', '/api/v1/generalInformations/'.$generalInformation->id);

        $this->assertApiResponse($generalInformation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateGeneralInformation()
    {
        $generalInformation = $this->makeGeneralInformation();
        $editedGeneralInformation = $this->fakeGeneralInformationData();

        $this->json('PUT', '/api/v1/generalInformations/'.$generalInformation->id, $editedGeneralInformation);

        $this->assertApiResponse($editedGeneralInformation);
    }

    /**
     * @test
     */
    public function testDeleteGeneralInformation()
    {
        $generalInformation = $this->makeGeneralInformation();
        $this->json('DELETE', '/api/v1/generalInformations/'.$generalInformation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/generalInformations/'.$generalInformation->id);

        $this->assertResponseStatus(404);
    }
}
