<?php

use App\Models\Admin\GeneralInformation;
use App\Repositories\Admin\GeneralInformationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class GeneralInformationRepositoryTest extends TestCase
{
    use MakeGeneralInformationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var GeneralInformationRepository
     */
    protected $generalInformationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->generalInformationRepo = App::make(GeneralInformationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateGeneralInformation()
    {
        $generalInformation = $this->fakeGeneralInformationData();
        $createdGeneralInformation = $this->generalInformationRepo->create($generalInformation);
        $createdGeneralInformation = $createdGeneralInformation->toArray();
        $this->assertArrayHasKey('id', $createdGeneralInformation);
        $this->assertNotNull($createdGeneralInformation['id'], 'Created GeneralInformation must have id specified');
        $this->assertNotNull(GeneralInformation::find($createdGeneralInformation['id']), 'GeneralInformation with given id must be in DB');
        $this->assertModelData($generalInformation, $createdGeneralInformation);
    }

    /**
     * @test read
     */
    public function testReadGeneralInformation()
    {
        $generalInformation = $this->makeGeneralInformation();
        $dbGeneralInformation = $this->generalInformationRepo->find($generalInformation->id);
        $dbGeneralInformation = $dbGeneralInformation->toArray();
        $this->assertModelData($generalInformation->toArray(), $dbGeneralInformation);
    }

    /**
     * @test update
     */
    public function testUpdateGeneralInformation()
    {
        $generalInformation = $this->makeGeneralInformation();
        $fakeGeneralInformation = $this->fakeGeneralInformationData();
        $updatedGeneralInformation = $this->generalInformationRepo->update($fakeGeneralInformation, $generalInformation->id);
        $this->assertModelData($fakeGeneralInformation, $updatedGeneralInformation->toArray());
        $dbGeneralInformation = $this->generalInformationRepo->find($generalInformation->id);
        $this->assertModelData($fakeGeneralInformation, $dbGeneralInformation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteGeneralInformation()
    {
        $generalInformation = $this->makeGeneralInformation();
        $resp = $this->generalInformationRepo->delete($generalInformation->id);
        $this->assertTrue($resp);
        $this->assertNull(GeneralInformation::find($generalInformation->id), 'GeneralInformation should not exist in DB');
    }
}
