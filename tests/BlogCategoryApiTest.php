<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogCategoryApiTest extends TestCase
{
    use MakeBlogCategoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBlogCategory()
    {
        $blogCategory = $this->fakeBlogCategoryData();
        $this->json('POST', '/api/v1/blogCategories', $blogCategory);

        $this->assertApiResponse($blogCategory);
    }

    /**
     * @test
     */
    public function testReadBlogCategory()
    {
        $blogCategory = $this->makeBlogCategory();
        $this->json('GET', '/api/v1/blogCategories/'.$blogCategory->id);

        $this->assertApiResponse($blogCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBlogCategory()
    {
        $blogCategory = $this->makeBlogCategory();
        $editedBlogCategory = $this->fakeBlogCategoryData();

        $this->json('PUT', '/api/v1/blogCategories/'.$blogCategory->id, $editedBlogCategory);

        $this->assertApiResponse($editedBlogCategory);
    }

    /**
     * @test
     */
    public function testDeleteBlogCategory()
    {
        $blogCategory = $this->makeBlogCategory();
        $this->json('DELETE', '/api/v1/blogCategories/'.$blogCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/blogCategories/'.$blogCategory->id);

        $this->assertResponseStatus(404);
    }
}
