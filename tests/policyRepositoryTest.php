<?php

use App\Models\Admin\policy;
use App\Repositories\Admin\policyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class policyRepositoryTest extends TestCase
{
    use MakepolicyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var policyRepository
     */
    protected $policyRepo;

    public function setUp()
    {
        parent::setUp();
        $this->policyRepo = App::make(policyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatepolicy()
    {
        $policy = $this->fakepolicyData();
        $createdpolicy = $this->policyRepo->create($policy);
        $createdpolicy = $createdpolicy->toArray();
        $this->assertArrayHasKey('id', $createdpolicy);
        $this->assertNotNull($createdpolicy['id'], 'Created policy must have id specified');
        $this->assertNotNull(policy::find($createdpolicy['id']), 'policy with given id must be in DB');
        $this->assertModelData($policy, $createdpolicy);
    }

    /**
     * @test read
     */
    public function testReadpolicy()
    {
        $policy = $this->makepolicy();
        $dbpolicy = $this->policyRepo->find($policy->id);
        $dbpolicy = $dbpolicy->toArray();
        $this->assertModelData($policy->toArray(), $dbpolicy);
    }

    /**
     * @test update
     */
    public function testUpdatepolicy()
    {
        $policy = $this->makepolicy();
        $fakepolicy = $this->fakepolicyData();
        $updatedpolicy = $this->policyRepo->update($fakepolicy, $policy->id);
        $this->assertModelData($fakepolicy, $updatedpolicy->toArray());
        $dbpolicy = $this->policyRepo->find($policy->id);
        $this->assertModelData($fakepolicy, $dbpolicy->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletepolicy()
    {
        $policy = $this->makepolicy();
        $resp = $this->policyRepo->delete($policy->id);
        $this->assertTrue($resp);
        $this->assertNull(policy::find($policy->id), 'policy should not exist in DB');
    }
}
