<?php

use App\Models\Admin\RoomCategoryFeature;
use App\Repositories\Admin\RoomCategoryFeatureRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomCategoryFeatureRepositoryTest extends TestCase
{
    use MakeRoomCategoryFeatureTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RoomCategoryFeatureRepository
     */
    protected $roomCategoryFeatureRepo;

    public function setUp()
    {
        parent::setUp();
        $this->roomCategoryFeatureRepo = App::make(RoomCategoryFeatureRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRoomCategoryFeature()
    {
        $roomCategoryFeature = $this->fakeRoomCategoryFeatureData();
        $createdRoomCategoryFeature = $this->roomCategoryFeatureRepo->create($roomCategoryFeature);
        $createdRoomCategoryFeature = $createdRoomCategoryFeature->toArray();
        $this->assertArrayHasKey('id', $createdRoomCategoryFeature);
        $this->assertNotNull($createdRoomCategoryFeature['id'], 'Created RoomCategoryFeature must have id specified');
        $this->assertNotNull(RoomCategoryFeature::find($createdRoomCategoryFeature['id']), 'RoomCategoryFeature with given id must be in DB');
        $this->assertModelData($roomCategoryFeature, $createdRoomCategoryFeature);
    }

    /**
     * @test read
     */
    public function testReadRoomCategoryFeature()
    {
        $roomCategoryFeature = $this->makeRoomCategoryFeature();
        $dbRoomCategoryFeature = $this->roomCategoryFeatureRepo->find($roomCategoryFeature->id);
        $dbRoomCategoryFeature = $dbRoomCategoryFeature->toArray();
        $this->assertModelData($roomCategoryFeature->toArray(), $dbRoomCategoryFeature);
    }

    /**
     * @test update
     */
    public function testUpdateRoomCategoryFeature()
    {
        $roomCategoryFeature = $this->makeRoomCategoryFeature();
        $fakeRoomCategoryFeature = $this->fakeRoomCategoryFeatureData();
        $updatedRoomCategoryFeature = $this->roomCategoryFeatureRepo->update($fakeRoomCategoryFeature, $roomCategoryFeature->id);
        $this->assertModelData($fakeRoomCategoryFeature, $updatedRoomCategoryFeature->toArray());
        $dbRoomCategoryFeature = $this->roomCategoryFeatureRepo->find($roomCategoryFeature->id);
        $this->assertModelData($fakeRoomCategoryFeature, $dbRoomCategoryFeature->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRoomCategoryFeature()
    {
        $roomCategoryFeature = $this->makeRoomCategoryFeature();
        $resp = $this->roomCategoryFeatureRepo->delete($roomCategoryFeature->id);
        $this->assertTrue($resp);
        $this->assertNull(RoomCategoryFeature::find($roomCategoryFeature->id), 'RoomCategoryFeature should not exist in DB');
    }
}
