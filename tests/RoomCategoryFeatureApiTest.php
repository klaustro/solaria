<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RoomCategoryFeatureApiTest extends TestCase
{
    use MakeRoomCategoryFeatureTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRoomCategoryFeature()
    {
        $roomCategoryFeature = $this->fakeRoomCategoryFeatureData();
        $this->json('POST', '/api/v1/roomCategoryFeatures', $roomCategoryFeature);

        $this->assertApiResponse($roomCategoryFeature);
    }

    /**
     * @test
     */
    public function testReadRoomCategoryFeature()
    {
        $roomCategoryFeature = $this->makeRoomCategoryFeature();
        $this->json('GET', '/api/v1/roomCategoryFeatures/'.$roomCategoryFeature->id);

        $this->assertApiResponse($roomCategoryFeature->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRoomCategoryFeature()
    {
        $roomCategoryFeature = $this->makeRoomCategoryFeature();
        $editedRoomCategoryFeature = $this->fakeRoomCategoryFeatureData();

        $this->json('PUT', '/api/v1/roomCategoryFeatures/'.$roomCategoryFeature->id, $editedRoomCategoryFeature);

        $this->assertApiResponse($editedRoomCategoryFeature);
    }

    /**
     * @test
     */
    public function testDeleteRoomCategoryFeature()
    {
        $roomCategoryFeature = $this->makeRoomCategoryFeature();
        $this->json('DELETE', '/api/v1/roomCategoryFeatures/'.$roomCategoryFeature->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/roomCategoryFeatures/'.$roomCategoryFeature->id);

        $this->assertResponseStatus(404);
    }
}
