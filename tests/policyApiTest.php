<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class policyApiTest extends TestCase
{
    use MakepolicyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatepolicy()
    {
        $policy = $this->fakepolicyData();
        $this->json('POST', '/api/v1/policies', $policy);

        $this->assertApiResponse($policy);
    }

    /**
     * @test
     */
    public function testReadpolicy()
    {
        $policy = $this->makepolicy();
        $this->json('GET', '/api/v1/policies/'.$policy->id);

        $this->assertApiResponse($policy->toArray());
    }

    /**
     * @test
     */
    public function testUpdatepolicy()
    {
        $policy = $this->makepolicy();
        $editedpolicy = $this->fakepolicyData();

        $this->json('PUT', '/api/v1/policies/'.$policy->id, $editedpolicy);

        $this->assertApiResponse($editedpolicy);
    }

    /**
     * @test
     */
    public function testDeletepolicy()
    {
        $policy = $this->makepolicy();
        $this->json('DELETE', '/api/v1/policies/'.$policy->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/policies/'.$policy->id);

        $this->assertResponseStatus(404);
    }
}
