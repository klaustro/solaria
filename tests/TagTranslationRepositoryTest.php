<?php

use App\Models\Admin\TagTranslation;
use App\Repositories\Admin\TagTranslationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TagTranslationRepositoryTest extends TestCase
{
    use MakeTagTranslationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TagTranslationRepository
     */
    protected $tagTranslationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->tagTranslationRepo = App::make(TagTranslationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateTagTranslation()
    {
        $tagTranslation = $this->fakeTagTranslationData();
        $createdTagTranslation = $this->tagTranslationRepo->create($tagTranslation);
        $createdTagTranslation = $createdTagTranslation->toArray();
        $this->assertArrayHasKey('id', $createdTagTranslation);
        $this->assertNotNull($createdTagTranslation['id'], 'Created TagTranslation must have id specified');
        $this->assertNotNull(TagTranslation::find($createdTagTranslation['id']), 'TagTranslation with given id must be in DB');
        $this->assertModelData($tagTranslation, $createdTagTranslation);
    }

    /**
     * @test read
     */
    public function testReadTagTranslation()
    {
        $tagTranslation = $this->makeTagTranslation();
        $dbTagTranslation = $this->tagTranslationRepo->find($tagTranslation->id);
        $dbTagTranslation = $dbTagTranslation->toArray();
        $this->assertModelData($tagTranslation->toArray(), $dbTagTranslation);
    }

    /**
     * @test update
     */
    public function testUpdateTagTranslation()
    {
        $tagTranslation = $this->makeTagTranslation();
        $fakeTagTranslation = $this->fakeTagTranslationData();
        $updatedTagTranslation = $this->tagTranslationRepo->update($fakeTagTranslation, $tagTranslation->id);
        $this->assertModelData($fakeTagTranslation, $updatedTagTranslation->toArray());
        $dbTagTranslation = $this->tagTranslationRepo->find($tagTranslation->id);
        $this->assertModelData($fakeTagTranslation, $dbTagTranslation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteTagTranslation()
    {
        $tagTranslation = $this->makeTagTranslation();
        $resp = $this->tagTranslationRepo->delete($tagTranslation->id);
        $this->assertTrue($resp);
        $this->assertNull(TagTranslation::find($tagTranslation->id), 'TagTranslation should not exist in DB');
    }
}
