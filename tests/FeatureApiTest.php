<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FeatureApiTest extends TestCase
{
    use MakeFeatureTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateFeature()
    {
        $feature = $this->fakeFeatureData();
        $this->json('POST', '/api/v1/features', $feature);

        $this->assertApiResponse($feature);
    }

    /**
     * @test
     */
    public function testReadFeature()
    {
        $feature = $this->makeFeature();
        $this->json('GET', '/api/v1/features/'.$feature->id);

        $this->assertApiResponse($feature->toArray());
    }

    /**
     * @test
     */
    public function testUpdateFeature()
    {
        $feature = $this->makeFeature();
        $editedFeature = $this->fakeFeatureData();

        $this->json('PUT', '/api/v1/features/'.$feature->id, $editedFeature);

        $this->assertApiResponse($editedFeature);
    }

    /**
     * @test
     */
    public function testDeleteFeature()
    {
        $feature = $this->makeFeature();
        $this->json('DELETE', '/api/v1/features/'.$feature->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/features/'.$feature->id);

        $this->assertResponseStatus(404);
    }
}
