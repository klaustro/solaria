<?php

use App\Models\Admin\BlogCategory;
use App\Repositories\Admin\BlogCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogCategoryRepositoryTest extends TestCase
{
    use MakeBlogCategoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BlogCategoryRepository
     */
    protected $blogCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->blogCategoryRepo = App::make(BlogCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBlogCategory()
    {
        $blogCategory = $this->fakeBlogCategoryData();
        $createdBlogCategory = $this->blogCategoryRepo->create($blogCategory);
        $createdBlogCategory = $createdBlogCategory->toArray();
        $this->assertArrayHasKey('id', $createdBlogCategory);
        $this->assertNotNull($createdBlogCategory['id'], 'Created BlogCategory must have id specified');
        $this->assertNotNull(BlogCategory::find($createdBlogCategory['id']), 'BlogCategory with given id must be in DB');
        $this->assertModelData($blogCategory, $createdBlogCategory);
    }

    /**
     * @test read
     */
    public function testReadBlogCategory()
    {
        $blogCategory = $this->makeBlogCategory();
        $dbBlogCategory = $this->blogCategoryRepo->find($blogCategory->id);
        $dbBlogCategory = $dbBlogCategory->toArray();
        $this->assertModelData($blogCategory->toArray(), $dbBlogCategory);
    }

    /**
     * @test update
     */
    public function testUpdateBlogCategory()
    {
        $blogCategory = $this->makeBlogCategory();
        $fakeBlogCategory = $this->fakeBlogCategoryData();
        $updatedBlogCategory = $this->blogCategoryRepo->update($fakeBlogCategory, $blogCategory->id);
        $this->assertModelData($fakeBlogCategory, $updatedBlogCategory->toArray());
        $dbBlogCategory = $this->blogCategoryRepo->find($blogCategory->id);
        $this->assertModelData($fakeBlogCategory, $dbBlogCategory->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBlogCategory()
    {
        $blogCategory = $this->makeBlogCategory();
        $resp = $this->blogCategoryRepo->delete($blogCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(BlogCategory::find($blogCategory->id), 'BlogCategory should not exist in DB');
    }
}
