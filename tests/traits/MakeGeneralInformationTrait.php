<?php

use Faker\Factory as Faker;
use App\Models\Admin\GeneralInformation;
use App\Repositories\Admin\GeneralInformationRepository;

trait MakeGeneralInformationTrait
{
    /**
     * Create fake instance of GeneralInformation and save it in database
     *
     * @param array $generalInformationFields
     * @return GeneralInformation
     */
    public function makeGeneralInformation($generalInformationFields = [])
    {
        /** @var GeneralInformationRepository $generalInformationRepo */
        $generalInformationRepo = App::make(GeneralInformationRepository::class);
        $theme = $this->fakeGeneralInformationData($generalInformationFields);
        return $generalInformationRepo->create($theme);
    }

    /**
     * Get fake instance of GeneralInformation
     *
     * @param array $generalInformationFields
     * @return GeneralInformation
     */
    public function fakeGeneralInformation($generalInformationFields = [])
    {
        return new GeneralInformation($this->fakeGeneralInformationData($generalInformationFields));
    }

    /**
     * Get fake data of GeneralInformation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeGeneralInformationData($generalInformationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'slug' => $fake->word,
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $generalInformationFields);
    }
}
