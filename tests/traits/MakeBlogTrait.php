<?php

use Faker\Factory as Faker;
use App\Models\Admin\Blog;
use App\Repositories\Admin\BlogRepository;

trait MakeBlogTrait
{
    /**
     * Create fake instance of Blog and save it in database
     *
     * @param array $blogFields
     * @return Blog
     */
    public function makeBlog($blogFields = [])
    {
        /** @var BlogRepository $blogRepo */
        $blogRepo = App::make(BlogRepository::class);
        $theme = $this->fakeBlogData($blogFields);
        return $blogRepo->create($theme);
    }

    /**
     * Get fake instance of Blog
     *
     * @param array $blogFields
     * @return Blog
     */
    public function fakeBlog($blogFields = [])
    {
        return new Blog($this->fakeBlogData($blogFields));
    }

    /**
     * Get fake data of Blog
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBlogData($blogFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'slug' => $fake->word,
            'blog_category_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'status_id' => $fake->randomDigitNotNull,
            'image' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $blogFields);
    }
}
