<?php

use Faker\Factory as Faker;
use App\Models\Admin\Ally;
use App\Repositories\Admin\AllyRepository;

trait MakeAllyTrait
{
    /**
     * Create fake instance of Ally and save it in database
     *
     * @param array $allyFields
     * @return Ally
     */
    public function makeAlly($allyFields = [])
    {
        /** @var AllyRepository $allyRepo */
        $allyRepo = App::make(AllyRepository::class);
        $theme = $this->fakeAllyData($allyFields);
        return $allyRepo->create($theme);
    }

    /**
     * Get fake instance of Ally
     *
     * @param array $allyFields
     * @return Ally
     */
    public function fakeAlly($allyFields = [])
    {
        return new Ally($this->fakeAllyData($allyFields));
    }

    /**
     * Get fake data of Ally
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAllyData($allyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'address' => $fake->word,
            'phone' => $fake->word,
            'email' => $fake->word,
            'website' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $allyFields);
    }
}
