<?php

use Faker\Factory as Faker;
use App\Models\Admin\Rates;
use App\Repositories\Admin\RatesRepository;

trait MakeRatesTrait
{
    /**
     * Create fake instance of Rates and save it in database
     *
     * @param array $ratesFields
     * @return Rates
     */
    public function makeRates($ratesFields = [])
    {
        /** @var RatesRepository $ratesRepo */
        $ratesRepo = App::make(RatesRepository::class);
        $theme = $this->fakeRatesData($ratesFields);
        return $ratesRepo->create($theme);
    }

    /**
     * Get fake instance of Rates
     *
     * @param array $ratesFields
     * @return Rates
     */
    public function fakeRates($ratesFields = [])
    {
        return new Rates($this->fakeRatesData($ratesFields));
    }

    /**
     * Get fake data of Rates
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRatesData($ratesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'rooms_id' => $fake->randomDigitNotNull,
            'start_date' => $fake->word,
            'end_date' => $fake->word,
            'packages_id' => $fake->randomDigitNotNull,
            'price' => $fake->randomDigitNotNull,
            'num_people' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $ratesFields);
    }
}
