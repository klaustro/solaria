<?php

use Faker\Factory as Faker;
use App\Models\Admin\RoomCategoryFeature;
use App\Repositories\Admin\RoomCategoryFeatureRepository;

trait MakeRoomCategoryFeatureTrait
{
    /**
     * Create fake instance of RoomCategoryFeature and save it in database
     *
     * @param array $roomCategoryFeatureFields
     * @return RoomCategoryFeature
     */
    public function makeRoomCategoryFeature($roomCategoryFeatureFields = [])
    {
        /** @var RoomCategoryFeatureRepository $roomCategoryFeatureRepo */
        $roomCategoryFeatureRepo = App::make(RoomCategoryFeatureRepository::class);
        $theme = $this->fakeRoomCategoryFeatureData($roomCategoryFeatureFields);
        return $roomCategoryFeatureRepo->create($theme);
    }

    /**
     * Get fake instance of RoomCategoryFeature
     *
     * @param array $roomCategoryFeatureFields
     * @return RoomCategoryFeature
     */
    public function fakeRoomCategoryFeature($roomCategoryFeatureFields = [])
    {
        return new RoomCategoryFeature($this->fakeRoomCategoryFeatureData($roomCategoryFeatureFields));
    }

    /**
     * Get fake data of RoomCategoryFeature
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRoomCategoryFeatureData($roomCategoryFeatureFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'room_category_id' => $fake->randomDigitNotNull,
            'feature_id' => $fake->randomDigitNotNull,
            'value' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $roomCategoryFeatureFields);
    }
}
