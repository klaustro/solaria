<?php

use Faker\Factory as Faker;
use App\Models\Admin\BlogComment;
use App\Repositories\Admin\BlogCommentRepository;

trait MakeBlogCommentTrait
{
    /**
     * Create fake instance of BlogComment and save it in database
     *
     * @param array $blogCommentFields
     * @return BlogComment
     */
    public function makeBlogComment($blogCommentFields = [])
    {
        /** @var BlogCommentRepository $blogCommentRepo */
        $blogCommentRepo = App::make(BlogCommentRepository::class);
        $theme = $this->fakeBlogCommentData($blogCommentFields);
        return $blogCommentRepo->create($theme);
    }

    /**
     * Get fake instance of BlogComment
     *
     * @param array $blogCommentFields
     * @return BlogComment
     */
    public function fakeBlogComment($blogCommentFields = [])
    {
        return new BlogComment($this->fakeBlogCommentData($blogCommentFields));
    }

    /**
     * Get fake data of BlogComment
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBlogCommentData($blogCommentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'comment' => $fake->word,
            'blog_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $blogCommentFields);
    }
}
