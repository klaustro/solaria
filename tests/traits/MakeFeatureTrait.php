<?php

use Faker\Factory as Faker;
use App\Models\Admin\Feature;
use App\Repositories\Admin\FeatureRepository;

trait MakeFeatureTrait
{
    /**
     * Create fake instance of Feature and save it in database
     *
     * @param array $featureFields
     * @return Feature
     */
    public function makeFeature($featureFields = [])
    {
        /** @var FeatureRepository $featureRepo */
        $featureRepo = App::make(FeatureRepository::class);
        $theme = $this->fakeFeatureData($featureFields);
        return $featureRepo->create($theme);
    }

    /**
     * Get fake instance of Feature
     *
     * @param array $featureFields
     * @return Feature
     */
    public function fakeFeature($featureFields = [])
    {
        return new Feature($this->fakeFeatureData($featureFields));
    }

    /**
     * Get fake data of Feature
     *
     * @param array $postFields
     * @return array
     */
    public function fakeFeatureData($featureFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'icon' => $fake->text,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $featureFields);
    }
}
