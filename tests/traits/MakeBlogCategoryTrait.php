<?php

use Faker\Factory as Faker;
use App\Models\Admin\BlogCategory;
use App\Repositories\Admin\BlogCategoryRepository;

trait MakeBlogCategoryTrait
{
    /**
     * Create fake instance of BlogCategory and save it in database
     *
     * @param array $blogCategoryFields
     * @return BlogCategory
     */
    public function makeBlogCategory($blogCategoryFields = [])
    {
        /** @var BlogCategoryRepository $blogCategoryRepo */
        $blogCategoryRepo = App::make(BlogCategoryRepository::class);
        $theme = $this->fakeBlogCategoryData($blogCategoryFields);
        return $blogCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of BlogCategory
     *
     * @param array $blogCategoryFields
     * @return BlogCategory
     */
    public function fakeBlogCategory($blogCategoryFields = [])
    {
        return new BlogCategory($this->fakeBlogCategoryData($blogCategoryFields));
    }

    /**
     * Get fake data of BlogCategory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBlogCategoryData($blogCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'code' => $fake->word,
            'status_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $blogCategoryFields);
    }
}
