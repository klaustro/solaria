<?php

use Faker\Factory as Faker;
use App\Models\Admin\TagTranslation;
use App\Repositories\Admin\TagTranslationRepository;

trait MakeTagTranslationTrait
{
    /**
     * Create fake instance of TagTranslation and save it in database
     *
     * @param array $tagTranslationFields
     * @return TagTranslation
     */
    public function makeTagTranslation($tagTranslationFields = [])
    {
        /** @var TagTranslationRepository $tagTranslationRepo */
        $tagTranslationRepo = App::make(TagTranslationRepository::class);
        $theme = $this->fakeTagTranslationData($tagTranslationFields);
        return $tagTranslationRepo->create($theme);
    }

    /**
     * Get fake instance of TagTranslation
     *
     * @param array $tagTranslationFields
     * @return TagTranslation
     */
    public function fakeTagTranslation($tagTranslationFields = [])
    {
        return new TagTranslation($this->fakeTagTranslationData($tagTranslationFields));
    }

    /**
     * Get fake data of TagTranslation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeTagTranslationData($tagTranslationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'tag' => $fake->word,
            'value' => $fake->text,
            'front_section_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $tagTranslationFields);
    }
}
