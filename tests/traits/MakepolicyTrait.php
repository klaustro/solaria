<?php

use Faker\Factory as Faker;
use App\Models\Admin\policy;
use App\Repositories\Admin\policyRepository;

trait MakepolicyTrait
{
    /**
     * Create fake instance of policy and save it in database
     *
     * @param array $policyFields
     * @return policy
     */
    public function makepolicy($policyFields = [])
    {
        /** @var policyRepository $policyRepo */
        $policyRepo = App::make(policyRepository::class);
        $theme = $this->fakepolicyData($policyFields);
        return $policyRepo->create($theme);
    }

    /**
     * Get fake instance of policy
     *
     * @param array $policyFields
     * @return policy
     */
    public function fakepolicy($policyFields = [])
    {
        return new policy($this->fakepolicyData($policyFields));
    }

    /**
     * Get fake data of policy
     *
     * @param array $postFields
     * @return array
     */
    public function fakepolicyData($policyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $policyFields);
    }
}
