<?php

use App\Models\Admin\BlogComment;
use App\Repositories\Admin\BlogCommentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogCommentRepositoryTest extends TestCase
{
    use MakeBlogCommentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BlogCommentRepository
     */
    protected $blogCommentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->blogCommentRepo = App::make(BlogCommentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBlogComment()
    {
        $blogComment = $this->fakeBlogCommentData();
        $createdBlogComment = $this->blogCommentRepo->create($blogComment);
        $createdBlogComment = $createdBlogComment->toArray();
        $this->assertArrayHasKey('id', $createdBlogComment);
        $this->assertNotNull($createdBlogComment['id'], 'Created BlogComment must have id specified');
        $this->assertNotNull(BlogComment::find($createdBlogComment['id']), 'BlogComment with given id must be in DB');
        $this->assertModelData($blogComment, $createdBlogComment);
    }

    /**
     * @test read
     */
    public function testReadBlogComment()
    {
        $blogComment = $this->makeBlogComment();
        $dbBlogComment = $this->blogCommentRepo->find($blogComment->id);
        $dbBlogComment = $dbBlogComment->toArray();
        $this->assertModelData($blogComment->toArray(), $dbBlogComment);
    }

    /**
     * @test update
     */
    public function testUpdateBlogComment()
    {
        $blogComment = $this->makeBlogComment();
        $fakeBlogComment = $this->fakeBlogCommentData();
        $updatedBlogComment = $this->blogCommentRepo->update($fakeBlogComment, $blogComment->id);
        $this->assertModelData($fakeBlogComment, $updatedBlogComment->toArray());
        $dbBlogComment = $this->blogCommentRepo->find($blogComment->id);
        $this->assertModelData($fakeBlogComment, $dbBlogComment->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBlogComment()
    {
        $blogComment = $this->makeBlogComment();
        $resp = $this->blogCommentRepo->delete($blogComment->id);
        $this->assertTrue($resp);
        $this->assertNull(BlogComment::find($blogComment->id), 'BlogComment should not exist in DB');
    }
}
