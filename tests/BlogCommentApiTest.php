<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BlogCommentApiTest extends TestCase
{
    use MakeBlogCommentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBlogComment()
    {
        $blogComment = $this->fakeBlogCommentData();
        $this->json('POST', '/api/v1/blogComments', $blogComment);

        $this->assertApiResponse($blogComment);
    }

    /**
     * @test
     */
    public function testReadBlogComment()
    {
        $blogComment = $this->makeBlogComment();
        $this->json('GET', '/api/v1/blogComments/'.$blogComment->id);

        $this->assertApiResponse($blogComment->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBlogComment()
    {
        $blogComment = $this->makeBlogComment();
        $editedBlogComment = $this->fakeBlogCommentData();

        $this->json('PUT', '/api/v1/blogComments/'.$blogComment->id, $editedBlogComment);

        $this->assertApiResponse($editedBlogComment);
    }

    /**
     * @test
     */
    public function testDeleteBlogComment()
    {
        $blogComment = $this->makeBlogComment();
        $this->json('DELETE', '/api/v1/blogComments/'.$blogComment->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/blogComments/'.$blogComment->id);

        $this->assertResponseStatus(404);
    }
}
