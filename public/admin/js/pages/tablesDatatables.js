/*
 *  Document   : tablesDatatables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables Datatables page
 */

var TablesDatatables = function() {

    return {
        init: function() {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.tableGeneral').dataTable({
                columnDefs: [ { orderable: false, targets: [ -1 ] } ],
                pageLength: 10,
                order: [ [ 0, "desc" ] ],
                stateSave: true,
                lengthMenu: [[10, 20, 30, -1], [10, 20, 30, 'All']],
                language: {
                    processing:     "Elaborazione in corso...",
                    // search:         "Ricerca: ",
                    lengthMenu:     "Mostra _MENU_ elementi",
                    info:           "Mostra da _START_ a _END_ di _TOTAL_ elementi",
                    infoEmpty:      "Mostra da 0 a 0 di 0 elementi",
                    infoFiltered:   "(filtrato da _MAX_ voci totali)",
                    infoPostFix:    "",
                    loadingRecords: "Caricamento...",
                    zeroRecords:    "Nessun elemento da visualizzare",
                    emptyTable:     "Nessun dato disponibile nella tabella",
                    paginate: {
                    first:      "Prima",
                    previous:   "Precedente",
                    next:       "Seguente",
                    last:       "Ultimo"
                    },
                    aria: {
                    sortAscending:  ": attivare per ordinare la colonna in ordine crescente",
                    sortDescending: ": attivare per ordinare la colonna in ordine decrescente"
                    }
                },
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Ricerca');
        }
    };
}();