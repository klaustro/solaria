<?php

namespace App\Listeners;

use App\Events\EventPackage;
use App\Models\Admin\NewsletterUser;
use App\Notifications\PackageNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class PackageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventPackage  $event
     * @return void
     */
    public function handle(EventPackage $event)
    {
        $newsletterUsers = NewsletterUser::all();
        Notification::send($newsletterUsers, new PackageNotification($event->package));
    }
}
