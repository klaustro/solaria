<?php

namespace App\Http\Controllers;

use App\Models\Admin\Blog;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use SEO;

class FrontController extends Controller
{
    public function index()
    {
        $title = env('APP_NAME');

        if(request()->is('*promozioni/*')){
            $slug = str_replace(env('APP_URL') . '/promozioni/', '', request()->url());
            $blog = Blog::where('slug', $slug)->first();
            if ($blog) {
                $title = env('APP_NAME') . ' - ' . $blog->title;
                SEOMeta::setTitle($title);
                SEOMeta::setCanonical(request()->url());

                SEOMeta::setDescription($blog->title);
                SEOMeta::addKeyword($blog->title);
                SEO::opengraph()->addProperty('type', 'article');
                SEO::opengraph()->addImage(url('/') . $blog->image);
            }
        }

        SEOMeta::addMeta('author', env('APP_NAME'), 'name');

        return view('home.index');
    }
}
