<?php

namespace App\Http\Controllers;

use App\Models\Admin\Setting;
use Mail;

class MailController extends Controller
{

	public static function sendMail($request,$plantilla, $plantillaForAdmin = '')
	{
		// dd($setting->value);

		$request['msg']				= isset($request['msg'])?$request['msg']:'';
		$request['subject']			= isset($request['subject'])?$request['subject']:'Informazioni';
		$request['plantilla'] 		= 'emails.'.$plantilla;
		$request['plantillaAdmin']  = 'emails.admin.'.$plantillaForAdmin;


        $subject 					= $request['subject'];
		$destinatario 				= $request['email'];

        try{
			//envio de email al usuario
			$response = Mail::send($request['plantilla'], $request, function ($message) use ($destinatario, $subject) {
				$message->to($destinatario)->subject($subject);});
				//envio de email al administrador
			if(explode('.',$request['plantilla'])[1] == 'order'){
				Mail::send($request['plantilla'], $request, function ($message) use ($subject) {
				$booking = Setting::where('key' ,'=', 'booking_email')->first();
				$message->to($booking->value)->subject($subject);});
			}else{
				Mail::send($request['plantilla'], $request, function ($message) use ($subject) {
				$contact = Setting::where('key' ,'=', 'contact_email')->first();
				$message->to($contact->value)->subject($subject);});
			}

		}catch(\Swift_TransportException $e){
			return $e->getMessage();
		}

		if ($plantillaForAdmin != '') {
			try{
                if(explode('.',$request['plantilla'])[1] == 'order'){
                    Mail::send($request['plantillaAdmin'], $request, function ($message) use ($subject) {
                    $message->to("booking@solariahomeholiday.com")->subject($subject);});
                }else{
				    Mail::send($request['plantillaAdmin'], $request, function ($message) use ($subject) {
					$message->to("info@solariahomeholiday.com")->subject($subject);});
                }

			}catch(\Swift_TransportException $e){
				return $e->getMessage();
			}
		}
		return "OK";
	}
}
