<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\Admin\RoomAPIController;
use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\MailController;
use App\Http\Requests\API\Admin\CreateBookingAPIRequest;
use App\Http\Requests\Admin\BookingRequest;
use App\Http\Requests\Admin\CreateBookingRequest;
use App\Http\Requests\Admin\SearchBookingRequest;
use App\Http\Requests\Admin\UpdateBookingRequest;
use App\Repositories\Admin\BookingDetailRepository;
use App\Repositories\Admin\BookingRepository;
use App\Repositories\Admin\LockedRoomRepository;
use App\Repositories\Admin\PaymentMethodRepository;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\Admin\StatusRepository;
use App\Repositories\Admin\SettingRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Database\Eloquent\only;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BookingController extends AppBaseController
{
    /** @var  BookingRepository */
    private $bookingRepository;

    /** @var  BookingDetailRepository */
    private $bookingDetailRepository;

    /** @var  StatusRepository */
    private $statusRepository;

    /** @var  RoomRepository */
    private $roomRepository;

    /** @var  LockedRoomRepository */
    private $lockedRoomRepository;

    /** @var  LockedRoomRepository */
    private $paymentMethodRepository;

    /** @var  SettingRepository */
    private $settingRepository;

    public function __construct( RoomRepository $roomRepo,
        BookingRepository $bookingRepo,
        LockedRoomRepository $lockedRoomRepo,
        BookingDetailRepository $bookingDetailRepo,
        StatusRepository $statusRepo,
        PaymentMethodRepository $paymentMethodRepo,
        SettingRepository $settingRepo )
    {
        $this->roomRepository          = $roomRepo;
        $this->bookingRepository       = $bookingRepo;
        $this->bookingDetailRepository = $bookingDetailRepo;
        $this->statusRepository        = $statusRepo;
        $this->lockedRoomRepository    = $lockedRoomRepo;
        $this->paymentMethodRepository = $paymentMethodRepo;
        $this->settingRepository       = $settingRepo;
    }

    /**
     * Display a listing of the Booking.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->bookingRepository->pushCriteria( new RequestCriteria( $request ) );
        $bookings = $this->bookingRepository->orderBy( 'id', 'desc' )->all();

        $statuses = $this->statusRepository->findWhere( [
            [ 'id', '>=', 3 ],
            [ 'id', '<=', 5 ]
        ] )->pluck( 'name', 'id' );

        return view( 'admin.bookings.bookings.index' )
            ->with( 'bookings', $bookings )
            ->with( 'statuses', $statuses );
    }

    /**
     * Show the form for creating a new Booking.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'admin.bookings.bookings.create' );
    }

    /**
     * Display the booking form.
     * GET /admin/searchRoom
     *
     * @param Request $request
     * @return Response
     */
    public function searchRoom( SearchBookingRequest $request )
    {
        $this->roomRepository->pushCriteria( new RequestCriteria( $request ) );
        $this->roomRepository->pushCriteria( new LimitOffsetCriteria( $request ) );

        // inputs
        $start_date = $request->get( 'checkin' );
        $end_date   = $request->get( 'checkout' );
        $checkin        = Carbon::createFromFormat( 'd/m/Y H', $start_date . ' 0' )->format( 'Y-m-d' );
        $checkout       = Carbon::createFromFormat( 'd/m/Y H', $end_date . ' 0' )->format( 'Y-m-d' );
        $roomLocationId = $request->has( 'roomLocationId' ) ? (int)$request->get( 'roomLocationId' ) : null;
        $email          = $request->get( 'email' );
        $input          = compact( 'checkin', 'checkout', 'roomLocationId', 'email' );

        // obtener las rooms no disponibles por BookingDetails
        $unavailableBookingDetailRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {

            $bookingDetails = $this->bookingDetailRepository->findWhere( [
                [ 'checkin_date',    '>', $start_range_ ],
                [ 'checkout_date',   '<', $end_range_ ]
            ] );

            // validacion de status_id = 5 ( reservas anuladas )
            $bookingDetails = $bookingDetails->filter( function ( $item, $key ) {
                return $item->booking->status_id !== 5;
            } );

            return $bookingDetails;
        } );

        // obtener las rooms no disponibles por LockedRooms
        $unavailableLockedRoomRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {
            return $this->lockedRoomRepository->findWhere( [
                [ 'checkin_date',    '>', $start_range_ ],
                [ 'checkout_date',   '<', $end_range_ ],
                [ 'locked_at',       '>', Carbon::now()->subMinutes( env( 'LOCKED_ROOM_TIME', 5 ) ) ] // se buscan solo las rooms cuyo locked_at sea mayor a hace cinco minutos
            ] );
        } );

        // union de las rooms no disponibles (BookingDetails y LockedRooms)
        $unavailableRooms = array_unique( array_merge( $unavailableBookingDetailRooms, $unavailableLockedRoomRooms ) );

        // obtener tanto disponibles como no disponibles
        $rooms = $this->roomRepository->getCustomized( $input, $unavailableRooms );

        // Select de status
        $statuses = $this->statusRepository->findWhere( [
            [ 'id', '>=', 3 ],
            [ 'id', '<=', 4 ]
        ] )->pluck( 'name', 'id' );

        // select de metodo de pago
        $method_paymenties = $this->paymentMethodRepository->all();

        // dd( $rooms );

        return view( 'admin.bookings.bookings.search' )
            ->with( 'checkin', $start_date )
            ->with( 'checkout', $end_date )
            ->with( 'checkin_ymd', $checkin )
            ->with( 'checkout_ymd', $checkout )
            ->with( 'statuses', $statuses )
            ->with( 'method_paymenties', $method_paymenties )
            ->with( 'rooms', $rooms )
            ->with( 'rate_room_id', $request->get( 'rate_room_id' ) )
            ->with( 'email', $email );
    }

    /**
     * Store a newly created Booking in storage.
     * POST /bookings
     *
     * @param CreateBookingRequest $request
     *
     * @return Response
     */
    public function store( BookingRequest $request )
    {
        // roomId
        $roomId = $request->get( 'room_id' );

        // Ubicamos el detalle de la habitación
        $room = $this->roomRepository->findWithoutFail( $roomId );

        if ( empty( $room ) ) {
            Flash::error( 'Appartamento non trovato' );

            return redirect( route( 'admin.bookings.index' ) );
        }

        $input = array (
            'code' => null,
            'personResponsible' => array (
                'name'              => $request->get( 'name' ) . ' ' . $request->get( 'lastname' ),
                'email'             => $request->get( 'email' ),
                'phone'             => $request->get( 'phone' ),
                'fiscalCode'        => $request->get( 'fiscalCode' ),
            ),
            'rooms' => array (
                0 => array (
                    'name'              => $room->translation()->name,
                    'description'       => $room->translation()->description,
                    'roomId'            => $room->id,
                    'room_category_id'  => $room->room_category_id,
                    'personResponsible' => array (
                        'name'              => $request->get( 'name' ) . ' ' . $request->get( 'lastname' ),
                        'email'             => $request->get( 'email' ),
                        'phone'             => $request->get( 'phone' ),
                        'fiscalCode'        => $request->get( 'fiscalCode' ),
                    ),
                    'adults_quantity' => $request->adults_quantity,
                    'children_quantity' => $request->children_quantity,
                    'totalItem'         => null,
                    'bookingDate'       => array (
                        'checkin'           => $request->get( 'checkin_ymd' ),
                        'checkout'          => $request->get( 'checkout_ymd' ),
                        'roomLocationId'    => 0,
                    ),
                ),
            ),
            'totalAmount'       => null,
            'comment'           => null,
            'method_payment'    => $request->get( 'method_payment' ),
            'status'            => $request->get( 'status_id' ),
            'code_payment'      => null,
            'img_payment'       => null,
            'created_at'        => null,
            'price_backoffice'  => $request->has('price_backoffice')? $request->price_backoffice : null,

            // para las ofertas
            'email' => $request->get( 'email' ),
        );

        $data = $input;

        // Save Booking
        $booking = $this->bookingRepository->createCustomized( $data );

        // Get Booking with BookingDetails
        $bookingWithRelations = $this->bookingRepository->findCustomized( $booking->id );

        // Enviar Correo
        if ($request->get( 'status_id' ) == 3) {
            $sended = $this->sendMail( $bookingWithRelations, 'preorder' );
        }else{
            $sended = $this->sendMail( $bookingWithRelations, 'order' );
        }

        if ( @$sended == 'OK' ) {
            $message = 'Prenotazione salvato correttamente, email inviata';
        }
        else {
            $message = 'Prenotazione salvato correttamente, email non inviata';
        }

        Flash::success( $message );

        return redirect( route( 'admin.bookings.index' ) );
    }

    /**
     * Display the specified Booking.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show( $id )
    {
        $booking = $this->bookingRepository->findWithoutFail( $id );
        $setting = $this->settingRepository->all();

        if ( empty( $booking ) ) {
            Flash::error( 'Prenotazione non trovata' );

            return redirect( route( 'admin.bookings.index' ) );
        }

        $bookingDetails = $this->bookingDetailRepository->findByField( 'booking_id', $booking->id );

        return view( 'admin.bookings.bookings.show' )
            ->with( 'setting', $setting )
            ->with( 'booking', $booking )
            ->with( 'bookingDetails', $bookingDetails );
    }

    /**
     * Update the specified Booking in storage.
     *
     * @param  int              $id
     * @param UpdateBookingRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateBookingRequest $request )
    {
        $booking = $this->bookingRepository->findWithoutFail( $id );

        if ( empty( $booking ) ) {
            Flash::error( 'Prenotazione non trovata' );

            return redirect( route( 'admin.bookings.index' ) );
        }

        // valido el estatus actual del booking
        if ( $booking->status_id === 5 ) {

            // devuelvo error porque no se puede cambiar del estatus 5
            Flash::error( 'Non è possibile modificare quando la prenotazione è annullato.' );
            return redirect( route( 'admin.bookings.index' ) )
                ->withInput();
        }

        // valido el estatus actual del booking
        if ( $request->status_id == 3 ) {

            // devuelvo error porque no se puede colocar en pre reserva
            Flash::error( 'Non è possibile modificare lo stato quando l\'appartamento è cancellato o prenotato. Questo stato è inserito solo creando una prenotazione.' );
            return redirect( route( 'admin.bookings.index' ) )
                ->withInput();
        }

        // Update
        $booking = $this->bookingRepository->update( $request->only( 'status_id' ), $id );

        // Get Booking with BookingDetails
        $bookingWithRelations = $this->bookingRepository->findCustomized( $booking->id );

        // Enviar Correo
        if ($request->status_id == 4) {

            $sended = $this->sendMail( $bookingWithRelations, 'order' );

            if ( @$sended == 'OK' ) {
                $message = ', email inviata.';
            }else {
                $message = ', email non inviata.';
            }

        }elseif ($request->status_id == 5) {
            $sended = $this->sendMail( $bookingWithRelations, 'anulada' );

            if ( @$sended == 'OK' ) {
                $message = ', email inviata.';
            }else {
                $message = ', email non inviata.';
            }

            Flash::success( 'Si ha cancellato prenotazione correttamente.'.$message );
            return redirect( route( 'admin.bookings.index' ) );
        }else{
            $message = '.';
        }

        Flash::success( 'Si ha modificato prenotazione correttamente'.$message );

        return redirect( route( 'admin.bookings.index' ) );
    }

    /**
     * Send the mail of the invoice and the purchase BOOKING
     * @param array $bookingWithRelations
     * @param string $template
     * @return void
     */
    public function sendMail( $bookingWithRelations, $template )
    {

        if ( $bookingWithRelations[ 'status_id' ] == 3 ) {

            $subject = tags( 'order_email_subject_pre' );

        }
        elseif ( $bookingWithRelations[ 'status_id' ] == 5 ) {

            $subject = tags( 'order_email_subject_anulado' );

        }
        else {

            $subject = tags( 'order_email_subject' );

        }

        $request = [
            'subject'   => $subject,
            'msg'       => json_encode( $bookingWithRelations ),
            'email'     => $bookingWithRelations[ 'personResponsible' ][ 'email' ],
            'data'      => (array)$bookingWithRelations
        ];

        return MailController::sendMail( $request, $template );
    }
}
