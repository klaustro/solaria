<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateServiceCategoryRequest;
use App\Http\Requests\Admin\UpdateServiceCategoryRequest;
use App\Repositories\Admin\ServiceCategoryRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ServiceCategoryController extends AppBaseController
{
    /** @var  ServiceCategoryRepository */
    private $serviceCategoryRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct(ServiceCategoryRepository $serviceCategoryRepo)
    {
        $this->serviceCategoryRepository = $serviceCategoryRepo;
    }

    /**
     * Display a listing of the ServiceCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->serviceCategoryRepository->pushCriteria(new RequestCriteria($request));
        $serviceCategories = $this->serviceCategoryRepository->all();

        return view('admin.services.service_categories.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('serviceCategories', $serviceCategories);
    }

    /**
     * Show the form for creating a new ServiceCategory.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );

        return view('admin.services.service_categories.create')
            ->with( 'translation', $translation );
    }

    /**
     * Store a newly created ServiceCategory in storage.
     *
     * @param CreateServiceCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceCategoryRequest $request)
    {

        // inputs
        $input               = $this->input( $request );
        $inputTranslation    = $this->inputTranslation( $request );

        // create model
        $model = $this->serviceCategoryRepository->create($input);

        // create translation
        $model->translations()->create( $inputTranslation );


        Flash::success('Categoria di servizio salvato correttamente.');

        return redirect(route('admin.serviceCategories.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    private function input( $request )
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $model =  $request->only([
            // vacio
        ]);

        $model['status_id'] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    private function inputTranslation( $request )
    {
        $translation = $request->only([
            'name',
            'description',
            'language_id',
        ]);

        return $translation;
    }

    /**
     * Show the form for editing the specified ServiceCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $lang, $id )
    {
        $serviceCategory = $this->serviceCategoryRepository->findWithoutFail($id);

        if ( empty( $serviceCategory ) ) {
            Flash::error( 'Categoria di servizio non trovata' );

            return redirect( route( 'admin.serviceCategoriess.index' ) );
        }

        // Logica translation
        $translation = $serviceCategory->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view( 'admin.services.service_categories.edit' )
            ->with( 'translation', $translation )
            ->with( 'serviceCategory', $serviceCategory );
    }

    /**
     * Update the specified ServiceCategory in storage.
     *
     * @param  int              $id
     * @param UpdateServiceCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceCategoryRequest $request)
    {
        $serviceCategory = $this->serviceCategoryRepository->findWithoutFail($id);

        if (empty($serviceCategory)) {
            Flash::error('Categoria di servizio non trovata');

            return redirect(route('admin.serviceCategories.index'));
        }
        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->serviceCategoryRepository->update($request->all(), $id);

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );


        Flash::success('Categoria di servizio modificato correttamente.');

        return redirect(route('admin.serviceCategories.index'));
    }

    /**
     * Remove the specified ServiceCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serviceCategory = $this->serviceCategoryRepository->findWithoutFail($id);

        if (empty($serviceCategory)) {
            Flash::error('Categoria di servizio non trovata');

            return redirect(route('admin.serviceCategories.index'));
        }

        // validation if there are child relations
        if( $serviceCategory->services->isEmpty() === false ) {
            Flash::error( 'la servizi categorie non può essere cancellato, ha servizies associati.' );

            return redirect( route( 'admin.serviceCategories.index' ) );
        }

        // delete translations
        $translations = $serviceCategory->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        $this->serviceCategoryRepository->delete($id);

        Flash::success('La categoria di servizio è stata cancellato correttamente..');

        return redirect(route('admin.serviceCategories.index'));
    }
}
