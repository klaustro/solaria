<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateBlogCommentRequest;
use App\Http\Requests\Admin\UpdateBlogCommentRequest;
use App\Repositories\Admin\BlogCommentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BlogCommentController extends AppBaseController
{
    /** @var  BlogCommentRepository */
    private $blogCommentRepository;

    public function __construct(BlogCommentRepository $blogCommentRepo)
    {
        $this->blogCommentRepository = $blogCommentRepo;
    }

    /**
     * Display a listing of the BlogComment.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->blogCommentRepository->pushCriteria(new RequestCriteria($request));
        $blogComments = $this->blogCommentRepository->all();

        return view('admin.blog_comments.index')
            ->with('blogComments', $blogComments);
    }

    /**
     * Show the form for creating a new BlogComment.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.blog_comments.create');
    }

    /**
     * Store a newly created BlogComment in storage.
     *
     * @param CreateBlogCommentRequest $request
     *
     * @return Response
     */
    public function store(CreateBlogCommentRequest $request)
    {
        $input = $request->all();

        $blogComment = $this->blogCommentRepository->create($input);

        Flash::success('Blog Comment salvato correttamente.');

        return redirect(route('admin.blogComments.index'));
    }

    /**
     * Display the specified BlogComment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $blogComment = $this->blogCommentRepository->findWithoutFail($id);

        if (empty($blogComment)) {
            Flash::error('Blog Comment non trovato');

            return redirect(route('admin.blogComments.index'));
        }

        return view('admin.blog_comments.show')->with('blogComment', $blogComment);
    }

    /**
     * Show the form for editing the specified BlogComment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $blogComment = $this->blogCommentRepository->findWithoutFail($id);

        if (empty($blogComment)) {
            Flash::error('Blog Comment non trovato');

            return redirect(route('admin.blogComments.index'));
        }

        return view('admin.blog_comments.edit')->with('blogComment', $blogComment);
    }

    /**
     * Update the specified BlogComment in storage.
     *
     * @param  int              $id
     * @param UpdateBlogCommentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlogCommentRequest $request)
    {
        $blogComment = $this->blogCommentRepository->findWithoutFail($id);

        if (empty($blogComment)) {
            Flash::error('Blog Comment non trovato');

            return redirect(route('admin.blogComments.index'));
        }

        $blogComment = $this->blogCommentRepository->update($request->all(), $id);

        Flash::success('Blog Comment modificato correttamente.');

        return redirect(route('admin.blogComments.index'));
    }

    /**
     * Remove the specified BlogComment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $blogComment = $this->blogCommentRepository->findWithoutFail($id);

        if (empty($blogComment)) {
            Flash::error('Blog Comment non trovato');

            return redirect(route('admin.blogComments.index'));
        }

        $this->blogCommentRepository->delete($id);

        Flash::success('Blog Comment cancellato correttamente.');

        return redirect(route('admin.blogComments.index'));
    }
}
