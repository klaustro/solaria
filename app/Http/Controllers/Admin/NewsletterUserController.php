<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateNewsletterUserRequest;
use App\Http\Requests\Admin\UpdateNewsletterUserRequest;
use App\Repositories\Admin\NewsletterUserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class NewsletterUserController extends AppBaseController
{
    /** @var  NewsletterUserRepository */
    private $newsletterUserRepository;

    public function __construct(NewsletterUserRepository $newsletterUserRepo)
    {
        $this->newsletterUserRepository = $newsletterUserRepo;
    }

    /**
     * Display a listing of the NewsletterUser.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->newsletterUserRepository->pushCriteria(new RequestCriteria($request));
        $newsletterUsers = $this->newsletterUserRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.newsletter_users.index')
            ->with('newsletterUsers', $newsletterUsers);
    }

    /**
     * Remove the specified NewsletterUser from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $newsletterUser = $this->newsletterUserRepository->findWithoutFail($id);

        if (empty($newsletterUser)) {
            Flash::error('Utente della Newsletter non trovato');

            return redirect(route('admin.newsletterUsers.index'));
        }

        $this->newsletterUserRepository->delete($id);

        Flash::success('Si ha cancellato Utente della Newsletter correttamente.');

        return redirect(route('admin.newsletterUsers.index'));
    }
}
