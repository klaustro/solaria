<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateRoomSeasonRequest;
use App\Http\Requests\Admin\UpdateRoomSeasonRequest;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\Admin\RoomSeasonRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomSeasonController extends AppBaseController
{
    /** @var  RoomSeasonRepository */
    private $roomSeasonRepository;
    private $roomRepository;

    public function __construct(RoomSeasonRepository $roomSeasonRepo, RoomRepository $roomRepository)
    {
        $this->roomSeasonRepository = $roomSeasonRepo;
        $this->roomRepository = $roomRepository;
    }

    /**
     * Display a listing of the RoomSeason.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomSeasonRepository->pushCriteria(new RequestCriteria($request));
        $roomSeasons = $this->roomSeasonRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.rooms.room_seasons.index')
            ->with('roomSeasons', $roomSeasons);
    }

    /**
     * Show the form for creating a new RoomSeason.
     *
     * @return Response
     */
    public function create()
    {
        $rooms = $this->roomRepository->all()->pluck('name', 'id');
        return view('admin.rooms.room_seasons.create')->with('rooms', $rooms);
    }

    /**
     * Store a newly created RoomSeason in storage.
     *
     * @param CreateRoomSeasonRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomSeasonRequest $request)
    {
        $input = $this->input($request);

        foreach ($request->get('room_id') as $key => $room) {
            $input['room_id'] = $room;
            $roomSeason = $this->roomSeasonRepository->create($input);
        }

        Flash::success('Prezzi per Stagione salvato correttamente.');

        return redirect(route('admin.roomSeasons.index'));
    }

    public function searchOferte($checkin, $checkout)
    {
        $roomSeason = $this->roomSeasonRepository->findWithoutFail($id);

        foreach ($roomSeason as $roomOferte) {

        }
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request )
    {
        $model =  $request->only([
            'start_date',
            'end_date',
            'price',
            'iva',
            'additional_price',
            'pet_price',
        ]);

        $start_date = date_create(str_replace('/', '-', $request->start_date));
        $end_date = date_create(str_replace('/', '-', $request->end_date));

        $model['start_date'] = date_format($start_date, 'Y-m-d');
        $model['end_date'] = date_format($end_date, 'Y-m-d');


        return $model;
    }

    /**
     * Display the specified RoomSeason.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roomSeason = $this->roomSeasonRepository->findWithoutFail($id);

        if (empty($roomSeason)) {
            Flash::error('Prezzi per Stagione non trovata');

            return redirect(route('admin.roomSeasons.index'));
        }

        return view('admin.rooms.room_seasons.show')->with('roomSeason', $roomSeason);
    }

    /**
     * Show the form for editing the specified RoomSeason.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roomSeason = $this->roomSeasonRepository->findWithoutFail($id);

        if (empty($roomSeason)) {
            Flash::error('Prezzi per Stagione non trovata');

            return redirect(route('admin.roomSeasons.index'));
        }
        $rooms = $this->roomRepository->all()->pluck('name', 'id');
        return view('admin.rooms.room_seasons.edit')->with('roomSeason', $roomSeason)->with('rooms', $rooms);
    }

    /**
     * Update the specified RoomSeason in storage.
     *
     * @param  int              $id
     * @param UpdateRoomSeasonRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomSeasonRequest $request)
    {
        $roomSeason = $this->roomSeasonRepository->findWithoutFail($id);

        if (empty($roomSeason)) {
            Flash::error('Prezzi per Stagione non trovata');

            return redirect(route('admin.roomSeasons.index'));
        }

        $input = $this->input($request);

        $roomSeason = $this->roomSeasonRepository->update($input, $id);

        Flash::success('Si ha modificato Prezzi per Stagione correttamente.');

        return redirect(route('admin.roomSeasons.index'));
    }

    /**
     * Remove the specified RoomSeason from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roomSeason = $this->roomSeasonRepository->findWithoutFail($id);

        if (empty($roomSeason)) {
            Flash::error('Prezzi per Stagione non trovata');

            return redirect(route('admin.roomSeasons.index'));
        }

        $this->roomSeasonRepository->delete($id);

        Flash::success('Si ha cancellato Prezzi per Stagione correttamente.');

        return redirect(route('admin.roomSeasons.index'));
    }
}
