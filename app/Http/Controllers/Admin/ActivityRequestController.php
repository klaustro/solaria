<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateActivityRequestRequest;
use App\Http\Requests\Admin\UpdateActivityRequestRequest;
use App\Repositories\Admin\ActivityRequestRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ActivityRequestController extends AppBaseController
{
    /** @var  ActivityRequestRepository */
    private $activityRequestRepository;

    public function __construct(ActivityRequestRepository $activityRequestRepo)
    {
        $this->activityRequestRepository = $activityRequestRepo;
    }

    /**
     * Display a listing of the ActivityRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->activityRequestRepository->pushCriteria(new RequestCriteria($request));
        $activityRequests = $this->activityRequestRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.activities.activity_requests.index')
            ->with('activityRequests', $activityRequests);
    }

    /**
     * Show the form for creating a new ActivityRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.activities.activity_requests.create');
    }

    /**
     * Store a newly created ActivityRequest in storage.
     *
     * @param CreateActivityRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateActivityRequestRequest $request)
    {
        $input = $request->all();

        $activityRequest = $this->activityRequestRepository->create($input);

        Flash::success('Richiesta esperienza salvato correttamente.');

        return redirect(route('admin.activityRequests.index'));
    }

    /**
     * Display the specified ActivityRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $activityRequest = $this->activityRequestRepository->findWithoutFail($id);

        if (empty($activityRequest)) {
            Flash::error('Richiesta esperienza non trovato');

            return redirect(route('admin.activityRequests.index'));
        }

        return view('admin.activities.activity_requests.show')->with('activityRequest', $activityRequest);
    }

    /**
     * Show the form for editing the specified ActivityRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $activityRequest = $this->activityRequestRepository->findWithoutFail($id);

        if (empty($activityRequest)) {
            Flash::error('Richiesta esperienza non trovato');

            return redirect(route('admin.activityRequests.index'));
        }

        $this->markAsRead($activityRequest);

        return view('admin.activities.activity_requests.edit')
            ->with('activityRequest', $activityRequest);
    }

    /**
     * Update the specified ActivityRequest in storage.
     *
     * @param  int              $id
     * @param UpdateActivityRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivityRequestRequest $request)
    {
        $activityRequest = $this->activityRequestRepository->findWithoutFail($id);

        if (empty($activityRequest)) {
            Flash::error('Richiesta esperienza non trovato');

            return redirect(route('admin.activityRequests.index'));
        }

        $activityRequest = $this->activityRequestRepository->update($request->all(), $id);

        Flash::success('Si ha modificato richiesta esperienza correttamente.');

        return redirect(route('admin.activityRequests.index'));
    }

    /**
     * Remove the specified ActivityRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $activityRequest = $this->activityRequestRepository->findWithoutFail($id);

        if (empty($activityRequest)) {
            Flash::error('Richiesta esperienza non trovato');

            return redirect(route('admin.activityRequests.index'));
        }

        $this->activityRequestRepository->delete($id);

        Flash::success('Si ha cancellato richiesta esperienza correttamente.');

        return redirect(route('admin.activityRequests.index'));
    }

    private function markAsRead($activityRequest)
    {
        $activityRequest->status_id = 7;
        $activityRequest->save();
    }
}
