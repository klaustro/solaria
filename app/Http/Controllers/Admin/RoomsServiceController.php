<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateRoomsServiceRequest;
use App\Http\Requests\Admin\UpdateRoomsServiceRequest;
use App\Models\Admin\Service;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\Admin\RoomsServiceRepository;
use App\Repositories\Admin\ServiceRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomsServiceController extends AppBaseController
{
    /** @var  RoomsServiceRepository */
    private $roomsServiceRepository;

    /** @var  RoomRepository */
    private $roomRepository;

    /** @var  ServiceRepository */
    private $serviceRepository;

    public function __construct( RoomsServiceRepository $roomsServiceRepo,
        RoomRepository $roomRepo,
        ServiceRepository $serviceRepo )
    {
        $this->roomsServiceRepository = $roomsServiceRepo;
        $this->roomRepository = $roomRepo;
        $this->serviceRepository = $serviceRepo;
    }

    /**
     * Display a listing of the RoomsService.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomsServiceRepository->pushCriteria(new RequestCriteria($request));
        $roomsServices = $this->roomsServiceRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.rooms.rooms_services.index')
            ->with('roomsServices', $roomsServices);
    }

    /**
     * Show the form for creating a new RoomsService.
     *
     * @return Response
     */
    public function create()
    {
        $rooms      = $this->roomRepository->all()->pluck( 'name', 'id' );
        // $services   = $this->serviceRepository->all()->pluck( 'name', 'id' );
        $services = Service::all()->sortBy('ico');

        return view('admin.rooms.rooms_services.create')
            ->with('rooms', $rooms)
            ->with('services', $services);
    }

    /**
     * Store a newly created RoomsService in storage.
     *
     * @param CreateRoomsServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomsServiceRequest $request)
    {
        $input = $request->all();

        $roomsService = $this->roomsServiceRepository->create($input);

        Flash::success('servizio associato all\'appartamento correttamente.');

        return redirect(route('admin.roomsServices.index'));
    }

    /**
     * Display the specified RoomsService.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roomsService = $this->roomsServiceRepository->findWithoutFail($id);

        if (empty($roomsService)) {
            Flash::error('Servizi Appartamenti non trovato');

            return redirect(route('admin.roomsServices.index'));
        }

        return view('admin.rooms.rooms_services.show')->with('roomsService', $roomsService);
    }

    /**
     * Show the form for editing the specified RoomsService.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roomsService = $this->roomsServiceRepository->findWithoutFail($id);

        if (empty($roomsService)) {
            Flash::error('Servizi Appartamenti non trovato');

            return redirect(route('admin.roomsServices.index'));
        }

        $rooms      = $this->roomRepository->all()->pluck( 'name', 'id' );
        // $services   = $this->serviceRepository->all()->pluck( 'name', 'id' );
        $services = Service::all()->sortBy('ico');

        return view('admin.rooms.rooms_services.edit')->with('roomsService', $roomsService)
            ->with('rooms', $rooms)
            ->with('services', $services);
    }

    /**
     * Update the specified RoomsService in storage.
     *
     * @param  int              $id
     * @param UpdateRoomsServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomsServiceRequest $request)
    {
        $roomsService = $this->roomsServiceRepository->findWithoutFail($id);

        if (empty($roomsService)) {
            Flash::error('Servizi Appartamenti non trovato');

            return redirect(route('admin.roomsServices.index'));
        }

        $roomsService = $this->roomsServiceRepository->update($request->all(), $id);

        Flash::success('Servizi aggiornati correttamente.');

        return redirect(route('admin.roomsServices.index'));
    }

    /**
     * Remove the specified RoomsService from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roomsService = $this->roomsServiceRepository->findWithoutFail($id);

        if (empty($roomsService)) {
            Flash::error('Servizi Appartamenti non trovato');

            return redirect(route('admin.roomsServices.index'));
        }

        $this->roomsServiceRepository->forceDelete($id);

        Flash::success('Servizio di appartamento correttamente rimosso.');

        return redirect(route('admin.roomsServices.index'));
    }
}
