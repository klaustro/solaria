<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\Admin\RoomAPIController;
use App\Http\Controllers\API\Admin\findUnavailableRooms;
use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\MailController;
use App\Http\Requests\Admin\CreateRateRequest;
use App\Http\Requests\Admin\SearchRateRequest;
use App\Http\Requests\Admin\UpdateRateRequest;
use App\Models\Admin\RoomTranslation;
use App\Repositories\Admin\BlogRepository;
use App\Repositories\Admin\BlogTranslationRepository;
use App\Repositories\Admin\BookingDetailRepository;
use App\Repositories\Admin\PackageRepository;
use App\Repositories\Admin\RateRepository;
use App\Repositories\Admin\RoomCategoryRepository;
use App\Repositories\Admin\RoomLocationRepository;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\Admin\RoomSeasonRepository;
use Carbon\Carbon;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Validator;

class RatesController extends AppBaseController
{
    /** @var  RateRepository */
    private $rateRepository;
    private $roomRepository;
    private $packageRepository;
    private $blogRepository;
    private $blogTranslationRepository;
    /** @var  integer */
    private $defaultLanguage = 1;

    /** @var  BookingDetailRepository */
    private $bookingDetailRepository;

    /** @var  RoomSeasonRepository */
    private $roomSeasonRepository;

    /** @var  RoomCategoryRepository */
    private $roomCategoryRepository;

    /** @var  RoomLocationRepository */
    private $roomLocationRepository;

    public function __construct( RateRepository $rateRepo,
        RoomRepository $roomRepository,
        PackageRepository $packageRepository,
        BlogRepository $blogRepository,
        BlogTranslationRepository $blogTranslationRepository,
        BookingDetailRepository $bookingDetailRepo,
        RoomSeasonRepository $roomSeasonRepo,
        RoomCategoryRepository $roomCategoryRepo,
        RoomLocationRepository $roomLocationRepo )
    {
        $this->rateRepository = $rateRepo;
        $this->roomRepository = $roomRepository;
        $this->packageRepository = $packageRepository;
        $this->blogRepository = $blogRepository;
        $this->blogTranslationRepository = $blogTranslationRepository;
        $this->bookingDetailRepository = $bookingDetailRepo;
        $this->roomSeasonRepository = $roomSeasonRepo;
        $this->roomCategoryRepository = $roomCategoryRepo;
        $this->roomLocationRepository = $roomLocationRepo;
    }

    /**
     * Display a listing of the Rates.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->rateRepository->pushCriteria( new RequestCriteria( $request ) );
        $rates = $this->rateRepository->orderBy( 'id', 'desc' )->all();

        return view( 'admin.rates.index' )
            ->with( 'rates', $rates );
    }

    /**
     * Show the form for creating a new Rates.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'admin.rates.create' );
    }

    /**
     * Buscar las habitaciones sin ofertas en el rango dado.
     *
     * @param Request $request
     * @return Response
     */
    public function search( SearchRateRequest $request )
    {
        $this->roomRepository->pushCriteria( new RequestCriteria( $request ) );
        $this->roomRepository->pushCriteria( new LimitOffsetCriteria( $request ) );

        // inputs
        $start_date = $request->get( 'start_date' );
        $end_date   = $request->get( 'end_date' );
        $checkin        = Carbon::createFromFormat( 'd/m/Y H', $start_date . ' 0' )->format( 'Y-m-d' );
        $checkout       = Carbon::createFromFormat( 'd/m/Y H', $end_date . ' 0' )->format( 'Y-m-d' );
        $input          = compact( 'checkin', 'checkout' );

        // obtener las rooms no disponibles por Rates
        /*$unavailableRateRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {

            $rates = $this->rateRepository->all();

            return $rates;
        }, 'start_date', 'end_date' );*/

        // obtener las rooms no disponibles por BookingDetails
        $unavailableBookingDetailRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {

            $bookingDetails = $this->bookingDetailRepository->findWhere( [
                [ 'checkin_date',    '>', $start_range_ ],
                [ 'checkout_date',   '<', $end_range_ ]
            ] );

            // validacion de status_id = 5 ( reservas anuladas )
            $bookingDetails = $bookingDetails->filter( function ( $item, $key ) {
                return $item->booking->status_id !== 5;
            } );

            return $bookingDetails;
        } );

        // union de las rooms no disponibles (BookingDetails y LockedRooms)
        // $unavailableRooms = array_unique( array_merge( $unavailableRateRooms, $unavailableBookingDetailRooms ) );

        // obtener tanto disponibles como no disponibles
        $rooms = $this->roomRepository->getCustomized( $input, $unavailableBookingDetailRooms, true, false );

        return view( 'admin.rates.search' )
            ->with( 'rooms', $rooms )
            ->with( 'start_date', $start_date )
            ->with( 'end_date', $end_date );
    }

    /**
     * Store a newly created Rates in storage.
     *
     * @param CreateRateRequest $request
     *
     * @return Response
     */
    public function store( Request $request )
    {
        $input = $this->inputStore( $request );

        // validacion
        $messages = [
            'rooms.required' => 'Deve selezionare almeno un appartamento.',
        ];
        $validator = Validator::make( $input, [
            'start_date'        => [ 'required' ],
            'end_date'          => [ 'required' ],
            'rooms'             => [ 'required', 'array' ],
            'rooms.*.discount'  => [ 'required', 'integer', 'between:0,100' ],
            'emailOrPost'       => [ 'required', Rule::in( [ 'post', 'email' ] ) ],
            'email'             => [ 'nullable', 'email', Rule::requiredIf( function () use ( $request ) {
                return $request->get( 'emailOrPost' ) === 'email';
            } ) ],
            'title'             => [ 'nullable', Rule::requiredIf( function () use ( $request ) {
                return $request->get( 'emailOrPost' ) === 'post';
            } ) ],
        ], $messages );
        if ( $validator->fails() ) {
            return redirect( route( 'admin.rates.search', [ 'start_date' => $input[ 'start_date' ], 'end_date' => $input[ 'end_date' ] ] ) )
                ->withErrors( $validator )
                ->withInput();
        }

        $rates = [];
        foreach ( $input[ 'rooms' ] as $roomId => $roomItem ) {
            // proceso solo las rooms seleccionadas
            if ( !array_key_exists( 'selected', $roomItem ) ) {
                continue;
            }

            // obtengo el modelo del room
            $room = $this->roomRepository->findWithoutFail( $roomId );

            // obtengo formato correcto para guardar fechas en BD
            $startDate  = Carbon::createFromFormat( 'd/m/Y H', $input[ 'start_date' ] . ' 0' );
            $endDate    = Carbon::createFromFormat( 'd/m/Y H', $input[ 'end_date' ] . ' 0' );

            // obtengo el descuento de la room
            $discount   = $roomItem[ 'discount' ];

            // obtengo el precio del rango seleccionado
            $dates = [
                'checkin' => $startDate->format( 'Y-m-d' ),
                'checkout' => $endDate->format( 'Y-m-d' ),
            ];
            $roomArray = $this->roomRepository->getPrices( $room->toArray(), $dates, false );
            $price = $roomArray[ 'price_request' ];

            // cantidad dias de la oferta
            $days = $roomArray[ 'price_detail_request' ][ 'totalDays' ];

            // seteo valores para guardar la oferta
            $rateModel = [];
            $rateModel[ 'room_id' ]     = $roomId;
            $rateModel[ 'start_date' ]  = $startDate->format( 'Y-m-d' );
            $rateModel[ 'end_date' ]    = $endDate->format( 'Y-m-d' );
            $rateModel[ 'package_id' ]  = 1; # QUITAR CABLE

            // dias de la oferta
            $rateModel[ 'days' ]        = $days;

            // descuento
            $rateModel[ 'discount' ]    = $discount;

            // este es el precio total sin oferta
            $rateModel[ 'price_temp' ]  = $price;

            // este es el precio total con oferta
            $rateModel[ 'price_total' ] = $price - ( ( $discount / 100 ) * $price );

            // este es el precio por noche con oferta
            $rateModel[ 'price' ] = $rateModel[ 'price_total' ] / $rateModel[ 'days' ];

            $rateModel[ 'iva' ]         = 0.22; # DEJAR CABLE
            $rateModel[ 'blog_id' ]     = null; // va null
            $rateModel[ 'email' ]       = $input[ 'email' ]; // va null
            $rateModel[ 'comment' ]     = $input[ 'comment' ]; // va null

            // token
            $rateModel[ 'token' ] = str_random( 32 );

            $rate = $this->rateRepository->create( $rateModel );

            $rates[] = $rate;
        }

        // se crea post
        $blog = null;
        if ( !empty( $rates ) ) {
            $blog = $this->postRates( $rates, $input, $input[ 'emailOrPost' ] );
        }

        $data = $blog->toArray();

        $roomLocationes  = $this->roomLocationRepository->all();
        $roomCategories = $this->roomCategoryRepository->all();

        // Envio de email personalizado
        $message = null;
        if ( $input[ 'emailOrPost' ] === 'email' ) {

            // $sended = $this->sendMail( $blog->toArray(), 'Promozione' );
            $sended = $this->sendMail( $data, 'Promozione', $roomCategories,$roomLocationes);

            if ( @$sended == 'OK' ) {
                $message = 'email inviata a ' . $input[ 'email' ] ;
            }
            else {
                $message = 'email non inviata';
            }
        }

        Flash::success( tags( 'back_rates_title' ).' salvato correttamente.' . $message  );

        return redirect( route( 'admin.rates.index' ) );
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function inputStore( $request )
    {
        $model = $request->only( [
            'start_date',
            'end_date',

            'emailOrPost',

            'email',

            'title',
            'subtitle',
            'description',
            'comment'
        ] );

        // tomar en cuenta solo las rooms seleccionadas
        $model[ 'rooms' ] = array_filter( $request->get( 'rooms' ), function ( $value, $key ) {
            // devuelvo solo los que tengan el indice selected
            return array_key_exists( 'selected', $value );
        }, ARRAY_FILTER_USE_BOTH );

        return $model;
    }

    /**
     * Display the specified Rates.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show( $id )
    {
        $rates = $this->rateRepository->findWithoutFail( $id );

        if ( empty( $rates ) ) {
            Flash::error( 'Offerte Especiali non trovata' );

            return redirect( route( 'admin.rates.index' ) );
        }

        return view( 'admin.rates.show' )->with( 'rates', $rates );
    }

    /**
     * Show the form for editing the specified Rates.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $id )
    {
        $rate = $this->rateRepository->findWithoutFail( $id );

        if (empty( $rate ) ) {
            Flash::error( 'Offerte Especiali non trovata' );

            return redirect( route( 'admin.rates.index' ) );
        }

        $rooms = $this->roomRepository->all()->pluck( 'name', 'id' );
        // $packages = $this->packageRepository->all()->pluck( 'name', 'id' );

        return view( 'admin.rates.edit' )
            ->with( 'rooms', $rooms)
            // ->with( 'packages', $packages )
            ->with( 'rate', $rate );
    }

    /**
     * Update the specified Rates in storage.
     *
     * @param  int              $id
     * @param UpdateRateRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateRateRequest $request )
    {
        $rate = $this->rateRepository->findWithoutFail( $id );

        if ( empty( $rate ) ) {
            Flash::error( 'Offerte Especiali non trovata' );

            return redirect( route( 'admin.rates.index' ) );
        }

        // inputs
        $input = $this->inputUpdate( $request );

        // update model
        $rate = $this->rateRepository->update( $input, $id );

        // update price (precio por noche)
        $rate->price = $rate->price_total / $rate->days;
        $rate->save();

        if ( $request->post ) {
            $this->postRates( [ $rate ], $input );
        }

        Flash::success( 'Si ha modificato '.tags( 'back_rates_title' ).' correttamente.' );

        return redirect( route( 'admin.rates.index' ) );
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function inputUpdate( $request )
    {
        $model = $request->only( [
            'room_id',
            'price_total',
            'discount',
            'iva',
        ] );

        return $model;
    }

    /**
     * Remove the specified Rates from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $rates = $this->rateRepository->findWithoutFail( $id );

        if ( empty( $rates ) ) {
            Flash::error( 'Offerte Especiali non trovata' );

            return redirect( route( 'admin.rates.index' ) );
        }

        $this->rateRepository->delete( $id );


        Flash::success( 'Si ha cancellato '.tags( 'back_rates_title' ).' correttamente.' );

        return redirect( route( 'admin.rates.index' ) );
    }

    /**
     * Method to post a rate
     * @param  Rate $rate
     */
    private function postRates( $rates, array $input, ?string $type = 'post' )
    {
        $data = [
            'slug'              => str_slug( $input[ 'title' ] ),
            'blog_category_id'  => 1,
            'user_id'           => auth()->user()->id,
            'image'             => null,
            'status_id'         => ( $type === 'post' ) ? 1 : 2,
        ];

        // obtengo el blog del primer rate del array proporcionado
        $blog = $this->blogRepository->findWithoutFail( $rates[ 0 ]->blog_id );

        if ( $blog == null ) {
            $blog = $this->blogRepository->create( $data );
        }
        else {
            $blog->fill( $data );
            $blog->save();
        }

        $dataTranslation = [
            'language_id'   => 1,
            'title'         => @$input[ 'title' ] ?? 'Offerte Especiali',
            'subtitle'      => @$input[ 'subtitle' ] ?? 'Offerte Especiali',
            'description'   => @$input[ 'description' ] ?? 'Offerte Especiali',
            'comment'       => @$input[ 'description' ] ?? 'Offerte Especiali',
        ];

        // update or create translation
        $this->updateOrCreateTranslation( $blog, $dataTranslation );

        // guardar el id del blog en cada rate
        foreach ( $rates as $key => $rate ) {
            $rate->blog_id = $blog->id;
            $rate->save();
        }

        return $blog;
    }

    /**
     * Send the mail of the invoice and the purchase BOOKING
     * @param array $data
     * @param string $template
     * @return void
     */
    public function sendMail( $data, $template, $roomCategories = null, $roomLocationes = null)
    {

        $request = [
            'subject'        => tags( 'back_rates_title' ),
            'msg'            => 'Promozione',
            'email'          => $data[ 'rates' ][ 0 ][ 'email' ],
            'data'           => $data,
            'roomCategories' => $roomCategories,
            'roomLocationes' => $roomLocationes,

        ];

        return MailController::sendMail( $request, $template );
    }
}
