<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateGeneralInformationRequest;
use App\Http\Requests\Admin\UpdateGeneralInformationRequest;
use App\Repositories\Admin\GeneralInformationRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GeneralInformationController extends AppBaseController
{
    /** @var  GeneralInformationRepository */
    private $generalInformationRepository;

    public function __construct(GeneralInformationRepository $generalInformationRepo)
    {
        $this->generalInformationRepository = $generalInformationRepo;
    }

    /**
     * Display a listing of the GeneralInformation.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->generalInformationRepository->pushCriteria(new RequestCriteria($request));
        $generalInformations = $this->generalInformationRepository->all();

        return view('admin.general_informations.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('generalInformations', $generalInformations);
    }

    /**
     * Show the form for creating a new GeneralInformation.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.general_informations.create');
    }

    /**
     * Store a newly created GeneralInformation in storage.
     *
     * @param CreateGeneralInformationRequest $request
     *
     * @return Response
     */
    public function store(CreateGeneralInformationRequest $request)
    {
        $input = $request->all();

        $generalInformation = $this->generalInformationRepository->create($input);

        Flash::success('General Information saved successfully.');

        return redirect(route('admin.generalInformations.index'));
    }

    /**
     * Display the specified GeneralInformation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $generalInformation = $this->generalInformationRepository->findWithoutFail($id);

        if (empty($generalInformation)) {
            Flash::error('General Information not found');

            return redirect(route('admin.generalInformations.index'));
        }

        return view('admin.general_informations.show')->with('generalInformation', $generalInformation);
    }

    /**
     * Show the form for editing the specified GeneralInformation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $generalInformation = $this->generalInformationRepository->findWithoutFail($id);

        if (empty($generalInformation)) {
            Flash::error('General Information not found');

            return redirect(route('admin.generalInformations.index'));
        }

        return view('admin.general_informations.edit')->with('generalInformation', $generalInformation);
    }

    /**
     * Update the specified GeneralInformation in storage.
     *
     * @param  int              $id
     * @param UpdateGeneralInformationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGeneralInformationRequest $request)
    {
        $generalInformation = $this->generalInformationRepository->findWithoutFail($id);

        if (empty($generalInformation)) {
            Flash::error('General Information not found');

            return redirect(route('admin.generalInformations.index'));
        }

        $generalInformation = $this->generalInformationRepository->update($request->all(), $id);

        Flash::success('General Information updated successfully.');

        return redirect(route('admin.generalInformations.index'));
    }

    /**
     * Remove the specified GeneralInformation from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $generalInformation = $this->generalInformationRepository->findWithoutFail($id);

        if (empty($generalInformation)) {
            Flash::error('General Information not found');

            return redirect(route('admin.generalInformations.index'));
        }

        $this->generalInformationRepository->delete($id);

        Flash::success('General Information deleted successfully.');

        return redirect(route('admin.generalInformations.index'));
    }
}
