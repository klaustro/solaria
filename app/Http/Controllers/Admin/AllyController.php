<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateAllyRequest;
use App\Http\Requests\Admin\UpdateAllyRequest;
use App\Repositories\Admin\AllyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AllyController extends AppBaseController
{
    /** @var  AllyRepository */
    private $allyRepository;

    public function __construct(AllyRepository $allyRepo)
    {
        $this->allyRepository = $allyRepo;
    }

    /**
     * Display a listing of the Ally.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->allyRepository->pushCriteria(new RequestCriteria($request));
        $allies = $this->allyRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.activities.allies.index')
            ->with('allies', $allies);
    }

    /**
     * Show the form for creating a new Ally.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.activities.allies.create');
    }

    /**
     * Store a newly created Ally in storage.
     *
     * @param CreateAllyRequest $request
     *
     * @return Response
     */
    public function store(CreateAllyRequest $request)
    {
        $input = $request->all();

        $ally = $this->allyRepository->create($input);

        Flash::success('Alleato salvato correttamente.');

        return redirect(route('admin.allies.index'));
    }

    /**
     * Show the form for editing the specified Ally.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ally = $this->allyRepository->findWithoutFail($id);

        if (empty($ally)) {
            Flash::error('Alleato non trovato');

            return redirect(route('admin.allies.index'));
        }

        return view('admin.activities.allies.edit')->with('ally', $ally);
    }

    /**
     * Update the specified Ally in storage.
     *
     * @param  int              $id
     * @param UpdateAllyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAllyRequest $request)
    {
        $ally = $this->allyRepository->findWithoutFail($id);

        if (empty($ally)) {
            Flash::error('Alleato non trovato');

            return redirect(route('admin.allies.index'));
        }

        $ally = $this->allyRepository->update($request->all(), $id);

        Flash::success('Si ha modificato parnet correttamente.');

        return redirect(route('admin.allies.index'));
    }

    /**
     * Remove the specified Ally from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ally = $this->allyRepository->findWithoutFail($id);

        if (empty($ally)) {
            Flash::error('Alleato non trovato');

            return redirect(route('admin.allies.index'));
        }

        $this->allyRepository->delete($id);

        Flash::success('Su ha cancellato parnet correttamente.');

        return redirect(route('admin.allies.index'));
    }
}
