<?php

namespace App\Http\Controllers\Admin;

use App\Events\EventPackage;
use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreatePackageRequest;
use App\Http\Requests\Admin\UpdatePackageRequest;
use App\Models\Admin\translation;
use App\Repositories\Admin\PackageRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PackageController extends AppBaseController
{
    /** @var  PackageRepository */
    private $packageRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct(PackageRepository $packageRepo)
    {
        $this->packageRepository = $packageRepo;
    }

    /**
     * Display a listing of the Package.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->packageRepository->pushCriteria(new RequestCriteria($request));
        $packages = $this->packageRepository->all();

        return view('admin.packages.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('packages', $packages);
    }

    /**
     * Show the form for creating a new Package.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );
        return view('admin.packages.create')
        ->with( 'translation', $translation );
    }

    /**
     * Store a newly created Package in storage.
     *
     * @param CreatePackageRequest $request
     *
     * @return Response
     */
    public function store(CreatePackageRequest $request)
    {
        // inputs
        $input               = $this->input( $request );

        $inputTranslation    = $this->inputTranslation( $request );

        // create Model
        $model = $this->packageRepository->create($input);

        // create translation
        $model->translations()->create( $inputTranslation );

        if ($request->notificate) {
            event(new EventPackage($model));
        }

        Flash::success('Pacchetto salvato correttamente.');

        return redirect(route('admin.packages.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request )
    {
        $model =  $request->only([
            'date',
            'min_nights',
            'max_nights'
        ]);

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $translation = $request->only([
            'name',
            'description',
            'language_id',
        ]);

        return $translation;
    }

    /**
     * Display the specified Package.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('Pacchetto non trovato');

            return redirect(route('admin.packages.index'));
        }

        return view('admin.packages.show')->with('package', $package);
    }

    /**
     * Show the form for editing the specified Package.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($lang, $id )
    {
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('Pacchetto non trovato');

            return redirect(route('admin.packages.index'));
        }

        // Logica translation
        $translation = $package->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view('admin.packages.edit')
        ->with( 'translation', $translation )
        ->with('package', $package);
    }

    /**
     * Update the specified Package in storage.
     *
     * @param  int              $id
     * @param UpdatePackageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePackageRequest $request)
    {
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('Pacchetto non trovato');

            return redirect(route('admin.packages.index'));
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );
        // update model
        $model = $this->packageRepository->update( $input, $id );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        if ($request->notificate) {
            event(new EventPackage($model));
        }

        Flash::success('Pacchetto modificato correttamente.');

        return redirect(route('admin.packages.index'));
    }

    /**
     * Remove the specified Package from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $package = $this->packageRepository->findWithoutFail($id);

        if (empty($package)) {
            Flash::error('Pacchetto non trovato');

            return redirect(route('admin.packages.index'));
        }

        $this->packageRepository->delete($id);

        Flash::success('Pacchetto cancellato correttamente.');

        return redirect(route('admin.packages.index'));
    }
}
