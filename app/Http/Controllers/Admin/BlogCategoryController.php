<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateBlogCategoryRequest;
use App\Http\Requests\Admin\UpdateBlogCategoryRequest;
use App\Repositories\Admin\BlogCategoryRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BlogCategoryController extends AppBaseController
{
    /** @var  BlogCategoryRepository */
    private $blogCategoryRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct(BlogCategoryRepository $blogCategoryRepo)
    {
        $this->blogCategoryRepository = $blogCategoryRepo;
    }

    /**
     * Display a listing of the BlogCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->blogCategoryRepository->pushCriteria(new RequestCriteria($request));
        $blogCategories = $this->blogCategoryRepository->all();

        return view('admin.blogs.blog_categories.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('blogCategories', $blogCategories);
    }

    /**
     * Show the form for creating a new BlogCategory.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );

        return view('admin.blogs.blog_categories.create')
            ->with( 'translation', $translation );
    }

    /**
     * Store a newly created BlogCategory in storage.
     *
     * @param CreateBlogCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateBlogCategoryRequest $request)
    {
        // inputs
        $input               = $this->input( $request );
        $inputTranslation    = $this->inputTranslation( $request );

        // create model
        $model = $this->blogCategoryRepository->create($input);

        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success('Categoria Blog salvato correttamente.');

        return redirect(route('admin.blogCategories.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    private function input( $request )
    {
        $model =  $request->only([
            //
        ]);

        if ($request->hasFile('image')) {
            $model['image'] = saveFile($request->file('image'), 'public/multimedia/activities', 'category');
        }

        $model['status_id'] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    private function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);
        $translation = $request->only([
            'name',
            'description',
            'language_id',
        ]);

        return $translation;
    }



    /**
     * Show the form for editing the specified BlogCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($lang, $id)
    {
        $blogCategory = $this->blogCategoryRepository->findWithoutFail($id);

        if (empty($blogCategory)) {
            Flash::error('Categoria Blog non trovata');

            return redirect(route('admin.blogCategories.index'));
        }

        // Logica translation
        $translation = $blogCategory->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view('admin.blogs.blog_categories.edit')
            ->with( 'translation', $translation )
            ->with('blogCategory', $blogCategory);
    }

    /**
     * Update the specified BlogCategory in storage.
     *
     * @param  int              $id
     * @param UpdateBlogCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlogCategoryRequest $request)
    {
        $blogCategory = $this->blogCategoryRepository->findWithoutFail($id);

        if (empty($blogCategory)) {
            Flash::error('Categoria Blog non trovata');

            return redirect(route('admin.blogCategories.index'));
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->blogCategoryRepository->update($input, $id);

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );
        Flash::success('Categoria Blog modificato correttamente.');

        return redirect(route('admin.blogCategories.index'));
    }

    /**
     * Remove the specified BlogCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $blogCategory = $this->blogCategoryRepository->findWithoutFail($id);

        if (empty($blogCategory)) {
            Flash::error('Categoria Blog non trovata');

            return redirect(route('admin.blogCategories.index'));
        }

        $this->blogCategoryRepository->delete($id);

        Flash::success('Categoria Blog cancellato correttamente.');

        return redirect(route('admin.blogCategories.index'));
    }
}
