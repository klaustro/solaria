<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateUsersRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Models\Admin\Role;
use App\Models\Admin\RoleUser;
use App\Models\Admin\UserDetail;
use App\Repositories\Admin\RoleRepository;
use App\Repositories\Admin\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BackofficeUserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    /** @var  UserRepository */
    private $roleRepository;

    public function __construct(UserRepository $userRepo, RoleRepository $rolesRepo)
    {
        $this->userRepository = $userRepo;
        $this->roleRepository = $rolesRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $users = $this->userRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.users.users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.users.users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUsersRequest $request)
    {
        $input = $this->input($request);

        $user = $this->userRepository->create($input);

        $details = new UserDetail();
        $details->user_id = $user->id;
        $details->save();

        Flash::success('Utente salvato correttamente.');

        return redirect(route('admin.users.index'));
    }

    private function input($request)
    {
        $model =  $request->only([
            'name',
            'lastname',
            'email',
        ]);

        $model['active'] = 1;
        $model['activation_token'] = 'Backoffice Created';
        $model['password'] = bcrypt($request->password);

        return $model;
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Utente non trovato');

            return redirect(route('admin.users.index'));
        }

        $role = $this->roleRepository->all()->pluck( 'name', 'id' );

        return view('admin.users.users.edit')
        ->with('role',$role)
        ->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        if (empty($user)) {
            Flash::error('Utente non trovato');

            return redirect(route('admin.users.index'));
        }

        $model = $this->userRepository->update($request->all(), $id);

        $model->roles()->detach();
        $roleUser = new RoleUser();
        $roleUser->role_id = $request->get('role_id');

        $model->roleUser()->save($roleUser);

        // pregunto si el usuario que se esta actalizando es el mismo usuario que esta logueado
        if ( $model->id === Auth::user()->id ) {

            // pregunto si el usuario NO conserva el rol admin
            if ( $model->roles->first()->id !== 1 ) {

                // deslogueo al usuario
                Auth::logout();
            }
        }

        Flash::success('Si ha modificato utente correttamente.');

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('Utente non trovato');

            return redirect(route('admin.users.index'));
        }

        $this->userRepository->update(['email' => $id], $id);

        $this->userRepository->delete($id);

        Flash::success('Si ha cancellato utente correttamente.');

        return redirect(route('admin.users.index'));
    }
}
