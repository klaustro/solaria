<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreatepolicyRequest;
use App\Http\Requests\Admin\UpdatepolicyRequest;
use App\Models\Admin\PolicyTranslations;
use App\Repositories\Admin\policyRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class policyController extends AppBaseController
{
    /** @var  policyRepository */
    private $policyRepository;

    public function __construct(policyRepository $policyRepo)
    {
        $this->policyRepository = $policyRepo;
    }

    /**
     * Display a listing of the policy.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->policyRepository->pushCriteria(new RequestCriteria($request));
        $policies = $this->policyRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.policies.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('policies', $policies);
    }

    /**
     * Show the form for creating a new policy.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( \App::getLocale() );
        return view('admin.policies.create')
                ->with('translation', $translation);
    }

    /**
     * Store a newly created policy in storage.
     *
     * @param CreatepolicyRequest $request
     *
     * @return Response
     */
    public function store(CreatepolicyRequest $request)
    {
        $input= $this->input($request);
        $inputTranslations= $this->inputTranslation($request);

        $policy = $this->policyRepository->create($input);
        $policy->policyTranslations()->create($inputTranslations);

        Flash::success('Politica salvato correttamente.');

        return redirect(route('admin.policies.index'));
    }

    /**
     * Display the specified policy.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $policy = $this->policyRepository->findWithoutFail($id);

        if (empty($policy)) {
            Flash::error('Politica non trovata');

            return redirect(route('admin.policies.index'));
        }

        return view('admin.policies.show')->with('policy', $policy);
    }

    /**
     * Show the form for editing the specified policy.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($lang, $id)
    {
        $policy = $this->policyRepository->findWithoutFail($id);

        if (empty($policy)) {
            Flash::error('Politica non trovata');

            return redirect(route('admin.policies.index'));
        }

        $translation = $policy->policyTranslation($lang);
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage($lang);
        } else {
            $translation = $translation->language;
        }

        return view('admin.policies.edit')
            ->with('translation', $translation)
            ->with('policy', $policy);
    }

    /**
     * Update the specified policy in storage.
     *
     * @param  int              $id
     * @param UpdatepolicyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepolicyRequest $request)
    {
        $policy = $this->policyRepository->findWithoutFail($id);

        if (empty($policy)) {
            Flash::error('Politica non trovata');

            return redirect(route('admin.policies.index'));
        }


        $input= $this->input($request);
        $inputTranslations= $this->inputTranslation($request);

        // ACA SE VALIDA SI EL IDIOMA INDICADO YA ESTA GUARDADO
        $policyTranslation = $policy->policyTranslations->filter(function($model) use ($request) {
            return $model->language_id == $request->language_id;
        })->first();

        if ( $policyTranslation ) {
            // SI ENTRA AQUI SIGNIFICA QUE EL IDIOMA YA EXISTE, ENTONCES SE ACTUALIZA
            $policyTranslation->update($request->only(['name','description']), ['id' => $policyTranslation->id]);
        }
        else {
            // SI ENTRA AQUI SIGNIFICA QUE EL IDIOMA ES NUEVO, ENTONCES SE GUARDA POR PRIMERA VEZ
            $policy->policyTranslations()->create($inputTranslations);
        }

        Flash::success('Si ha modificato politica correttamente.');

        return redirect(route('admin.policies.index'));
    }

    /**
     * Remove the specified policy from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $policy = $this->policyRepository->findWithoutFail($id);

        if (empty($policy)) {
            Flash::error('Politica non trovata');

            return redirect(route('admin.policies.index'));
        }

        $this->policyRepository->delete($id);

        Flash::success('Si ha cancellato politica correttamente.');

        return redirect(route('admin.policies.index'));
    }

    /**
     * Ordenar los camposque se guardan en policy
     *
     * @return array $policy
     */
    public function input($request)
    {

        $policy =  $request->only([
                        'slug',
                        // 'image',
                        'link'
                    ]);

        $policy['slug'] = str_slug($request->name, '-');
        return $policy;
    }

    /**
     * Ordenar los camposque se guardan en policy Translations
     *
     * @return array $policyTranslation
     */
    public function inputTranslation($request)
    {
        $policyTranslation =   $request->only([
                                    'name',
                                    'description',
                                    'language_id',
                                    'policies_id'
                                ]);

        return $policyTranslation;
    }
}
