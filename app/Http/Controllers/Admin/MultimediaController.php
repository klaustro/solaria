<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateMultimediaRequest;
use App\Http\Requests\Admin\UpdateMultimediaRequest;
use App\Models\Admin\Module;
use App\Repositories\Admin\MultimediaRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MultimediaController extends AppBaseController
{
    /** @var  MultimediaRepository */
    private $multimediaRepository;

    public function __construct( MultimediaRepository $multimediaRepo )
    {
        $this->multimediaRepository = $multimediaRepo;
    }

    /**
     * Display a listing of the Multimedia.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        return view( 'admin.utils.multimedia.index' )
            ->with( 'object', $object )
            ->with( 'filesDB', $filesDB );
    }

    /**
     * Show the form for creating a new Multimedia.
     *
     * @return Response
     */
    public function create()
    {
        $modules = Module::pluck( 'name', 'name' );
        return view( 'admin.utils.multimedia.create', compact( 'modules' ) );
    }

    public function store( $request, $object, $class, $main )
    {
        //se valida y almacena la imagen
        $image = $this->validateAndSaveMultimedia( $request, $class );

        if ( !$image ) {
            $message = "L\'immagine non ha un\'estensione corretta";
            $type = "ERROR";
            Session::flash( $type, $message );
            return false;
        }

        //se crea el row
        if ( $object->row === null ) {
            $row = new Rows();
            $object->row()->save( $row );

            // $object = Product::find( $object->id );
        }

        //se crea el image_module
        $this->saveMultimediaModule( $object, $image->id, $main );
    }

    public function saveMultimediaModule( $object_, $media_id, $main_ = 1 )
    {
        //se crea el image_module
        $row_multimedia                 = new RowsMultimedia();
        $row_multimedia->row_id         = $object_->row->id;
        $row_multimedia->multimedia_id  = $media_id;
        /*$image_module->main           = $main_;
        $image_module->order            = null;*/
        $row_multimedia->status_id      = 1;
        $row_multimedia->save();
    }
    /**
     * Store a newly image in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Images                $imgs
     */
    public function validateAndSaveMultimedia($request, $class )
    {
        //se valida que haya enviado imagen
        if ( $request->file( "image_jumperr" ) ) {
            $file = $request->file( "image_jumperr" );
        }
        else if ( $request->file( "file" ) ) {
            $file = $request->file( "file" );
        }
        else {
            return false;
        }
        $this->validate( $request,[
            'image_jumperr' => 'image|mimes:jpg,png,gif,jpeg'
        ] );

        $mime = $file->getClientMimeType();
        $contain = strstr( $mime, 'image' );

        if ( $contain ) {

            //se guarda en 'storage/public/app/multimedia' la imagen
            $name = $class . time() . "." . $file->getClientOriginalName();
            $name = str_replace( " ", "_", $name );
            \Storage::disk( 'multimedia' )->put( $name, \File::get( $file ) );

            // datos de la imagen
            $request[ 'name' ]      = $name;
            $size                   = getimagesize( $file );
            $request[ 'width' ]     = $size[ 0 ];
            $request[ 'height' ]    = $size[ 1 ];
            $request[ 'size' ]      = $size[ 2 ];

            //se almacena la ruta de la imagen en bd
            $path               = storage_path( $name );
            $pathParts          = pathinfo( $path );
            $request[ 'path' ]  = 'storage/multimedia'; // $request[ 'path' ] = $pathParts[ 'dirname' ];

            // save
            $imgs = new Multimedia($request->all());
            $imgs->name = $name;
            $imgs->save();

            return $imgs;
        } else {
            $type='success';
            $message="L\'immagine non ha un\'estensione corretta";

            Session::flash($type, $message);
            return false;
        }
    }

    /**
     * Display the specified Multimedia.
     *
     * @param  int $id
     *
     * @return Response
     */
    /*public function show($id)
    {
        $multimedia = $this->multimediaRepository->findWithoutFail($id);

        if (empty($multimedia)) {
            Flash::error('Multimedia non trovato');

            return redirect(route('admin.multimedia.index'));
        }

        return view('admin.utils.multimedia.show')->with('multimedia', $multimedia);
    }*/

    /**
     * Show the form for editing the specified Multimedia.
     *
     * @param  int $id
     *
     * @return Response
     */
    /*public function edit($id)
    {
        $multimedia = $this->multimediaRepository->findWithoutFail($id);

        if (empty($multimedia)) {
            Flash::error('Multimedia non trovato');

            return redirect(route('admin.multimedia.index'));
        }
        $modules = Module::pluck('name', 'name');

        return view('admin.utils.multimedia.edit')
                ->with('modules', $modules)
                ->with('multimedia', $multimedia);
    }*/

    /**
     * Update the specified Multimedia in storage.
     *
     * @param  int              $id
     * @param UpdateMultimediaRequest $request
     *
     * @return Response
     */
    /*public function update($id, UpdateMultimediaRequest $request)
    {
        $multimedia = $this->multimediaRepository->findWithoutFail($id);

        if (empty($multimedia)) {
            Flash::error('Multimedia non trovato');

            return redirect(route('admin.multimedia.index'));
        }

        $multimedia = $this->multimediaRepository->update($request->all(), $id);

        Flash::success('Multimedia modificato correttamente.');

        return redirect(route('admin.multimedia.index'));
    }*/

    /**
     * Remove the specified Multimedia from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    /*public function destroy($id)
    {
        $multimedia = $this->multimediaRepository->findWithoutFail($id);

        if (empty($multimedia)) {
            Flash::error('Multimedia non trovato');

            return redirect(route('admin.multimedia.index'));
        }

        $this->multimediaRepository->delete($id);

        Flash::success('Multimedia cancellato correttamente.');

        return redirect(route('admin.multimedia.index'));
    }*/

    // PARA DROPZONE

    public function saveGalery( $request, $language, $row_multimedia, $object_id  )
    {
        //se valida la imagen y se guarda
        $multimedia = $this->store( $request, $object, $class, 1 );

        // if($multimedia==null){
        //     $message="L\'immagine non ha un\'estensione corretta";
        //     $type = "ERROR";
        // } else {
        //     $type='success';
        //     $message='Le immagini sono state salvate correttamente';
        // }
        // Session::flash($type, $message);

        //redireccionamos
        // return view('admin.utils.multimedia.index');
        return true;
    }

}
