<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateActivityRequest;
use App\Http\Requests\Admin\UpdateActivityRequest;
use App\Models\Admin\Multimedia;
use App\Models\Admin\Row;
use App\Models\Admin\RowsMultimedia;
use App\Repositories\Admin\ActivityCategoryRepository;
use App\Repositories\Admin\ActivityRepository;
use App\Repositories\Admin\AllyRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ActivityController extends AppBaseController
{
    /** @var  ActivityRepository */
    private $activityRepository;
    private $activityCategoryRepository;
    private $allyRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct(ActivityRepository $activityRepo, ActivityCategoryRepository $activityCategoryRepository, AllyRepository $allyRepository)
    {
        $this->activityRepository = $activityRepo;
        $this->activityCategoryRepository = $activityCategoryRepository;
        $this->allyRepository = $allyRepository;
    }

    /**
     * Display a listing of the Activity.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->activityRepository->pushCriteria(new RequestCriteria($request));
        $activities = $this->activityRepository->orderBy( 'id', 'desc' )->all();

        if (request()->edited) {
            Flash::success('Esperienza modificato correttamente.');
        }

        return view('admin.activities.activities.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('activities', $activities);
    }

    /**
     * Show the form for creating a new Activity.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );
        $activity_categories = $this->activityCategoryRepository->all()->pluck( 'name', 'id' );
        $allies = $this->allyRepository->all()->pluck( 'name', 'id' );

        return view('admin.activities.activities.create')
            ->with('activity_categories', $activity_categories)
            ->with('allies', $allies)
            ->with( 'translation', $translation );
    }

    /**
     * Store a newly created Activity in storage.
     *
     * @param CreateActivityRequest $request
     *
     * @return Response
     */
    public function store(CreateActivityRequest $request)
    {
        // inputs
        $input               = $this->input( $request );
        $inputTranslation    = $this->inputTranslation( $request );

        //Images are required
        if ($request->hasFile('file')) {
            $file = $request->file('file')[0];
            if($file->extension() == ''){
                return response([
                    'message' => 'I dati dati non erano validi',
                    'errors' => [
                        'file' => ['Il campo del file è richiesto']
                    ],
                ], 422);
            }
        }

        // create model
        $model = $this->activityRepository->create($input);


        //Store images
        $this->inputImages($model, $request);

        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success('Esperienza salvato correttamente.');

        return redirect(route('admin.activities.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    private function input( $request )
    {
        $model =  $request->only([
            'activity_category_id',
            'ally_id',
        ]);
        $model[ 'slug' ] = str_slug( $request->input( 'title' ), '-' );

        $model['status_id'] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    private function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $translation = $request->only([
            'title',
            'subtitle',
            'description',
            'language_id',
        ]);

        return $translation;
    }

    private function inputImages( $model, $request )
    {
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            if ($files[0]->extension() != '') {
                if($model->row == null){
                    $row = new Row();
                    $model->row()->save($row);
                } else {
                    $row = $model->row;
                }

                foreach ($files as $key => $file) {
                    $fileName = saveFile($file, 'public/multimedia/activities', 'activity' . $key);
                    $multimedia = new Multimedia();
                    $multimedia->name = basename($fileName);
                    $multimedia->path = '/' . str_replace(basename($fileName), '', $fileName);
                    $multimedia->save();

                    $rows_multimedia = new RowsMultimedia();
                    $rows_multimedia->row_id = $row->id;
                    $rows_multimedia->multimedia_id = $multimedia->id;
                    $rows_multimedia->save();
                }
            }
        }
    }

    /**
     * Show the form for editing the specified Activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $lang, $id )
    {
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            Flash::error('Esperienza non trovato');

            return redirect(route('admin.activities.index'));
        }

        // Logica translation
        $translation = $activity->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        $activity_categories = $this->activityCategoryRepository->all()->pluck( 'name', 'id' );
        $allies = $this->allyRepository->all()->pluck( 'name', 'id' );

        return view('admin.activities.activities.edit')
            ->with( 'translation', $translation )
            ->with('activity_categories', $activity_categories)
            ->with('allies', $allies)
            ->with('activity', $activity);
    }

    /**
     * Update the specified Activity in storage.
     *
     * @param  int              $id
     * @param UpdateActivityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivityRequest $request)
    {
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            Flash::error('Esperienza non trovato');

            return redirect(route('admin.activities.index'));
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->activityRepository->update($request->all(), $id);

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        $this->inputImages($model, $request);

        Flash::success('Si ha modificato esperienza correttamente.');

        return redirect(route('admin.activities.index'));
    }

    /**
     * Remove the specified Activity from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            Flash::error('Esperienza non trovata');

            return redirect(route('admin.activities.index'));
        }

        // delete translations
        $translations = $activity->translations;

        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        $this->activityRepository->delete($id);

        Flash::success('Si ha cancellato esperienza correttamente.');

        return redirect(route('admin.activities.index'));
    }
}
