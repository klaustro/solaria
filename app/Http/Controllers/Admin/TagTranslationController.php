<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateTagTranslationRequest;
use App\Http\Requests\Admin\UpdateTagTranslationRequest;
use App\Models\Admin\Language;
use App\Models\Admin\TagTranslation;
use App\Repositories\Admin\TagTranslationRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TagTranslationController extends AppBaseController
{
    /** @var  TagTranslationRepository */
    private $tagTranslationRepository;

    public function __construct(TagTranslationRepository $tagTranslationRepo)
    {
        $this->tagTranslationRepository = $tagTranslationRepo;
    }

    /**
     * Get tag value.
     *
     * @param  string $tagKey
     * @param  string $language
     *
     * @return string
     */
    public static function getTagsValues( string $tagKey, string $language )
    {
        try {
            $language = Language::where('code', $language)->first();

            $value = TagTranslation::where( 'language_id', $language->id )
                ->where( 'tag', $tagKey )
                ->pluck( 'value', 'tag' )
                ->first();

            return empty( $value ) === false ? $value : $tagKey;
        }
        catch ( Exception $e ) {
            return $e->message();
        }
    }

    /**
     * Display a listing of the TagTranslation.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->tagTranslationRepository->pushCriteria(new RequestCriteria($request));
        $tagTranslations = $this->tagTranslationRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.tag_translations.index')
            ->with('tagTranslations', $tagTranslations);
    }

    /**
     * Show the form for creating a new TagTranslation.
     *
     * @return Response
     */
    /*public function create()
    {
        return view('admin.tag_translations.create');
    }*/

    /**
     * Store a newly created TagTranslation in storage.
     *
     * @param CreateTagTranslationRequest $request
     *
     * @return Response
     */
    /*public function store(CreateTagTranslationRequest $request)
    {
        $input = $request->all();

        $tagTranslation = $this->tagTranslationRepository->create($input);

        Flash::success('Traduzione di tag salvato correttamente.');

        return redirect(route('admin.tagTranslations.index'));
    }*/

    /**
     * Display the specified TagTranslation.
     *
     * @param  int $id
     *
     * @return Response
     */
    /*public function show($id)
    {
        $tagTranslation = $this->tagTranslationRepository->findWithoutFail($id);

        if (empty($tagTranslation)) {
            Flash::error('Traduzione di tag non trovata');

            return redirect(route('admin.tagTranslations.index'));
        }

        return view('admin.tag_translations.show')->with('tagTranslation', $tagTranslation);
    }*/

    /**
     * Show the form for editing the specified TagTranslation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tagTranslation = $this->tagTranslationRepository->findWithoutFail($id);

        if (empty($tagTranslation)) {
            Flash::error('Traduzione di tag non trovata');

            return redirect(route('admin.tagTranslations.index'));
        }

        return view('admin.tag_translations.edit')->with('tagTranslation', $tagTranslation);
    }

    /**
     * Update the specified TagTranslation in storage.
     *
     * @param  int              $id
     * @param UpdateTagTranslationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTagTranslationRequest $request)
    {
        $tagTranslation = $this->tagTranslationRepository->findWithoutFail($id);

        if (empty($tagTranslation)) {
            Flash::error('Traduzione di tag non trovata');

            return redirect(route('admin.tagTranslations.index'));
        }

        $input = $request->only([ 'value' ]);

        $tagTranslation = $this->tagTranslationRepository->update($input, $id);

        Flash::success('Si ha modificato Tag Traduzioni correttamente.');

        return redirect(route('admin.tagTranslations.index'));
    }

    /**
     * Remove the specified TagTranslation from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    /*public function destroy($id)
    {
        $tagTranslation = $this->tagTranslationRepository->findWithoutFail($id);

        if (empty($tagTranslation)) {
            Flash::error('Traduzione di tag non trovata');

            return redirect(route('admin.tagTranslations.index'));
        }

        $this->tagTranslationRepository->delete($id);

        Flash::success('Traduzione di tag cancellato correttamente.');

        return redirect(route('admin.tagTranslations.index'));
    }*/
}
