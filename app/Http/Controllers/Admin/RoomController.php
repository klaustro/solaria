<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateRoomRequest;
use App\Http\Requests\Admin\UpdateRoomRequest;
use App\Models\Admin\Multimedia;
use App\Models\Admin\Row;
use App\Models\Admin\RowsMultimedia;
use App\Repositories\Admin\RoomCategoryRepository;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\Admin\RoomSeasonRepository;
use App\Repositories\Admin\ServiceRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomController extends AppBaseController
{
    /** @var  RoomRepository */
    private $roomRepository;

    /** @var  RoomCategoryRepository */
    private $roomCategoryRepository;
    private $serviceRepository;
    private $roomSeasonRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct( RoomRepository $roomRepo, RoomCategoryRepository $roomCategoryRepo,
        ServiceRepository $serviceRepository, RoomSeasonRepository $roomSeasonRepository)
    {
        $this->roomRepository = $roomRepo;
        $this->roomCategoryRepository = $roomCategoryRepo;
        $this->serviceRepository = $serviceRepository;
        $this->roomSeasonRepository = $roomSeasonRepository;
    }

    /**
     * Display a listing of the Room.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->roomRepository->pushCriteria(new RequestCriteria($request));
        $rooms = $this->roomRepository->orderBy( 'id', 'desc' )->all();

        if (request()->edited) {
            Flash::success('Appartamento modificato correttamente.');
        }

        return view('admin.rooms.rooms.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('rooms', $rooms);
    }

    /**
     * Show the form for creating a new Room.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );

        $roomCategories = $this->roomCategoryRepository->all()->pluck( 'name', 'id' );
        $services = $this->serviceRepository->all();

        return view('admin.rooms.rooms.create')
            ->with( 'roomCategories', $roomCategories )
            ->with( 'services', $services )
            ->with( 'translation', $translation );
    }

    /**
     * Store a newly created Room in storage.
     *
     * @param CreateRoomRequest $request
     *
     * @return Response
     */
    public function store( CreateRoomRequest $request )
    {
        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );
        $inputServices      = $this->inputServices( $request );

        // create model
        $model = $this->roomRepository->create( $input );

        //Store images
        $this->inputImages( $model, $request );

        // attach services
        if ( $request->services ) {
            $model->services()->attach( $inputServices );
        }

        // $this->setDefaultSeason($model);

        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success('Appartamento salvato correttamente.');

        return redirect(route('admin.rooms.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request )
    {
        $model =  $request->only( [
            'room_category_id',
            'price',
            'iva',
            'adults_quantity',
            'children_quantity'
        ] );
        $model[ 'slug' ] = str_slug( $request->input( 'name' ), '-' );

        $model[ 'status_id' ] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $translation = $request->only([
            'name',
            'description',
            'language_id',
            'subtitle1',
            'subtitle2'
        ]);

        return $translation;
    }

    private function inputServices($request)
    {
        $services = explode(',', $request->services);

        return $services;
    }

    /**
     * Method to store images in storage
     * @param  [type] $model   [description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    private function inputImages( $model, $request )
    {
        if ( $request->hasFile( 'file' ) ) {
            $files = $request->file( 'file' );
            if ( $files[ 0 ]->extension() != '' ) {
                if ( $model->row == null ) {
                    $row = new Row();
                    $model->row()->save( $row );
                } else {
                    $row = $model->row;
                }

                foreach ( $files as $key => $file ) {
                    $fileName = saveFile( $file, 'public/multimedia/rooms', 'room' . $key );
                    $multimedia = new Multimedia();
                    $multimedia->name = basename( $fileName );
                    $multimedia->path = '/' . str_replace( basename( $fileName ), '', $fileName );
                    $multimedia->save();

                    $rows_multimedia = new RowsMultimedia();
                    $rows_multimedia->row_id = $row->id;
                    $rows_multimedia->multimedia_id = $multimedia->id;
                    $rows_multimedia->save();
                }
            }
        }
    }

    /**
     * Show the form for editing the specified Room.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $lang, $id )
    {
        $room = $this->roomRepository->findWithoutFail($id);

        // dd( $room->price );

        if (empty($room)) {
            Flash::error('Appartamento non trovato');

            return redirect(route('admin.rooms.index'));
        }

        $roomCategories = $this->roomCategoryRepository->all()->pluck( 'name', 'id' );
        $services = $this->serviceRepository->all();

        // Logica translation
        $translation = $room->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view('admin.rooms.rooms.edit')
            ->with('room', $room)
            ->with('roomCategories', $roomCategories)
            ->with('services', $services)
            ->with('translation', $translation);
    }

    /**
     * Update the specified Room in storage.
     *
     * @param  int              $id
     * @param UpdateRoomRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateRoomRequest $request )
    {
        $room = $this->roomRepository->findWithoutFail($id);

        if (empty($room)) {
            Flash::error('Appartamento non trovato');

            return redirect(route('admin.rooms.index'));
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );
        //$inputServices      = $this->inputServices( $request );

        // update model
        $model = $this->roomRepository->update( $input, $id );

        //Store images
        $this->inputImages($model, $request);

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success('Si ha modificato Appartamento correttamente.');

        return redirect(route('admin.rooms.index'));
    }

    /**
     * Remove the specified Room from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $room = $this->roomRepository->findWithoutFail($id);

        if (empty($room)) {
            Flash::error('Appartamento non trovato');

            return redirect(route('admin.rooms.index'));
        }

        // validation if there are child relations
         if( $room->services->isEmpty() === false ) {
            Flash::error( 'L\'appartamento non può essere cancellato, ha servizi associati.' );

            return redirect( route( 'admin.rooms.index' ) );
        }

        // validation if there are child relations
         if( $room->roomSeasons->isEmpty() === false ) {
            Flash::error( 'Il dipartimento non può essere cancellato, ha stagioni associate.' );

            return redirect( route( 'admin.rooms.index' ) );
        }


        // delete translations
        $translations = $room->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->roomRepository->delete( $id );

        Flash::success('Si ha cancellato Appartamento correttamente.');

        return redirect(route('admin.rooms.index'));
    }
    // PARA DROPZONE

    public function saveGalery( CreateMultimediaRequest $request )
    {
        //nombre de la clase
        $class = class_basename( $this );
        $object = Room::find( $request->get( 'item_id' ) );

        //se valida la imagen y se guarda
        $multimedia = new MultimediaController();
        $multimedia = $multimedia->store( $request, $object, $class, 1 );

        if ( $multimedia == null ) {
            $message = "L\'immagine non ha un\'estensione corretta";
            $type = "ERROR";
        } else {
            $type = 'success';
            $message = 'Le immagini sono state salvate correttamente';
        }
        Session::flash( $type, $message );

        //redireccionamos
        // return view('admin.utils.multimedia.index');
        return true;
    }

    public function deleteGalery( Request $request, $language, $row_multimedia, $object_id )
    {
        try {
            //accedemos al evento al que pertenece la imagen
            $row_multimedia = RowsMultimedia::find($row_multimedia);
            if($row_multimedia!=null) {
                $object_rowable = $row_multimedia->row->rowable;

                //eliminamos la imagen
                $imgs = Multimedia::find($row_multimedia->multimedia->id);
                $row_multimedia->delete();

                //mensaje de exito
                $type='success';
                $message='Le immagini sono state cancellate correttamente';
                Session::flash($type, $message);
            }

            //redireccionamos
            return redirect()->route('admin.products.edit', ['language' => $language, 'id' => $object_id]);

        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Display a listing of the Room.
     *
     * @param Request $request
     * @return Response
     */
    public function indexPrice( Request $request )
    {
        $this->roomRepository->pushCriteria(new RequestCriteria($request));
        $rooms = $this->roomRepository->all();

        if (request()->edited) {
            Flash::success('Appartamento modificato correttamente.');
        }

        return view('admin.rooms.room_minimi.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('rooms', $rooms);
    }

        /**
     * Show the form for editing the specified Room.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function editPrice( $id )
    {
        $room = $this->roomRepository->findWithoutFail($id);

        if (empty($room)) {
            Flash::error('Appartamento non trovato');

            return redirect(route('admin.roomPrezzi.index'));
        }

        $roomCategories = $this->roomCategoryRepository->all()->pluck( 'name', 'id' );

        return view('admin.rooms.room_minimi.edit')
            ->with('room', $room)
            ->with('roomCategories', $roomCategories);
    }

    /**
     * Update the specified Room in storage.
     *
     * @param  int              $id
     * @param UpdateRoomRequest $request
     *
     * @return Response
     */
    public function updatePrice( $id, UpdateRoomRequest $request )
    {
        $room = $this->roomRepository->findWithoutFail($id);

        if (empty($room)) {
            Flash::error('Appartamento non trovato');

            return redirect(route('admin.rooms.index'));
        }

        // inputs
        $input              = $this->input( $request );

        // update model
        $model = $this->roomRepository->update( $input, $id );

        Flash::success('Si ha modificato Prezzi Minimi correttamente.');

        return redirect(route('admin.roomPrezzi.index'));
    }
}
