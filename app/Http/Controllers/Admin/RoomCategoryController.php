<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateRoomCategoryRequest;
use App\Http\Requests\Admin\UpdateRoomCategoryRequest;
use App\Models\Admin\Multimedia;
use App\Models\Admin\Row;
use App\Models\Admin\RowsMultimedia;
use App\Repositories\Admin\RoomCategoryRepository;
use App\Repositories\Admin\RoomLocationRepository;
use App\Repositories\Admin\ServiceRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomCategoryController extends AppBaseController
{
    /** @var  RoomCategoryRepository */
    private $roomCategoryRepository;

    /** @var  RoomLocationRepository */
    private $roomLocationRepository;

    /** @var  ServiceRepository */
    private $serviceRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct( RoomCategoryRepository $roomCategoryRepo,
        RoomLocationRepository $roomLocationRepo,
        ServiceRepository $serviceRepo )
    {
        $this->roomCategoryRepository = $roomCategoryRepo;
        $this->roomLocationRepository = $roomLocationRepo;
        $this->serviceRepository = $serviceRepo;
    }

    /**
     * Display a listing of the RoomCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->roomCategoryRepository->pushCriteria(new RequestCriteria($request));
        $roomCategories = $this->roomCategoryRepository->orderBy( 'id', 'desc' )->all();

        if (request()->edited) {
            Flash::success('Casa Vacanza modificato correttamente.');
        }


        return view('admin.rooms.room_categories.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('roomCategories', $roomCategories);
    }

    /**
     * Show the form for creating a new RoomCategory.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );

        $roomLocations = $this->roomLocationRepository->all()->pluck( 'name', 'id' );

        $services = $this->serviceRepository->all();

        return view('admin.rooms.room_categories.create')
            ->with( 'roomLocations', $roomLocations )
            ->with('services', $services)
            ->with( 'translation', $translation );
    }

    /**
     * Store a newly created RoomCategory in storage.
     *
     * @param CreateRoomCategoryRequest $request
     *
     * @return Response
     */
    public function store( CreateRoomCategoryRequest $request )
    {
        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );
        $inputServices      = $this->inputServices( $request );


        // create model
        $model = $this->roomCategoryRepository->create( $input );

        //Store images
        $this->inputImages($model, $request);

        // attach services
        if ( $request->services ) {
            $model->services()->attach( $inputServices );
        }

        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success('Casa Vacanza salvato correttamente.');

        return redirect(route('admin.roomCategories.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request )
    {
        $model =  $request->only( [
            'room_location_id'
        ] );

        // to update main image
        if ( $request->has( 'image' ) ) {
            $model[ 'image' ] = $request->image;
        }

        $model[ 'status_id' ] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request )
    {
        $translation = $request->only([
            'name',
            'description',
            'language_id',
        ]);

        return $translation;
    }

    private function inputServices($request)
    {
        $services = explode(',', $request->services);

        return $services;
    }

    /**
     * Method to store images in storage
     * @param  [type] $model   [description]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    private function inputImages( $model, $request )
    {
        if ( $request->hasFile( 'file' ) ) {
            $files = $request->file( 'file' );
            if ( $files[ 0 ]->extension() != '' ) {
                if ( $model->row == null ) {
                    $row = new Row();
                    $model->row()->save( $row );
                } else {
                    $row = $model->row;
                }

                foreach ( $files as $key => $file ) {
                    $fileName = saveFile( $file, 'public/multimedia/casa', 'room_categories.' . $model->id . '.' . $key );

                    // main image
                    if ( $key === 0 ) {
                        $model->image = '/' . $fileName;
                        $model->save();
                    }

                    $multimedia = new Multimedia();
                    $multimedia->name = basename( $fileName );
                    $multimedia->path = '/' . str_replace( basename( $fileName ), '', $fileName );
                    $multimedia->save();

                    $rows_multimedia = new RowsMultimedia();
                    $rows_multimedia->row_id = $row->id;
                    $rows_multimedia->multimedia_id = $multimedia->id;
                    $rows_multimedia->save();
                }
            }
        }
    }

    /**
     * Show the form for editing the specified RoomCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $lang, $id )
    {
        $roomCategory = $this->roomCategoryRepository->findWithoutFail($id);

        if (empty($roomCategory)) {
            Flash::error('Casa Vacanza non trovata');

            return redirect(route('admin.roomCategories.index'));
        }

        $roomLocations = $this->roomLocationRepository->all()->pluck( 'name', 'id' );

        // Logica translation
        $translation = $roomCategory->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        $services = $this->serviceRepository->all();
        return view('admin.rooms.room_categories.edit')
            ->with('roomCategory', $roomCategory)
            ->with('services', $services)
            ->with( 'translation', $translation )
            ->with( 'roomLocations', $roomLocations );
    }

    /**
     * Update the specified RoomCategory in storage.
     *
     * @param  int              $id
     * @param UpdateRoomCategoryRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateRoomCategoryRequest $request )
    {
        $roomCategory = $this->roomCategoryRepository->findWithoutFail($id);

        if (empty($roomCategory)) {
            Flash::error('Casa Vacanza non trovata');

            return redirect(route('admin.roomCategories.index'));
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );
        $inputServices    = $this->inputServices( $request );

        // update model
        $model = $this->roomCategoryRepository->update( $input, $id );

        //Store images
        $this->inputImages($model, $request);

        // services
        // if ($request->services) {
        //     $model->services()->detach();
        //     $model->services()->attach($inputServices);
        // }

        // // recorrer los servicios que ya tiene la habitacion
        foreach ( $model->services()->get() as $service ) {

        //     // valido si este servicio viene en el request
             if ( in_array( $service->id, $inputServices ) ) {

        //         // se mantiene el servicio a esta habitacion

        //         // elimino este servicio del array de request para no volver a enlazarlo a esta habitacion
                $key = array_search( $service->id, $inputServices );
                unset( $inputServices[ $key ] );
            }
            else {

        //         // se desenlaza el servicio a esta habitacion
                $model->services()->detach( $service->id );
            }
        }

        if ( $request->services !== null ) {
            $model->services()->attach( $inputServices );
        }

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success('Si ha modificato Casa Vacanza correttamente.');

        return redirect(route('admin.roomCategories.index'));
    }

    /**
     * Remove the specified RoomCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $roomCategory = $this->roomCategoryRepository->findWithoutFail($id);

        if (empty($roomCategory)) {
            Flash::error('Casa Vacanza non trovata');

            return redirect(route('admin.roomCategories.index'));
        }

        // validation if there are child relations
        if( $roomCategory->rooms->isEmpty() === false ) {
            Flash::error( 'La casa vacanze non può essere cancellato, ha appartamento associati.' );

            return redirect( route( 'admin.roomCategories.index' ) );
        }

        // validation if there are child relations
        if( $roomCategory->services->isEmpty() === false ) {
            Flash::error( 'La casa Vacanza non può essere cancellato, ha servizi associate.' );

            return redirect( route( 'admin.roomCategories.index' ) );
        }


        // delete translations
        $translations = $roomCategory->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->roomCategoryRepository->delete( $id );

        Flash::success('Si ha cancellato Casa Vacanza correttamente.');

        return redirect(route('admin.roomCategories.index'));
    }
}
