<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateBlogRequest;
use App\Http\Requests\Admin\UpdateBlogRequest;
use App\Repositories\Admin\BlogCategoryRepository;
use App\Repositories\Admin\BlogRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BlogController extends AppBaseController
{
    /** @var  BlogRepository */
    private $blogRepository;
    private $blogCategoryRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct( BlogRepository $blogRepo,
        BlogCategoryRepository $blogCategoryRepository )
    {
        $this->blogRepository = $blogRepo;
        $this->blogCategoryRepository = $blogCategoryRepository;
    }

    /**
     * Display a listing of the Blog.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->blogRepository->pushCriteria( new RequestCriteria( $request ) );
        $blogs = $this->blogRepository->getCustomized();

        return view( 'admin.blogs.blogs.index' )
            ->with( 'languages', LanguageController::getLanguageAll() )
            ->with( 'blogs', $blogs );
    }

    /**
     * Show the form for creating a new Blog.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );
        $blog_categories = $this->blogCategoryRepository->all()->pluck( 'name', 'id' );

        return view( 'admin.blogs.blogs.create' )
            ->with( 'blog_categories', $blog_categories )
            ->with( 'translation', $translation );
    }

    /**
     * Store a newly created Blog in storage.
     *
     * @param CreateBlogRequest $request
     *
     * @return Response
     */
    public function store( CreateBlogRequest $request )
    {
        // inputs
        $input               = $this->input( $request );
        $inputTranslation    = $this->inputTranslation( $request );

        // create model
        $model = $this->blogRepository->create( $input );

        // create translation
        $model->translations()->create( $inputTranslation );

        //Save Image
        $this->inputImages($model, $request);

        Flash::success( 'Promozioni salvato correttamente.' );

        return redirect( route( 'admin.blogs.index' ) );
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    private function input( $request )
    {
        $model =  $request->only( [
            'blog_category_id',
            'slug'
        ] );

        if ( $request->hasFile( 'image' ) ) {
            $model['image'] = '/' . saveFile( $request->file( 'image' ), 'public/multimedia/blogs', 'blog' );
        }

        $model['user_id'] = auth()->user()->id;
        $model['slug'] = str_slug( $request->title, '-' );
        $model['status_id'] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    private function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate( [
            'title' => 'required',
            'subtitle' => 'required',
            'description' => 'required',
        ] );

        $translation = $request->only( [
            'title',
            'subtitle',
            'description',
            'language_id',
        ] );

        return $translation;
    }

    /**
     * Show the form for editing the specified Blog.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $lang, $id )
    {
        $blog = $this->blogRepository->findWithoutFail( $id );
        $blog_categories = $this->blogCategoryRepository->all()->pluck( 'name', 'id' );
        if ( empty( $blog ) ) {
            Flash::error( 'Promozioni non trovato' );

            return redirect( route( 'admin.blogs.index' ) );
        }

        // Logica translation
        $translation = $blog->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view( 'admin.blogs.blogs.edit' )->with( 'blog', $blog )
            ->with( 'blog_categories', $blog_categories )
            ->with( 'translation', $translation );
    }

    /**
     * Update the specified Blog in storage.
     *
     * @param  int              $id
     * @param UpdateBlogRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateBlogRequest $request )
    {
        $blog = $this->blogRepository->findWithoutFail( $id );

        if ( empty( $blog ) ) {
            Flash::error( 'Promozioni non trovato' );

            return redirect( route( 'admin.blogs.index' ) );
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->blogRepository->update( $input, $id );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        //Save Image
        $this->inputImages($model, $request);

        Flash::success( 'Si ha modificato promozioni correttamente.' );

        return redirect( route( 'admin.blogs.index' ) );
    }

    /**
     * Remove the specified Blog from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $blog = $this->blogRepository->findWithoutFail( $id );

        if ( empty( $blog ) ) {
            Flash::error( 'Promozioni non trovato' );

            return redirect( route( 'admin.blogs.index' ) );
        }

        $this->blogRepository->delete( $id );

        Flash::success( 'Si ha cancellato promozioni correttamente.' );

        return redirect( route( 'admin.blogs.index' ) );
    }

    private function inputImages( $model, $request )
    {
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            if ($files[0]->extension() != '') {
                $fileName = saveFile($files[0], 'public/multimedia', 'blog-' . uniqid() );
                $model->image = "/$fileName";
                $model->save();
            }
        }
    }
}
