<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateRoomLocationRequest;
use App\Http\Requests\Admin\UpdateRoomLocationRequest;
use App\Repositories\Admin\RoomLocationRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomLocationController extends AppBaseController
{
    /** @var  RoomLocationRepository */
    private $roomLocationRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct( RoomLocationRepository $roomLocationRepo )
    {
        $this->roomLocationRepository = $roomLocationRepo;
    }

    /**
     * Display a listing of the RoomLocation.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->roomLocationRepository->pushCriteria(new RequestCriteria($request));
        $roomLocations = $this->roomLocationRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.rooms.room_locations.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('roomLocations', $roomLocations);
    }

    /**
     * Show the form for creating a new RoomLocation.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );

        return view('admin.rooms.room_locations.create')
            ->with( 'translation', $translation );
    }

    /**
     * Store a newly created RoomLocation in storage.
     *
     * @param CreateRoomLocationRequest $request
     *
     * @return Response
     */
    public function store( CreateRoomLocationRequest $request )
    {

        // inputs
        $input               = $this->input( $request);
        $inputTranslation    = $this->inputTranslation( $request );

        // create model
        $model = $this->roomLocationRepository->create( $input );
        // create translation
        $model->translations()->create( $inputTranslation );

        Flash::success('Località salvato correttamente.');

        return redirect(route('admin.roomLocations.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request )
    {
        $model =  $request->only([
            // vacio
        ]);

        $model['status_id'] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'name' => 'required',
            // 'description' => 'required',
        ]);

        $translation = $request->only([
            'name',
            // 'description',
            'language_id',
        ]);

        return $translation;
    }

    /**
     * Display the specified RoomLocation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show( $id )
    {
        $roomLocation = $this->roomLocationRepository->findWithoutFail($id);

        if (empty($roomLocation)) {
            Flash::error('Località non trovata');

            return redirect(route('admin.roomLocations.index'));
        }

        return view('admin.rooms.room_locations.show')->with('roomLocation', $roomLocation);
    }

    /**
     * Show the form for editing the specified RoomLocation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $id  )
    {
        $roomLocation = $this->roomLocationRepository->findWithoutFail( $id );

        if ( empty( $roomLocation ) ) {
            Flash::error( 'Località non trovata' );

            return redirect( route( 'admin.roomLocations.index' ) );
        }

        // Logica translation
        $translation = $roomLocation->translation( 'it' );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view( 'admin.rooms.room_locations.edit' )
            ->with( 'translation', $translation )
            ->with( 'roomLocation', $roomLocation );
    }

    /**
     * Update the specified RoomLocation in storage.
     *
     * @param  int              $id
     * @param UpdateRoomLocationRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateRoomLocationRequest $request )
    {
        $roomLocation = $this->roomLocationRepository->findWithoutFail( $id );

        if (empty($roomLocation)) {
            Flash::error('Località non trovata');

            return redirect(route('admin.roomLocations.index'));
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->roomLocationRepository->update( $input, $id );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success( 'Si ha modificato Località correttamente.' );

        return redirect( route( 'admin.roomLocations.index' ) );
    }

    /**
     * Remove the specified RoomLocation from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $roomLocation = $this->roomLocationRepository->findWithoutFail($id);

        if (empty($roomLocation)) {
            Flash::error('Località non trovata');

            return redirect(route('admin.roomLocations.index'));
        }

        // validation if there are child relations
        if( $roomLocation->roomCategories->isEmpty() === false ) {
            Flash::error( 'La località non può essere cancellato, ha villas associati.' );

            return redirect( route( 'admin.roomLocations.index' ) );
        }

        // delete translations
        $translations = $roomLocation->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->roomLocationRepository->delete( $id );

        Flash::success('Si ha cancellato Località correttamente.');

        return redirect(route('admin.roomLocations.index'));
    }
}
