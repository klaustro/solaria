<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateGallerySliderRequest;
use App\Http\Requests\Admin\UpdateGallerySliderRequest;
use App\Repositories\Admin\GallerySliderRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GallerySliderController extends AppBaseController
{
    /** @var  GallerySliderRepository */
    private $gallerySliderRepository;

    public function __construct( GallerySliderRepository $gallerySliderRepo )
    {
        $this->gallerySliderRepository = $gallerySliderRepo;
    }

    /**
     * Display a listing of the GallerySlider.
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->gallerySliderRepository->pushCriteria( new RequestCriteria( $request ) );
        $gallerySliders = $this->gallerySliderRepository->orderBy( 'id', 'desc' )->all();

        return view( 'admin.gallery_sliders.index' )
            ->with( 'languages', LanguageController::getLanguageAll() )
            ->with( 'gallerySliders', $gallerySliders );
    }

    /**
     * Show the form for creating a new GallerySlider.
     *
     * @return Response
     */
    public function create()
    {
        $translation   = LanguageController::getModelLanguage( \App::getLocale() );

        return view( 'admin.gallery_sliders.create', compact( 'translation' ) );
    }

    /**
     * Show the form for editing the specified GallerySlider.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $lang, $id )
    {
        $model = $this->gallerySliderRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Gallery Slider not found' );

            return redirect( route( 'admin.gallerySliders.index' ) );
        }

        // translation
        $translation = $model->translation( $lang );
        if ( !$translation ) {
            // $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view( 'admin.gallery_sliders.edit' )
            ->with( 'translation', $translation )
            ->with( 'gallerySlider', $model->toArray() );
    }

    /**
     * Remove the specified GallerySlider from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id )
    {
        $model = $this->gallerySliderRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            Flash::error( 'Gallery Slider not found' );

            return redirect( route( 'admin.gallerySliders.index' ) );
        }

        // delete translations
        $translations = $model->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        // delete model
        $this->gallerySliderRepository->delete( $id );

        Flash::success( 'Gallery Slider deleted successfully.' );

        return redirect( route( 'admin.gallerySliders.index' ) );
    }
}
