<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateRoomCategoryServiceRequest;
use App\Http\Requests\Admin\UpdateRoomCategoryServiceRequest;
use App\Models\Admin\Service;
use App\Repositories\Admin\RoomCategoryRepository;
use App\Repositories\Admin\RoomCategoryServiceRepository;
use App\Repositories\Admin\ServiceRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomCategoryServiceController extends AppBaseController
{
    /** @var  RoomCategoryServiceRepository */
    private $roomCategoryServiceRepository;

    /** @var  RoomCategoryRepository */
    private $roomCategoryRepository;

    /** @var  ServiceRepository */
    private $serviceRepository;

    public function __construct(RoomCategoryServiceRepository $roomCategoryServiceRepo,
        RoomCategoryRepository $roomCategoryRepo,
        ServiceRepository $serviceRepo)
    {
        $this->roomCategoryServiceRepository = $roomCategoryServiceRepo;
        $this->roomCategoryRepository        = $roomCategoryRepo;
        $this->serviceRepository             = $serviceRepo;
    }

    /**
     * Display a listing of the RoomCategoryService.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomCategoryServiceRepository->pushCriteria(new RequestCriteria($request));
        $roomCategoryServices = $this->roomCategoryServiceRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.room_category_services.index')
            ->with('roomCategoryServices', $roomCategoryServices);
    }

    /**
     * Show the form for creating a new RoomCategoryService.
     *
     * @return Response
     */
    public function create()
    {
        $roomCategories = $this->roomCategoryRepository->all()->pluck( 'name', 'id' );
        // $services      = $this->serviceRepository->all()->pluck( 'name', 'id' );
        // $services      = $this->serviceRepository->all();
        $services = Service::all()->sortBy('ico');

        return view('admin.room_category_services.create')
            ->with('roomCategories', $roomCategories)
            ->with('services', $services);
    }

    /**
     * Store a newly created RoomCategoryService in storage.
     *
     * @param CreateRoomCategoryServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomCategoryServiceRequest $request)
    {
        $input = $request->all();

        $roomCategoryService = $this->roomCategoryServiceRepository->create($input);

        Flash::success('Servizi Casa Vacanza salvato correttamente.');

        return redirect(route('admin.roomCategoryServices.index'));
    }

    /**
     * Display the specified RoomCategoryService.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roomCategoryService = $this->roomCategoryServiceRepository->findWithoutFail($id);

        if (empty($roomCategoryService)) {
            Flash::error('Servizi Casa Vacanza non trovato');

            return redirect(route('admin.roomCategoryServices.index'));
        }

        return view('admin.room_category_services.show')->with('roomCategoryService', $roomCategoryService);
    }

    /**
     * Show the form for editing the specified RoomCategoryService.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roomCategoryService = $this->roomCategoryServiceRepository->findWithoutFail($id);
        if (empty($roomCategoryService)) {
            Flash::error('Servizi Casa Vacanza non trovato');

            return redirect(route('admin.roomCategoryServices.index'));
        }

        $roomCategories = $this->roomCategoryRepository->all()->pluck( 'name', 'id' );
        // $services      = $this->serviceRepository->all()->pluck( 'name', 'id' );
        $services = Service::all()->sortBy('ico');

        return view('admin.room_category_services.edit')
            ->with('roomCategoryService', $roomCategoryService)
            ->with('roomCategories', $roomCategories)
            ->with('services', $services);
    }

    /**
     * Update the specified RoomCategoryService in storage.
     *
     * @param  int              $id
     * @param UpdateRoomCategoryServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomCategoryServiceRequest $request)
    {

        $roomCategoryService = $this->roomCategoryServiceRepository->findWithoutFail($id);
        if (empty($roomCategoryService)) {
            Flash::error('Servizi Casa Vacanza non trovato');

            return redirect(route('admin.roomCategoryServices.index'));
        }

        $roomCategoryService = $this->roomCategoryServiceRepository->update($request->all(), $id);

        Flash::success('Si ha modificato Servizi Casa Vacanza correttamente.');

        return redirect(route('admin.roomCategoryServices.index'));
    }

    /**
     * Remove the specified RoomCategoryService from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roomCategoryService = $this->roomCategoryServiceRepository->findWithoutFail($id);

        if (empty($roomCategoryService)) {
            Flash::error('Servizi Casa Vacanza non trovato');

            return redirect(route('admin.roomCategoryServices.index'));
        }

        $this->roomCategoryServiceRepository->delete($id);

        Flash::success('Si ha cancellato Servizi Casa Vacanza correttamente.');

        return redirect(route('admin.roomCategoryServices.index'));
    }
}
