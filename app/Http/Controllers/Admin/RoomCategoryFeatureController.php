<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateRoomCategoryFeatureRequest;
use App\Http\Requests\Admin\UpdateRoomCategoryFeatureRequest;
use App\Repositories\Admin\FeatureRepository;
use App\Repositories\Admin\RoomCategoryFeatureRepository;
use App\Repositories\Admin\RoomCategoryRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RoomCategoryFeatureController extends AppBaseController
{
    /** @var  RoomCategoryFeatureRepository */
    private $roomCategoryFeatureRepository;
    private $roomCategoryRepository;
    private $featureRepository;

    public function __construct(RoomCategoryFeatureRepository $roomCategoryFeatureRepo, RoomCategoryRepository $roomCategoryRepository, FeatureRepository $featureRepository)
    {
        $this->roomCategoryFeatureRepository = $roomCategoryFeatureRepo;
        $this->roomCategoryRepository = $roomCategoryRepository;
        $this->featureRepository = $featureRepository;
    }

    /**
     * Display a listing of the RoomCategoryFeature.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomCategoryFeatureRepository->pushCriteria(new RequestCriteria($request));
        $roomCategoryFeatures = $this->roomCategoryFeatureRepository->all();

        return view('admin.features.room_category_features.index')
            ->with('roomCategoryFeatures', $roomCategoryFeatures);
    }

    /**
     * Show the form for creating a new RoomCategoryFeature.
     *
     * @return Response
     */
    public function create()
    {
        $room_categories = $this->roomCategoryRepository->all()->pluck( 'name', 'id' );
        $features = $this->featureRepository->all()->pluck( 'name', 'id' );
        return view('admin.features.room_category_features.create')
            ->with('room_categories', $room_categories)
            ->with('features', $features);
    }

    /**
     * Store a newly created RoomCategoryFeature in storage.
     *
     * @param CreateRoomCategoryFeatureRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomCategoryFeatureRequest $request)
    {
        $input = $request->all();

        $roomCategoryFeature = $this->roomCategoryFeatureRepository->create($input);

        Flash::success('Room Category Feature saved successfully.');

        return redirect(route('admin.roomCategoryFeatures.index'));
    }

    /**
     * Display the specified RoomCategoryFeature.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $roomCategoryFeature = $this->roomCategoryFeatureRepository->findWithoutFail($id);

        if (empty($roomCategoryFeature)) {
            Flash::error('Room Category Feature non trovato');

            return redirect(route('admin.roomCategoryFeatures.index'));
        }

        return view('admin.features.room_category_features.show')->with('roomCategoryFeature', $roomCategoryFeature);
    }

    /**
     * Show the form for editing the specified RoomCategoryFeature.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $roomCategoryFeature = $this->roomCategoryFeatureRepository->findWithoutFail($id);

        if (empty($roomCategoryFeature)) {
            Flash::error('Room Category Feature non trovato');

            return redirect(route('admin.roomCategoryFeatures.index'));
        }

        $room_categories = $this->roomCategoryRepository->all()->pluck( 'name', 'id' );
        $features = $this->featureRepository->all()->pluck( 'name', 'id' );

        return view('admin.features.room_category_features.edit')
            ->with('roomCategoryFeature', $roomCategoryFeature)
            ->with('room_categories', $room_categories)
            ->with('features', $features);
    }

    /**
     * Update the specified RoomCategoryFeature in storage.
     *
     * @param  int              $id
     * @param UpdateRoomCategoryFeatureRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomCategoryFeatureRequest $request)
    {
        $roomCategoryFeature = $this->roomCategoryFeatureRepository->findWithoutFail($id);

        if (empty($roomCategoryFeature)) {
            Flash::error('Room Category Feature non trovato');

            return redirect(route('admin.roomCategoryFeatures.index'));
        }

        $roomCategoryFeature = $this->roomCategoryFeatureRepository->update($request->all(), $id);

        Flash::success('Room Category Feature updated successfully.');

        return redirect(route('admin.roomCategoryFeatures.index'));
    }

    /**
     * Remove the specified RoomCategoryFeature from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $roomCategoryFeature = $this->roomCategoryFeatureRepository->findWithoutFail($id);

        if (empty($roomCategoryFeature)) {
            Flash::error('Room Category Feature non trovato');

            return redirect(route('admin.roomCategoryFeatures.index'));
        }
        // dd($roomCategoryFeature->feature);
        // validation if there are child relations
        if( isset($roomCategoryFeature->feature) === false ) {
            Flash::error( 'La casa vacanza non può essere cancellato, ha caratteristiche associate.' );

            return redirect( route( 'admin.roomCategories.index' ) );
        }

        $this->roomCategoryFeatureRepository->delete($id);

        Flash::success('Room Category Feature deleted successfully.');

        return redirect(route('admin.roomCategoryFeatures.index'));
    }
}
