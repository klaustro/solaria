<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateServiceRequest;
use App\Http\Requests\Admin\UpdateServiceRequest;
use App\Repositories\Admin\ServiceCategoryRepository;
use App\Repositories\Admin\ServiceRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Validator;

class ServiceController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $serviceRepository;
    private $serviceCategoryRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct(ServiceRepository $serviceRepo, ServiceCategoryRepository $serviceCategoryRepository)
    {
        $this->serviceRepository = $serviceRepo;
        $this->serviceCategoryRepository = $serviceCategoryRepository;
    }

    /**
     * Display a listing of the Service.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->serviceRepository->pushCriteria(new RequestCriteria($request));
        $services = $this->serviceRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.services.services.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('services', $services);
    }

    /**
     * Show the form for creating a new Service.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );
        $serviceIcon = $this->serviceRepository->pluck( 'ico' );

        $service_categories = $this->serviceCategoryRepository->all()->pluck( 'name', 'id' );

        return view('admin.services.services.create')
            ->with('service_categories', $service_categories)
            ->with('serviceIcon',$serviceIcon)
            ->with( 'translation', $translation );
    }

    /**
     * Store a newly created Service in storage.
     *
     * @param CreateServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceRequest $request)
    {
        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );
        $inputImages        = $this->inputImages( $request );

        // create model
        $model = $this->serviceRepository->create($input);

        // create translation
        $model->translations()->create( $inputTranslation );

        //Save Image
        $this->saveImages($model, $request);

        Flash::success('Servizi salvato correttamente.');

        return redirect(route('admin.services.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    private function input( $request )
    {

        $model =  $request->only([
            'service_category_id',
        ]);

        if ($request->hasFile('ico')) {
            $model['ico'] = '/' . saveFile($request->file('ico'), 'public/multimedia/services', 'service');
        }

        $model['status_id'] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    private function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'name' => 'required',
        ]);

        $translation = $request->only([
            'name',
            // 'description',
            'language_id',
        ]);

        return $translation;
    }

    /**
     * Obtener las imagenes
     *
     * @return array
     */
    private function inputImages( $request )
    {
        // para validar las imagenes
        $aux = 1;
        if ( $request->file[ 0 ]->extension() === null ) {
            $aux = 0;
        }
        $request->request->add( [ 'immagine' => $aux ] );
        $request->validate( [
            'immagine' => [ 'integer', 'min:1' ],
        ] );

        $images = $request->only( [
            'file',
        ] );

        return $images;
    }

    /**
     * Show the form for editing the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $lang, $id )
    {
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            Flash::error('Servizi non trovato');

            return redirect(route('admin.services.index'));
        }

        // Logica translation
        $translation = $service->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        $service_categories = $this->serviceCategoryRepository->all()->pluck( 'name', 'id' );
        return view('admin.services.services.edit')
            ->with( 'translation', $translation )
            ->with('service_categories', $service_categories)
            ->with('service', $service);
    }

    /**
     * Update the specified Service in storage.
     *
     * @param  int              $id
     * @param UpdateServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceRequest $request)
    {
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            Flash::error('Servizi non trovato');

            return redirect(route('admin.services.index'));
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );
        // $inputImages        = $this->inputImages( $request );

        // update model
        $model = $this->serviceRepository->update($input, $id);

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        //Save Image
        $this->saveImages($model, $request);

        Flash::success('Si ha modificato Servizi correttamente.');

        return redirect(route('admin.services.index'));
    }

    /**
     * Remove the specified Service from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            Flash::error('Servizi non trovato');

            return redirect(route('admin.services.index'));
        }

        // validation if there are child relations
        if( $service->roomsServices->isEmpty() === false ) {
            Flash::error( 'Questo servizi non può essere cancellato, ha i servizi in camera associati.' );

            return redirect( route( 'admin.services.index' ) );
        }

        // delete translations
        $translations = $service->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        $this->serviceRepository->delete($id);

        Flash::success('Si ha cancellato Servizi correttamente.');

        return redirect(route('admin.services.index'));
    }

    private function saveImages( $model, $request )
    {
        if ( $request->hasFile('file') ) {
            $files = $request->file('file');
            if ($files[0]->extension() != '') {
                $fileName = saveFile($files[0], 'public/multimedia', 'blog-' . uniqid() );
                $model->ico = "/$fileName";
                $model->save();
            }
        }
    }
}
