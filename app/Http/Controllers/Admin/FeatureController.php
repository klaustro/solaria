<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateFeatureRequest;
use App\Http\Requests\Admin\UpdateFeatureRequest;
use App\Repositories\Admin\FeatureRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class FeatureController extends AppBaseController
{
    /** @var  FeatureRepository */
    private $featureRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct(FeatureRepository $featureRepo)
    {
        $this->featureRepository = $featureRepo;
    }

    /**
     * Display a listing of the Feature.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->featureRepository->pushCriteria(new RequestCriteria($request));
        $features = $this->featureRepository->all();

        return view('admin.features.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('features', $features);
    }

    /**
     * Show the form for creating a new Feature.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );
        return view('admin.features.create')
            ->with('translation', $translation);
    }

    /**
     * Store a newly created Feature in storage.
     *
     * @param CreateFeatureRequest $request
     *
     * @return Response
     */
    public function store(CreateFeatureRequest $request)
    {
        // inputs
        $input               = $this->input( $request );

        $inputTranslation    = $this->inputTranslation( $request );

        // create Model
        $model = $this->featureRepository->create($input);

        // create translation
        $model->featureTranslations()->create( $inputTranslation );

        Flash::success('Caratteristica salvato correttamente.');

        return redirect(route('admin.features.index'));
    }

    /**
     * Display the specified Feature.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $feature = $this->featureRepository->findWithoutFail($id);

        if (empty($feature)) {
            Flash::error('Caratteristica non trovata');

            return redirect(route('admin.features.index'));
        }

        return view('admin.features.show')->with('feature', $feature);
    }

        /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request )
    {
        $model = [];

        if ($request->hasFile('iconblack')) {
            $model['iconblack'] = saveFile($request->file('iconblack'), 'public/multimedia/icons', 'feature');
            $model['iconblack'] = '/' . $model['iconblack'];
        }
        if ($request->hasFile('iconwhite')) {
            $model['iconwhite'] = saveFile($request->file('iconwhite'), 'public/multimedia/icons', 'feature');
            $model['iconwhite'] = '/' . $model['iconwhite'];
        }
        if ($request->hasFile('icongreen')) {
            $model['icongreen'] = saveFile($request->file('icongreen'), 'public/multimedia/icons', 'feature');
            $model['icongreen'] = '/' . $model['icongreen'];
        }
        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'name' => 'required',
        ]);

        $translation = $request->only([
            'name',
            'language_id'
        ]);

        return $translation;
    }

    /**
     * Show the form for editing the specified Feature.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($lang, $id)
    {
        $feature = $this->featureRepository->findWithoutFail($id);

        if (empty($feature)) {
            Flash::error('Caratteristica non trovata');

            return redirect(route('admin.features.index'));
        }

        // Logica translation
        $translation = $feature->featureTranslation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view('admin.features.edit')
        ->with( 'translation', $translation )
        ->with('feature', $feature);
    }

    /**
     * Update the specified Feature in storage.
     *
     * @param  int              $id
     * @param UpdateFeatureRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFeatureRequest $request)
    {
        $feature = $this->featureRepository->findWithoutFail($id);

        if (empty($feature)) {
            Flash::error('Caratteristica non trovata');

            return redirect(route('admin.features.index'));
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );
        // update model
        $model = $this->featureRepository->update( $input, $id );

        $this->updateOrCreateTranslation( $model, $inputTranslation );

        Flash::success('Caratteristica modificato correttamente.');

        return redirect(route('admin.features.index'));
    }

    /**
     * Remove the specified Feature from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $feature = $this->featureRepository->findWithoutFail($id);

        if (empty($feature)) {
            Flash::error('Caratteristica non trovata');

            return redirect(route('admin.features.index'));
        }

        if ($feature->roomCategoriesFeatures->isEmpty() === false) {

            Flash::error('Questa funzione non può essere annullata, ha le caratteristiche della Casa associata.');

            return redirect(route('admin.features.index'));
        }

        $this->featureRepository->delete($id);

        Flash::success('Caratteristica cancellato correttamente.');

        return redirect(route('admin.features.index'));
    }
}
