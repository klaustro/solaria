<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\API\LanguageController;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\Admin\CreateActivityCategoryRequest;
use App\Http\Requests\Admin\UpdateActivityCategoryRequest;
use App\Repositories\Admin\ActivityCategoryRepository;
use Flash;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ActivityCategoryController extends AppBaseController
{
    /** @var  ActivityCategoryRepository */
    private $activityCategoryRepository;

    /** @var  integer */
    private $defaultLanguage = 1;

    public function __construct(ActivityCategoryRepository $activityCategoryRepo)
    {
        $this->activityCategoryRepository = $activityCategoryRepo;
    }

    /**
     * Display a listing of the ActivityCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->activityCategoryRepository->pushCriteria(new RequestCriteria($request));
        $activityCategories = $this->activityCategoryRepository->orderBy( 'id', 'desc' )->all();

        return view('admin.activities.activity_categories.index')
            ->with('languages', LanguageController::getLanguageAll())
            ->with('activityCategories', $activityCategories);
    }

    /**
     * Show the form for creating a new ActivityCategory.
     *
     * @return Response
     */
    public function create()
    {
        $translation = LanguageController::getModelLanguage( $this->defaultLanguage );

        return view('admin.activities.activity_categories.create')
                ->with( 'translation', $translation );
    }

    /**
     * Store a newly created ActivityCategory in storage.
     *
     * @param CreateActivityCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateActivityCategoryRequest $request)
    {
        // inputs
        $input               = $this->input( $request );
        $inputTranslation    = $this->inputTranslation( $request );

        // create model
        $model = $this->activityCategoryRepository->create($input);

        // create translation
        $model->translations()->create( $inputTranslation );

        //Save Image
        $this->inputImages($model, $request);

        Flash::success('Categoria di esperienza salvato correttamente.');

        return redirect(route('admin.activityCategories.index'));
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    private function input( $request )
    {
        $model =  $request->only([
            //
        ]);

        if ($request->hasFile('image')) {
            $model['image'] = saveFile($request->file('image'), 'public/multimedia/activities', 'category');
        }

        $model['status_id'] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    private function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'name' => 'required'
            // 'description' => 'required',
        ]);
        $translation = $request->only([
            'name',
            'description',
            'language_id',
        ]);

        return $translation;
    }

    /**
     * Show the form for editing the specified ActivityCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($lang, $id)
    {
        $activityCategory = $this->activityCategoryRepository->findWithoutFail($id);

        if ( empty( $activityCategory ) ) {
            Flash::error( 'Categoria di esperienza non trovata' );

            return redirect( route( 'admin.activityCategories.index' ) );
        }

        // Logica translation
        $translation = $activityCategory->translation( $lang );
        if ( !$translation ) {
            $translation = LanguageController::getModelLanguage( $lang );
        } else {
            $translation = $translation->language;
        }

        return view( 'admin.activities.activity_categories.edit' )
            ->with( 'translation', $translation )
            ->with( 'activityCategory', $activityCategory );
    }

    /**
     * Update the specified ActivityCategory in storage.
     *
     * @param  int              $id
     * @param UpdateActivityCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivityCategoryRequest $request)
    {
        $activityCategory = $this->activityCategoryRepository->findWithoutFail($id);

        if (empty($activityCategory)) {
            Flash::error('Categoria di esperienza non trovata');

            return redirect(route('admin.activityCategories.index'));
        }
        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->activityCategoryRepository->update($input, $id);

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        //Save Image
        $this->inputImages($model, $request);

        Flash::success('Si ha modificato Categoria di activityCategories correttamente.');

        return redirect(route('admin.activityCategories.index'));
    }

    /**
     * Remove the specified ActivityCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $activityCategory = $this->activityCategoryRepository->findWithoutFail($id);

        if (empty($activityCategory)) {
            Flash::error('Categoria di esperienza non trovata');

            return redirect(route('admin.activityCategories.index'));
        }

        // validation if there are child relations
        if( $activityCategory->activities->isEmpty() === false ) {
            Flash::error( 'la esperienza categorie non può essere cancellato, ha esperienze associati.' );

            return redirect( route( 'admin.activityCategories.index' ) );
        }

        // delete translations
        $translations = $activityCategory->translations;
        foreach ( $translations as $translation ) {
            $translation->delete();
        }

        $this->activityCategoryRepository->delete($id);

        Flash::success('Si ha cancellato la categoria di esperienza è stata correttamente.');

        return redirect(route('admin.activityCategories.index'));
    }

    private function inputImages( $model, $request )
    {
        if ($request->hasFile('file')) {
            $files = $request->file('file');
            if ($files[0]->extension() != '') {
                $fileName = saveFile($files[0], 'public/multimedia', 'activity_category-' . uniqid() );
                $model->image = "/$fileName";
                $model->save();
            }
        }
    }
}
