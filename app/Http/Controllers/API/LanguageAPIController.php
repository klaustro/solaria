<?php

namespace App\Http\Controllers\API;

use App;
use App\Http\Controllers\AppBaseController;
use App\Models\Admin\FrontSection;
use App\Models\Admin\Language;
use App\Models\Admin\Screen;
use App\Models\Admin\ScreenFrontSection;
use App\Models\Admin\TagTranslation;
use Illuminate\Http\Request;

class LanguageAPIController extends AppBaseController
{
    /**
    * Realizar un recorrido para insertar los tag de idiomas
    *
    * @param Request $request
    */
    public function insertTagsIdiomas(Request $request)
    {
        foreach ($request->componente as $keyI => $valueI) {
            foreach ($valueI as $keyJ) {
                // Verifica si la pantalla esta registrada.
                $existScreen = $this->verifyModel(new Screen, ['name' => $keyJ['screen']['name']]);

                // Verifica si el front section esta registrada.
                $existFrontSection = $this->verifyModel(new FrontSection, ['name' => $keyJ['front_sections']['name']]);

                // Verifica si el front section esta registrada.
                $existScreenFrontSection = $this->verifyModel(new ScreenFrontSection, ['front_section_id' => $existFrontSection->id, 'screen_id' => $existScreen->id]);
                // dd($existScreenFrontSection);
                foreach ($keyJ['tag_translations'] as $keyK) {
                    foreach ($keyK as $keyL) {
                        foreach ($keyL[0]['tag'] as $keyM) {
                            // Verifica si la etiqueta esta almacenada para con el idioma $language_id.
                            $existTag = $this->verifyTag(new TagTranslation, [
                                            'tag' => $keyM['tag'],
                                            'value' => $keyM['value'],
                                            'language_id' => $keyL[0]['language_id'],
                                            'front_section_id' => $existFrontSection->id
                                        ]);
                        }
                    }
                }
            }
        }

        return response()->json('succes insertados tag de idioma');
    }

    /**
    * Verifica si el registro esta almacenado.
    *
    * @param Model
    * @param Array
    * @return Object
    */
    public function verifyModel($model, $array)
    {
        $exist = $model->where(key($array), $array[key($array)])->first();

        if(!$exist)
            return $this->createNewModel($model, $array);

        return $exist;
    }

    /**
    * Verifica si el tag esta almacenado.
    *
    * @param Model
    * @param Array
    * @return Object
    */
    public function verifyTag($model, $array)
    {
        $exist = $model
            ->where('tag', $array['tag'])
            ->where('language_id', $array['language_id'])
            ->first();

        if(!$exist)
            return $this->createNewModel($model, $array);

        return $exist;
    }

    /**
    * Almacenar un registro.
    *
    * @param Model
    * @param Array
    * @return Object
    */
    public function createNewModel($model, $array)
    {
        return $model->create($array);
    }

    /* Obtener todos los lenguajes */
    public static function getLanguageAll(){
        return Language::all();
    }

    /* Obtener todos los lenguajes */
    public static function getLanguageStatuses(){
        return Status::all();
    }

    public static function setLanguage(Request $request){
        //dd($request);
        try {
            $lang=$request->lang;
            App::setLocale($lang);
            $data=$this->getResource($lang);
            return response()->json( [ 'oper' => true , 'lang' => $lang, 'tags'=>$data], 200);
        } catch (Exception $e) {

        }
    }

    public static function getLanguage(Request $request){
        try {
            $lang=$request->lang;
            App::setLocale($lang);
            $data=$this-    getResource($lang);
            return response()->json( [ 'oper' => true , 'lang' => $lang, 'tags'=>$data], 200);
        } catch (Exception $e) {

        }
    }

    public static function getResource($lang){
        $arr_archivo=array();
        $arr_archivo["validate"]=__("validate_js");
        //consulta de etiquetas por bd
        if($lang=='it'){
            $arr_archivo["tags"]=[
                0=>array(
                  'tag'=>'menu_1',
                  'value'=>'Menu Prueba IT',
                )
            ];
        }else{
            $arr_archivo["tags"]=[
                0=>array(
                  'tag'=>'menu_1',
                  'value'=>'Menu Prueba EN',
                )
            ];
        }

        return $arr_archivo;
    }
}

// namespace App\Http\Controllers;

// use App;
// use App\Models\Admin\Language;
// use App\Models\Admin\Status;
// use Illuminate\Http\Request;

// class LanguageController extends Controller
// {
//     /* Obtener todos los lenguajes */
//     public static function getLanguageAll(){
//         return Language::all();
//     }

//     /* Obtener todos los lenguajes */
//     public static function getLanguageStatuses(){
//         return Status::all();
//     }

//     function setLanguage(Request $request){
//         try {
//             $lang=$request->lang;
//             App::setLocale($lang);
//             $data=$this->getResource($lang);
//             return response()->json( [ 'oper' => true , 'lang' => $lang, 'tags'=>$data], 200);
//         } catch (Exception $e) {

//         }
//     }

//     function getLanguage(Request $request){
//         try {
//             $lang=$request->lang;
//             App::setLocale($lang);
//             $data=$this->getResource($lang);
//             return response()->json( [ 'oper' => true , 'lang' => $lang, 'tags'=>$data], 200);
//         } catch (Exception $e) {

//         }
//     }

//     function getResource($lang){
//         $arr_archivo=array();
//         $arr_archivo["validate"]=__("validate_js");
//         //consulta de etiquetas por bd
//         if($lang=='it'){
//             $arr_archivo["tags"]=[
//                0=>array(
//                  'tag'=>'menu_1',
//                  'value'=>'Menu Prueba IT',
//                )
//             ];
//         }else{
//             $arr_archivo["tags"]=[
//               0=>array(
//                  'tag'=>'menu_1',
//                  'value'=>'Menu Prueba EN',
//                )
//             ];
//         }

//         return $arr_archivo;
//     }
// }
