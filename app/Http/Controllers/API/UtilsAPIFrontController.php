<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\MailController;
use App\Http\Requests\API\Admin\CreateNewsletterUserAPIRequest;
use App\Models\Admin\Multimedia;
use App\Models\Admin\Row;
use App\Repositories\Admin\ModuleRepository;
use App\Repositories\Admin\NewsletterUserRepository;
use Illuminate\Http\Request;

class UtilsAPIFrontController extends AppBaseController
{
    /** @var  NewsletterUserRepository */
    private $newsletterUserRepository;

    /** @var  ModuleRepository */
    private $moduleRepository;

    public function __construct(NewsletterUserRepository $newsletterUserRepo, ModuleRepository $moduleRepo)
    {
        $this->newsletterUserRepository = $newsletterUserRepo;
        $this->moduleRepository = $moduleRepo;
    }

    /**
     * Send the email to contact us.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function contactUs(Request $request)
    {
         // Si request en su collections no trae la clave subject entonces se la asignamos
        if (!$request->has(['subject'])) {
            $request->merge(['subject' => 'messaggio di contatto']);
        }
        try{
            $request['textmail'] = 'Titulo';
            $request['textmail_sub'] = 'Sub titulo';
            $flag="ERROR NO ENVIO";

            $send = MailController::sendMail($request->all(),'contacto', 'contacto');

            if($send=='OK'){
                $flag="FINO";
            }
            else{
                $flag=var_dump($send);
            }

            return response()->json(["success"=>true,"message"=>$flag],200);
        }catch(Exeption $e){
            return $e->message();
        }
    }

    /**
     * Send the email to newsletter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function newsLetter(CreateNewsletterUserAPIRequest $request)
    {
        // Si request en su collections no trae la clave subject entonces se la asignamos
        if (!$request->has(['subject'])) {
            $request->merge(['subject' => 'Iscritto alla nostra newsletter']);
        }
        try{
            $exist = $this->newsletterUserRepository->findByField('email',$request->email)->first();
            if(!$exist){
                $newsletterUsers = $this->newsletterUserRepository->create($request->all());
                $send=MailController::sendMail($request->all(),'newsletter', 'newsletter');
                if($send=='OK'){
                    return $this->sendResponse($newsletterUsers->toArray(), 'Newsletter Utente salvato correttamente');
                }
            }

            return response()->json(["success"=>true,"message"=>"La tua email è già registrata"],422);
        }catch(Exeption $e){
            return response()->json(["success"=>true,"message"=>$e->message()],500);
            return $e->message();
        }
    }

    /**
     * Function unsubscribe newslatter.
     *
     * @param  $email
     * @return \Illuminate\Http\Response
     */
    public function unsubscribeNewsletter($email)
    {
        try{
            $newsletter = $this->newsletterUserRepository->findByField('email',$email)->first();
            // return response()->json($newsletter);
            if(empty($newsletter)){
                return response()->json(["success"=>true,"message"=>"El email no existe..!"],301);
            }
            $newsletter->forceDelete();
            return response()->json(["success"=>true,"message"=>$newsletter],200);
        }catch(Exeption $e){
            return response()->json(["success"=>true,"message"=>$e->message()],500);
        }
    }

    /**
     * Send the email to contact us.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function experienceForm(Request $request)
    {
        // dd( $request->all() );
        try{
            $flag="ERROR NO ENVIO";

            $send = MailController::sendMail($request->all(),'experience', 'experience');

            if($send=='OK'){
                $flag="FINO";
            }
            else{
                $flag=var_dump($send);
            }

            return response()->json(["success"=>true,"message"=>$flag],200);
        }catch(Exeption $e){
            return $e->message();
        }
    }

    /**
     * Guardar las Imagenes para el Front-End
     */
    public function storeMultimediaFront(Request $request)
    {
        $module = $this->moduleRepository->findByField('name',$request->name)->first();
        if(!$module){
            $moduleRepository = $this->moduleRepository->create($request->all());
            return $this->sendResponse($moduleRepository->toArray(), 'Module save success');
        }

        $this->saveMultimedia( $module, $request );

        return $this->sendResponse($module->toArray(), 'Module success');
    }

    public function saveMultimedia($module, $request)
    {
        $this->validate($request,[
            'file' => 'image|mimes:jpg,png,gif,jpeg,bmp,webp,vnd.microsoft.icon'
        ]);

        $file = $request->file("file");


        $name = $file->getClientOriginalName();
        $name = str_replace(" ", "_", $name);
        \Storage::disk('multimedia')->put($name, \File::get($file));

        $size = getimagesize($file);
        $request['name']     = $name;
        $request['width']    = $size[0];
        $request['height']   = $size[1];
        $request['size']     = $size[2];
        $request['path']     = 'storage/multimedia';

        // save
        $multimedia = new Multimedia( $request->all() );
        $multimedia->save();

        $row = $module->row()->save( new Row() );

        $multimedia->rowsMultimedia()->create(['row_id' => $row->id]);

        return $multimedia;
    }

    /**
     * Obtener las Imagenes para el Front-End
     */
    public function getMultimediaFront(Request $request)
    {

    }
}
