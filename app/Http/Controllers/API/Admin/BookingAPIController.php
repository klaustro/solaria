<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Controllers\MailController;
use App\Http\Requests\API\Admin\CreateBookingAPIRequest;
use App\Http\Requests\API\Admin\UpdateBookingAPIRequest;
use App\Models\Admin\Booking;
use App\Repositories\Admin\BookingDetailRepository;
use App\Repositories\Admin\BookingRepository;
use App\Repositories\Admin\DataFormRepository;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BookingController
 * @package App\Http\Controllers\API\Admin
 */

class BookingAPIController extends AppBaseController
{
    /** @var  BookingRepository */
    private $bookingRepository;

    /** @var  BookingDetailRepository */
    private $bookingDetailRepository;

    /** @var  DataFormRepository */
    private $dataFormRepository;

    /** @var  UserRepository */
    private $userRepository;

    /** @var  RoomRepository */
    private $roomRepository;

    public function __construct( BookingRepository $bookingRepo,
        BookingDetailRepository $bookingDetailRepo,
        DataFormRepository $dataFormRepo,
        UserRepository $userRepo,
        RoomRepository $roomRepo )
    {
        $this->bookingRepository = $bookingRepo;
        $this->bookingDetailRepository = $bookingDetailRepo;
        $this->dataFormRepository = $dataFormRepo;
        $this->userRepository = $userRepo;
        $this->roomRepository = $roomRepo;
    }

    /**
     * Display a listing of the Booking.
     * GET|HEAD /bookings
     *
     * @param Request $request
     * @param integer $user_id
     * @return Response
     */
    public function index( Request $request, $user_id )
    {
        $this->bookingRepository->pushCriteria( new RequestCriteria( $request ) );
        $this->bookingRepository->pushCriteria( new LimitOffsetCriteria( $request ) );

        // OBTENEMOS LAS BOOKINGS QUE PERTENECEN AL USUARIO Y QUE TIENEN show_to_user=1
        $bookings = $this->bookingRepository->findWhere( [ 'user_id' => $user_id, 'show_to_user' => 1 ] );

        $bookingsWithRelations = collect();
        foreach ( $bookings as $booking ) {
            $bookingsWithRelations->push( $this->bookingRepository->with( 'status.translations' )->findCustomized( $booking->id ) );
            //\Log::info($bookings->load( 'status' ) );
        }


        return $this->sendResponse( [ 'bookings' => $bookingsWithRelations->toArray() ], 'Bookings retrieved successfully' );
    }

    /**
     * Store a newly created Booking in storage.
     * POST /bookings
     * @see http://localhost:8011/api/admin/bookings
     *
     * @param CreateBookingAPIRequest $request
     *
     * @return Response
     */
    public function store( CreateBookingAPIRequest $request )
    {
        // Si request en su collections no trae la clave subject entonces se la asignamos
        if ( !$request->has( [ 'subject' ] ) ) {
            $request->merge( [ 'subject' => 'Nuovo Ordine' ] );
        }

        $input = $request->all();

        $inputCable = array (
            'code' => NULL,
            'personResponsible' => array (
                'name' => 'Steven Sucre',
                'email' => 'steven.sucre@jumperr.com',
                'phone' => '90817230918723',
                'fiscalCode' => 'nd19uehnd192',
            ),
            'rooms' => array (
                0 => array (
                    'name' => 'Villetta Stella Maris Room 1',
                    'description' => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati per poter mangiare all\'aperto, Minipiscina Idromassaggio esterna, solarium. Dispone di WiFi, aria condizionata, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto. Consumi luce, acqua e gas inclusi.',
                    'roomId' => 1,
                    'room_category_id' => 1,
                    'personResponsible' => array (
                        'name' => 'John Doe',
                        'email' => 'ssucre23@gmail.com',
                        'phone' => '9238109238',
                        'fiscalCode' => NULL,
                    ),
                    'personsQuantity' => '2',
                    'totalItem' => 71,
                    'bookingDate' => array (
                        'checkin' => '2019-01-27',
                        'checkout' => '2019-02-10',
                        'roomLocationId' => 0,
                    ),
                ),
                1 => array (
                    'name' => 'Solaria 3',
                    'description' => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata,Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto. Consumi luce, acqua e gas inclusi.',
                    'roomId' => 3,
                    'room_category_id' => 2,
                    'personResponsible' => array (
                        'name' => 'Mary Doe',
                        'email' => 'steven.g.s.p@gmail.com',
                        'phone' => '92038109238',
                        'fiscalCode' => NULL,
                    ),
                    'personsQuantity' => '3',
                    'totalItem' => 47,
                    'bookingDate' => array (
                        'checkin' => '2019-01-27',
                        'checkout' => '2019-02-10',
                        'roomLocationId' => 0,
                    ),
                ),
            ),
            'totalAmount' => 118,
            'comment' => 'I want breakfast.',
            'created_at' => NULL,
        );

        // $data = $inputCable;
        $data = $input;

        // Save Booking
        $booking = $this->bookingRepository->createCustomized( $data );

        // Get Booking with BookingDetails
        $bookingWithRelations = $this->bookingRepository->findCustomized( $booking->id );

        // Enviar Correo
        $sended = $this->sendMail( $bookingWithRelations, 'order' );

        if ( @$sended == 'OK' ) {
            $message = 'Booking saved successfully, email sended';
        }
        else {
            $message = 'Booking saved successfully, email not sended';
        }

        return $this->sendResponse( $bookingWithRelations, $message );
    }

    /**
     * Send the mail of the invoice and the purchase BOOKING
     * @param array $bookingWithRelations
     * @param string $template
     * @return void
     */
    public function sendMail( $bookingWithRelations, $template )
    {
        $request = [
            'subject'   => tags( 'order_email_subject' ),
            'msg'       => json_encode( $bookingWithRelations ),
            'email'     => $bookingWithRelations[ 'personResponsible' ][ 'email' ],
            'data'      => (array)$bookingWithRelations
        ];

        return MailController::sendMail( $request, $template );
    }

    /**
     * Display the specified Booking.
     * GET|HEAD /bookings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show( $id )
    {
        $booking = $this->bookingRepository->findWithoutFail( $id );

        if ( empty( $booking ) ) {
            return $this->sendError( 'Booking not found' );
        }

        // $bookingWithRelations = $this->bookingRepository->getBookingWithRelations($booking);
        $bookingWithRelations = $this->bookingRepository->findCustomized( $booking->id );

        return $this->sendResponse( [ 'booking' => $bookingWithRelations ], 'Booking2 retrieved successfully' );
    }

    /**
     * Update the field 'show_to_user' to '0'.
     * PUT/PATCH /bookings/hide/{id}
     *
     * @param  int $id
     * @param Request $request
     *
     * @return Response
     */
    public function hide( $id, Request $request )
    {
        /** @var Booking $booking */
        $booking = $this->bookingRepository->findWithoutFail( $id );

        if ( empty( $booking ) ) {
            return $this->sendError( 'Booking not found' );
        }

        $booking = $this->bookingRepository->update( [ 'show_to_user' => 0 ], $id );

        return $this->sendResponse( $booking->id, 'Booking hidden successfully' );
    }

    /**
     * Update the specified Booking in storage.
     * PUT/PATCH /bookings/{id}
     *
     * @param  int $id
     * @param UpdateBookingAPIRequest $request
     *
     * @return Response
     */
    /*public function update($id, UpdateBookingAPIRequest $request)
    {
        $input = $request->all();

        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking) ) {
            return $this->sendError( 'Booking not found' );
        }

        $booking = $this->bookingRepository->update($input, $id);

        return $this->sendResponse($booking->toArray(), 'Booking updated successfully' );
    }*/

    /**
     * Remove the specified Booking from storage.
     * DELETE /bookings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    /*public function destroy($id)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking) ) {
            return $this->sendError( 'Booking not found' );
        }

        $booking->delete();

        return $this->sendResponse($id, 'Booking deleted successfully' );
    }*/
}
