<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateRoomCategoryAPIRequest;
use App\Http\Requests\API\Admin\UpdateRoomCategoryAPIRequest;
use App\Models\Admin\RoomCategory;
use App\Models\Admin\RoomCategoryService;
use App\Repositories\Admin\RoomCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class RoomCategoryController
 * @package App\Http\Controllers\API\Admin
 */

class RoomCategoryAPIController extends AppBaseController
{
    /** @var  RoomCategoryRepository */
    private $roomCategoryRepository;

    public function __construct(RoomCategoryRepository $roomCategoryRepo)
    {
        $this->roomCategoryRepository = $roomCategoryRepo;
    }

    /**
     * Display a listing of the RoomCategory.
     * GET|HEAD /roomCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // \Log::info('here');
        $this->roomCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->roomCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $roomCategories = $this->roomCategoryRepository->getCustomized();
        $roomCategories->load('features');
        // dd($roomCategories);
        return $this->sendResponse($roomCategories->toArray(), 'Room Categories retrieved successfully');
    }

    public function roomCategoryServices( $id )
    {
        $roomCategory = $this->roomCategoryRepository->findWithoutFail($id);
        return $roomCategory->services;
    }

    /**
     * Store a newly created RoomCategory in storage.
     * POST /room_categories
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->except(['name', 'services']);
        $inputTranslation = $request->only(['name', 'description', 'language_id']);
        $services = $request->input('services');
        try
        {
            DB::beginTransaction();
            $roomCategory = RoomCategory::create($input);
            $roomCategory->translations()->create($inputTranslation);
            if (!empty($services))
                $this->saveServices($roomCategory, $services);
            $this->inputGallery($roomCategory, $request, 'casa', true);
            DB::commit();
        }
        catch (Exception $e) {
            DB::rollBack();
        }
        return $this->sendResponse($roomCategory->toArray(), 'Room Category saved successfully');
    }


    public function update(int $id, Request $request)
    {
        $input = $request->except(['name', 'services']);
        $inputTranslation = $request->only(['name', 'description', 'language_id']);
        $services = $request->input('services');
        try
        {
            DB::beginTransaction();
            $roomCategory = RoomCategory::find($id);
            $roomCategory->update($input);
            $this->updateOrCreateTranslation($roomCategory, $inputTranslation);

            if (empty($services))
                $roomCategory->roomCategoryServices()->forceDelete();
            else
                $this->saveServices($roomCategory, $services);

            // Gallery
            $this->inputGallery($roomCategory, $request, 'casa' );
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }

        return $this->sendResponse($roomCategory->toArray(), 'Room Category updated successfully');
    }

    /**
     * Display the specified RoomCategory.
     * GET|HEAD /roomCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RoomCategory $roomCategory */
        $roomCategory = $this->roomCategoryRepository->findCustomized($id);

        if (empty($roomCategory)) {
            return $this->sendError('Room Category not found');
        }

        return $this->sendResponse($roomCategory->toArray(), 'Room Category retrieved successfully');
    }

    /**
     * Method to drop multimedia relationship
     * @param  integer $room_category_id
     * @param  integer $multimedia_id
     * @return JSON
     */
    public function deleteImage($room_category_id, $multimedia_id)
    {
        $room_category = $this->roomCategoryRepository->findWithoutFail($room_category_id);

        if (empty($room_category)) {
            Flash::error('Room Category not found');

            return redirect(route('admin.activities.index'));
        }
        $room_category->row->multimedias()->detach($multimedia_id);
        return $this->sendResponse($multimedia_id, 'Image deleted successfully');

        /*
        $room_category = $this->roomCategoryRepository->findWithoutFail($room_category_id);

        if (empty($room_category)) {
            Flash::error('Room Category not found');

            return redirect(route('admin.activities.index'));
        }

        $room_category->row->multimedias()->detach($multimedia_id);

        return ['message' => 'Image dropped successfully'];
        */
    }

    /**
     * Method to save services for RoomCategories
     * @param  RoomCategory $roomCategory
     * @param  array $services
     */
    private function saveServices(RoomCategory $roomCategory, array $services)
    {
        $roomCategory->roomCategoryServices()->forceDelete();
        foreach ($services as $service)
        {
            $item = new RoomCategoryService;
            $item->service_id = $service['id'];
            if (array_key_exists('pivot', $service))
            {
                $item->info = $service['pivot']['info'];
            }
            $roomCategory->roomCategoryServices()->save($item);
        }
    }

}
