<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateActivityAPIRequest;
use App\Http\Requests\API\Admin\UpdateActivityAPIRequest;
use App\Models\Admin\Activity;
use App\Repositories\Admin\ActivityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ActivityController
 * @package App\Http\Controllers\API\Admin
 */

class ActivityAPIController extends AppBaseController
{
    /** @var  ActivityRepository */
    private $activityRepository;

    public function __construct(ActivityRepository $activityRepo)
    {
        $this->activityRepository = $activityRepo;
    }

    /**
     * Display a listing of the Activity.
     * GET|HEAD /activities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->activityRepository->pushCriteria(new RequestCriteria($request));
        $this->activityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $activities = $this->activityRepository
                        ->with('translations')
                        ->all();

        return $this->sendResponse($activities->toArray(), 'Activities retrieved successfully');
    }

    /**
     * Store a newly created Activity in storage.
     * POST /activities
     *
     * @param CreateActivityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateActivityAPIRequest $request)
    {
        $input = $this->input( $request );
        $inputTranslation = $this->inputTranslation( $request );

        // create model
        $activity = $this->activityRepository->create($input);
        $activity->translations()->create($inputTranslation);
        // Store images
        // $this->inputImages($model, $request);
        $this->inputGallery($activity, $request, 'experience', false);
        return $this->sendResponse($activity->toArray(), 'Activity saved successfully');
    }

    /**
     * Display the specified Activity.
     * GET|HEAD /activities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($slug)
    {
        /** @var Activity $activity */
        $activity = $this->activityRepository
                    ->with('translations')
                    ->with('activityCategory.translations')
                    ->findByField('slug', $slug);
        \Log::info($slug);

        if (empty($activity)) {
            return $this->sendError('Activity not found');
        }

        return $this->sendResponse($activity->toArray(), 'Activity retrieved successfully');
    }

    /**
     * Update the specified Activity in storage.
     * PUT/PATCH /activities/{id}
     *
     * @param  int $id
     * @param UpdateActivityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivityAPIRequest $request)
    {
        // $input = $request->all();
        $input = $request->only(['activity_category_id', 'ally_id']);
        $inputTranslation = $this->inputTranslation($request);

        /** @var Activity $activity */
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            return $this->sendError('Activity not found');
        }

        $activity = $this->activityRepository->update($input, $id);
        $this->updateOrCreateTranslation($activity, $inputTranslation);
        $this->inputGallery($activity, $request, 'experience', false);

        return $this->sendResponse($activity->toArray(), 'Activity updated successfully');
    }

    /**
     * Remove the specified Activity from storage.
     * DELETE /activities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Activity $activity */
        $activity = $this->activityRepository->findWithoutFail($id);

        if (empty($activity)) {
            return $this->sendError('Activity not found');
        }

        $activity->delete();

        return $this->sendResponse($id, 'Activity deleted successfully');
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    private function input( $request )
    {
        $model =  $request->only([
            'activity_category_id',
            'ally_id',
        ]);
        $model[ 'slug' ] = str_slug( $request->input( 'title' ), '-' );

        $model['status_id'] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    private function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $translation = $request->only([
            'title',
            'subtitle',
            'description',
            'language_id',
        ]);

        return $translation;
    }

    /**
     * Method to drop multimedia relationship
     * @param  integer $activity_id
     * @param  integer $multimedia_id
     * @return JSON
     */
    public function deleteImage($activity_id, $multimedia_id)
    {
        $activity = $this->activityRepository->findWithoutFail($activity_id);

        if (empty($activity)) {
            Flash::error('Activity not found');

            return redirect(route('admin.activities.index'));
        }

        $activity->row->multimedias()->detach($multimedia_id);

        return ['message' => 'Image dropped successfully'];

    }
}
