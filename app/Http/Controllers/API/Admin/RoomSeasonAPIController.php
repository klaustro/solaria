<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\Admin\CreateRoomSeasonAPIRequest;
use App\Http\Requests\API\Admin\UpdateRoomSeasonAPIRequest;
use App\Models\Admin\Rate;
use App\Models\Admin\Room;
use App\Models\Admin\RoomSeason;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\Admin\RoomSeasonRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RoomSeasonController
 * @package App\Http\Controllers\API\Admin
 */

class RoomSeasonAPIController extends AppBaseController
{
    /** @var  RoomSeasonRepository */
    private $roomSeasonRepository;
    /** @var  RoomRepository */
    private $roomRepository;

    public function __construct(RoomSeasonRepository $roomSeasonRepo,
        RoomRepository $roomRepo)
    {
        $this->roomSeasonRepository = $roomSeasonRepo;
        $this->roomRepository       = $roomRepo;
    }

    /**
     * Display a listing of the RoomSeason.
     * GET|HEAD /roomSeasons
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomSeasonRepository->pushCriteria(new RequestCriteria($request));
        $this->roomSeasonRepository->pushCriteria(new LimitOffsetCriteria($request));
        $roomSeasons = $this->roomSeasonRepository->with('translations')->findWhere($request->all());

        return $this->sendResponse($roomSeasons->toArray(), 'Room Seasons retrieved successfully');
    }

    /**
     * Store a newly created RoomSeason in storage.
     * POST /roomSeasons
     *
     * @param CreateRoomSeasonAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomSeasonAPIRequest $request)
    {
        // Recorre las temporadas enviadas
        $seasons = $request->input('seasons');
        foreach ($seasons as $season) {
            $input = $this->input($season);
            $inputTranslation = $this->inputTranslation( $season );

            $rooms = $season['rooms'];
            foreach ($rooms as $key => $room) {
                $input['room_id'] = $room['id'];
                // Create RoomSeason
                $roomSeason = $this->roomSeasonRepository->create($input);
                // create translation
                $roomSeason->translations()->create( $inputTranslation );
            }
        }

        return $this->sendResponse($roomSeason->toArray(), 'Room Season saved successfully');
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    private function input( $request )
    {
        $model = array(
            'price' => $request['price'],
            'iva' => $request['iva'],
            'start_date' => $request['date_range']['start'],
            'end_date'  => $request['date_range']['end'],
        );

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    private function inputTranslation( $request )
    {

        $translation = array(
            'name' => $request['name'],
            'language_id' => 1,
        );

        return $translation;
    }

    public function searchOferte( Request $request)
    {
        $this->roomRepository->pushCriteria( new RequestCriteria( $request ) );
        $this->roomRepository->pushCriteria( new LimitOffsetCriteria( $request ) );

        $checkin        = $request->get( 'start_date' );
        $checkout       = $request->get( 'end_date' );
        $input          = compact( 'checkin', 'checkout' );

        // obtener las rooms no disponibles por BookingDetails
        $unavailableRoomSeasonRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {

            $roomSeasons = $this->roomSeasonRepository->findWhere( [
                [ 'start_date', '>', $start_range_ ],
                [ 'end_date',   '<', $end_range_ ]
            ] );

            return $roomSeasons;
        }, 'start_date', 'end_date' );

        // obtener tanto disponibles como no disponibles
        $rooms = $this->roomRepository->getCustomized( $input, $unavailableRoomSeasonRooms );

        return $this->sendResponse( [ 'rooms' => $rooms ], 'Rooms retrieved successfully' );
    }

    /**
     * Display the specified RoomSeason.
     * GET|HEAD /roomSeasons/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RoomSeason $roomSeason */
        $roomSeason = $this->roomSeasonRepository->findWithoutFail($id);

        if (empty($roomSeason)) {
            return $this->sendError('Room Season not found');
        }

        return $this->sendResponse($roomSeason->toArray(), 'Room Season retrieved successfully');
    }

    /**
     * Update the specified RoomSeason in storage.
     * PUT/PATCH /roomSeasons/{id}
     *
     * @param  int $id
     * @param UpdateRoomSeasonAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomSeasonAPIRequest $request)
    {
        // $input = $request->all();
        $input = $this->input($request);
        $inputTranslation = $this->inputTranslation($request);

        /** @var RoomSeason $roomSeason */
        $roomSeason = $this->roomSeasonRepository->findWithoutFail($id);

        if (empty($roomSeason)) {
            return $this->sendError('Room Season not found');
        }

        $roomSeason = $this->roomSeasonRepository->update($input, $id);
        $roomSeason->translations()->update($inputTranslation);

        return $this->sendResponse($roomSeason->toArray(), 'RoomSeason updated successfully');
    }

    /**
     * Remove the specified RoomSeason from storage.
     * DELETE /roomSeasons/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RoomSeason $roomSeason */
        $roomSeason = $this->roomSeasonRepository->findWithoutFail($id);

        if (empty($roomSeason)) {
            return $this->sendError('Room Season not found');
        }

        $roomSeason->delete();

        return $this->sendResponse($id, 'Room Season deleted successfully');
    }

    /**
     * Display a listing of the RoomSeason.
     * GET|HEAD /roomSeasons
     *
     * @param Request $request
     * @return Response
     */
    public function calendar(Request $request)
    {
        $date = new Carbon($request->month);

        $current = $date->firstOfMonth();

        $date = new Carbon($request->month);

        $end = $date->lastOfMonth();

        $days = $current->diffInDays($end);

        $room = Room::find($request->room_id);

        $schedules = [];

        $season_colors = ['#af64cc', '#27ae60'];


        for ($i=0; $i <= $days; $i++) {

            $rate = Rate::with('translations')->where('room_id', $request->room_id)->whereDate('start_date', '<=', $current)->where('end_date', '>=', $current)->first();

            if ($rate) {
                array_push($schedules, [
                    'id' => $rate->id,
                    'calendarId' => '1',
                    'title' => "{$rate->title} (€ {$rate->price})",
                    'start_date' => $current->toDateString(),
                    'end_date' => $current->toDateString(),
                    'bgColor' => '#e74c3c',
                    'url' => "/admin/rates/{$rate->id}/edit",
                ]);
            } else {
                $season = RoomSeason::with('translations')->where('room_id', $request->room_id)->whereDate('start_date', '<=', $current)->where('end_date', '>=', $current)->first();
                if ($season) {
                    if (! isset($last_season)) {
                        $last_season = $season->id;
                    }
                    array_push($schedules, [
                        'id' => $season->id,
                        'calendarId' => '1',
                        'title' => "{$season->name} (€ $season->price)",
                        'start_date' => $current->toDateString(),
                        'end_date' => $current->toDateString(),
                        'bgColor' => $season_colors[$season->id % 2 == 0],
                        'url' => "/admin/roomSeasons/{$season->id}/edit"
                    ]);
                } else {
                    array_push($schedules, [
                        'id' => $room->id,
                        'calendarId' => '1',
                        'title' => "Prezzo Regolare (€ {$room->price})",
                        'start_date' => $current->toDateString(),
                        'end_date' => $current->toDateString(),
                        'bgColor' => '#e67e22',
                        'url' => "/admin/rooms/it/{$room->id}",
                        
                    ]);
                }
            }
            $current->addDay();
        }

        return $this->sendResponse($schedules, 'Schedules retrieved successfully');
    }

}
