<?php

namespace App\Http\Controllers\API\Admin;

use App\Events\EventRoomAvailability;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\Admin\CreateRoomAPIRequest;
use App\Http\Requests\API\Admin\UpdateRoomAPIRequest;
use App\Models\Admin\Room;
use App\Repositories\Admin\RoomRepository;
use App\Repositories\Admin\LockedRoomRepository;
use App\Repositories\Admin\BookingDetailRepository;
use App\Repositories\Admin\RoomCategoryRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class RoomController
 * @package App\Http\Controllers\API\Admin
 */

class RoomAPIController extends AppBaseController
{
    /** @var  RoomRepository */
    private $roomRepository;

    /** @var  LockedRoomRepository */
    private $lockedRoomRepository;

    /** @var  BookingDetailRepository */
    private $bookingDetailRepository;

    /** @var  RoomCategoryRepository */
    private $roomCategoryRepository;

    public function __construct( RoomRepository $roomRepo,
        LockedRoomRepository $lockedRoomRepo,
        BookingDetailRepository $bookingDetailRepo,
        RoomCategoryRepository $roomCategoryRepo )
    {
        $this->roomRepository          = $roomRepo;
        $this->lockedRoomRepository    = $lockedRoomRepo;
        $this->bookingDetailRepository = $bookingDetailRepo;
        $this->roomCategoryRepository  = $roomCategoryRepo;
    }

    /**
     * Display a listing of the Room.
     * POST /rooms
     * @see http://localhost:8011/api/admin/rooms?checkin=2019-01-27&checkout=2019-02-10&roomLocationId=0
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->roomRepository->pushCriteria( new RequestCriteria( $request ) );
        $this->roomRepository->pushCriteria( new LimitOffsetCriteria( $request ) );

        $checkin        = $request->get( 'checkin' );
        $checkout       = $request->get( 'checkout' );
        $roomLocationId = $request->has( 'roomLocationId' ) ? (int)$request->get( 'roomLocationId' ) : null;
        $input          = compact( 'checkin', 'checkout', 'roomLocationId' );

        // obtener las rooms no disponibles por BookingDetails
        $unavailableBookingDetailRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {

            $bookingDetails = $this->bookingDetailRepository->findWhere( [
                [ 'checkin_date',    '>', $start_range_ ],
                [ 'checkout_date',   '<', $end_range_ ]
            ] );

            // validacion de status_id = 5 ( reservas anuladas )
            $bookingDetails = $bookingDetails->filter( function ( $item, $key ) {
                return $item->booking->status_id !== 5;
            } );

            return $bookingDetails;
        } );

        // obtener las rooms no disponibles por LockedRooms
        $unavailableLockedRoomRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {
            $lockRooms = $this->lockedRoomRepository->findWhere( [
                [ 'checkin_date',    '>', $start_range_ ],
                [ 'checkout_date',   '<', $end_range_ ],
                [ 'locked_at',       '>', Carbon::now()->subMinutes( env( 'LOCKED_ROOM_TIME', 5 ) ) ] // se buscan solo las rooms cuyo locked_at sea mayor a hace cinco minutos
            ] );

            return $lockRooms;
        } );

        // union de las rooms no disponibles (BookingDetails y LockedRooms)
        $unavailableRooms = array_unique( array_merge( $unavailableBookingDetailRooms, $unavailableLockedRoomRooms ) );

        // obtener tanto disponibles como no disponibles
        $rooms = $this->roomRepository->getCustomized( $input, $unavailableRooms );

        // filtro por localidad
        if ( $roomLocationId !== null && $roomLocationId !== 0 ) {
            $rooms = $rooms->filter( function( $room ) use ( $roomLocationId ) {
                $roomCategory = $this->roomCategoryRepository->findWithoutFail( $room[ 'room_category_id' ] );

                return (int)$roomCategory->room_location_id === $roomLocationId;
            } );
            $rooms = array_values( $rooms->toArray() );
        }

        return $this->sendResponse( [ 'rooms' => $rooms ], 'Rooms retrieved successfully' );
    }

    /**
     * Display the specified Room.
     * GET|HEAD /rooms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show( $id )
    {
        /** @var Room $room */
        $room = $this->roomRepository->findCustomized($id);

        if (empty($room)) {
            return $this->sendError('Room not found');
        }

        return $this->sendResponse(['room' => $room], 'Room retrieved successfully');
    }


    /**
     * Store a newly created Room in storage.
     *
     * @param CreateRoomRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomAPIRequest $request)
    {
        $input = $this->input($request);
        $inputTranslation = $this->inputTranslation($request);

        $model = $this->roomRepository->create($input);
        $model->translations()->create($inputTranslation);
        $this->inputGallery($model, $request, 'casa', true);

        return $this->sendResponse($model->toArray(), 'Appartamento salvato correttamente.');
    }

    /**
     * Update the specified Room in storage.
     *
     * @param  int              $id
     * @param UpdateRoomRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomAPIRequest $request)
    {
        $room = $this->roomRepository->findWithoutFail($id);
        if (empty($room)) {
            return response()->json(['Room not found'], 404);
        }
        // inputs
        $input = $this->input($request);
        $inputTranslation = $this->inputTranslation($request);

        $room = $this->roomRepository->update($input, $id);
        $this->updateOrCreateTranslation($room, $inputTranslation);
        $this->inputGallery($room, $request, 'casa', true);

        return $this->sendResponse($room->toArray(), 'Si ha modificato Appartamento correttamente.');
    }

    /**
     * Ordenar los campos que se guardan en Model
     *
     * @return array
     */
    public function input( $request )
    {
        $model =  $request->only( [
            'room_category_id',
            'price',
            'iva',
            'adults_quantity',
            'children_quantity'
        ] );
        $model[ 'slug' ] = str_slug( $request->input( 'name' ), '-' );

        $model[ 'status_id' ] = 1;

        return $model;
    }

    /**
     * Ordenar los campos que se guardan en Translations
     *
     * @return array
     */
    public function inputTranslation( $request )
    {
        //Translation Validation
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $translation = $request->only([
            'name',
            'description',
            'language_id',
            'subtitle1',
            'subtitle2'
        ]);

        return $translation;
    }







    /**
     * Lock the specified Room in the given range (checkin_date, checkout_date).
     *
     * $request = [
     *     'roomId'         => 1,
     *     'checkinDate'    => '2018-12-23',
     *     'checkoutDate'   => '2018-12-25',
     *     'datetime'       => '2018-12-10 22:24:43'
     * ];
     *
     * POST /rooms/lock
     * @see http://localhost:8011/api/admin/rooms/lock?roomId=1&checkinDate=2019-01-27&checkoutDate=2019-02-10&datetime=2019-01-17%2017:00:00
     *
     * @param Request $request
     *
     * @return Response
     */
    public function lock( Request $request )
    {
        $input = $request->only( 'roomId', 'checkinDate', 'checkoutDate', 'datetime' );

        /** @var Room $room */
        $room = $this->roomRepository->findWithoutFail( $input[ 'roomId' ] );

        if ( empty( $room ) ) {
            return $this->sendError( 'Room not found' );
        }

        // obtener las rooms no disponibles por BookingDetails
        $unavailableRoomByBookingDetail = $this->roomRepository->findUnavailableRooms( $input[ 'checkinDate' ], $input[ 'checkoutDate' ], function ( $start_range_, $end_range_ ) use ( $room ) {

            $bookingDetails = $this->bookingDetailRepository->findWhere( [
                [ 'room_id',        '=', $room->id ],
                [ 'checkin_date',   '>', $start_range_ ],
                [ 'checkout_date',  '<', $end_range_ ]
            ] );

            // validacion de status_id = 5 ( reservas anuladas )
            $bookingDetails = $bookingDetails->filter( function ( $item, $key ) {
                return $item->booking->status_id !== 5;
            } );

            return $bookingDetails;
        } );

        // determino si esta disponible o no (por BookingDetail)
        $isAvailable = empty( $unavailableRoomByBookingDetail ) === true;

        // si esta disponible se intenta bloquear temporalmente, sino entonces no se bloquea
        if ( $isAvailable === true ) {
            // Bloquear la habitacion (por BookingDetail)
            $lockedRoom = $this->lockedRoomRepository->lockRoom( $room->id, $input[ 'checkinDate' ], $input[ 'checkoutDate' ], $input[ 'datetime' ] );
        }
        else {
            $lockedRoom = false;
        }

        // valido si fue bloqueada la habitacion correctamente
        if ( $lockedRoom === false ) {
            // la habitacion NO fue bloqueada, se genera mensaje
            $message = 'Room NOT locked, it was already locked.';

            // data = null
            $data = null;
        }
        else {
            // la habitacion fue bloqueada, se genera mensaje, data y evento
            $message = 'Room locked successfully.';

            // availability = 0
            $room = $room->toArray();
            $room[ 'availability' ] = 0;
            $roomBrocast = $room;

            unset($roomBrocast['services']);
            unset($roomBrocast['gallery']);

            // evento broadcasting
            event( new EventRoomAvailability( $roomBrocast ) );

            // data = room
            $data = [ 'room' => $room ];

        }

        return $this->sendResponse( $data, $message );
    }

    /**
     * Unlock the specified Room in the given range (checkin_date, checkout_date).
     *
     * $request = [
     *     'roomId'         => 1,
     *     'checkinDate'    => '2018-12-23',
     *     'checkoutDate'   => '2018-12-25'
     * ];
     *
     * POST /rooms/unlock
     *
     * @param Request $request
     *
     * @return Response
     */
    public function unlock( Request $request )
    {
        $input = $request->only( 'roomId', 'checkinDate', 'checkoutDate' );

        /** @var Room $room */
        $room = $this->roomRepository->findWithoutFail( $input[ 'roomId' ] );

        if ( empty( $room ) ) {
            return $this->sendError( 'Room not found' );
        }

        /** @var LockedRoom $lockedRoom */
        $lockedRoom = $this->lockedRoomRepository->findByField( 'room_id', $input[ 'roomId' ] );

        if ( empty( $lockedRoom ) ) {
            return $this->sendError( 'Locked Room not found' );
        }

        // Desbloquear la habitacion
        $unlocked = $this->lockedRoomRepository->unlockRoom( $room->id, $input[ 'checkinDate' ], $input[ 'checkoutDate' ] );

        // validar si la room fue desbloqueada, o no estaba bloqueada
        if ( $unlocked === 1 ) {
            $message = 'Room unlocked successfully';
        }
        else {
            $message = 'Room wasn\'t unlocked';
        }

        // availability = 1
        $room = $room->toArray();
        $room[ 'availability' ] = 1;
        $roomBrocast = $room;

        unset($roomBrocast['services']);
        unset($roomBrocast['gallery']);

        // evento broadcasting
        event( new EventRoomAvailability( $roomBrocast ) );

        return $this->sendResponse( [ 'room' => $room ], $message );
    }

    public function roomServices( $id )
    {
        $room = $this->roomRepository->findWithoutFail($id);

        return $room->services;
    }

    /**
     * Method to drop multimedia relationship
     * @param  integer $room_id
     * @param  integer $multimedia_id
     * @return JSON
     */
    public function deleteImage( $room_id, $multimedia_id )
    {
        $room = $this->roomRepository->findWithoutFail($room_id);

        if (empty($room)) {
            Flash::error('Room not found');

            return redirect(route('admin.rooms.index'));
        }

        $room->row->multimedias()->detach($multimedia_id);

        return ['message' => 'Image dropped successfully'];

    }

    /**
     * Display a listing of the Room.
     * POST /roomAll
     *
     * @param Request $request
     * @return Response
     */
    public function roomAll( Request $request )
    {
        $this->roomRepository->pushCriteria( new RequestCriteria( $request ) );
        $this->roomRepository->pushCriteria( new LimitOffsetCriteria( $request ) );
        $rooms = $this->roomRepository->all();

        return $this->sendResponse( [ 'rooms' => $rooms ], 'Rooms retrieved successfully' );
    }

    /**
     * Display the specified Room.
     * GET|HEAD /rooms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function all(  )
    {
        /** @var Room $room */
        $rooms = $this->roomRepository->all();

        if (empty($rooms)) {
            return $this->sendError('Room not found');
        }

        return $this->sendResponse(['rooms' => $rooms], 'Room retrieved successfully');
    }
}
