<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateBlogCommentAPIRequest;
use App\Http\Requests\API\Admin\UpdateBlogCommentAPIRequest;
use App\Models\Admin\BlogComment;
use App\Repositories\Admin\BlogCommentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BlogCommentController
 * @package App\Http\Controllers\API\Admin
 */

class BlogCommentAPIController extends AppBaseController
{
    /** @var  BlogCommentRepository */
    private $blogCommentRepository;

    public function __construct(BlogCommentRepository $blogCommentRepo)
    {
        $this->blogCommentRepository = $blogCommentRepo;
    }

    /**
     * Display a listing of the BlogComment.
     * GET|HEAD /blogComments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->blogCommentRepository->pushCriteria(new RequestCriteria($request));
        $this->blogCommentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $blogComments = $this->blogCommentRepository->all();

        return $this->sendResponse($blogComments->toArray(), 'Blog Comments retrieved successfully');
    }

    /**
     * Store a newly created BlogComment in storage.
     * POST /blogComments
     *
     * @param CreateBlogCommentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateBlogCommentAPIRequest $request)
    {
        $input = $request->all();

        $blogComments = $this->blogCommentRepository->create($input);

        return $this->sendResponse($blogComments->toArray(), 'Blog Comment saved successfully');
    }

    /**
     * Display the specified BlogComment.
     * GET|HEAD /blogComments/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BlogComment $blogComment */
        $blogComment = $this->blogCommentRepository->findWithoutFail($id);

        if (empty($blogComment)) {
            return $this->sendError('Blog Comment not found');
        }

        return $this->sendResponse($blogComment->toArray(), 'Blog Comment retrieved successfully');
    }

    /**
     * Update the specified BlogComment in storage.
     * PUT/PATCH /blogComments/{id}
     *
     * @param  int $id
     * @param UpdateBlogCommentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlogCommentAPIRequest $request)
    {
        $input = $request->all();

        /** @var BlogComment $blogComment */
        $blogComment = $this->blogCommentRepository->findWithoutFail($id);

        if (empty($blogComment)) {
            return $this->sendError('Blog Comment not found');
        }

        $blogComment = $this->blogCommentRepository->update($input, $id);

        return $this->sendResponse($blogComment->toArray(), 'BlogComment updated successfully');
    }

    /**
     * Remove the specified BlogComment from storage.
     * DELETE /blogComments/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BlogComment $blogComment */
        $blogComment = $this->blogCommentRepository->findWithoutFail($id);

        if (empty($blogComment)) {
            return $this->sendError('Blog Comment not found');
        }

        $blogComment->delete();

        return $this->sendResponse($id, 'Blog Comment deleted successfully');
    }
}
