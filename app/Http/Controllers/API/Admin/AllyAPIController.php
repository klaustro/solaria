<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateAllyAPIRequest;
use App\Http\Requests\API\Admin\UpdateAllyAPIRequest;
use App\Models\Admin\Ally;
use App\Repositories\Admin\AllyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AllyController
 * @package App\Http\Controllers\API\Admin
 */

class AllyAPIController extends AppBaseController
{
    /** @var  AllyRepository */
    private $allyRepository;

    public function __construct(AllyRepository $allyRepo)
    {
        $this->allyRepository = $allyRepo;
    }

    /**
     * Display a listing of the Ally.
     * GET|HEAD /allies
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->allyRepository->pushCriteria(new RequestCriteria($request));
        $this->allyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $allies = $this->allyRepository->all();

        return $this->sendResponse($allies->toArray(), 'Allies retrieved successfully');
    }

    /**
     * Store a newly created Ally in storage.
     * POST /allies
     *
     * @param CreateAllyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAllyAPIRequest $request)
    {
        $input = $request->all();

        $allies = $this->allyRepository->create($input);

        return $this->sendResponse($allies->toArray(), 'Ally saved successfully');
    }

    /**
     * Display the specified Ally.
     * GET|HEAD /allies/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Ally $ally */
        $ally = $this->allyRepository->findWithoutFail($id);

        if (empty($ally)) {
            return $this->sendError('Ally not found');
        }

        return $this->sendResponse($ally->toArray(), 'Ally retrieved successfully');
    }

    /**
     * Update the specified Ally in storage.
     * PUT/PATCH /allies/{id}
     *
     * @param  int $id
     * @param UpdateAllyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAllyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Ally $ally */
        $ally = $this->allyRepository->findWithoutFail($id);

        if (empty($ally)) {
            return $this->sendError('Ally not found');
        }

        $ally = $this->allyRepository->update($input, $id);

        return $this->sendResponse($ally->toArray(), 'Ally updated successfully');
    }

    /**
     * Remove the specified Ally from storage.
     * DELETE /allies/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Ally $ally */
        $ally = $this->allyRepository->findWithoutFail($id);

        if (empty($ally)) {
            return $this->sendError('Ally not found');
        }

        $ally->delete();

        return $this->sendResponse($id, 'Ally deleted successfully');
    }
}
