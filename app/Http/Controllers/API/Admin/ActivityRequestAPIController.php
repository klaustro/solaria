<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\Admin\CreateActivityRequestAPIRequest;
use App\Http\Requests\API\Admin\UpdateActivityRequestAPIRequest;
use App\Models\Admin\ActivityRequest;
use App\Notifications\ActivityRequestNotification;
use App\Notifications\AllyActivityRequestNotification;
use App\Repositories\Admin\ActivityRequestRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ActivityRequestController
 * @package App\Http\Controllers\API\Admin
 */

class ActivityRequestAPIController extends AppBaseController
{
    /** @var  ActivityRequestRepository */
    private $activityRequestRepository;

    public function __construct(ActivityRequestRepository $activityRequestRepo)
    {
        $this->activityRequestRepository = $activityRequestRepo;
    }

    /**
     * Display a listing of the ActivityRequest.
     * GET|HEAD /activityRequests
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->activityRequestRepository->pushCriteria(new RequestCriteria($request));
        $this->activityRequestRepository->pushCriteria(new LimitOffsetCriteria($request));
        $activityRequests = $this->activityRequestRepository->all();

        return $this->sendResponse($activityRequests->toArray(), 'Activity Requests retrieved successfully');
    }

    /**
     * Store a newly created ActivityRequest in storage.
     * POST /activityRequests
     *
     * @param CreateActivityRequestAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateActivityRequestAPIRequest $request)
    {
        $input = $request->all();

        $activityRequests = $this->activityRequestRepository->create($input);

        Notification::send($activityRequests, new ActivityRequestNotification($activityRequests));

        $ally = $activityRequests->activity->ally;

        if (isset($ally->email)) {
            Notification::send($ally, new AllyActivityRequestNotification($activityRequests));
        }

        return $this->sendResponse($activityRequests->toArray(), 'Grazie per averci contattato, risponderemo presto');
    }

    /**
     * Display the specified ActivityRequest.
     * GET|HEAD /activityRequests/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ActivityRequest $activityRequest */
        $activityRequest = $this->activityRequestRepository->findWithoutFail($id);

        if (empty($activityRequest)) {
            return $this->sendError('Activity Request not found');
        }

        return $this->sendResponse($activityRequest->toArray(), 'Activity Request retrieved successfully');
    }

    /**
     * Update the specified ActivityRequest in storage.
     * PUT/PATCH /activityRequests/{id}
     *
     * @param  int $id
     * @param UpdateActivityRequestAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateActivityRequestAPIRequest $request)
    {
        $input = $request->all();

        /** @var ActivityRequest $activityRequest */
        $activityRequest = $this->activityRequestRepository->findWithoutFail($id);

        if (empty($activityRequest)) {
            return $this->sendError('Activity Request not found');
        }

        $activityRequest = $this->activityRequestRepository->update($input, $id);

        return $this->sendResponse($activityRequest->toArray(), 'ActivityRequest updated successfully');
    }

    /**
     * Remove the specified ActivityRequest from storage.
     * DELETE /activityRequests/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ActivityRequest $activityRequest */
        $activityRequest = $this->activityRequestRepository->findWithoutFail($id);

        if (empty($activityRequest)) {
            return $this->sendError('Activity Request not found');
        }

        $activityRequest->delete();

        return $this->sendResponse($id, 'Activity Request deleted successfully');
    }
}
