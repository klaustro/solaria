<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreatepolicyAPIRequest;
use App\Http\Requests\API\Admin\UpdatepolicyAPIRequest;
use App\Models\Admin\policy;
use App\Repositories\Admin\policyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class policyController
 * @package App\Http\Controllers\API\Admin
 */

class policyAPIController extends AppBaseController
{
    /** @var  policyRepository */
    private $policyRepository;

    public function __construct(policyRepository $policyRepo)
    {
        $this->policyRepository = $policyRepo;
    }

    /**
     * Display a listing of the policy.
     * GET|HEAD /policies
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->policyRepository->pushCriteria(new RequestCriteria($request));
        $this->policyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $policies = $this->policyRepository->all();

        return $this->sendResponse($policies->toArray(), 'Policies retrieved successfully');
    }

    /**
     * Store a newly created policy in storage.
     * POST /policies
     *
     * @param CreatepolicyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatepolicyAPIRequest $request)
    {
        $input = $request->all();

        $policies = $this->policyRepository->create($input);

        return $this->sendResponse($policies->toArray(), 'Policy saved successfully');
    }

    /**
     * Display the specified policy.
     * GET|HEAD /policies/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var policy $policy */
        $policy = $this->policyRepository->findWithoutFail($id);

        if (empty($policy)) {
            return $this->sendError('Policy not found');
        }

        return $this->sendResponse($policy->toArray(), 'Policy retrieved successfully');
    }

    /**
     * Update the specified policy in storage.
     * PUT/PATCH /policies/{id}
     *
     * @param  int $id
     * @param UpdatepolicyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepolicyAPIRequest $request)
    {
        $input = $request->all();

        /** @var policy $policy */
        $policy = $this->policyRepository->findWithoutFail($id);

        if (empty($policy)) {
            return $this->sendError('Policy not found');
        }

        $policy = $this->policyRepository->update($input, $id);

        return $this->sendResponse($policy->toArray(), 'policy updated successfully');
    }

    /**
     * Remove the specified policy from storage.
     * DELETE /policies/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var policy $policy */
        $policy = $this->policyRepository->findWithoutFail($id);

        if (empty($policy)) {
            return $this->sendError('Policy not found');
        }

        $policy->delete();

        return $this->sendResponse($id, 'Policy deleted successfully');
    }
}
