<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\Admin\CreateRateAPIRequest;
use App\Http\Requests\API\Admin\UpdateRateAPIRequest;
use App\Models\Admin\Rate;
use App\Repositories\Admin\BookingDetailRepository;
use App\Repositories\Admin\LockedRoomRepository;
use App\Repositories\Admin\RateRepository;
use App\Repositories\Admin\RoomRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RateController
 * @package App\Http\Controllers\API\Admin
 */

class RateAPIController extends AppBaseController
{
    /** @var  RateRepository */
    private $rateRepository;

    /** @var  RoomRepository */
    private $roomRepository;

    /** @var  LockedRoomRepository */
    private $lockedRoomRepository;

    /** @var  BookingDetailRepository */
    private $bookingDetailRepository;

    public function __construct( RateRepository $rateRepo,
        RoomRepository $roomRepo,
        LockedRoomRepository $lockedRoomRepo,
        BookingDetailRepository $bookingDetailRepo )
    {
        $this->rateRepository = $rateRepo;
        $this->roomRepository = $roomRepo;
        $this->lockedRoomRepository = $lockedRoomRepo;
        $this->bookingDetailRepository = $bookingDetailRepo;
    }

    /**
     * Display a listing of the Rate.
     * GET|HEAD /rates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->rateRepository->pushCriteria(new RequestCriteria($request));
        $this->rateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $rates = $this->rateRepository->findWhere( [ 'room_id' => $request->room_id ] );

        return $this->sendResponse($rates->toArray(), 'Rate retrieved successfully');
    }

    /**
     * Valid if a rate can be purchased.
     * POST /rates/get_rate/{token}
     *
     * @param CreateRateAPIRequest $request
     *
     * @return Response
     */
    public function getRate( $token, Request $request )
    {
        $rate = $this->rateRepository->findByField( 'token', $token )->first();

        if ( empty( $rate ) === true ) {
            return $this->sendError( 'Rate not found' );
        }

        // input
        $checkin        = $rate->start_date->format( 'Y-m-d' );
        $checkout       = $rate->end_date->format( 'Y-m-d' );
        $input          = compact( 'checkin', 'checkout', 'token' );

        // obtener las rooms no disponibles por BookingDetails
        $unavailableBookingDetailRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {

            $bookingDetails = $this->bookingDetailRepository->findWhere( [
                [ 'checkin_date',    '>', $start_range_ ],
                [ 'checkout_date',   '<', $end_range_ ]
            ] );

            // validacion de status_id = 5 ( reservas anuladas )
            $bookingDetails = $bookingDetails->filter( function ( $item, $key ) {
                return $item->booking->status_id !== 5;
            } );

            return $bookingDetails;
        } );

        // obtener las rooms no disponibles por LockedRooms
        $unavailableLockedRoomRooms = $this->roomRepository->findUnavailableRooms( $checkin, $checkout, function ( $start_range_, $end_range_ ) {
            $lockRooms = $this->lockedRoomRepository->findWhere( [
                [ 'checkin_date',    '>', $start_range_ ],
                [ 'checkout_date',   '<', $end_range_ ],
                [ 'locked_at',       '>', Carbon::now()->subMinutes( env( 'LOCKED_ROOM_TIME', 5 ) ) ] // se buscan solo las rooms cuyo locked_at sea mayor a hace cinco minutos
            ] );

            return $lockRooms;
        } );

        // union de las rooms no disponibles (BookingDetails y LockedRooms)
        $unavailableRooms = array_unique( array_merge( $unavailableBookingDetailRooms, $unavailableLockedRoomRooms ) );

        // obtener tanto disponibles como no disponibles
        $rooms = $this->roomRepository->getCustomized( $input, $unavailableRooms );

        // obtengo solo la room que indica la oferta
        $rooms = $rooms->filter( function ( $value, $key ) use ( $rate ) {
            return $value[ 'id' ] === $rate->room_id;
        } )->values();

        if ( $rooms->isEmpty() === true ) {
            return $this->sendError( 'Room of rate not found' );
        }

        // output
        $output = [
            'checkin' => $checkin,
            'checkout' => $checkout,
            'rooms' => $rooms,
        ];

        return $this->sendResponse( $output, 'Room retrieved successfully' );
    }

    /**
     * Display the specified Rate.
     * GET|HEAD /rates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Rate $rate */
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            return $this->sendError('Rate not found');
        }

        return $this->sendResponse($rate->toArray(), 'Rate retrieved successfully');
    }

    /**
     * Update the specified Rate in storage.
     * PUT/PATCH /rates/{id}
     *
     * @param  int $id
     * @param UpdateRateAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRateAPIRequest $request)
    {
        $input = $request->all();

        /** @var Rate $rates */
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            return $this->sendError('Rate not found');
        }

        $rate = $this->rateRepository->update($input, $id);

        return $this->sendResponse($rate->toArray(), 'Rate updated successfully');
    }

    /**
     * Remove the specified Rate from storage.
     * DELETE /rates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Rate $rate */
        $rate = $this->rateRepository->findWithoutFail($id);

        if (empty($rate)) {
            return $this->sendError('Rate not found');
        }

        $rate->delete();

        return $this->sendResponse($id, 'Rate deleted successfully');
    }
}
