<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateRoomCategoryServiceAPIRequest;
use App\Http\Requests\API\Admin\UpdateRoomCategoryServiceAPIRequest;
use App\Models\Admin\RoomCategoryService;
use App\Repositories\Admin\RoomCategoryServiceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RoomCategoryServiceController
 * @package App\Http\Controllers\API\Admin
 */

class RoomCategoryServiceAPIController extends AppBaseController
{
    /** @var  RoomCategoryServiceRepository */
    private $roomCategoryServiceRepository;

    public function __construct(RoomCategoryServiceRepository $roomCategoryServiceRepo)
    {
        $this->roomCategoryServiceRepository = $roomCategoryServiceRepo;
    }

    /**
     * Display a listing of the RoomCategoryService.
     * GET|HEAD /roomCategoryServices
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomCategoryServiceRepository->pushCriteria(new RequestCriteria($request));
        $this->roomCategoryServiceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $roomCategoryServices = $this->roomCategoryServiceRepository->all();

        return $this->sendResponse($roomCategoryServices->toArray(), 'Room Category Services retrieved successfully');
    }

    /**
     * Store a newly created RoomCategoryService in storage.
     * POST /roomCategoryServices
     *
     * @param CreateRoomCategoryServiceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomCategoryServiceAPIRequest $request)
    {
        $input = $request->all();

        $roomCategoryServices = $this->roomCategoryServiceRepository->create($input);

        return $this->sendResponse($roomCategoryServices->toArray(), 'Room Category Service saved successfully');
    }

    /**
     * Display the specified RoomCategoryService.
     * GET|HEAD /roomCategoryServices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RoomCategoryService $roomCategoryService */
        $roomCategoryService = $this->roomCategoryServiceRepository->findWithoutFail($id);

        if (empty($roomCategoryService)) {
            return $this->sendError('Room Category Service not found');
        }

        return $this->sendResponse($roomCategoryService->toArray(), 'Room Category Service retrieved successfully');
    }

    /**
     * Update the specified RoomCategoryService in storage.
     * PUT/PATCH /roomCategoryServices/{id}
     *
     * @param  int $id
     * @param UpdateRoomCategoryServiceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomCategoryServiceAPIRequest $request)
    {
        $input = $request->all();

        /** @var RoomCategoryService $roomCategoryService */
        $roomCategoryService = $this->roomCategoryServiceRepository->findWithoutFail($id);

        if (empty($roomCategoryService)) {
            return $this->sendError('Room Category Service not found');
        }

        $roomCategoryService = $this->roomCategoryServiceRepository->update($input, $id);

        return $this->sendResponse($roomCategoryService->toArray(), 'RoomCategoryService updated successfully');
    }

    /**
     * Remove the specified RoomCategoryService from storage.
     * DELETE /roomCategoryServices/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RoomCategoryService $roomCategoryService */
        $roomCategoryService = $this->roomCategoryServiceRepository->findWithoutFail($id);

        if (empty($roomCategoryService)) {
            return $this->sendError('Room Category Service not found');
        }

        $roomCategoryService->delete();

        return $this->sendResponse($id, 'Room Category Service deleted successfully');
    }
}
