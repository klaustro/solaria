<?php

namespace App\Http\Controllers\API\Admin;

use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Prettus\Repository\Criteria\RequestCriteria;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Repositories\Admin\GallerySliderRepository;
use App\Http\Requests\API\Admin\CreateGallerySliderAPIRequest;
use App\Http\Requests\API\Admin\UpdateGallerySliderAPIRequest;
use App\Models\Admin\Row;
use App\Models\Admin\Multimedia;
use App\Models\Admin\GallerySlider;
use App\Models\Admin\RowsMultimedia;

/**
 * Class GallerySliderController
 * @package App\Http\Controllers\API\Admin
 */

class GallerySliderAPIController extends AppBaseController
{
    /** @var  GallerySliderRepository */
    private $gallerySliderRepository;

    public function __construct( GallerySliderRepository $gallerySliderRepo )
    {
        $this->gallerySliderRepository = $gallerySliderRepo;
    }

    /**
     * Display a listing of the GallerySlider.
     * GET|HEAD /gallerySliders
     *
     * @param Request $request
     * @return Response
     */
    public function index( Request $request )
    {
        $this->gallerySliderRepository->pushCriteria( new RequestCriteria( $request ) );
        $this->gallerySliderRepository->pushCriteria( new LimitOffsetCriteria( $request ) );

        // limit
        if ( $request->has( 'limit' ) ) {
            // page
            $page = $request->get( 'page' );

            $gallerySliders = $this->gallerySliderRepository->orderBy( 'created_at', 'desc' )->paginate( $request->get( 'limit' ), [ '*' ], 'paginate', 'page', $page );
        }
        else {
            $gallerySliders = $this->gallerySliderRepository->orderBy( 'created_at', 'desc' )->all();
        }

        return $this->sendResponse( $gallerySliders->toArray(), 'Gallery Sliders retrieved successfully' );
    }

    /**
     * Store a newly created GallerySlider in storage.
     * POST /gallerySliders
     *
     * @param CreateGallerySliderAPIRequest $request
     *
     * @return Response
     */
    public function store( CreateGallerySliderAPIRequest $request )
    {
        // inputs
        $input = $this->input( $request );
        $inputTranslation = $this->inputTranslation( $request );

        // create model
        $model = $this->gallerySliderRepository->create( $input );

        // store gallery
        $this->inputGallery( $model, $request, 'gallery' );

        // create translation
        $model->translations()->create( $inputTranslation );

        return $this->sendResponse( $model->toArray(), 'Gallery Slider saved successfully' );
    }

    /**
     * Campos del model.
     *
     * @return array
     */
    public function input( $request )
    {
        $input =  $request->only( [
            'status_id'
        ] );
        $input[ 'status_id' ] = 1;

        return $input;
    }

    /**
     * Campos del translation.
     *
     * @return array
     */
    public function inputTranslation( $request )
    {
        $inputTranslation = $request->only( [
            'language_id',
            'title',
        ] );

        return $inputTranslation;
    }

    /**
     * Display the specified GallerySlider.
     * GET|HEAD /gallerySliders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show( $code )
    {
        /** @var GallerySlider $model */
        $model = $this->gallerySliderRepository->findByField( 'code', $code )->first();

        if ( empty( $model ) ) {
            return $this->sendError( 'Gallery Slider not found' );
        }

        return $this->sendResponse( $model->toArray(), 'Gallery Slider retrieved successfully' );
    }

    /**
     * Update the specified GallerySlider in storage.
     * PUT/PATCH /gallerySliders/{id}
     *
     * @param  int $id
     * @param UpdateGallerySliderAPIRequest $request
     *
     * @return Response
     */
    public function update( $id, UpdateGallerySliderAPIRequest $request )
    {
        /** @var GallerySlider $model */
        $model = $this->gallerySliderRepository->findWithoutFail( $id );

        if ( empty( $model ) ) {
            return $this->sendError( 'Gallery Slider not found' );
        }

        // inputs
        $input              = $this->input( $request );
        $inputTranslation   = $this->inputTranslation( $request );

        // update model
        $model = $this->gallerySliderRepository->update( $input, $id );

        // store gallery
        $this->inputGallery( $model, $request, 'gallery' );

        // update or create translation
        $this->updateOrCreateTranslation( $model, $inputTranslation );

        return $this->sendResponse( $model->toArray(), 'GallerySlider updated successfully' );
    }

    /**
     * Remove the specified GallerySlider from storage.
     * DELETE /gallerySliders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    /*public function destroy( $id )
    {
        // * @var GallerySlider $gallerySlider
        $gallerySlider = $this->gallerySliderRepository->findWithoutFail( $id );

        if ( empty( $gallerySlider ) ) {
            return $this->sendError( 'Gallery Slider not found' );
        }

        $gallerySlider->delete();

        return $this->sendResponse( $id, 'Gallery Slider deleted successfully' );
    }*/
}
