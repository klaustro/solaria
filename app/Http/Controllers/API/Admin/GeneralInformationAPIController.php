<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateGeneralInformationAPIRequest;
use App\Http\Requests\API\Admin\UpdateGeneralInformationAPIRequest;
use App\Models\Admin\GeneralInformation;
use App\Repositories\Admin\GeneralInformationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class GeneralInformationController
 * @package App\Http\Controllers\API\Admin
 */

class GeneralInformationAPIController extends AppBaseController
{
    /** @var  GeneralInformationRepository */
    private $generalInformationRepository;

    public function __construct(GeneralInformationRepository $generalInformationRepo)
    {
        $this->generalInformationRepository = $generalInformationRepo;
    }

    /**
     * Display a listing of the GeneralInformation.
     * GET|HEAD /generalInformations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->generalInformationRepository->pushCriteria(new RequestCriteria($request));
        $this->generalInformationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $generalInformations = $this->generalInformationRepository->all();

        return $this->sendResponse($generalInformations->toArray(), 'General Informations retrieved successfully');
    }

    /**
     * Store a newly created GeneralInformation in storage.
     * POST /generalInformations
     *
     * @param CreateGeneralInformationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGeneralInformationAPIRequest $request)
    {
        $input= $this->input($request);
        $inputTranslations= $this->inputTranslation($request);

        $generalInformations = $this->generalInformationRepository->create($input);
        $generalInformations->generalInformationTranslations()->create($inputTranslations);

        return $this->sendResponse($generalInformations->toArray(), 'General Information saved successfully');
    }

    /**
     * Display the specified GeneralInformation.
     * GET|HEAD /generalInformations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var GeneralInformation $generalInformation */
        $generalInformation = $this->generalInformationRepository->findWithoutFail($id);

        if (empty($generalInformation)) {
            return $this->sendError('General Information not found');
        }

        return $this->sendResponse($generalInformation->toArray(), 'General Information retrieved successfully');
    }

    /**
     * Update the specified GeneralInformation in storage.
     * PUT/PATCH /generalInformations/{id}
     *
     * @param  int $id
     * @param UpdateGeneralInformationAPIRequest $request
     *
     * @return Response
     */
    public function update(UpdateGeneralInformationAPIRequest $request)
    {
        $input = $request->all();

        /** @var GeneralInformation $generalInformation */
        $generalInformation = $this->generalInformationRepository->findWithoutFail($input['id']);

        if (empty($generalInformation)) {
            return $this->sendError('General Information not found');
        }

        $input = $this->input($request);
        $inputTranslations = $this->inputTranslation($request);

        if ( $inputTranslations ) {
            foreach ($inputTranslations as $value) {
                $generalInformation->generalInformationTranslations()->updateOrcreate(['id' => $value['language_id']],$value);
            }
        }

        return $this->sendResponse($generalInformation->toArray(), 'GeneralInformation updated successfully');
    }

    /**
     * Remove the specified GeneralInformation from storage.
     * DELETE /generalInformations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var GeneralInformation $generalInformation */
        $generalInformation = $this->generalInformationRepository->findWithoutFail($id);

        if (empty($generalInformation)) {
            return $this->sendError('General Information not found');
        }

        $generalInformation->delete();

        return $this->sendResponse($id, 'General Information deleted successfully');
    }

    /**
     * Ordenar los camposque se guardan en generalInformation
     *
     * @return array $generalInformation
     */
    public function input($request)
    {
        $generalInformation =  $request->only([
                        'slug',
                    ]);

        $generalInformation['slug'] = str_slug($request->name, '-');
        return $generalInformation;
    }

    /**
     * Ordenar los camposque se guardan en generalInformation Translations
     *
     * @return array $generalInformationTranslation
     */
    public function inputTranslation($request)
    {
        $translations = $request->only(['translation']);

        $generalInformationTranslation = [];

        foreach ($translations as $translation) {
            foreach ($translation as $key => $value) {
                $value['general_information_id'] =  $request->id;
                $generalInformationTranslation[]= $value;
            }
        }

        return $generalInformationTranslation;
    }
}
