<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\API\Admin\CreateTagTranslationAPIRequest;
use App\Http\Requests\API\Admin\UpdateTagTranslationAPIRequest;
use App\Models\Admin\Language;
use App\Models\Admin\Screen;
use App\Models\Admin\TagTranslation;
use App\Repositories\Admin\TagTranslationRepository;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TagTranslationController
 * @package App\Http\Controllers\API\Admin
 */

class TagTranslationAPIController extends AppBaseController
{
    /** @var  TagTranslationRepository */
    private $tagTranslationRepository;

    public function __construct(TagTranslationRepository $tagTranslationRepo)
    {
        $this->tagTranslationRepository = $tagTranslationRepo;
    }

    /**
     * Display a listing of the TagTranslation.
     * GET|HEAD /tagTranslations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $array = [];
        // Step 1.
        $array = $this->objectLanguage();
        //$return = ['lang' => $array];
        //dd($array);
        $this->tagTranslationRepository->pushCriteria(new RequestCriteria($request));
        $this->tagTranslationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $tagTranslations = $this->tagTranslationRepository->all();

        return $this->sendResponse($array, 'Tag Translations retrieved successfully');
    }

    /**
     * Step 1.
     * Create the object with the language
     *
     * @param Request $request
     * @return $array
     */
    public function objectLanguage()
    {
        $langs = Language::all();
        foreach ($langs as $lang) {
            $language[] = [
                    'lang' => $lang->code,
                    'screens'=> $this->objectAllTag($lang->id)
            ];
        }
        return $language;
    }

    /**
     * Step 2.
     * Create the object with the screens
     *
     * @param Request $request
     * @return $array
     */
    public function objectAllTag($lang)
    {

        $screens = Screen::all();
        $arrayFrontSection = [];
        foreach ($screens as $screen) {
            if(!$screen->screenFrontSection->isEmpty()){
                foreach ($screen->screenFrontSection as $screenFrontSection) {
                    foreach ($screenFrontSection->frontSection->tagTranslations as $frontSection) {
                        if($frontSection->language_id==$lang) {
                            $arrayFrontSection[] =  [
                                'tag'   => $frontSection->tag,
                                'value'   => $frontSection->value,
                            ];
                        }
                    }
                }
            }
        }
        return $arrayFrontSection;
    }

    /**
     * Step 2.
     * Create the object with the screens
     *
     * @param Request $request
     * @return $array
     */
    public function objectScreen($lang)
    {
        $screens = Screen::all();
        foreach ($screens as $screen) {
            if(!$screen->screenFrontSection->isEmpty()){
                $pantalla =   [
                                    //$this->objectScreenFrontSection($lang, $screen->screenFrontSection),
                                    $screen->code => $this->objectScreenFrontSection($lang, $screen->screenFrontSection),
                                ];

            }
        }
        return $pantalla;
    }

    public function objectScreenFrontSection($lang, $screenFrontSections)
    {
        foreach ($screenFrontSections as $screenFrontSection) {
            $arrayScreenFrontSection[] =    [
                                                //$this->objectFrontSection($screenFrontSection->frontSection),
                                                $screenFrontSection->frontSection->code   => $this->objectFrontSection($screenFrontSection->frontSection),
                                            ];
        }
        return $arrayScreenFrontSection;
    }

    public function objectFrontSection($frontSections)
    {
        $arrayFrontSection = [];
        foreach ($frontSections->tagTranslations as $frontSection) {
                $arrayFrontSection[] =  [
                                'tag'   => $frontSection->tag,
                                'value'   => $frontSection->value,
                            ];
        }
        return $arrayFrontSection;
    }

    /**
     * Store a newly created TagTranslation in storage.
     * POST /tagTranslations
     *
     * @param CreateTagTranslationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTagTranslationAPIRequest $request)
    {
        $input = $request->all();

        $tagTranslations = $this->tagTranslationRepository->create($input);

        return $this->sendResponse($tagTranslations->toArray(), 'Tag Translation saved successfully');
    }

    /**
     * Display the specified TagTranslation.
     * GET|HEAD /tagTranslations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TagTranslation $tagTranslation */
        $tagTranslation = $this->tagTranslationRepository->findWithoutFail($id);

        if (empty($tagTranslation)) {
            return $this->sendError('Tag Translation not found');
        }

        return $this->sendResponse($tagTranslation->toArray(), 'Tag Translation retrieved successfully');
    }

    /**
     * Update the specified TagTranslation in storage.
     * PUT/PATCH /tagTranslations/{id}
     *
     * @param  int $id
     * @param UpdateTagTranslationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTagTranslationAPIRequest $request)
    {
        $input = $request->all();

        /** @var TagTranslation $tagTranslation */
        $tagTranslation = $this->tagTranslationRepository->findWithoutFail($id);

        if (empty($tagTranslation)) {
            return $this->sendError('Tag Translation not found');
        }

        $tagTranslation = $this->tagTranslationRepository->update($input, $id);

        return $this->sendResponse($tagTranslation->toArray(), 'TagTranslation updated successfully');
    }

    /**
     * Remove the specified TagTranslation from storage.
     * DELETE /tagTranslations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TagTranslation $tagTranslation */
        $tagTranslation = $this->tagTranslationRepository->findWithoutFail($id);

        if (empty($tagTranslation)) {
            return $this->sendError('Tag Translation not found');
        }

        $tagTranslation->delete();

        return $this->sendResponse($id, 'Tag Translation deleted successfully');
    }
}
