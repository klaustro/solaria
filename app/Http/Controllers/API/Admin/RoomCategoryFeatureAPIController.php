<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Requests\API\Admin\CreateRoomCategoryFeatureAPIRequest;
use App\Http\Requests\API\Admin\UpdateRoomCategoryFeatureAPIRequest;
use App\Models\Admin\RoomCategoryFeature;
use App\Repositories\Admin\RoomCategoryFeatureRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RoomCategoryFeatureController
 * @package App\Http\Controllers\API\Admin
 */

class RoomCategoryFeatureAPIController extends AppBaseController
{
    /** @var  RoomCategoryFeatureRepository */
    private $roomCategoryFeatureRepository;

    public function __construct(RoomCategoryFeatureRepository $roomCategoryFeatureRepo)
    {
        $this->roomCategoryFeatureRepository = $roomCategoryFeatureRepo;
    }

    /**
     * Display a listing of the RoomCategoryFeature.
     * GET|HEAD /roomCategoryFeatures
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->roomCategoryFeatureRepository->pushCriteria(new RequestCriteria($request));
        $this->roomCategoryFeatureRepository->pushCriteria(new LimitOffsetCriteria($request));
        $roomCategoryFeatures = $this->roomCategoryFeatureRepository->all();

        return $this->sendResponse($roomCategoryFeatures->toArray(), 'Room Category Features retrieved successfully');
    }

    /**
     * Store a newly created RoomCategoryFeature in storage.
     * POST /roomCategoryFeatures
     *
     * @param CreateRoomCategoryFeatureAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRoomCategoryFeatureAPIRequest $request)
    {
        $input = $request->all();

        $roomCategoryFeatures = $this->roomCategoryFeatureRepository->create($input);

        return $this->sendResponse($roomCategoryFeatures->toArray(), 'Room Category Feature saved successfully');
    }

    /**
     * Display the specified RoomCategoryFeature.
     * GET|HEAD /roomCategoryFeatures/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RoomCategoryFeature $roomCategoryFeature */
        $roomCategoryFeature = $this->roomCategoryFeatureRepository->findWithoutFail($id);

        if (empty($roomCategoryFeature)) {
            return $this->sendError('Room Category Feature not found');
        }

        return $this->sendResponse($roomCategoryFeature->toArray(), 'Room Category Feature retrieved successfully');
    }

    /**
     * Update the specified RoomCategoryFeature in storage.
     * PUT/PATCH /roomCategoryFeatures/{id}
     *
     * @param  int $id
     * @param UpdateRoomCategoryFeatureAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoomCategoryFeatureAPIRequest $request)
    {
        $input = $request->all();

        /** @var RoomCategoryFeature $roomCategoryFeature */
        $roomCategoryFeature = $this->roomCategoryFeatureRepository->findWithoutFail($id);

        if (empty($roomCategoryFeature)) {
            return $this->sendError('Room Category Feature not found');
        }

        $roomCategoryFeature = $this->roomCategoryFeatureRepository->update($input, $id);

        return $this->sendResponse($roomCategoryFeature->toArray(), 'RoomCategoryFeature updated successfully');
    }

    /**
     * Remove the specified RoomCategoryFeature from storage.
     * DELETE /roomCategoryFeatures/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RoomCategoryFeature $roomCategoryFeature */
        $roomCategoryFeature = $this->roomCategoryFeatureRepository->findWithoutFail($id);

        if (empty($roomCategoryFeature)) {
            return $this->sendError('Room Category Feature not found');
        }

        $roomCategoryFeature->delete();

        return $this->sendResponse($id, 'Room Category Feature deleted successfully');
    }
}
