<?php

namespace App\Http\Controllers;

use App\Models\Admin\Multimedia;
use App\Models\Admin\Row;
use App\Models\Admin\RowsMultimedia;
use Illuminate\Http\Request;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;

/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    /**
     * Update or create the translation for specified model.
     *
     * @param $model
     * @param array $inputTranslation
     *
     * @return void
     */
    protected function updateOrCreateTranslation( $model, $inputTranslation )
    {
        // ACA SE VALIDA SI EL IDIOMA INDICADO YA ESTA GUARDADO
        $translation = $model->translations->filter( function ( $trans ) use ( $inputTranslation ) {
            return $trans->language_id == $inputTranslation[ 'language_id' ];
        } )->first();

        if ( $translation !== null ) {
            // SI ENTRA AQUI SIGNIFICA QUE EL IDIOMA YA EXISTE, ENTONCES SE ACTUALIZA
            // $translation->update( $inputTranslation, [ 'id' => $translation->id ] );
            $translation->fill( $inputTranslation );
            $translation->save();

            // dd(
            //     $inputTranslation,
            //     $translation,
            //     \App\Models\Admin\RoomTranslation::find( $translation->id )->toArray()
            // );

        }
        else {
            // SI ENTRA AQUI SIGNIFICA QUE EL IDIOMA ES NUEVO, ENTONCES SE GUARDA POR PRIMERA VEZ
            $model->translations()->create( $inputTranslation );
        }
    }

    /**
     * Method to store gallery in storage
     * @param  [type] $model        [description]
     * @param  [type] $request      [description]
     * @return [type] $folderName   [description]
     */
    protected function inputGallery( $model, Request $request, string $folderName, $hasMain = NULL )
    {
        // gallery
        $request->validate( [
            'gallery' => 'required'
        ] );

        // get gallery
        $galleryInput = $request->get( 'gallery' );

        // validate row
        if ( $model->row == null ) {
            $row = new Row();
            $model->row()->save( $row );
        } else {
            $row = $model->row;
        }

        // para determinar las imagenes eliminadas
        $requestGallery = array_column( $galleryInput, 'name' );
        $currentGallery = array_column( $row->multimedias->toArray(), 'name' );
        $deletedImages = array_values( array_diff( $currentGallery, $requestGallery ) );

        // dd( $galleryInput, $requestGallery, $currentGallery, $deletedImages );

        // eliminar imagenes
        foreach ( $deletedImages as $deletedImage ) {
            $multimedia = Multimedia::where( 'name', $deletedImage )->first();
            $rowMultimedia = RowsMultimedia::where( 'multimedia_id', $multimedia->id )->first();

            $multimedia->delete();
            $rowMultimedia->delete();
        }

        // nuevas imagenes
        foreach ( $galleryInput as $key => $imageInput ) {
            if ( array_key_exists( 'dataURL', $imageInput ) ) {
                // save image
                $fileName   = $folderName . '.' . time() . '.' . $imageInput[ 'filename' ];
                $mainImage  = saveFileFromBase64( $imageInput[ 'dataURL' ], $fileName, $folderName );

                if ($key === 0 && $hasMain) {
                    $model->image = $mainImage;
                    $model->save();
                }

                $multimedia         = new Multimedia();
                $multimedia->name   = basename( $mainImage );
                $multimedia->path   = str_replace( basename( $mainImage ), '', $mainImage );
                $multimedia->width  = $imageInput[ 'width' ];
                $multimedia->height = $imageInput[ 'height' ];
                $multimedia->save();

                $rows_multimedia = new RowsMultimedia();
                $rows_multimedia->row_id = $row->id;
                $rows_multimedia->multimedia_id = $multimedia->id;
                $rows_multimedia->save();
            }
        }
    }
}
