<?php

namespace App\Http\Controllers;

use App\Models\Admin\ActivityRequest;
use App\Models\Admin\Blog;
use App\Models\Admin\Booking;
use App\Models\Admin\NewsletterUser;
use App\User;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use SEO;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unreadActivityRequests = ActivityRequest::where( 'status_id', 6 )->count();
        $users                  = User::all()->count();
        $bookings               = Booking::where( 'status_id', 4 )->count();
        $newsletterUser         = NewsletterUser::count();

        return view( 'home' )
            ->with( 'unreadActivityRequests', $unreadActivityRequests )
            ->with( 'bookings', $bookings )
            ->with( 'newsletterUser', $newsletterUser )
            ->with( 'users', $users );
    }
}
