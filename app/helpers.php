<?php

use App\Models\Admin\Setting;

function objectToArray($object, $campo = null) {
    $array = json_decode(json_encode( $object ));
    if ($campo != null) {
        return array_column((array) $array, $campo);
    }
    else {
        return $array;
    }
}

function delete_col(&$array, $offset) {
    return array_walk($array, function (&$v) use ($offset) {
        array_splice($v, $offset, 1);
    });
}

function tags( $tagKey ) {
    return App\Http\Controllers\Admin\TagTranslationController::getTagsValues( $tagKey, \App::getLocale() );
}

/**
 * Get first model, otherwise factory one up
 *
 * @param string $modelClass
 */
function firstOrFactory(string $modelClass)
{
    return $modelClass::first() ?? factory($modelClass)->create();
}

/**
 * Method to save files in staorage
 * @param  string $base64 [description]
 * @param  string $folder [description]
 * @param  string $name   [description]
 * @return null
 */
function saveFile($file, $folder, $name)
{
    try {
        $fileName = $name . '-' . time() . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs( $folder, $fileName );
        return str_replace( 'public', 'storage', $path );
    } catch ( Exception $e ) {
        return [ 'error' => $e->getMessage() ];
    }
}

/**
 * Retrieve the nights in the given date range.
 *
 * @param string $checkin
 * @param string $checkout
 *
 * @return array $daysRequest
 */
function getNightsInRange( string $checkin, string $checkout )
{
    $daysRequest = [];
    $period = Carbon\CarbonInterval::days( 1 )->toPeriod( $checkin, $checkout );
    foreach ( $period as $key => $date ) {
        $daysRequest[][ 'night_date' ] = $date;
    }

    array_pop( $daysRequest ); // se omite el ultimo dia porque se trabaja solo con noches

    return $daysRequest;
}

function countNightsInRange( string $checkin, string $checkout )
{
    return count( getNightsInRange( $checkin, $checkout ) );
}

/**
 * Method to get setting by key
 * @param  string $key
 * @return strong
 */
function getConfig($key)
{
    $setting = Setting::where('key', $key)->first();
    if (! isset($setting)) {
        return '';
    }

    return $setting->value;
}

/**
 * Validate if a date interval overlaps with other.
 *
 * Para este proceso se manejan los siguientes cinco escenarios, del 1 al 4 son casos donde
 * la habitación no está disponible, en el caso cinco sí lo está.
 *
 * En cada uno de los casos, el rango A representa las fechas indicadas por el usuario, mientras que
 * el rango (o los rangos) B representan las fechas en donde la habitación está bloqueada, bien sea por
 * un booking ya realizado (tabla booking_details) o por un bloqueo temporal (tabla locked_rooms).
 *
 * Case 1: ( overlaps )
 * |---- Date Range A ----|                   _
 * _                   |---- Date Range B ----|
 *
 *
 * Case 2: ( overlaps )
 * _                   |---- Date Range A ----|
 * |---- Date Range B ----|                   _
 *
 *
 * Case 3: ( overlaps )
 * _         |---- Date Range A ----|         _
 * |-------------- Date Range B --------------|
 *
 *
 * Case 4: ( overlaps )
 * |-------------- Date Range A --------------|
 * _         |---- Date Range B ----|         _
 *
 *
 * Case 5: ( SUCCESSFUL )
 * _                      |---- Date Range A ----|                      _
 * |---- Date Range B ----|                      |---- Date Range B ----|
 *
 *
 * Case 6: ( overlaps )
 * |---- Date Range A ----|
 * |---- Date Range B ----|
 *
 *
 * Case 7: ( overlaps )
 * |------------- Date Range A ---------------|
 * |------------ Date Range B ----------|     _
 *
 *
 * Case 8: ( overlaps )
 * |-------------- Date Range A --------------|
 * _     |----------- Date Range B -----------|
 *
 *
 * @param array $firstDates {
 *     First date interval.
 *
 *     @type \Carbon\Carbon
 *     @type \Carbon\Carbon
 * }
 * @param array $secondDates {
 *     Second date interval.
 *
 *     @type \Carbon\Carbon
 *     @type \Carbon\Carbon
 * }
 * @param bool $closedIntervalValidation Determines if the interval validation
 *        will be closed (true) or open (false).
 *        @see https://www.matematicasonline.es/pdf/Temas/1BachCT/Intervalos%2C%20semirrectas%2C%20entornos%20y%20valor%20absoluto.pdf
 *        **Defaults to** `false`.
 *
 * @return bool
 */
function validateIfDatesOverlaps( $firstDates, $secondDates, $closedIntervalValidation = false ): bool
{
    $firstCheckin   = $firstDates[ 0 ];
    $firstCheckout  = $firstDates[ 1 ];

    $secondCheckin  =  $secondDates[ 0 ];
    $secondCheckout = $secondDates[ 1 ];

    // Check if overlap (Cases 1, 2 and 3)
    if ( $firstCheckin->between( $secondCheckin, $secondCheckout, $closedIntervalValidation ) ||
            $firstCheckout->between( $secondCheckin, $secondCheckout, $closedIntervalValidation ) ) {
        return true;
    }

    // Check if overlap (Cases 4, 7 and 8)
    if ( $secondCheckin->between( $firstCheckin, $firstCheckout, $closedIntervalValidation ) ||
            $secondCheckout->between( $firstCheckin, $firstCheckout, $closedIntervalValidation ) ) {
        return true;
    }

    // Check if overlap (Case 6)
    if ( $firstCheckin->eq( $secondCheckin ) && $firstCheckout->eq( $secondCheckout ) ) {
        return true;
    }

    // Not overlaps (case 5)
    return false;
}

/**
 * Method to save Multimedias to a model
 * @param  string $input BaseG4 file
 * @param  Model $model
 * @param  integer $width
 * @param  integer $height
 * @return [type]
 */
function saveMultimedia($input, Model $model, string $storePath = '/storage')
{
    if ($input['gallery']){
        if (! isset($model->row)) {
            $row = new Row();
            $model->row()->save($row);
        } else {
            $row =  $model->row;
            $row->rowsMultimedia()->delete();
        }


        foreach ($input['gallery'] as $key => $image) {
            if (isset($image['dataURL'])) {
                // $fileName = storeImage($image, '/storage');
                $fileName = storeImage($image, $storePath);
                $multimedia = new Multimedias();
                // \Log::info($filename);
                $multimedia->name = basename($fileName);
                $multimedia->path =  str_replace(basename($fileName), '', $fileName);
                $multimedia->save();
            } else {
                $multimedia = Multimedias::find($image['id']);
            }
                $rows_multimedia = new RowMultimedia();
                $rows_multimedia->row_id = $row->id;
                $rows_multimedia->multimedia_id = $multimedia->id;
                $rows_multimedia->save();
        }
    }
}

/**
 * Method to save files in storage from base 64
 * @param  string $base64Url [description]
 * @param  string $fileName [description]
 * @param  string $folderName   [description]
 * @return void
 */
function saveFileFromBase64( string $base64Url, string $fileName, string $folderName )
{
    try {
        //get the base-64 from data
        $base64String = substr( $base64Url, strpos( $base64Url, ',' ) + 1 );

        //decode base64 string
        $image = base64_decode( $base64String );

        // folder path
        $filePath = $folderName . '/' . $fileName;

        // store file
        Storage::disk( 'public' )->put( $filePath, $image );
        $storagePath = Storage::disk( 'public' )->getDriver()->getAdapter()->getPathPrefix();
        $realPath = $storagePath . $filePath;

        // return relative path from public path
        return str_replace( public_path(), '', $realPath );
    }
    catch ( Exception $e ) {
        return [ 'error' => $e->getMessage() ];
    }
}