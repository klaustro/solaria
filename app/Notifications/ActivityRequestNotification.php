<?php

namespace App\Notifications;

use App\Models\Admin\ActivityRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ActivityRequestNotification extends Notification
{
    private $activityRequest;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ActivityRequest $activityRequest)
    {
        //
        $this->activityRequest = $activityRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Richiesta esperienza')
                    ->greeting('Ciao !!')
                    ->line('Abbiamo ricevuto le tue informazioni correttamente. Ti contatteremo i prima possible')
                    ->action('Vedi più esperienze', url('/esperienze'))
                    ->line('La tua richiesta: ')
                    ->line('Esperienza: '. $this->activityRequest->activity_title)
                    ->line('Telefono: '. $this->activityRequest->phone)
                    ->line('Adulti: ' . $this->activityRequest->adult)
                    ->line('Bambini: ' . $this->activityRequest->children)
                    ->line('Animali: ' . $this->activityRequest->pets)
                    ->line('Messagio: ' . $this->activityRequest->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
