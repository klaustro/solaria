<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AllyActivityRequestNotification extends Notification
{
    private $activityRequest;

    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($activityRequest)
    {
        $this->activityRequest = $activityRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Richiesta esperienza')
                    ->greeting('Ciao !!')
                    ->line("Ciao, l'utente " . $this->activityRequest->name ." ha richiesto di iscriversi all'attività " . $this->activityRequest->activity_title)
                    ->action('Vedere esperienze', url('/esperienze'))
                    ->line($this->activityRequest->name . " informazioni:")
                    ->line('Email: ' . $this->activityRequest->email)
                    ->line('Telefono: ' . $this->activityRequest->phone)
                    ->line('Adulti: ' . $this->activityRequest->adult)
                    ->line('Bambini: ' . $this->activityRequest->children)
                    ->line('Animali: ' . $this->activityRequest->pets)
                    ->line('Messagio: ' . $this->activityRequest->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
