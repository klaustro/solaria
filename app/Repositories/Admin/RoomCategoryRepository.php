<?php

namespace App\Repositories\Admin;

use App\Models\Admin\RoomCategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoomCategoryRepository
 * @package App\Repositories\Admin
 * @version August 28, 2018, 11:03 pm CEST
 *
 * @method RoomCategory findWithoutFail($id, $columns = ['*'])
 * @method RoomCategory find($id, $columns = ['*'])
 * @method RoomCategory first($columns = ['*'])
*/
class RoomCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'status_id'
    ];

    /**
     * @var array
     */
    protected $customDefaultColumns = [
        'id',
        'image',
        'room_location_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoomCategory::class;
    }

    /**
     * Find customized data of repository.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return array
     */
    public function findCustomized( $id, $columns = null )
    {
        $columns = $columns ?? $this->customDefaultColumns;
        $data = $this->with( ['rooms'] );

        $data = $this->findWithoutFail( $id, $columns );

        return $data;
    }

    /**
     * Retrieve customized data of repository.
     *
     * @param array $columns
     *
     * @return array
     */
    public function getCustomized( $columns = null )
    {
        $columns = $columns ?? $this->customDefaultColumns;
        $data = $this->with( ['rooms'] );


        $data = $this->all();

        return $data;

        return $array;
    }
}
