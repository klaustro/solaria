<?php

namespace App\Repositories\Admin;

use App\Models\Admin\GeneralInformation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GeneralInformationRepository
 * @package App\Repositories\Admin
 * @version May 7, 2019, 6:35 pm CEST
 *
 * @method GeneralInformation findWithoutFail($id, $columns = ['*'])
 * @method GeneralInformation find($id, $columns = ['*'])
 * @method GeneralInformation first($columns = ['*'])
*/
class GeneralInformationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GeneralInformation::class;
    }
}
