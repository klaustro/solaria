<?php

namespace App\Repositories\Admin;

use App\Models\Admin\RoomsService;
use InfyOm\Generator\Common\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityDeleted;

/**
 * Class RoomsServiceRepository
 * @package App\Repositories\Admin
 * @version February 7, 2019, 7:59 pm CET
 *
 * @method RoomsService findWithoutFail($id, $columns = ['*'])
 * @method RoomsService find($id, $columns = ['*'])
 * @method RoomsService first($columns = ['*'])
*/
class RoomsServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'room_id',
        'service_id',
        'info'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoomsService::class;
    }

    /**
     * Force delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function forceDelete( $id )
    {
        $this->applyScope();

        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);

        $model = $this->find($id);
        $originalModel = clone $model;

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        $deleted = $model->forceDelete();

        event(new RepositoryEntityDeleted($this, $originalModel));

        return $deleted;
    }
}
