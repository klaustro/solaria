<?php

namespace App\Repositories\Admin;

use App\Models\Admin\BookingDetailPrice;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BookingDetailPriceRepository
 * @package App\Repositories\Admin
 * @version January 11, 2019, 10:11 pm CET
 *
 * @method BookingDetailPrice findWithoutFail($id, $columns = ['*'])
 * @method BookingDetailPrice find($id, $columns = ['*'])
 * @method BookingDetailPrice first($columns = ['*'])
*/
class BookingDetailPriceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'booking_detail_id',
        'type',
        'name',
        'price',
        'nights'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BookingDetailPrice::class;
    }
}
