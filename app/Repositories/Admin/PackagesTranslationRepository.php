<?php

namespace App\Repositories\Admin;

use App\Models\Admin\PackagesTranslation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PackagesTranslationRepository
 * @package App\Repositories\Admin
 * @version December 21, 2018, 5:27 pm CET
 *
 * @method PackagesTranslation findWithoutFail($id, $columns = ['*'])
 * @method PackagesTranslation find($id, $columns = ['*'])
 * @method PackagesTranslation first($columns = ['*'])
*/
class PackagesTranslationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'packages_id',
        'language_id',
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PackagesTranslation::class;
    }
}
