<?php

namespace App\Repositories\Admin;

use App\Models\Admin\LockedRoom;
use Carbon\Carbon;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class LockedRoomRepository
 * @package App\Repositories\Admin
 * @version October 30, 2018, 11:11 pm CET
 *
 * @method LockedRoom findWithoutFail($id, $columns = ['*'])
 * @method LockedRoom find($id, $columns = ['*'])
 * @method LockedRoom first($columns = ['*'])
*/
class LockedRoomRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'room_id',
        'locked_at',
        'checkin_date',
        'checkout_date'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LockedRoom::class;
    }

    /**
     * Lock the specified Room in the given range (checkin_date, checkout_date).
     *
     * @param int       $room_id
     * @param string    $checkin_date
     * @param string    $checkout_date
     * @param string    $datetime
     *
     * @return App\Models\Admin\LockedRoom|null
     */
    public function lockRoom( $room_id, string $checkin_date, string $checkout_date, $datetime )
    {
        // pregunto si la habitacion ya esta bloqueada
        if ( $this->isLockedRoom( $room_id, $checkin_date, $checkout_date ) ) {
            // retorno false
            return false;
        }

        $data = [
            'room_id'       => $room_id,
            'checkin_date'  => $checkin_date,
            'checkout_date' => $checkout_date,
            // 'locked_at'     => Carbon::now()->format('Y-m-d H:i:s')
            'locked_at'     => $datetime
        ];

        $lockedRoom = $this->create( $data );

        return $lockedRoom;
    }

    /**
     * Unlock the specified Room in the given range (checkin_date, checkout_date).
     *
     * @param int       $room_id
     * @param string    $checkin_date
     * @param string    $checkout_date
     *
     * @return boolean
     */
    public function unlockRoom( $room_id, $checkin_date, $checkout_date )
    {
        $data = [
            'room_id'       => $room_id,
            'checkin_date'  => $checkin_date,
            'checkout_date' => $checkout_date
        ];

        $unlocked = $this->deleteWhere( $data );

        return $unlocked;
    }

    /**
     * Check if a room is locked.
     *
     * @param int       $room_id
     * @param string    $checkin_date
     * @param string    $checkout_date
     * @param string    $datetime
     *
     * @return App\Models\Admin\LockedRoom
     */
    public function isLockedRoom( $room_id, string $checkin, string $checkout ): bool
    {
        // transformo en Carbon
        $checkin_request    = Carbon::createFromFormat( 'Y-m-d H', $checkin . ' 0' );
        $checkout_request   = Carbon::createFromFormat( 'Y-m-d H', $checkout . ' 0' );

        // obtener los LockedRooms recientes (< o = 5min) de esta habitacion
        $lockedRooms = $this->findWhere( [
            [ 'room_id',        '=', $room_id ],

             // se buscan solo las rooms cuyo locked_at sea mayor a hace cinco minutos
            [ 'locked_at',      '>', Carbon::now()->subMinutes( env( 'LOCKED_ROOM_TIME', 5 ) ) ]
        ] );

        // se verifica si algun bloqueo temporal reciente se solapa con la solicitud actual
        $lockedRooms = $lockedRooms->filter( function ( $item, $key ) use ( $checkin_request, $checkout_request ) {
            return validateIfDatesOverlaps( [ $checkin_request, $checkout_request ], [ $item->checkin_date, $item->checkout_date ] ) === true;
        } );

        return $lockedRooms->isEmpty() === false;
    }
}
