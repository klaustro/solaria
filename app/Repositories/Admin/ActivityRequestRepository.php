<?php

namespace App\Repositories\Admin;

use App\Models\Admin\ActivityRequest;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ActivityRequestRepository
 * @package App\Repositories\Admin
 * @version December 27, 2018, 4:49 pm CET
 *
 * @method ActivityRequest findWithoutFail($id, $columns = ['*'])
 * @method ActivityRequest find($id, $columns = ['*'])
 * @method ActivityRequest first($columns = ['*'])
*/
class ActivityRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'phone',
        'email',
        'activity_id',
        'adult',
        'children',
        'pets',
        'message'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ActivityRequest::class;
    }
}
