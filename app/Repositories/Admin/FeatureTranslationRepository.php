<?php

namespace App\Repositories\Admin;

use App\Models\Admin\FeatureTranslation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FeatureTranslationRepository
 * @package App\Repositories\Admin
 * @version December 20, 2018, 9:05 pm CET
 *
 * @method FeatureTranslation findWithoutFail($id, $columns = ['*'])
 * @method FeatureTranslation find($id, $columns = ['*'])
 * @method FeatureTranslation first($columns = ['*'])
*/
class FeatureTranslationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'feature_id',
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FeatureTranslation::class;
    }
}
