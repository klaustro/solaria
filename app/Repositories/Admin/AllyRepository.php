<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Ally;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AllyRepository
 * @package App\Repositories\Admin
 * @version January 7, 2019, 6:04 pm CET
 *
 * @method Ally findWithoutFail($id, $columns = ['*'])
 * @method Ally find($id, $columns = ['*'])
 * @method Ally first($columns = ['*'])
*/
class AllyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'phone',
        'email',
        'website'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ally::class;
    }
}
