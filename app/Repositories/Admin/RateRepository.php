<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Rate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RateRepository
 * @package App\Repositories\Admin
 * @version December 21, 2018, 5:27 pm CET
 *
 * @method Rate findWithoutFail($id, $columns = ['*'])
 * @method Rate find($id, $columns = ['*'])
 * @method Rate first($columns = ['*'])
*/
class RateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'room_id',
        'start_date',
        'end_date',
        'packages_id',
        'discount',
        'price',
        'iva',
        'num_people',
        'email',
        'blog_id',
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rate::class;
    }
}
