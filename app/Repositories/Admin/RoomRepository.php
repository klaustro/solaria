<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Room;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoomRepository
 * @package App\Repositories\Admin
 * @version August 28, 2018, 11:03 pm CEST
 *
 * @method Room findWithoutFail($id, $columns = ['*'])
 * @method Room find($id, $columns = ['*'])
 * @method Room first($columns = ['*'])
*/
class RoomRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'room_category_id',
        'status_id'
    ];

    /**
     * @var array
     */
    protected $customDefaultColumns = [
        'id',
        'image',
        'price',
        'iva',
        'room_category_id',
        'adults_quantity',
        'children_quantity'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Room::class;
    }

    /**
     * Retrieve customized data of repository.
     *
     * @param array|null $input
     * @param array $unavailableRooms
     * @param bool $returnAsAvailableRoomsWithoutPrice Para saber si se quiere retornar
     *        como disponibles las habitaciones sin precio.
     *
     * @return Collection
     */
    public function getCustomized( $input = null, $unavailableRooms = [], bool $returnAsAvailableRoomsWithoutPrice = false, bool $checkRatePrices = true )
    {
        $columns = $columns ?? $this->customDefaultColumns;

        // obtengo las noches que estan en el rango
        $nightsRequest = getNightsInRange( $input[ 'checkin' ], $input[ 'checkout' ] );

        // obtengo la cantidad de dias solicitados
        $totalRequestedDays = count( $nightsRequest );

        // obtener rooms disponibles
        $dataAvailable = $this->findWhereNotIn( 'id', $unavailableRooms, $columns );
        $dataAvailable->transform( function ( $room, $key ) use ( $input, $totalRequestedDays, $returnAsAvailableRoomsWithoutPrice, $checkRatePrices ) {
            $roomArray = $room->toArray();

            // calcular precio
            $roomArray = $this->getPrices( $roomArray, $input, $checkRatePrices );

            // se verifica si se quiere considerar las habitaciones con dias sin precio
            if ( $returnAsAvailableRoomsWithoutPrice === false ) {
                // disponibilidad cero (0) si hay dias que no tienen precio
                $totalDaysWithPrice = $roomArray[ 'price_detail_request' ][ 'totalDays' ];
                if ( $totalDaysWithPrice < $totalRequestedDays ) {
                    $roomArray[ 'availability' ] = 0;
                }
            }

            return $roomArray;
        } );
        $dataAvailable = collect( $dataAvailable->toArray() );

        // obtener rooms NO disponibles
        $dataUnavailable = $this->findWhereIn( 'id', $unavailableRooms, $columns );
        $dataUnavailable->transform( function ( $room, $key ) use ( $input, $checkRatePrices ) {
            $roomArray = $room->toArray();

            // calcular precio
            $roomArray = $this->getPrices( $roomArray, $input, $checkRatePrices );

            // disponibilidad cero (0) porque viene en el array '$unavailableRooms'
            $roomArray[ 'availability' ] = 0;

            return $roomArray;
        } );
        $dataUnavailable = collect( $dataUnavailable->toArray() );

        // union de rooms disponibles con NO disponibles
        $dataMerged = $dataAvailable->merge( $dataUnavailable );

        return $dataMerged;
    }

    public function getPrices( $roomArray, $input, bool $checkRatePrices = true )
    {
        /*
         * Asigno el price correspondiente segun la siguente escalera:
         * Ofertas
         *     Temporada(s)
         */
        $priceInfo = $this->model->getPriceInfo( Room::find( $roomArray[ 'id' ] ), $input, $checkRatePrices );

        $roomArray[ 'price_request' ]         = $priceInfo[ 'price' ];
        $roomArray[ 'iva_request' ]           = $priceInfo[ 'iva' ];

        $roomArray[ 'price_detail_request' ]  = $priceInfo[ 'price_detail' ];

        $roomArray[ 'price_first_date' ]      = $priceInfo[ 'price_first_date' ][ 'night_price' ];

        return $roomArray;
    }

    /**
     * Find customized data of repository.
     *
     * @param array $id
     * @param array $columns
     *
     * @return array
     */
    public function findCustomized( $id, $columns = null )
    {
        $columns = $columns ?? $this->customDefaultColumns;

        $data = $this->findWithoutFail($id, $columns);

        return $data->toArray();
    }

    /**
     * Obtener las habitaciones no disponibles en el rango de fechas indicado.
     *
     * @param string    $checkin
     * @param string    $checkout
     * @param int       $callbackModel Retorna el modelo desde el cual se obtendrán las habitaciones no disponibles
     *
     * @return
     */
    public function findUnavailableRooms( string $checkin, string $checkout, $callbackModel, string $checkinField = 'checkin_date', string $checkoutField = 'checkout_date' )
    {
        // transformo en Carbon
        $checkin_request    = Carbon::createFromFormat( 'Y-m-d H', $checkin . ' 0' );
        $checkout_request   = Carbon::createFromFormat( 'Y-m-d H', $checkout . ' 0' );

        // holgura que se tendra en la busqueda contra los booking ya realizados (6 meses).
        $start_range    = Carbon::createFromFormat( 'Y-m-d H', $checkin . ' 0' )->subMonths( 6 );
        $end_range      = Carbon::createFromFormat( 'Y-m-d H', $checkout . ' 0' )->addMonths( 6 );

        // filtros
        $model = $callbackModel( $start_range, $end_range );

        $filteredModel = $model->filter( function ( $modelItem ) use ( $checkin_request, $checkout_request, $checkinField, $checkoutField ) {
            return validateIfDatesOverlaps( [ $checkin_request, $checkout_request ], [ $modelItem->{ $checkinField }, $modelItem->{ $checkoutField } ] ) === true;
        } );

        $unavailableRooms = [];
        foreach ( $filteredModel as $unavailableRoom ) {
            $unavailableRooms[] = $unavailableRoom->room->id;
        }

        return array_unique( $unavailableRooms );
    }

    /**
     * Applies the given where conditions to the model WITH OR OPERATOR.
     *
     * @param array $where
     * @return void
     */
    protected function applyConditions( array $where )
    {
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                $this->model = $this->model->orWhere($field, $condition, $val);
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }
}
