<?php

namespace App\Repositories\Admin;

use App\Models\Admin\RoomCategoryService;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoomCategoryServiceRepository
 * @package App\Repositories\Admin
 * @version March 7, 2019, 8:28 pm CET
 *
 * @method RoomCategoryService findWithoutFail($id, $columns = ['*'])
 * @method RoomCategoryService find($id, $columns = ['*'])
 * @method RoomCategoryService first($columns = ['*'])
*/
class RoomCategoryServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'room_categories_id',
        'service_id',
        'info'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoomCategoryService::class;
    }
}
