<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Blog;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BlogRepository
 * @package App\Repositories\Admin
 * @version January 25, 2019, 10:06 pm CET
 *
 * @method Blog findWithoutFail($id, $columns = ['*'])
 * @method Blog find($id, $columns = ['*'])
 * @method Blog first($columns = ['*'])
*/
class BlogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'blog_category_id',
        'user_id',
        'status_id',
        'image',
        'rate_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Blog::class;
    }

    /**
     * Retrieve customized data of repository.
     *
     * @return Collection
     */
    public function getCustomized()
    {
        $blogs = $this->findByField( 'status_id', 1 )->sortByDesc( 'id' );

        return $blogs;
    }
}
