<?php

namespace App\Repositories\Admin;

use App\Models\Admin\TagTranslation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TagTranslationRepository
 * @package App\Repositories\Admin
 * @version December 12, 2018, 9:34 pm CET
 *
 * @method TagTranslation findWithoutFail($id, $columns = ['*'])
 * @method TagTranslation find($id, $columns = ['*'])
 * @method TagTranslation first($columns = ['*'])
*/
class TagTranslationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tag',
        'value',
        'front_section_id',
        'language_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TagTranslation::class;
    }
}
