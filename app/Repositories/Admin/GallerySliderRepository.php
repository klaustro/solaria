<?php

namespace App\Repositories\Admin;

use App\Models\Admin\GallerySlider;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GallerySliderRepository
 * @package App\Repositories\Admin
 * @version March 20, 2019, 10:09 pm CET
 *
 * @method GallerySlider findWithoutFail($id, $columns = ['*'])
 * @method GallerySlider find($id, $columns = ['*'])
 * @method GallerySlider first($columns = ['*'])
*/
class GallerySliderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'image',
        'status_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GallerySlider::class;
    }
}
