<?php

namespace App\Repositories\Admin;

use App\Models\Admin\policy;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class policyRepository
 * @package App\Repositories\Admin
 * @version February 12, 2019, 9:58 pm CET
 *
 * @method policy findWithoutFail($id, $columns = ['*'])
 * @method policy find($id, $columns = ['*'])
 * @method policy first($columns = ['*'])
*/
class policyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return policy::class;
    }
}
