<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Feature;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FeatureRepository
 * @package App\Repositories\Admin
 * @version December 20, 2018, 9:05 pm CET
 *
 * @method Feature findWithoutFail($id, $columns = ['*'])
 * @method Feature find($id, $columns = ['*'])
 * @method Feature first($columns = ['*'])
*/
class FeatureRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'iconblack',
        'iconwhite',
        'icongreen'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Feature::class;
    }
}
