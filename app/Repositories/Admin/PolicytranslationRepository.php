<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Policytranslation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PolicytranslationRepository
 * @package App\Repositories\Admin
 * @version February 12, 2019, 9:58 pm CET
 *
 * @method Policytranslation findWithoutFail($id, $columns = ['*'])
 * @method Policytranslation find($id, $columns = ['*'])
 * @method Policytranslation first($columns = ['*'])
*/
class PolicytranslationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Policytranslation::class;
    }
}
