<?php

namespace App\Repositories\Admin;

use App\Models\Admin\GeneralInformationTranslation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GeneralInformationTranslationRepository
 * @package App\Repositories\Admin
 * @version May 7, 2019, 6:35 pm CEST
 *
 * @method GeneralInformationTranslation findWithoutFail($id, $columns = ['*'])
 * @method GeneralInformationTranslation find($id, $columns = ['*'])
 * @method GeneralInformationTranslation first($columns = ['*'])
*/
class GeneralInformationTranslationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'general_information_id',
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GeneralInformationTranslation::class;
    }
}
