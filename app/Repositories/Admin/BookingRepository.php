<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Booking;
use App\Models\Admin\Invoice;
use App\Models\Admin\Room;
use App\User;
use Carbon\Carbon;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BookingRepository
 * @package App\Repositories\Admin
 * @version August 28, 2018, 11:03 pm CEST
 *
 * @method Booking findWithoutFail($id, $columns = [ '*' ])
 * @method Booking find($id, $columns = [ '*' ])
 * @method Booking first($columns = [ '*' ])
*/
class BookingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'user_id',
        'subtotal',
        'iva',
        'total',
        'comment',
        'data_form_id',
        'show_to_user',
        'status_id'
    ];

    /**
     * @var array
     */
    protected $customDefaultColumns = [
        'id',
        'code',
        'user_id',
        'subtotal',
        'iva',
        'total',
        'comment',
        'data_form_id',
        'show_to_user',
        'status_id',
        'created_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Booking::class;
    }

    public function createCustomized( $data )
    {
        // montos
        $total  = 0;
        $iva    = 0;

        // email (para las ofertas)
        $email = array_key_exists( 'email', $data ) ? $data[ 'email' ] : null;

        // token (para las ofertas)
        $token = array_key_exists( 'token', $data ) ? $data[ 'token' ] : null;

        // Save Booking
        $booking = $this->setBookingModel( $data );

        // Save data_form_id for Booking
        $dataForm = $this->setDataFormModel( $data[ 'personResponsible' ], $booking );
        $booking = $this->update( [ 'data_form_id' => $dataForm->id ], $booking->id );

        foreach ( $data[ 'rooms' ] as $key => $roomItem ) {

            // Save BookingDetail
            $bookingDetail = $this->setBookingDetailModel( $roomItem, $booking, $email, $token, $data );

            // Save data_form_id for BookingDetail
            $dataForm = $this->setDataFormModel( $roomItem[ 'personResponsible' ], $bookingDetail );
            $bookingDetail->update( [ 'data_form_id' => $dataForm->id ] );

            // montos para el Booking
            $total  += $bookingDetail->total_item;
            $iva    += $bookingDetail->iva_item;
        }

        // Update Booking (para guardar los montos y generar code)
        $bookingData = [];
        $bookingData[ 'subtotal' ]  = $total - $iva;
        $bookingData[ 'iva' ]       = $iva;
        $bookingData[ 'total' ]     = $total;
        $bookingData[ 'code' ]      = $this->generateCode();
        $booking = $this->update( $bookingData, $booking->id );

        return $booking;
    }

    /**
     * Set and store the booking.
     *
     * @param array $data
     *
     * @return Booking
     */
    private function setBookingModel( $data )
    {
        $booking = [];
        $user_id = $data[ 'personResponsible' ][ 'userId' ] ?? null;

        // Save user_id
        $user                   = User::find( $user_id );
        $booking[ 'user_id' ]   = $user->id ?? null;

        $booking[ 'comment' ]           =  $data[ 'comment' ]?? null;
        $booking[ 'status_id']          =  @$data['status'] ?? 4;
        $booking[ 'payment_method_id']  =  @$data['method_payment'] ?? 1;
        $booking[ 'code_payment']       =  $data['code_payment'] ?? null;
        $booking[ 'img_payment']        =  $data['img_payment'] ?? null;

        return $this->create($booking);
    }

    /**
     * Set and store the booking detail.
     *
     * @param object        $roomItem
     * @param Order         $booking
     * @param string|null   $email
     *
     * @return BookingDetail
     */
    private function setBookingDetailModel( $roomItem, $booking, $email, $token, $data )
    {
        $roomId = $roomItem[ 'roomId' ];


        $room = Room::find( $roomId );

        // booking datail
        $bookingDetail                          = [];
        $bookingDetail[ 'booking_id' ]          = $booking->id;
        $bookingDetail[ 'room_id' ]             = $room->id;

        // $bookingDetail[ 'payment_method_id' ]   = $method_payment ?? 1;
        $bookingDetail[ 'room_price' ]          = $room->price;
        $bookingDetail[ 'room_iva' ]            = $room->iva;
        $bookingDetail[ 'room_name' ]           = $room->name;
        //$bookingDetail[ 'persons_quantity' ]    = $roomItem[ 'personsQuantity' ];
        $bookingDetail[ 'adults_quantity' ]     = $roomItem[ 'adults_quantity' ] ?? null;
        $bookingDetail[ 'children_quantity' ]   = $roomItem[ 'children_quantity' ] ?? null;
        // Save checkin & checkout
        $bookingDetail[ 'checkin_date' ]        = $roomItem[ 'bookingDate' ][ 'checkin' ];
        $bookingDetail[ 'checkout_date' ]       = $roomItem[ 'bookingDate' ][ 'checkout' ];

        // calcular los dias del rango (checkin_date y checkout_date)
        $nights = count( getNightsInRange( $bookingDetail[ 'checkin_date' ], $bookingDetail[ 'checkout_date' ] ) );

        //Modificacion de precio por backoffice
        if (isset($data['price_backoffice']) && $data['price_backoffice']!=null) {
             $priceRatio = [
                  "price" => $data['price_backoffice'],
                  "iva" => $data['price_backoffice'] - ($data['price_backoffice']/1.22),
                  "price_detail" => [
                    "season" =>[
                      "Prezzo dell' Appartamento" =>[
                        "quantity" => $nights,
                        "price" => $data['price_backoffice']/$nights,
                      ]
                    ]
                  ]
                ];
        }else{
            $priceRatio = $room->getPriceInfo( $room, [
                'checkin'   => $bookingDetail[ 'checkin_date' ],
                'checkout'  => $bookingDetail[ 'checkout_date' ],
                'email'     => $email,
                'token'     => $token,
            ] );
        }

        // Save amounts
        $bookingDetail[ 'total_item' ]          = $priceRatio[ 'price' ];
        $bookingDetail[ 'iva_item' ]            = $priceRatio[ 'iva' ];

        $bookingDetailModel = $booking->bookingDetails()->create( $bookingDetail );


        foreach ( $priceRatio[ 'price_detail' ] as $type => $prices ) {

            if ( is_array( $prices ) === true ) {
                foreach ( $prices as $name => $detail ) {

                    $bookingDetailPrices = [];

                    $bookingDetailPrices[ 'type' ]      = $type;
                    $bookingDetailPrices[ 'name' ]      = $name;
                    $bookingDetailPrices[ 'price' ]     = $detail[ 'price' ];
                    $bookingDetailPrices[ 'nights' ]    = $detail[ 'quantity' ];

                    $bookingDetailModel->bookingDetailPrices()->create( $bookingDetailPrices );
                }
            }
        }

        return $bookingDetailModel;
    }

    /**
     * Set and store the form data for the given booking detail.
     *
     * @param array                                                         $data
     * @param App\Models\Admin\Booking | App\Models\Admin\BookingDetail     $model
     *
     * @return DataForm
     */
    private function setDataFormModel( $data, $model )
    {
        $dataForm = [];
        $dataForm[ 'name' ]           = $data[ 'name' ] ?? null;
        $dataForm[ 'email' ]          = $data[ 'email' ] ?? null;
        $dataForm[ 'phone' ]          = $data[ 'phone' ] ?? null;
        $dataForm[ 'fiscal_code' ]    = $data[ 'fiscalCode' ] ?? null;

        $dataFormModel = $model->dataForms()->create( $dataForm );

        return $dataFormModel;
    }

    /**
     * Generate code by creating a new invoice.
     */
    public function generateCode()
    {
        $invoice = Invoice::create( [ 'status' => 1 ] );
        $invoice->code = str_pad( $invoice->id, 4, '0', STR_PAD_LEFT );
        $invoice->save();

        return $invoice->code;
    }

    /**
     * Find customized data of repository.
     *
     * @param int   $id
     * @param array $columns
     *
     * @return array
     */
    public function findCustomized( $id, $columns = null )
    {
        $columns = $columns ?? $this->customDefaultColumns;
        $data = $this->with( [ 'rooms' ] );

        $data = $this->findWithoutFail( $id, $columns );

        return $data->toArray();
    }
}
