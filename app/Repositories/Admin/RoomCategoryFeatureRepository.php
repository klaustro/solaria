<?php

namespace App\Repositories\Admin;

use App\Models\Admin\RoomCategoryFeature;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RoomCategoryFeatureRepository
 * @package App\Repositories\Admin
 * @version February 6, 2019, 6:56 pm CET
 *
 * @method RoomCategoryFeature findWithoutFail($id, $columns = ['*'])
 * @method RoomCategoryFeature find($id, $columns = ['*'])
 * @method RoomCategoryFeature first($columns = ['*'])
*/
class RoomCategoryFeatureRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'room_category_id',
        'feature_id',
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RoomCategoryFeature::class;
    }
}
