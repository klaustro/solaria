<?php

namespace App\Models\Admin;

use App\Models\Admin\RoomCategoryFeature;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * Class RoomCategory
 * @package App\Models\Admin
 * @version August 28, 2018, 11:03 pm CEST
 *
 * @property \App\Models\Admin\Status status
 * @property \Illuminate\Database\Eloquent\Collection activities
 * @property \Illuminate\Database\Eloquent\Collection activityCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection activityTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionals
 * @property \Illuminate\Database\Eloquent\Collection blogCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection blogComments
 * @property \Illuminate\Database\Eloquent\Collection blogTranslations
 * @property \Illuminate\Database\Eloquent\Collection bookings
 * @property \Illuminate\Database\Eloquent\Collection brandTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection orderDetailsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orders
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection productCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productPresentationTranslations
 * @property \Illuminate\Database\Eloquent\Collection productPresentationsProducts
 * @property \Illuminate\Database\Eloquent\Collection productSubcategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection RoomCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection roomSeasonTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasons
 * @property \Illuminate\Database\Eloquent\Collection roomTranslations
 * @property \Illuminate\Database\Eloquent\Collection Room
 * @property \Illuminate\Database\Eloquent\Collection roomsServices
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceTranslations
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string code
 * @property integer status_id
 */
class RoomCategory extends Model
{
    use SoftDeletes;

    public $table = 'room_categories';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'status_id'
    ];

    public $fillable = [
        'status_id',
        'image',
        'room_location_id',
        'address_map',
        'address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'room_location_id'  => 'required',
        'name'              => 'required',
        'description'       => 'required',
    ];

    /**
     * Attributes to appends
     *
     * @var array
     */
    public $appends = [
        'name',
        'description',
        'services',
        'features',
        'gallery',
        'translations',
        'slug',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Admin\Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function location()
    {
        return $this->belongsTo(\App\Models\Admin\RoomLocation::class, 'room_location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(\App\Models\Admin\Service::class, 'rooms_categories_services','room_categories_id','service_id')->withPivot('info');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\\BelongsToMany
     **/
    public function features()
    {
        return $this->belongsToMany(\App\Models\Admin\Feature::class, 'room_category_features')->withPivot('value');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\\BelongsToMany
     **/
    public function roomCategoryFeatures()
    {
        return $this->hasMany(\App\Models\Admin\RoomCategoryFeature::class);
    }

    public function roomCategoryServices()
    {
        return $this->hasMany(\App\Models\Admin\RoomCategoryService::class, 'room_categories_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function rooms()
    {
        return $this->hasMany(\App\Models\Admin\Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function row()
    {
        return $this->morphOne('App\Models\Admin\Row', 'rowable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function translations()
    {
        return $this->hasMany(\App\Models\Admin\RoomCategoryTranslation::class);
    }

    /**
     * Return itemByLanguage.
     *
     * @return string
     */
    public function itemByLanguage( $language_code, $default_lan_id = 1 )
    {
        $translationItem = $this->translation( $language_code );

        // valor por defecto (ITALIANO) en caso de que no este en idioma solicitado
        if ( empty( $translationItem ) === true ) {
            $translationItem = $this->translation( 1 );
        }

        return $translationItem;
    }

    /**
     * Return the RoomCategoryTranslation in current languague.
     *
     * @return RoomCategoryTranslation
     */
    public function translation( $code = null )
    {
        if ( empty( $code ) === false ) {
            $attribute = ( gettype( $code ) === 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        if ( $language === null ) {
            return [];
        }

        $trans = $this->translations()->where( 'language_id', $language->id )->first();

        return $trans;
    }

    public function getNameAttribute()
    {
        return optional($this->translation())->name;
    }

    public function getDescriptionAttribute()
    {
        return optional($this->translation())->description;
    }

    /**
     * Get the slug in the given translation (Accessor).
     *
     * @return string
     */
    public function getSlugAttribute()
    {
        return Str::slug($this->name);
    }

    /**
     * Get the gallery of the room.
     * Get the feature of the room.
     *
     * @return array
     */
    public function getGalleryAttribute()
    {
        if (! isset($this->row)) {
            return null;
        }

        return $this->row()->first()->multimedias;
    }

    /**
     * Get the services of the room.
     *
     * @return array
     */
    public function getServicesAttribute()
    {
        return $this->services()->get();
    }

    /**
     * Get the feature of the room.
     *
     * @return array
     */
    public function getFeaturesAttribute()
    {
        return $this->features()->get();
    }

    /**
     * Get the description in the given translation (Accessor).
     *
     * @return string
     */
    public function getTranslationsAttribute()
    {
        return $this->translations()->get();
    }
}
