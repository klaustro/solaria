<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class GallerySlider
 * @package App\Models\Admin
 * @version March 20, 2019, 10:09 pm CET
 *
 * @property \App\Models\Admin\Status status
 * @property \Illuminate\Database\Eloquent\Collection additionalTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalsPresentations
 * @property \Illuminate\Database\Eloquent\Collection blogCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection blogComments
 * @property \Illuminate\Database\Eloquent\Collection blogTranslations
 * @property \Illuminate\Database\Eloquent\Collection brandTranslations
 * @property \Illuminate\Database\Eloquent\Collection configurationTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection flourTranslations
 * @property \Illuminate\Database\Eloquent\Collection floursPresentations
 * @property \Illuminate\Database\Eloquent\Collection GallerySliderTranslation
 * @property \Illuminate\Database\Eloquent\Collection ingredientTranslations
 * @property \Illuminate\Database\Eloquent\Collection offerSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection orderDetailAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orderDetailFlours
 * @property \Illuminate\Database\Eloquent\Collection orderDetailIngredients
 * @property \Illuminate\Database\Eloquent\Collection orderDetailPresentations
 * @property \Illuminate\Database\Eloquent\Collection orderDetailSauces
 * @property \Illuminate\Database\Eloquent\Collection orderDetails
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection policyTranslations
 * @property \Illuminate\Database\Eloquent\Collection presentationTranslations
 * @property \Illuminate\Database\Eloquent\Collection productCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productSubcategories
 * @property \Illuminate\Database\Eloquent\Collection productSubcategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productTranslations
 * @property \Illuminate\Database\Eloquent\Collection productsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection productsFlours
 * @property \Illuminate\Database\Eloquent\Collection productsIngredients
 * @property \Illuminate\Database\Eloquent\Collection productsPresentations
 * @property \Illuminate\Database\Eloquent\Collection productsSauces
 * @property \Illuminate\Database\Eloquent\Collection requestCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection sauceTranslations
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection sliderTranslations
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string code
 * @property string slug
 * @property string image
 * @property integer status_id
 */
class GallerySlider extends Model
{
    use SoftDeletes;

    public $table = 'gallery_sliders';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = [ 'deleted_at' ];


    public $fillable = [
        'slug',
        'image',
        'status_id'
    ];

    protected $appends = [
        'title',
        'translations',
        'gallery',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'code' => 'string',
        'image' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function row()
    {
        return $this->morphOne( 'App\Models\Admin\Row', 'rowable' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo( \App\Models\Admin\Status::class );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function translations()
    {
        return $this->hasMany( \App\Models\Admin\GallerySliderTranslation::class );
    }

    /**
     * Return the translation in current languague.
     *
     * @return translation
     **/
    public function translation( $code = null )
    {
        if ( !empty( $code ) ) {
            $attribute = ( gettype( $code ) == 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        return $this->translations()->get()->filter( function ( $translation ) use ( $language ) {
            return $translation->language_id == $language->id;
        } )->first();
    }

    /**
     * Get the title in the given translation (Accessor).
     *
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->translation()->title;
    }

    /**
     * Get the description in the given translation (Accessor).
     *
     * @return string
     */
    public function getTranslationsAttribute()
    {
        return $this->translations()->get();
    }

    /**
     * Get the gallery of the room.
     *
     * @return array
     */
    public function getGalleryAttribute()
    {
        return optional( $this->row()->first() )->multimedias;
    }
}
