<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BlogCategory
 * @package App\Models\Admin
 * @version January 25, 2019, 10:06 pm CET
 *
 * @property \App\Models\Admin\Status status
 * @property \Illuminate\Database\Eloquent\Collection activityCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection activityRequests
 * @property \Illuminate\Database\Eloquent\Collection activityTranslations
 * @property \Illuminate\Database\Eloquent\Collection BlogCategoryTranslation
 * @property \Illuminate\Database\Eloquent\Collection blogComments
 * @property \Illuminate\Database\Eloquent\Collection blogTranslations
 * @property \Illuminate\Database\Eloquent\Collection Blog
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection featureTranslations
 * @property \Illuminate\Database\Eloquent\Collection packageTranslations
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection rates
 * @property \Illuminate\Database\Eloquent\Collection requestCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection roomCategories
 * @property \Illuminate\Database\Eloquent\Collection roomCategoriesFeatures
 * @property \Illuminate\Database\Eloquent\Collection roomCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomLocationTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasonTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasons
 * @property \Illuminate\Database\Eloquent\Collection roomTranslations
 * @property \Illuminate\Database\Eloquent\Collection rooms
 * @property \Illuminate\Database\Eloquent\Collection roomsCategoriesServices
 * @property \Illuminate\Database\Eloquent\Collection roomsServices
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceTranslations
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string code
 * @property integer status_id
 */
class BlogCategory extends Model
{
    use SoftDeletes;

    public $table = 'blog_categories';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'code',
        'status_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Admin\Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function Translations()
    {
        return $this->hasMany(\App\Models\Admin\BlogCategoryTranslation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function blogs()
    {
        return $this->hasMany(\App\Models\Admin\Blog::class);
    }

    /**
     * Return the RoomLocationTranslation in current languague.
     *
     * @return RoomLocationTranslation
     **/
    public function translation( $code = null )
    {
        if ( empty( $code ) === false ) {
            $attribute = ( gettype( $code ) === 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        if ( $language === null ) {
            return [];
        }

        $trans = $this->translations()->where( 'language_id', $language->id )->first();

        return $trans;
    }

    /**
     * Get the name in the given translation (Accessor).
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return optional($this->translation())->name;
    }

    /**
     * Get the name in the given translation (Accessor).
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return optional($this->translation())->description;
    }

    /**
     * Return itemByLanguage.
     *
     * @return string
     */
    public function itemByLanguage( $language_code, $default_lan_id = 1 )
    {
        $translationItem = $this->translation( $language_code );

        // valor por defecto (ITALIANO) en caso de que no este en idioma solicitado
        if ( empty( $translationItem ) === true ) {
            $translationItem = $this->translation( 1 );
        }

        return $translationItem;
    }
}
