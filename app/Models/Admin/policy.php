<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class policy
 * @package App\Models\Admin
 * @version February 11, 2019, 6:20 pm CET
 *
 * @property \Illuminate\Database\Eloquent\Collection additionalTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalsPresentations
 * @property \Illuminate\Database\Eloquent\Collection brandTranslations
 * @property \Illuminate\Database\Eloquent\Collection configurationTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection flourTranslations
 * @property \Illuminate\Database\Eloquent\Collection floursPresentations
 * @property \Illuminate\Database\Eloquent\Collection ingredientTranslations
 * @property \Illuminate\Database\Eloquent\Collection orderDetailAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orderDetailFlours
 * @property \Illuminate\Database\Eloquent\Collection orderDetailIngredients
 * @property \Illuminate\Database\Eloquent\Collection orderDetailPresentations
 * @property \Illuminate\Database\Eloquent\Collection orderDetailSauces
 * @property \Illuminate\Database\Eloquent\Collection orderDetails
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection PoliciesTranslation
 * @property \Illuminate\Database\Eloquent\Collection presentationTranslations
 * @property \Illuminate\Database\Eloquent\Collection productCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productSubcategories
 * @property \Illuminate\Database\Eloquent\Collection productSubcategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productTranslations
 * @property \Illuminate\Database\Eloquent\Collection productsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection productsFlours
 * @property \Illuminate\Database\Eloquent\Collection productsIngredients
 * @property \Illuminate\Database\Eloquent\Collection productsPresentations
 * @property \Illuminate\Database\Eloquent\Collection productsSauces
 * @property \Illuminate\Database\Eloquent\Collection requestCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection policiesTranslations
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection sliderTranslations
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string slug
 */
class policy extends Model
{
    use SoftDeletes;

    public $table = 'policies';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'slug'
    ];

    protected $appends = [
        'translations'
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function policyTranslations()
    {
        return $this->hasMany(\App\Models\Admin\Policytranslation::class);
    }

    /**
     * Return the SauceTranslation in current languague.
     *
     * @return SauceTranslation
     **/
    public function policyTranslation($code = null)
    {
        if( !empty($code) ) {
            $attribute = ( gettype($code) == 'string' ) ? 'code' : 'id';
        }else {
            $code = \App::getLocale(); $attribute = 'code';
        }

        $language = Language::where($attribute, $code)->first();
        return $this->policyTranslations()->get()->filter(function($policyTranslation) use($language){
            return $policyTranslation->language_id == $language->id;
        })->first();
    }

    /**
     * Get the name in the given translation (Accessor).
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->policyTranslation()->name;
    }

    /**
     * Get the description in the given translation (Accessor).
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return $this->policyTranslation()->description;
    }

    /**
     * Get the description in the given translation (Accessor).
     *
     * @return string
     */
    public function getTranslationsAttribute()
    {
        return $this->policyTranslations()->get();
    }
}
