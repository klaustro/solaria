<?php

namespace App\Models\Admin;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BookingDetail
 * @package App\Models\Admin
 * @version August 28, 2018, 11:03 pm CEST
 *
 * @property \App\Models\Admin\Booking booking
 * @property \App\Models\Admin\PaymentMethod paymentMethod
 * @property \App\Models\Admin\Row row
 * @property \Illuminate\Database\Eloquent\Collection activities
 * @property \Illuminate\Database\Eloquent\Collection activityCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection activityTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionals
 * @property \Illuminate\Database\Eloquent\Collection blogCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection blogComments
 * @property \Illuminate\Database\Eloquent\Collection blogTranslations
 * @property \Illuminate\Database\Eloquent\Collection bookings
 * @property \Illuminate\Database\Eloquent\Collection brandTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection orderDetailsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orders
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection productCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productPresentationTranslations
 * @property \Illuminate\Database\Eloquent\Collection productPresentationsProducts
 * @property \Illuminate\Database\Eloquent\Collection productSubcategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection roomCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasonTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasons
 * @property \Illuminate\Database\Eloquent\Collection roomTranslations
 * @property \Illuminate\Database\Eloquent\Collection rooms
 * @property \Illuminate\Database\Eloquent\Collection roomsServices
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceTranslations
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property date checkin_date
 * @property date checkout_date
 * @property integer persons_amount
 * @property integer booking_id
 * @property integer room_id
 * @property integer payment_method_id
 */
class BookingDetail extends Model
{
    use SoftDeletes;

    public $table = 'booking_details';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',

        'booking_id',
        'data_form_id',

        'room_name',
        'room_price',
        'room_iva'
    ];


    public $fillable = [
        'booking_id',
        'room_id',

        'room_price',
        'room_iva',
        'room_name',

        'persons_quantity',
        'data_form_id',
        'iva_item',
        'total_item',
        'checkin_date',
        'checkout_date',
        'adults_quantity',
        'children_quantity'

    ];

    protected $appends = [
        'name',
        'price',
        'bookingDate',
        'personResponsible',
        'room_category_id',
        'price_detail',
        'checkin_date_italian',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'booking_id' => 'integer',
        'room_id' => 'integer',

        'room_price' => 'float',
        'room_iva' => 'float',
        'room_name' => 'string',

        'persons_quantity' => 'integer',
        'data_form_id' => 'integer',
        'iva_item' => 'float',
        'total_item' => 'float',
        'checkin_date' => 'date',
        'checkout_date' => 'date',

        'adults_quantity' => 'integer',
        'children_quantity' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function booking()
    {
        return $this->belongsTo(\App\Models\Admin\Booking::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function bookingDetailPrices()
    {
        return $this->hasMany(\App\Models\Admin\BookingDetailPrice::class);
    }

    /**
     * Get the details of prices of the Booking.
     *
     * @return array
     */
    public function getPriceDetailAttribute()
    {
        $nightsDetail = collect( $this->bookingDetailPrices()->get()->toArray() );
        // dd( $nightsDetail );

        // para obtener el desglose del total por el rango (temporadas y promocioness)
        // $nightsDetail = collect( array_column( $priceDetails, 'type' ) );
        // dd( $nightsDetail );

        $nightsDetail = $nightsDetail->groupBy( [ 'type', 'name' ] );
        // dd( $nightsDetail );

        // foreach ( $nightsDetail as $keyNightType => $nightType ) {
        //     foreach ( $nightType as $keyNightTypeItem => $nightTypeItem ) {
        //         $nightType[ $keyNightTypeItem ] = [
        //             'quantity'  => count( $nightTypeItem ),
        //             'price'     => $nightTypeItem[ 0 ][ 'price' ],
        //         ];
        //     }
        // }

        // foreach (  as $key => $value ) {
        //     # code...
        // }

        return $nightsDetail;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function room()
    {
        return $this->belongsTo(\App\Models\Admin\Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function dataForms()
    {
        return $this->belongsTo(\App\Models\Admin\DataForm::class, 'data_form_id');
    }

    /**
     * Name of the room booked.
     *
     * @return array
     */
    public function getNameAttribute()
    {
        return $this->room_name;
    }

    /**
     * Price of the room booked.
     *
     * @return array
     */
    public function getPriceAttribute()
    {
        return $this->room_price;
    }

    /**
     * Object BookingDate that contains checkin_date y checkout_date.
     *
     * @return array
     */
    public function getBookingDateAttribute()
    {
        return [
            'checkin'   => $this->checkin_date->toDateString(),
            'checkout'  => $this->checkout_date->toDateString()
        ];
    }

    /**
     * Get the responsable of the BookingDetail.
     *
     * @return array
     */
    public function getPersonResponsibleAttribute()
    {
        return $this->booking->dataForms()->first();
    }

    /**
     * Get the responsable of the BookingDetail.
     *
     * @return array
     */
    public function getPaymentMethodsAttribute()
    {
        return $this->paymentMethod()->get();
    }

    /**
     * Get the responsable of the BookingDetail.
     *
     * @return array
     */
    public function getRoomCategoryIdAttribute()
    {
        return $this->room->room_category_id;
    }

    /**
     * Get date in italian format.
     *
     * @return array
     */
    public function getCheckinDateItalianAttribute()
    {
        return Carbon::createFromFormat( 'Y-m-d H:i:s', $this->checkin_date )->format( 'd/m/Y' );
    }

    /**
     * Get date in italian format.
     *
     * @return array
     */
    public function getCheckoutDateItalianAttribute()
    {
        return Carbon::createFromFormat( 'Y-m-d H:i:s', $this->checkout_date )->format( 'd/m/Y' );
    }

    /**
     * ACCESSOR iva_item.
     *
     * @return string
     */
    // public function getIvaItemAttribute( $iva_item )
    // {
    //     return number_format( $iva_item, 2, '.', '' );
    // }

    /**
     * ACCESSOR total_item.
     *
     * @return string
     */
    // public function getTotalItemAttribute( $total_item )
    // {
    //     return number_format( $total_item, 2, '.', '' );
    // }
}
