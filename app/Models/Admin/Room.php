<?php

namespace App\Models\Admin;

use App\Models\Admin\Language;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Room
 * @package App\Models\Admin
 * @version August 28, 2018, 11:03 pm CEST
 *
 * @property \App\Models\Admin\RoomCategory roomCategory
 * @property \App\Models\Admin\Status status
 * @property \Illuminate\Database\Eloquent\Collection activities
 * @property \Illuminate\Database\Eloquent\Collection activityCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection activityTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionals
 * @property \Illuminate\Database\Eloquent\Collection blogCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection blogComments
 * @property \Illuminate\Database\Eloquent\Collection blogTranslations
 * @property \Illuminate\Database\Eloquent\Collection bookings
 * @property \Illuminate\Database\Eloquent\Collection brandTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection orderDetailsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orders
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection productCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productPresentationTranslations
 * @property \Illuminate\Database\Eloquent\Collection productPresentationsProducts
 * @property \Illuminate\Database\Eloquent\Collection productSubcategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection roomCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasonTranslations
 * @property \Illuminate\Database\Eloquent\Collection RoomSeason
 * @property \Illuminate\Database\Eloquent\Collection RoomTranslation
 * @property \Illuminate\Database\Eloquent\Collection RoomsService
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceTranslations
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string slug
 * @property integer room_category_id
 * @property integer status_id
 */
class Room extends Model
{
    use SoftDeletes;

    public $table = 'rooms';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'status_id',
        'subtitle1',
        'subtitle2'
    ];

    public $fillable = [
        'slug',
        'image',
        'room_category_id',
        'status_id',
        'adults_quantity',
        'children_quantity'
    ];

    protected $appends = [
        'name',
        'subtitle1',
        'subtitle2',
        'description',
        'availability',
        'services',
        'gallery',
        'translations',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'room_category_id' => 'integer',
        'status_id' => 'integer',
        'adults_quantity' => 'integer',
        'children_quantity' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'room_category_id'  => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function roomCategory()
    {
        return $this->belongsTo(\App\Models\Admin\RoomCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Admin\Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function roomSeasons()
    {
        return $this->hasMany(\App\Models\Admin\RoomSeason::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function rates()
    {
        return $this->hasMany(\App\Models\Admin\Rate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function translations()
    {
        return $this->hasMany(\App\Models\Admin\RoomTranslation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function lockedRooms()
    {
        return $this->hasMany(\App\Models\Admin\LockedRoom::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function bookingDetails()
    {
        return $this->hasMany(\App\Models\Admin\BookingDetail::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     **/
    public function services()
    {
        return $this->belongsToMany(\App\Models\Admin\Service::class, 'rooms_services')->withPivot('info');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function row()
    {
        return $this->morphOne('App\Models\Admin\Row', 'rowable');
    }

    /**
     * Return the RoomTranslation in current languague.
     *
     * @return RoomTranslation
     */
    public function translation( $code = null )
    {
        if ( empty( $code ) === false ) {
            $attribute = ( gettype( $code ) === 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        if ( $language === null ) {
            return [];
        }

        $trans = $this->translations()->where( 'language_id', $language->id )->first();

        return $trans;
    }

    /**
     * Return itemByLanguage.
     *
     * @return string
     */
    public function itemByLanguage( $language_code, $default_lan_id = 1 )
    {
        $translationItem = $this->translation( $language_code );

        // valor por defecto (ITALIANO) en caso de que no este en idioma solicitado
        if ( empty( $translationItem ) === true ) {
            $translationItem = $this->translation( 1 );
        }

        return $translationItem;
    }

    /**
     * Get the name in the given translation (Accessor).
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return optional($this->translation())->name;
    }

    /**
     * Get the subtitle1 in the given translation (Accessor).
     *
     * @return string
     */
    public function getSubtitle1Attribute()
    {
        return optional($this->translation())->subtitle1;
    }

    /**
     * Get the subtitle2 in the given translation (Accessor).
     *
     * @return string
     */
    public function getSubtitle2Attribute()
    {
        return optional($this->translation())->subtitle2;
    }

    /**
     * Get the description in the given translation (Accessor).
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return optional($this->translation())->description;
    }

    /**
     * Get the description in the given translation (Accessor).
     *
     * @return string
     */
    public function getTranslationsAttribute()
    {
        return $this->translations()->get();
    }

    /**
     * Get the price in the current season.
     *
     * @return string
     */
    /*public function getPriceAndIva()
    {
        $dt = Carbon::now();

        $currentRoomSeason = $this->roomSeasons()->get()->filter( function ( $roomSeason, $key ) use ( $dt ) {

            // si la fecha de inicio de temporada es menor o igual a hoy
            // y si la fecha de fin de temporada es mayor o igual a hoy
            if ( $roomSeason->start_date->lessThanOrEqualTo( $dt ) &&
                 $roomSeason->end_date->greaterThanOrEqualTo( $dt ) ) {

                // entonces devuelvo el registro de esa temporada
                return true;
            }
        } );

        return $currentRoomSeason->first();
    }*/

    /**
     * Get the price in the current season.
     *
     * @return string
     */
    /*public function getPriceAttribute()
    {
        if ( $this->getPriceAndIva() !== null ) {
            return optional( $this->getPriceAndIva() )->price;
        }
        else {
            return $this->price;
        }
    }*/

    /**
     * Get the price in the current season.
     *
     * @return string
     */
    /*public function getIvaAttribute()
    {
        return optional( $this->getPriceAndIva() )->price * optional( $this->getPriceAndIva() )->iva;
    }*/

    /**
     * Availability.
     *
     * @return int
     */
    public function getAvailabilityAttribute()
    {
        return 1;
    }

    /**
     * Get the services of the room.
     *
     * @return array
     */
    public function getServicesAttribute()
    {
        return $this->services()->with('translations')->get();
    }

    /**
     * Get the gallery of the room.
     *
     * @return array
     */
    public function getGalleryAttribute()
    {
        return optional($this->row()->first())->multimedias;
    }

    /**
     * Retrieve the total price for the room.
     *
     * @param Room          $room
     * @param array         $input
     *
     * @return array
     */
    public function getPriceInfo( Room $room, array $input, bool $checkRates = true )
    {
        // obtengo los valores del rango a buscar (con Carbon)
        $checkinRequest = Carbon::createFromFormat( 'Y-m-d H', $input[ 'checkin' ] . ' 0' );
        $checkoutRequest = Carbon::createFromFormat( 'Y-m-d H', $input[ 'checkout' ] . ' 0' );

        // obtengo las noches que estan en el rango
        $nightsRequest = getNightsInRange( $input[ 'checkin' ], $input[ 'checkout' ] );

        // la primera noche
        $firstNigth = $nightsRequest[ 0 ][ 'night_date' ];

        // email (para las ofertas)
        $email = array_key_exists( 'email', $input ) ? $input[ 'email' ] : null;

        // token (para las ofertas)
        $token = array_key_exists( 'token', $input ) ? $input[ 'token' ] : null;

        // pregunto si se debe validar sobre ofertas
        if ( $checkRates === true ) {
            // validar si la fecha solicitada coincide con una oferta de principio a fin
            $filteredRates = $this->checkFullRate( $room, $checkinRequest, $checkoutRequest, $email, $token );
        }
        else {
            $filteredRates = collect();
        }

        // recorro cada una de las noches del rango
        foreach ( $nightsRequest as $keyNightRequest => $nightRequest ) {

            // obtener precio para un solo dia
            $nightPrice = $this->getSinglePrice( $room, $nightRequest[ 'night_date' ], $email, $token, $checkRates );

            $nightsRequest[ $keyNightRequest ] = $nightPrice;
        }

        if ( $filteredRates->isEmpty() === true ) {
            // para obtener precio e iva total por el rango
            $price  = array_sum( array_column( $nightsRequest, 'night_price' ) );
            $iva    = array_sum( array_column( $nightsRequest, 'night_iva' ) );
        }
        else {
            // se obtiene el precio de la oferta
            $price = $filteredRates->first()->price_total;
            $iva = $filteredRates->first()->price_total - ( $filteredRates->first()->price_total / ( 1 + $filteredRates->first()->iva ) );
        }

        // para obtener el desglose del total por el rango (temporadas y promocioness)
        $nightsDetail = collect( array_column( $nightsRequest, 'night_type' ) );
        $nightsDetail = $nightsDetail->groupBy( [ 'type', 'name' ] );

        // total days tanto de seasons como de rates
        $totalDays = 0;

        foreach ( $nightsDetail as $keyNightType => $nightType ) {
            // total dias por tipo de precio (season o rate)
            $days = 0;

            foreach ( $nightType as $keyNightTypeItem => $nightTypeItem ) {
                // obtengo todas las promociones que tiene la habitacion (en caso de que tenga)
                $rates = $room->rates()->get()->sortBy( 'created_at' )->values();

                $ratesEmail =

                $nightType[ $keyNightTypeItem ] = [
                    'quantity'  => count( $nightTypeItem ),
                    'price'     => $nightTypeItem[ 0 ][ 'price' ],

                    'price_total'               => @$nightTypeItem[ 0 ][ 'price_total' ],
                    'price_total_without_rate'  => @$nightTypeItem[ 0 ][ 'price_total_without_rate' ],
                    'discount'                  => @$nightTypeItem[ 0 ][ 'discount' ],
                    'email'                     => @$nightTypeItem[ 0 ][ 'email' ],
                ];

                // sumatoria para los dias de season o rate
                $days += count( $nightTypeItem );
            }

            // sumatoria para el total de dias
            $totalDays += $days;
        }

        // obtener el total de dias tanto de seasons como de rates
        $nightsDetail[ 'totalDays' ] = $totalDays;

        // cable para acomodar el iva
        $iva = $price - ( $price / ( 1 + 0.22 ) );

        // retorno los valores finales
        return [
            'price'         => $price,
            'iva'           => $iva,
            'price_detail'  => $nightsDetail->toArray(),

            // precio de la primera noche
            'price_first_date' => $this->getSinglePrice( $room, $firstNigth, $email, $token, $checkRates ),
        ];
    }

    /**
     * Retrieve the price for the room in the given date.
     *
     * @param Room $room
     * @param Carbon $nightDate
     * @param string|null $email
     * @param bool $checkRates
     *
     * @return array
     */
    protected function getSinglePrice( Room $room, Carbon $nightDate, $email = null, $token = null, bool $checkRates = true )
    {
        // pregunto si se debe validar sobre ofertas
        if ( $checkRates === true ) {
            // valido si el dia esta en una OFERTA, y guardo su precio en caso afirmativo
            $nightRate = $this->checkRates( $room, $nightDate, $email, $token );
            if ( !empty( $nightRate ) ) {
                return $nightRate;
            }
        }

        // valido si el dia esta en una TEMPORADA, y guardo su precio en caso afirmativo
        $nightSeason = $this->checkSeasons( $room, $nightDate );
        if ( !empty( $nightSeason ) ) {
            return $nightSeason;
        }

        // Se comenta debido a que se retira el precio minimo de habitacion.
        /*
        // guardo el PRECIO DEL ROOM en caso de que no exista ni temporada ni oferta
        $nightDefault = $this->getRoomDefaultPrice( $room );
        if ( !empty( $nightDefault ) ) {
            return $nightDefault;
        }
        */

        return null;
    }

    /**
     * Check if the given night is inside a rate.
     *
     * @param Room  $room
     * @param array $nightDate
     *
     * @return array $nightRate
     */
    private function checkRates( Room $room, Carbon $nightDate, $email, $token )
    {
        // obtengo todas las promociones que tiene la habitacion (en caso de que tenga)
        $rates = $room->rates()->get()->sortBy( 'created_at' )->values();

        // array
        $nightRate = [];

        // recorro las promociones
        foreach ( $rates as $keyRate => $rate ) {

            // valido que pueda recibir la oferta
            if ( $this->matchesEmail( $email, $rate ) === false && $this->matchesToken( $token, $rate ) === false ) {
                continue;
            }

            // pregunto si es el ultimo dia de la oferta
            if ( $nightDate->eq( $rate->end_date ) ) {
                continue;
            }

            // pregunto si este dia del rango esta dentro de una promocion
            if ( $nightDate->between( $rate->start_date, $rate->end_date ) ) {

                // obtengo array con los días de la semana de que contempla el paquete de esta promoción
                $daysPackage = explode( ',', $rate->package->date );

                // pregunto si este día es un día de la semana que contempla el paquete de esta promoción
                if ( in_array( $nightDate->dayOfWeek, $daysPackage ) ) {

                    // sumo el precio de la promocion
                    $nightRate[ 'night_price' ] = $rate->price;
                    $nightRate[ 'night_iva' ]   = $rate->price - ( $rate->price / ( 1 + $rate->iva ) );

                    // indico la relacion de precio
                    $nightRate[ 'night_type' ]  = [
                        'type'      => 'rate',
                        'name'      => $rate->package_name . ' #' . $rate->id,
                        'price'     => $rate->price,

                        'price_total'               => $rate->price_total,
                        'price_total_without_rate'  => $rate->price_temp,
                        'discount'                  => $rate->discount,

                        'iva'       => $rate->iva,
                        'email'     => $rate->email,

                    ];

                    continue;
                }
            }
        }

        return $nightRate;
    }

    private function matchesEmail( $email, $rate )
    {
        $matches = false;

        // valido que venga email
        if ( $email !== null ) {
            // valido que la oferta tenga email
            if ( $rate->email !== null ) {

                // valido que el email enviado sea igual al de la oferta
                if ( $rate->email === $email ) {
                    $matches = true;
                }
                else {
                    $matches = false;
                }
            }
            else {
                $matches = true;
            }
        }
        else if ( $rate->email === null ) {
            $matches = true;
        }

        return $matches;
    }

    private function matchesToken( $token, $rate )
    {
        $matches = false;

        // valido que venga token
        if ( $token !== null ) {
            // valido que la oferta tenga token
            if ( $rate->token !== null ) {

                // valido que el token enviado sea igual al de la oferta
                if ( $rate->token === $token ) {
                    $matches = true;
                }
                else {
                    $matches = false;
                }
            }
            else {
                $matches = true;
            }
        }
        else if ( $rate->token === null ) {
            $matches = true;
        }

        return $matches;
    }

    /**
     * Check if the given night is inside a season.
     *
     * @param Room  $room
     * @param array $nightDate
     *
     * @return array $nightSeason
     */
    private function checkSeasons( Room $room, Carbon $nightDate )
    {
        // obtengo todas las temporadas que tiene la habitacion (en caso de que tenga)
        $seasons = $room->roomSeasons->sortBy( 'start_date' );

        // array
        $nightSeason = [];

        // recorro las temporadas
        foreach ( $seasons as $keySeason => $season ) {

            // pregunto si es el ultimo dia de la oferta
            if ( $nightDate->eq( $season->end_date ) ) {
                continue;
            }

            // pregunto si este dia del rango esta dentro de la temporada
            if ( $nightDate->between( $season->start_date, $season->end_date ) ) {

                // sumo el precio de la temporada
                $nightSeason[ 'night_price' ] = $season->price;
                $nightSeason[ 'night_iva' ]   = $season->price - ( $season->price / ( 1 + $season->iva ) );

                // calcular precio total
                $nights = countNightsInRange( $season->start_date->format( 'Y-m-d' ), $season->end_date->format( 'Y-m-d' ) );
                $price_total = $season->price * $nights;

                // indico la relacion de precio
                $nightSeason[ 'night_type' ]  = [
                    'type'          => 'season',
                    'name'          => $season->start_date->format( 'd/m/Y' ) . ' - ' . $season->end_date->format( 'd/m/Y' ),
                    'price'         => $season->price,

                    'price_total'   => $price_total,

                    'iva'           => $season->iva,
                ];

                break;
            }
        }

        return $nightSeason;
    }

    /**
     * Validacion de oferta de principio a fin
     */
    private function checkFullRate( $room, $checkinRequest, $checkoutRequest, $email, $token )
    {
        $rates = $room->rates()->get()->sortBy( 'created_at' )->values();

        $filteredRates = $rates->filter( function ( $rate, $key ) use ( $checkinRequest, $checkoutRequest, $email, $token ) {
            // pregunto si la oferta coincide exactamente con el rango seleccionado (de la solicitud de booking)
            $sameDays = $rate->start_date->format( 'Y-m-d' ) === $checkinRequest->format( 'Y-m-d' )
                && $rate->end_date->format( 'Y-m-d' ) === $checkoutRequest->format( 'Y-m-d' );

            // valido que pueda recibir la oferta
            $matchesEmail = $this->matchesEmail( $email, $rate );

            // valido si el token es valido
            $isValidToken = $this->matchesToken( $token, $rate );

            return $sameDays && ( $matchesEmail || $isValidToken );
        } );

        return $filteredRates;
    }

    /**
     * get Room default price.
     *
     * @param Room  $room
     *
     * @return array $nightDefault
     */
    private function getRoomDefaultPrice( Room $room )
    {
        // array
        $nightDefault = [];

        // sumo el precio de la temporada
        $nightDefault[ 'night_price' ] = $room->price;
        $nightDefault[ 'night_iva' ]   = $room->price - ( $room->price / ( 1 + $room->iva ) );

        // indico la relacion de precio
        $nightDefault[ 'night_type' ]  = [
            'type'      => 'season',
            'name'      => 'Prezzo dell\'Appartamento',
            'price'     => $room->price,
            'iva'       => $room->iva,
        ];

        return $nightDefault;
    }
}
