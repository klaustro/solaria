<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Package
 * @package App\Models\Admin
 * @version December 21, 2018, 5:27 pm CET
 *
 * @property \Illuminate\Database\Eloquent\Collection activities
 * @property \Illuminate\Database\Eloquent\Collection activityCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection activityTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection featureTranslations
 * @property \Illuminate\Database\Eloquent\Collection PackageTranslation
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection Rate
 * @property \Illuminate\Database\Eloquent\Collection requestCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection roomCategories
 * @property \Illuminate\Database\Eloquent\Collection roomCategoriesFeatures
 * @property \Illuminate\Database\Eloquent\Collection roomCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomLocationTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasonTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasons
 * @property \Illuminate\Database\Eloquent\Collection roomTranslations
 * @property \Illuminate\Database\Eloquent\Collection rooms
 * @property \Illuminate\Database\Eloquent\Collection roomsCategoriesServices
 * @property \Illuminate\Database\Eloquent\Collection roomsServices
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceTranslations
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string date
 * @property integer min_nights
 * @property integer max_nights
 */
class Package extends Model
{
    use SoftDeletes;

    public $table = 'packages';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'date',
        'min_nights',
        'max_nights'
    ];

    protected $appends = [
        'name',
        'description',
        'date_array',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'date' => 'string',
        'min_nights' => 'integer',
        'max_nights' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function translations()
    {
        return $this->hasMany(\App\Models\Admin\PackagesTranslation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function rates()
    {
        return $this->hasMany(\App\Models\Admin\Rate::class);
    }

    /**
     * Return the RoomLocationTranslation in current languague.
     *
     * @return PackagesTranslation
     **/
    public function translation( $code = null )
    {
        if ( empty( $code ) === false ) {
            $attribute = ( gettype( $code ) === 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        if ( $language === null ) {
            return [];
        }

        $trans = $this->translations()->where( 'language_id', $language->id )->first();

        return $trans;
    }

    /**
     * Return itemByLanguage.
     *
     * @return string
     */
    public function itemByLanguage( $language_code, $default_lan_id = 1 )
    {
        $translationItem = $this->translation( $language_code );

        // valor por defecto (ITALIANO) en caso de que no este en idioma solicitado
        if ( empty( $translationItem ) === true ) {
            $translationItem = $this->translation( 1 );
        }

        return $translationItem;
    }

    // /**
    //  * Return the ProductCategoryTranslation in current languague.
    //  *
    //  * @return ProductCategoryTranslation
    //  **/
    // public function packagesTranslation()
    // {
    //     $language = Language::where('code', \App::getLocale())->first();
    //     return $this->packageTranslations->filter(function($packagesTranslation) use($language){
    //         return $packagesTranslation->language_id == $language->id;
    //     })->first();
    // }

    /**
     * Get the name in the given translation (Accessor).
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return isset($this->translation()->name) ? $this->translation()->name : '';
    }

    /**
     * Get the description in the given translation (Accessor).
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return isset($this->translation()->description) ? $this->translation()->description : '';
    }

    /**
     * Get the array of days.
     *
     * @return string
     */
    public function getDateArrayAttribute()
    {
        $dict = [
            0 => 'domenica',
            1 => 'lunedi',
            2 => 'martedì',
            3 => 'mercoledì',
            4 => 'giovedi',
            5 => 'venerdì',
            6 => 'sabato',
        ];

        $days = explode( ',', $this->date );

        $daysObjects = [];
        foreach ( $days as $day ) {
            $daysObjects[] = [
                'value' =>  $day,
                'label' =>  $dict[ $day ]
            ];
        }

        return $daysObjects;
    }
}
