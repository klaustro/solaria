<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Policytranslation
 * @package App\Models\Admin
 * @version February 12, 2019, 5:34 pm CET
 *
 * @property \App\Models\Admin\Language language
 * @property \App\Models\Admin\Policy policy
 * @property \Illuminate\Database\Eloquent\Collection additionalTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalsPresentations
 * @property \Illuminate\Database\Eloquent\Collection brandTranslations
 * @property \Illuminate\Database\Eloquent\Collection configurationTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection flourTranslations
 * @property \Illuminate\Database\Eloquent\Collection floursPresentations
 * @property \Illuminate\Database\Eloquent\Collection ingredientTranslations
 * @property \Illuminate\Database\Eloquent\Collection orderDetailAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orderDetailFlours
 * @property \Illuminate\Database\Eloquent\Collection orderDetailIngredients
 * @property \Illuminate\Database\Eloquent\Collection orderDetailPresentations
 * @property \Illuminate\Database\Eloquent\Collection orderDetailSauces
 * @property \Illuminate\Database\Eloquent\Collection orderDetails
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection presentationTranslations
 * @property \Illuminate\Database\Eloquent\Collection productCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productSubcategories
 * @property \Illuminate\Database\Eloquent\Collection productSubcategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productTranslations
 * @property \Illuminate\Database\Eloquent\Collection productsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection productsFlours
 * @property \Illuminate\Database\Eloquent\Collection productsIngredients
 * @property \Illuminate\Database\Eloquent\Collection productsPresentations
 * @property \Illuminate\Database\Eloquent\Collection productsSauces
 * @property \Illuminate\Database\Eloquent\Collection requestCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection sauceTranslations
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection sliderTranslations
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property integer language_id
 * @property integer policy_id
 * @property string name
 * @property string description
 */
class Policytranslation extends Model
{
    use SoftDeletes;

    public $table = 'policy_translations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'language_id',
        'policy_id',
        'name',
        'description'
    ];

    protected $appends = [
      'code'
    ];

    protected $hidden = [
        'id',
        // 'language_id',
        'policy_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'language_id' => 'integer',
        'policy_id' => 'integer',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function language()
    {
        return $this->belongsTo(\App\Models\Admin\Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function policy()
    {
        return $this->belongsTo(\App\Models\Admin\Policy::class);
    }

    /**
     * Get the status name of the current model.
     *
     * @return string
     */
    public function getCodeAttribute()
    {
        return $this->language()->first()->code;
    }
}
