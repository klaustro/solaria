<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Blog
 * @package App\Models\Admin
 * @version January 25, 2019, 10:06 pm CET
 *
 * @property \App\Models\Admin\BlogCategory blogCategory
 * @property \App\Models\Admin\Status status
 * @property \App\Models\Admin\User user
 * @property \Illuminate\Database\Eloquent\Collection activityCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection activityRequests
 * @property \Illuminate\Database\Eloquent\Collection activityTranslations
 * @property \Illuminate\Database\Eloquent\Collection blogCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection BlogComment
 * @property \Illuminate\Database\Eloquent\Collection BlogTranslation
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection featureTranslations
 * @property \Illuminate\Database\Eloquent\Collection packageTranslations
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection rates
 * @property \Illuminate\Database\Eloquent\Collection requestCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection roomCategories
 * @property \Illuminate\Database\Eloquent\Collection roomCategoriesFeatures
 * @property \Illuminate\Database\Eloquent\Collection roomCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomLocationTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasonTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasons
 * @property \Illuminate\Database\Eloquent\Collection roomTranslations
 * @property \Illuminate\Database\Eloquent\Collection rooms
 * @property \Illuminate\Database\Eloquent\Collection roomsCategoriesServices
 * @property \Illuminate\Database\Eloquent\Collection roomsServices
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceTranslations
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string slug
 * @property integer blog_category_id
 * @property integer user_id
 * @property integer status_id
 * @property string image
 */
class Blog extends Model
{
    use SoftDeletes;

    public $table = 'blogs';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'slug',
        'blog_category_id',
        'user_id',
        'status_id',
        'image',
        'rate_id'
    ];

    protected $appends = [
        'title',
        'subtitle',
        'description',
        'rates',
        'gallery'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'blog_category_id' => 'integer',
        'user_id' => 'integer',
        'status_id' => 'integer',
        'image' => 'string',
        'rate_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        // 'blog_category_id' => 'required'
        // 'slug' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blogCategory()
    {
        return $this->belongsTo(\App\Models\Admin\BlogCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function rates()
    {
        return $this->hasMany(\App\Models\Admin\Rate::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Admin\Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\Admin\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function blogComments()
    {
        return $this->hasMany(\App\Models\Admin\BlogComment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function translations()
    {
        return $this->hasMany(\App\Models\Admin\BlogTranslation::class);
    }

    /**
     * Return the RoomLocationTranslation in current languague.
     *
     * @return RoomLocationTranslation
     **/
    public function translation( $code = null )
    {
        if ( empty( $code ) === false ) {
            $attribute = ( gettype( $code ) === 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        if ( $language === null ) {
            return [];
        }

        $trans = $this->translations()->where( 'language_id', $language->id )->first();

        return $trans;
    }

    /**
     * Return itemByLanguage.
     *
     * @return string
     */
    public function itemByLanguage( $language_code, $default_lan_id = 1 )
    {
        $translationItem = $this->translation( $language_code );

        // valor por defecto (ITALIANO) en caso de que no este en idioma solicitado
        if ( empty( $translationItem ) === true ) {
            $translationItem = $this->translation( 1 );
        }

        return $translationItem;
    }

    /**
     * Get the title in the given translation (Accessor).
     *
     * @return string
     */
    public function getTitleAttribute()
    {
        return optional( $this->translation() )->title;
    }
    /**
     * Get the subtitle in the given translation (Accessor).
     *
     * @return string
     */
    public function getSubtitleAttribute()
    {
        return optional( $this->translation() )->subtitle;
    }

    /**
     * Get the name in the given translation (Accessor).
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return optional( $this->translation() )->description;
    }

    /**
     * Get the rates of the room.
     *
     * @return array
     */
    public function getRatesAttribute()
    {
        return $this->rates()->with( 'translations' )->get();
    }

    /**
     * Get the gallery of the rate (first image of each room).
     *
     * @return array
     */
    public function getGalleryAttribute()
    {
        // array
        $gallery = [];

        // si tiene imagen principal se coloca como la primera de la galeria
        if ( $this->image !== null ) {
            $gallery[] = [
                'id' => 0,
                'url' => $this->image
            ];
        }

        // se recorren las ofertas asociadas a este blog
        foreach ( $this->rates as $key => $rate ) {

            // galeria de room
            $roomGallery = $rate->room->gallery;

            // se agrega a la galeria la primera imagen de la habitacion asociada a esta oferta
            if ( !empty( $roomGallery ) ) {
                $gallery[] = $roomGallery[ 0 ];
            }
        }

        return $gallery;
    }

}
