<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Activity
 * @package App\Models\Admin
 * @version August 28, 2018, 11:02 pm CEST
 *
 * @property \App\Models\Admin\ActivityCategory activityCategory
 * @property \App\Models\Admin\Status status
 * @property \Illuminate\Database\Eloquent\Collection activityCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection ActivityTranslation
 * @property \Illuminate\Database\Eloquent\Collection additionalCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionals
 * @property \Illuminate\Database\Eloquent\Collection blogCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection blogComments
 * @property \Illuminate\Database\Eloquent\Collection blogTranslations
 * @property \Illuminate\Database\Eloquent\Collection bookings
 * @property \Illuminate\Database\Eloquent\Collection brandTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection orderDetailsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orders
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection productCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productPresentationTranslations
 * @property \Illuminate\Database\Eloquent\Collection productPresentationsProducts
 * @property \Illuminate\Database\Eloquent\Collection productSubcategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection roomCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasonTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasons
 * @property \Illuminate\Database\Eloquent\Collection roomTranslations
 * @property \Illuminate\Database\Eloquent\Collection rooms
 * @property \Illuminate\Database\Eloquent\Collection roomsServices
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection activityTranslation
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property string slug
 * @property float price
 * @property integer activity_category_id
 * @property integer status_id
 */
class Activity extends Model
{
    use SoftDeletes;

    public $table = 'activities';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'slug',
        'price',
        'activity_category_id',
        'status_id',
        'ally_id',
    ];

    protected $appends = [
        'title',
        'subtitle',
        'description',
        'activity_category_name',
        'gallery',
        'ally_phone',
        'ally_email',
        'ally_website',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'slug' => 'string',
        'price' => 'float',
        'activity_category_id' => 'integer',
        'status_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'activity_category_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function activityCategory()
    {
        return $this->belongsTo(\App\Models\Admin\ActivityCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function status()
    {
        return $this->belongsTo(\App\Models\Admin\Status::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function ally()
    {
        return $this->belongsTo(\App\Models\Admin\Ally::class);
    }
    /**
     * Return the ServiceTranslation in current languague.
     *
     * @return ServiceTranslation
     **/
    public function activityTranslation()
    {
        $language = Language::where('code', \App::getLocale())->first();

        $trans = $this->translations()->where( 'language_id', $language->id )->withTrashed()->first();

        return $trans;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function translations()
    {
        return $this->hasMany(\App\Models\Admin\ActivityTranslation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function row()
    {
        return $this->morphOne('App\Models\Admin\Row', 'rowable');
    }

    /**
     * Return the RoomLocationTranslation in current languague.
     *
     * @return RoomLocationTranslation
     **/
    public function translation( $code = null )
    {
        if ( empty( $code ) === false ) {
            $attribute = ( gettype( $code ) === 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        if ( $language === null ) {
            return [];
        }

        $trans = $this->translations()->where( 'language_id', $language->id )->first();

        return $trans;
    }

    /**
     * Get the name in the given translation (Accessor).
     *
     * @return string
     */
    public function getTitleAttribute()
    {
        return optional($this->activityTranslation())->title;
    }

    /**
     * Get the name in the given translation (Accessor).
     *
     * @return string
     */
    public function getSubtitleAttribute()
    {
        return optional($this->activityTranslation())->subtitle;
    }

    /**
     * Get the description in the given translation (Accessor).
     *
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return optional($this->activityTranslation())->description;
    }

    /**
     * Get the Activity Category in the given translation (Accessor).
     *
     * @return string
     */
    public function getActivityCategoryNameAttribute()
    {
        return optional($this->activityCategory)->name;
    }

    /**
     * Return itemByLanguage.
     *
     * @return string
     */
    public function itemByLanguage( $language_code, $default_lan_id = 1 )
    {
        $translationItem = $this->translation( $language_code );

        // valor por defecto (ITALIANO) en caso de que no este en idioma solicitado
        if ( empty( $translationItem ) === true ) {
            $translationItem = $this->translation( 1 );
        }

        return $translationItem;
    }

    /**
     * Get the gallery of the room.
     *
     * @return array
     */
    public function getGalleryAttribute()
    {
        return optional($this->row()->first())->multimedias;
    }

    /**
     * Get Ally phone
     * @return [type] [description]
     */
    public function getAllyPhoneAttribute()
    {
        return optional($this->ally)->phone;
    }

    /**
     * Get Ally phone
     * @return [type] [description]
     */
    public function getAllyEmailAttribute()
    {
        return optional($this->ally)->email;
    }

    public function getAllyWebsiteAttribute()
    {
        return optional($this->ally)->website;
    }
}
