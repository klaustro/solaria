<?php

namespace App\Models\Admin;

use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Rate
 * @package App\Models\Admin
 * @version December 21, 2018, 5:27 pm CET
 *
 * @property \App\Models\Admin\Package package
 * @property \App\Models\Admin\Room room
 * @property \Illuminate\Database\Eloquent\Collection activities
 * @property \Illuminate\Database\Eloquent\Collection activityCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection activityTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection featureTranslations
 * @property \Illuminate\Database\Eloquent\Collection packageTranslations
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection requestCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection roomCategories
 * @property \Illuminate\Database\Eloquent\Collection roomCategoriesFeatures
 * @property \Illuminate\Database\Eloquent\Collection roomCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomLocationTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasonTranslations
 * @property \Illuminate\Database\Eloquent\Collection roomSeasons
 * @property \Illuminate\Database\Eloquent\Collection roomTranslations
 * @property \Illuminate\Database\Eloquent\Collection rooms
 * @property \Illuminate\Database\Eloquent\Collection roomsCategoriesServices
 * @property \Illuminate\Database\Eloquent\Collection roomsServices
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection serviceTranslations
 * @property \Illuminate\Database\Eloquent\Collection services
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property integer room_id
 * @property date start_date
 * @property date end_date
 * @property integer packages_id
 * @property integer discount
 * @property float price
 * @property float iva
 * @property integer num_people
 * @property string email
 * @property integer blog_id
 */
class Rate extends Model
{
    use SoftDeletes;

    public $table = 'rates';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = [ 'deleted_at' ];


    public $fillable = [
        'room_id',
        'start_date',
        'end_date',
        'package_id',
        'discount',

        'price',
        'price_total',
        'days',

        'iva',
        'num_people',
        'email',
        'blog_id',
        'price_temp',
        'comment',

        // token
        'token',
    ];

    protected $appends = [
        'room_name',
        'package_name',
        'start_date_italian',
        'end_date_italian',
        'title'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'room_id' => 'integer',
        'start_date' => 'date',
        'end_date' => 'date',
        'packages_id' => 'integer',
        'discount' => 'integer',
        'price' => 'float',
        'iva' => 'float',
        'num_people' => 'integer',
        'email' => 'string',
        'blog_id' => 'integer',
        'price_temp'=> 'float',
        'price_total'=> 'float',
        'days'=> 'integer',
        'comment' => 'string',

        // token
        'token' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function package()
    {
        return $this->belongsTo(\App\Models\Admin\Package::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function room()
    {
        return $this->belongsTo(\App\Models\Admin\Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function blog()
    {
        return $this->belongsTo(\App\Models\Admin\Blog::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function translations()
    {
        return $this->hasMany( \App\Models\Admin\RateTranslation::class );
    }

    /**
     * Return the RoomTranslation in current languague.
     *
     * @return RoomTranslation
     */
    public function translation( $code = null )
    {
        if ( empty( $code ) === false ) {
            $attribute = ( gettype( $code ) === 'string' ) ? 'code' : 'id';
        }
        else {
            $code = \App::getLocale();
            $attribute = 'code';
        }

        $language = Language::where( $attribute, $code )->first();
        if ( $language === null ) {
            return [];
        }

        $trans = $this->translations()->where( 'language_id', $language->id )->first();

        return $trans;
    }

    public function getRoomNameAttribute()
    {
        return $this->room->name;
    }

    public function getPackageNameAttribute()
    {
        return optional($this->package)->name;
    }

    /**
     * Get start_date in italian format.
     *
     * @return string
     */
    public function getStartDateItalianAttribute()
    {
        return $this->start_date->format( 'd/m/Y' );
    }

    /**
     * Get end_date in italian format.
     *
     * @return string
     */
    public function getEndDateItalianAttribute()
    {
        return $this->end_date->format( 'd/m/Y' );
    }

    public function getTitleAttribute()
    {
        return optional($this->translation())->title;
    }
}
