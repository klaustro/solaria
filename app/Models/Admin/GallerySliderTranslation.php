<?php

namespace App\Models\Admin;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class GallerySliderTranslation
 * @package App\Models\Admin
 * @version March 21, 2019, 7:02 pm CET
 *
 * @property \App\Models\Admin\GallerySlider gallerySlider
 * @property \App\Models\Admin\Language language
 * @property \Illuminate\Database\Eloquent\Collection additionalTranslations
 * @property \Illuminate\Database\Eloquent\Collection additionalsPresentations
 * @property \Illuminate\Database\Eloquent\Collection blogCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection blogComments
 * @property \Illuminate\Database\Eloquent\Collection blogTranslations
 * @property \Illuminate\Database\Eloquent\Collection brandTranslations
 * @property \Illuminate\Database\Eloquent\Collection configurationTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection eventTranslations
 * @property \Illuminate\Database\Eloquent\Collection events
 * @property \Illuminate\Database\Eloquent\Collection flourTranslations
 * @property \Illuminate\Database\Eloquent\Collection floursPresentations
 * @property \Illuminate\Database\Eloquent\Collection ingredientTranslations
 * @property \Illuminate\Database\Eloquent\Collection offerSliderItemTranslations
 * @property \Illuminate\Database\Eloquent\Collection orderDetailAdditionals
 * @property \Illuminate\Database\Eloquent\Collection orderDetailFlours
 * @property \Illuminate\Database\Eloquent\Collection orderDetailIngredients
 * @property \Illuminate\Database\Eloquent\Collection orderDetailPresentations
 * @property \Illuminate\Database\Eloquent\Collection orderDetailSauces
 * @property \Illuminate\Database\Eloquent\Collection orderDetails
 * @property \Illuminate\Database\Eloquent\Collection permissionRole
 * @property \Illuminate\Database\Eloquent\Collection permissionUser
 * @property \Illuminate\Database\Eloquent\Collection policyTranslations
 * @property \Illuminate\Database\Eloquent\Collection presentationTranslations
 * @property \Illuminate\Database\Eloquent\Collection productCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productSubcategories
 * @property \Illuminate\Database\Eloquent\Collection productSubcategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection productTranslations
 * @property \Illuminate\Database\Eloquent\Collection productsAdditionals
 * @property \Illuminate\Database\Eloquent\Collection productsFlours
 * @property \Illuminate\Database\Eloquent\Collection productsIngredients
 * @property \Illuminate\Database\Eloquent\Collection productsPresentations
 * @property \Illuminate\Database\Eloquent\Collection productsSauces
 * @property \Illuminate\Database\Eloquent\Collection requestCategoryTranslations
 * @property \Illuminate\Database\Eloquent\Collection roleUser
 * @property \Illuminate\Database\Eloquent\Collection sauceTranslations
 * @property \Illuminate\Database\Eloquent\Collection screensFrontSections
 * @property \Illuminate\Database\Eloquent\Collection seoTranslations
 * @property \Illuminate\Database\Eloquent\Collection sliderTranslations
 * @property \Illuminate\Database\Eloquent\Collection statusTranslations
 * @property \Illuminate\Database\Eloquent\Collection tagTranslations
 * @property \Illuminate\Database\Eloquent\Collection userAddresses
 * @property integer gallery_slider_id
 * @property integer language_id
 * @property string title
 * @property string description
 */
class GallerySliderTranslation extends Model
{
    use SoftDeletes;

    public $table = 'gallery_slider_translations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public $fillable = [
        'gallery_slider_id',
        'language_id',
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'gallery_slider_id' => 'integer',
        'language_id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function gallerySlider()
    {
        return $this->belongsTo(\App\Models\Admin\GallerySlider::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function language()
    {
        return $this->belongsTo(\App\Models\Admin\Language::class);
    }
}
