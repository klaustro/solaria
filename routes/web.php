<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Route::get( '/room-categories-service', function (){

// 		$roomCategoriesService = App\Models\Admin\RoomCategory::findOrFail(5);

// 		return $roomCategoriesService->services;

// 		// $roomCategoriesService = App\Models\Admin\Service::findOrFail(2);

// 		// return $roomCategoriesService->roomCategories;
// });


// testing
Route::get( '/dd',  function () {
    return view( 'dd' );
});


Route::post( '/paypal', 'PaypalController@payWithpaypal');
Route::get( 'status', 'PaypalController@getPaymentStatus');

Route::post( 'register', 'Auth\RegisterController@register');

Route::get( 'logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::middleware([ 'auth','hasrole:admin' ] )->group(function() {

    // Home
    Route::get( '/home', 'HomeController@index')->name( 'home');
	Route::get( '/admin/home', 'HomeController@index')->name( 'home');


    // Users
	Route::post( '/admin/profile/{user}', 'Admin\UserController@updateProfile')->name( 'admin.profile.update');
	Route::get( '/admin/role', 'Admin\UserController@indexRole')->name( 'admin.role.index')->middleware( 'hasrole:admin');
	Route::get( '/admin/role/create', 'Admin\UserController@createRole')->name( 'admin.role.create')->middleware( 'hasrole:admin');
	Route::get( '/admin/role/{role}', 'Admin\UserController@showRole')->name( 'admin.role.show')->middleware( 'hasrole:admin');
	Route::get( '/admin/role/{role}/edit', 'Admin\UserController@index')->name( 'admin.role.edit')->middleware( 'hasrole:admin');
	Route::post( '/admin/role', 'Admin\UserController@storeRole')->name( 'admin.role.store')->middleware( 'hasrole:admin');
	Route::get( '/admin/search/user', 'Admin\UserController@getSearchUser')->name( 'admin.user.get.search')->middleware( 'hasrole:admin');
	Route::post( '/admin/search/user', 'Admin\UserController@searchUser')->name( 'admin.user.search')->middleware( 'hasrole:admin');
	Route::get( '/admin/delete/user/{user}', 'Admin\UserController@dropOutUser')->name( 'admin.drop.out.user')->middleware( 'hasrole:admin');
	Route::get( '/admin/permission/to/role/{role}', 'Admin\UserController@permissionToRole')->name( 'admin.permission.to.role')->middleware( 'hasrole:admin');
	Route::post( '/admin/permission/to/role/{role}', 'Admin\UserController@addPermissionToRole')->name( 'admin.add.permission.to.role')->middleware( 'hasrole:admin');
	Route::post( '/admin/role/to/user/{user}', 'Admin\UserController@addRoleToUser')->name( 'admin.role.to.user')->middleware( 'hasrole:admin');
    Route::post( 'searchuser', 'Admin\UserController@searchUserF')->name( 'admin.search.user');

    Route::get( 'admin/roles', [ 'as'=> 'admin.roles.index', 'uses' => 'Admin\RoleController@index' ] );
    Route::post( 'admin/roles', [ 'as'=> 'admin.roles.store', 'uses' => 'Admin\RoleController@store' ] );
    Route::get( 'admin/roles/create', [ 'as'=> 'admin.roles.create', 'uses' => 'Admin\RoleController@create' ] );
    Route::put( 'admin/roles/{roles}', [ 'as'=> 'admin.roles.update', 'uses' => 'Admin\RoleController@update' ] );
    Route::patch( 'admin/roles/{roles}', [ 'as'=> 'admin.roles.update', 'uses' => 'Admin\RoleController@update' ] );
    Route::delete( 'admin/roles/{roles}', [ 'as'=> 'admin.roles.destroy', 'uses' => 'Admin\RoleController@destroy' ] );
    Route::get( 'admin/roles/{roles}', [ 'as'=> 'admin.roles.show', 'uses' => 'Admin\RoleController@show' ] );
    Route::get( 'admin/roles/{roles}/edit', [ 'as'=> 'admin.roles.edit', 'uses' => 'Admin\RoleController@edit' ] );

    Route::get( 'admin/users', [ 'as'=> 'admin.users.index', 'uses' => 'Admin\BackofficeUserController@index' ] );
    Route::post( 'admin/users', [ 'as'=> 'admin.users.store', 'uses' => 'Admin\BackofficeUserController@store' ] );
    Route::get( 'admin/users/create', [ 'as'=> 'admin.users.create', 'uses' => 'Admin\BackofficeUserController@create' ] );
    Route::put( 'admin/users/{users}', [ 'as'=> 'admin.users.update', 'uses' => 'Admin\BackofficeUserController@update' ] );
    Route::patch( 'admin/users/{users}', [ 'as'=> 'admin.users.update', 'uses' => 'Admin\BackofficeUserController@update' ] );
    Route::delete( 'admin/users/{users}', [ 'as'=> 'admin.users.destroy', 'uses' => 'Admin\BackofficeUserController@destroy' ] );
    Route::get( 'admin/users/{user}', [ 'as'=> 'admin.users.edit', 'uses' => 'Admin\BackofficeUserController@edit' ] );

    // RoomLocations
    Route::get( 'admin/roomLocations', [ 'as'=> 'admin.roomLocations.index', 'uses' => 'Admin\RoomLocationController@index' ] );
    Route::post( 'admin/roomLocations', [ 'as'=> 'admin.roomLocations.store', 'uses' => 'Admin\RoomLocationController@store' ] );
    Route::get( 'admin/roomLocations/create', [ 'as'=> 'admin.roomLocations.create', 'uses' => 'Admin\RoomLocationController@create' ] );
    Route::put( 'admin/roomLocations/{roomLocations}', [ 'as'=> 'admin.roomLocations.update', 'uses' => 'Admin\RoomLocationController@update' ] );
    Route::patch( 'admin/roomLocations/{roomLocations}', [ 'as'=> 'admin.roomLocations.update', 'uses' => 'Admin\RoomLocationController@update' ] );
    Route::delete( 'admin/roomLocations/{roomLocations}', [ 'as'=> 'admin.roomLocations.destroy', 'uses' => 'Admin\RoomLocationController@destroy' ] );
    // Route::get( 'admin/roomLocations/{roomLocations}', [ 'as'=> 'admin.roomLocations.show', 'uses' => 'Admin\RoomLocationController@show' ] );
    Route::get( 'admin/roomLocations/{edit}', [ 'as'=> 'admin.roomLocations.edit', 'uses' => 'Admin\RoomLocationController@edit' ] );


    // RoomCategories
    Route::get( 'admin/roomCategories', [ 'as'=> 'admin.roomCategories.index', 'uses' => 'Admin\RoomCategoryController@index' ] );
    Route::post( 'admin/roomCategories', [ 'as'=> 'admin.roomCategories.store', 'uses' => 'Admin\RoomCategoryController@store' ] );
    Route::get( 'admin/roomCategories/create', [ 'as'=> 'admin.roomCategories.create', 'uses' => 'Admin\RoomCategoryController@create' ] );
    Route::put( 'admin/roomCategories/{roomCategories}', [ 'as'=> 'admin.roomCategories.update', 'uses' => 'Admin\RoomCategoryController@update' ] );
    Route::patch( 'admin/roomCategories/{roomCategories}', [ 'as'=> 'admin.roomCategories.update', 'uses' => 'Admin\RoomCategoryController@update' ] );
    Route::delete( 'admin/roomCategories/{roomCategories}', [ 'as'=> 'admin.roomCategories.destroy', 'uses' => 'Admin\RoomCategoryController@destroy' ] );
    Route::get( 'admin/roomCategories/{roomCategories}', [ 'as'=> 'admin.roomCategories.show', 'uses' => 'Admin\RoomCategoryController@show' ] );
    Route::get( 'admin/roomCategories/{language}/{model}', [ 'as'=> 'admin.roomCategories.edit', 'uses' => 'Admin\RoomCategoryController@edit' ] );


    // Rooms
    Route::get( 'admin/rooms', [ 'as'=> 'admin.rooms.index', 'uses' => 'Admin\RoomController@index' ] );
    Route::post( 'admin/rooms', [ 'as'=> 'admin.rooms.store', 'uses' => 'Admin\RoomController@store' ] );
    Route::get( 'admin/rooms/create', [ 'as'=> 'admin.rooms.create', 'uses' => 'Admin\RoomController@create' ] );
    Route::put( 'admin/rooms/{rooms}', [ 'as'=> 'admin.rooms.update', 'uses' => 'Admin\RoomController@update' ] );
    Route::patch( 'admin/rooms/{rooms}', [ 'as'=> 'admin.rooms.update', 'uses' => 'Admin\RoomController@update' ] );
    Route::delete( 'admin/rooms/{rooms}', [ 'as'=> 'admin.rooms.destroy', 'uses' => 'Admin\RoomController@destroy' ] );
    Route::get( 'admin/rooms/{rooms}', [ 'as'=> 'admin.rooms.show', 'uses' => 'Admin\RoomController@show' ] );
    Route::get( 'admin/rooms/{language}/{model}', [ 'as'=> 'admin.rooms.edit', 'uses' => 'Admin\RoomController@edit' ] );
    Route::post( 'admin/deleteImage/{rooms}/{image}', [ 'as'=> 'admin.rooms.deleteImage', 'uses' => 'Admin\RoomController@deleteImage' ] );


    // Service
    Route::get( 'admin/services', [ 'as'=> 'admin.services.index', 'uses' => 'Admin\ServiceController@index' ] );
    Route::post( 'admin/services', [ 'as'=> 'admin.services.store', 'uses' => 'Admin\ServiceController@store' ] );
    Route::get( 'admin/services/create', [ 'as'=> 'admin.services.create', 'uses' => 'Admin\ServiceController@create' ] );
    Route::put( 'admin/services/{services}', [ 'as'=> 'admin.services.update', 'uses' => 'Admin\ServiceController@update' ] );
    Route::patch( 'admin/services/{services}', [ 'as'=> 'admin.services.update', 'uses' => 'Admin\ServiceController@update' ] );
    Route::delete( 'admin/services/{services}', [ 'as'=> 'admin.services.destroy', 'uses' => 'Admin\ServiceController@destroy' ] );
    Route::get( 'admin/services/{services}', [ 'as'=> 'admin.services.show', 'uses' => 'Admin\ServiceController@show' ] );
    Route::get( 'admin/services/{language}/{model}', [ 'as'=> 'admin.services.edit', 'uses' => 'Admin\ServiceController@edit' ] );


    // Booking
    Route::get( 'admin/bookings', [ 'as'=> 'admin.bookings.index', 'uses' => 'Admin\BookingController@index' ] );
    Route::get( 'admin/searchRoom', [ 'as'=> 'admin.bookings.searchRoom', 'uses' => 'Admin\BookingController@searchRoom' ] );
    Route::post( 'admin/bookings', [ 'as'=> 'admin.bookings.store', 'uses' => 'Admin\BookingController@store' ] );
    Route::get( 'admin/bookings/create', [ 'as'=> 'admin.bookings.create', 'uses' => 'Admin\BookingController@create' ] );
    Route::put( 'admin/bookings/{bookings}', [ 'as'=> 'admin.bookings.update', 'uses' => 'Admin\BookingController@update' ] );
    Route::patch( 'admin/bookings/{bookings}', [ 'as'=> 'admin.bookings.update', 'uses' => 'Admin\BookingController@update' ] );
    // Route::delete( 'admin/bookings/{bookings}', [ 'as'=> 'admin.bookings.destroy', 'uses' => 'Admin\BookingController@destroy' ] );
    Route::get( 'admin/bookings/{bookings}', [ 'as'=> 'admin.bookings.show', 'uses' => 'Admin\BookingController@show' ] );
    // Route::get( 'admin/bookings/{bookings}/edit', [ 'as'=> 'admin.bookings.edit', 'uses' => 'Admin\BookingController@edit' ] );


    // BookingDetails
    // Route::get( 'admin/bookingDetails', [ 'as'=> 'admin.bookingDetails.index', 'uses' => 'Admin\BookingDetailController@index' ] );
    // Route::post( 'admin/bookingDetails', [ 'as'=> 'admin.bookingDetails.store', 'uses' => 'Admin\BookingDetailController@store' ] );
    // Route::get( 'admin/bookingDetails/create', [ 'as'=> 'admin.bookingDetails.create', 'uses' => 'Admin\BookingDetailController@create' ] );
    // Route::put( 'admin/bookingDetails/{bookingDetails}', [ 'as'=> 'admin.bookingDetails.update', 'uses' => 'Admin\BookingDetailController@update' ] );
    // Route::patch( 'admin/bookingDetails/{bookingDetails}', [ 'as'=> 'admin.bookingDetails.update', 'uses' => 'Admin\BookingDetailController@update' ] );
    // Route::delete( 'admin/bookingDetails/{bookingDetails}', [ 'as'=> 'admin.bookingDetails.destroy', 'uses' => 'Admin\BookingDetailController@destroy' ] );
    // Route::get( 'admin/bookingDetails/{bookingDetails}', [ 'as'=> 'admin.bookingDetails.show', 'uses' => 'Admin\BookingDetailController@show' ] );
    // Route::get( 'admin/bookingDetails/{bookingDetails}/edit', [ 'as'=> 'admin.bookingDetails.edit', 'uses' => 'Admin\BookingDetailController@edit' ] );

    // Newsletters
	Route::get( 'admin/newsletterUsers', [ 'as'=> 'admin.newsletterUsers.index', 'uses' => 'Admin\NewsletterUserController@index' ] );
	Route::post( 'admin/newsletterUsers', [ 'as'=> 'admin.newsletterUsers.store', 'uses' => 'Admin\NewsletterUserController@store' ] );
	Route::get( 'admin/newsletterUsers/create', [ 'as'=> 'admin.newsletterUsers.create', 'uses' => 'Admin\NewsletterUserController@create' ] );
	Route::put( 'admin/newsletterUsers/{newsletterUsers}', [ 'as'=> 'admin.newsletterUsers.update', 'uses' => 'Admin\NewsletterUserController@update' ] );
	Route::patch( 'admin/newsletterUsers/{newsletterUsers}', [ 'as'=> 'admin.newsletterUsers.update', 'uses' => 'Admin\NewsletterUserController@update' ] );
	Route::delete( 'admin/newsletterUsers/{newsletterUsers}', [ 'as'=> 'admin.newsletterUsers.destroy', 'uses' => 'Admin\NewsletterUserController@destroy' ] );
	Route::get( 'admin/newsletterUsers/{newsletterUsers}', [ 'as'=> 'admin.newsletterUsers.show', 'uses' => 'Admin\NewsletterUserController@show' ] );
	Route::get( 'admin/newsletterUsers/{newsletterUsers}/edit', [ 'as'=> 'admin.newsletterUsers.edit', 'uses' => 'Admin\NewsletterUserController@edit' ] );


    // Multimedias
	Route::get( 'admin/multimedia', [ 'as'=> 'admin.multimedia.index', 'uses' => 'Admin\MultimediaController@index' ] );
	Route::post( 'admin/multimedia', [ 'as'=> 'admin.multimedia.store', 'uses' => 'Admin\MultimediaController@store' ] );
	Route::get( 'admin/multimedia/create', [ 'as'=> 'admin.multimedia.create', 'uses' => 'Admin\MultimediaController@create' ] );
	Route::put( 'admin/multimedia/{multimedia}', [ 'as'=> 'admin.multimedia.update', 'uses' => 'Admin\MultimediaController@update' ] );
	Route::patch( 'admin/multimedia/{multimedia}', [ 'as'=> 'admin.multimedia.update', 'uses' => 'Admin\MultimediaController@update' ] );
	Route::delete( 'admin/multimedia/{multimedia}', [ 'as'=> 'admin.multimedia.destroy', 'uses' => 'Admin\MultimediaController@destroy' ] );
	Route::get( 'admin/multimedia/{multimedia}', [ 'as'=> 'admin.multimedia.show', 'uses' => 'Admin\MultimediaController@show' ] );
	Route::get( 'admin/multimedia/{multimedia}/edit', [ 'as'=> 'admin.multimedia.edit', 'uses' => 'Admin\MultimediaController@edit' ] );

    // Route::name( 'admin.modal_galery.galery.saveGalery')->post( '/modal_galery/galery/{id}/saveGalery', 'Admin\MultimediaController@saveGalery');
    Route::name( 'admin.modal_galery.galery.saveGalery')->post( '/modal_galery/galery/saveGalery', 'Admin\RoomController@saveGalery');


    // Seos
    Route::get( 'admin/seos', [ 'as'=> 'admin.seos.index', 'uses' => 'Admin\SeoController@index' ] );
    Route::post( 'admin/seos', [ 'as'=> 'admin.seos.store', 'uses' => 'Admin\SeoController@store' ] );
    Route::get( 'admin/seos/create', [ 'as'=> 'admin.seos.create', 'uses' => 'Admin\SeoController@create' ] );
    Route::put( 'admin/seos/{seos}', [ 'as'=> 'admin.seos.update', 'uses' => 'Admin\SeoController@update' ] );
    Route::patch( 'admin/seos/{seos}', [ 'as'=> 'admin.seos.update', 'uses' => 'Admin\SeoController@update' ] );
    Route::delete( 'admin/seos/{seos}', [ 'as'=> 'admin.seos.destroy', 'uses' => 'Admin\SeoController@destroy' ] );
    Route::get( 'admin/seos/{seos}', [ 'as'=> 'admin.seos.show', 'uses' => 'Admin\SeoController@show' ] );
    Route::get( 'admin/seos/{seos}/edit', [ 'as'=> 'admin.seos.edit', 'uses' => 'Admin\SeoController@edit' ] );


    // TagTranslations
    Route::get( 'admin/tagTranslations', [ 'as'=> 'admin.tagTranslations.index', 'uses' => 'Admin\TagTranslationController@index' ] );
    Route::post( 'admin/tagTranslations', [ 'as'=> 'admin.tagTranslations.store', 'uses' => 'Admin\TagTranslationController@store' ] );
    Route::get( 'admin/tagTranslations/create', [ 'as'=> 'admin.tagTranslations.create', 'uses' => 'Admin\TagTranslationController@create' ] );
    Route::put( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.update', 'uses' => 'Admin\TagTranslationController@update' ] );
    Route::patch( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.update', 'uses' => 'Admin\TagTranslationController@update' ] );
    Route::delete( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.destroy', 'uses' => 'Admin\TagTranslationController@destroy' ] );
    Route::get( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.show', 'uses' => 'Admin\TagTranslationController@show' ] );
    Route::get( 'admin/tagTranslations/{tagTranslations}/edit', [ 'as'=> 'admin.tagTranslations.edit', 'uses' => 'Admin\TagTranslationController@edit' ] );
    Route::get( 'admin/tagTranslations/{tagTranslations}/beforeEdit', [ 'as'=> 'admin.tagTranslations.beforeEdit', 'uses' => 'Admin\TagTranslationController@beforeEdit' ] );
    Route::patch( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.preparedEdit', 'uses' => 'Admin\TagTranslationController@preparedEdit' ] );


    // ClientRequests
    Route::get( 'admin/clientRequests', [ 'as'=> 'admin.clientRequests.index', 'uses' => 'Admin\ClientRequestController@index' ] );
	Route::post( 'admin/clientRequests', [ 'as'=> 'admin.clientRequests.store', 'uses' => 'Admin\ClientRequestController@store' ] );
	Route::get( 'admin/clientRequests/create', [ 'as'=> 'admin.clientRequests.create', 'uses' => 'Admin\ClientRequestController@create' ] );
	Route::put( 'admin/clientRequests/{clientRequests}', [ 'as'=> 'admin.clientRequests.update', 'uses' => 'Admin\ClientRequestController@update' ] );
	Route::patch( 'admin/clientRequests/{clientRequests}', [ 'as'=> 'admin.clientRequests.update', 'uses' => 'Admin\ClientRequestController@update' ] );
	Route::delete( 'admin/clientRequests/{clientRequests}', [ 'as'=> 'admin.clientRequests.destroy', 'uses' => 'Admin\ClientRequestController@destroy' ] );
	Route::get( 'admin/clientRequests/{clientRequests}', [ 'as'=> 'admin.clientRequests.show', 'uses' => 'Admin\ClientRequestController@show' ] );
	Route::get( 'admin/clientRequests/{clientRequests}/edit', [ 'as'=> 'admin.clientRequests.edit', 'uses' => 'Admin\ClientRequestController@edit' ] );


    /**************************************************************************************************************************************************************/


	Route::get( 'admin/requestCategories', [ 'as'=> 'admin.requestCategories.index', 'uses' => 'Admin\RequestCategoryController@index' ] );
    Route::post( 'admin/requestCategories', [ 'as'=> 'admin.requestCategories.store', 'uses' => 'Admin\RequestCategoryController@store' ] );
    Route::get( 'admin/requestCategories/create', [ 'as'=> 'admin.requestCategories.create', 'uses' => 'Admin\RequestCategoryController@create' ] );
    Route::put( 'admin/requestCategories/{requestCategories}', [ 'as'=> 'admin.requestCategories.update', 'uses' => 'Admin\RequestCategoryController@update' ] );
    Route::patch( 'admin/requestCategories/{requestCategories}', [ 'as'=> 'admin.requestCategories.update', 'uses' => 'Admin\RequestCategoryController@update' ] );
    Route::delete( 'admin/requestCategories/{requestCategories}', [ 'as'=> 'admin.requestCategories.destroy', 'uses' => 'Admin\RequestCategoryController@destroy' ] );
    Route::get( 'admin/requestCategories/{requestCategories}', [ 'as'=> 'admin.requestCategories.show', 'uses' => 'Admin\RequestCategoryController@show' ] );
    Route::get( 'admin/requestCategories/{requestCategories}/edit', [ 'as'=> 'admin.requestCategories.edit', 'uses' => 'Admin\RequestCategoryController@edit' ] );

    Route::get( 'admin/activityCategories', [ 'as'=> 'admin.activityCategories.index', 'uses' => 'Admin\ActivityCategoryController@index' ] );
    Route::post( 'admin/activityCategories', [ 'as'=> 'admin.activityCategories.store', 'uses' => 'Admin\ActivityCategoryController@store' ] );
    Route::get( 'admin/activityCategories/create', [ 'as'=> 'admin.activityCategories.create', 'uses' => 'Admin\ActivityCategoryController@create' ] );
    Route::put( 'admin/activityCategories/{activityCategories}', [ 'as'=> 'admin.activityCategories.update', 'uses' => 'Admin\ActivityCategoryController@update' ] );
    Route::patch( 'admin/activityCategories/{activityCategories}', [ 'as'=> 'admin.activityCategories.update', 'uses' => 'Admin\ActivityCategoryController@update' ] );
    Route::delete( 'admin/activityCategories/{activityCategories}', [ 'as'=> 'admin.activityCategories.destroy', 'uses' => 'Admin\ActivityCategoryController@destroy' ] );
    Route::get( 'admin/activityCategories/{activityCategories}', [ 'as'=> 'admin.activityCategories.show', 'uses' => 'Admin\ActivityCategoryController@show' ] );
    Route::get( 'admin/activityCategories/{language}/{model}', [ 'as'=> 'admin.activityCategories.edit', 'uses' => 'Admin\ActivityCategoryController@edit' ] );


    Route::get( 'admin/activities', [ 'as'=> 'admin.activities.index', 'uses' => 'Admin\ActivityController@index' ] );
    Route::post( 'admin/activities', [ 'as'=> 'admin.activities.store', 'uses' => 'Admin\ActivityController@store' ] );
    Route::get( 'admin/activities/create', [ 'as'=> 'admin.activities.create', 'uses' => 'Admin\ActivityController@create' ] );
    Route::put( 'admin/activities/{activities}', [ 'as'=> 'admin.activities.update', 'uses' => 'Admin\ActivityController@update' ] );
    Route::patch( 'admin/activities/{activities}', [ 'as'=> 'admin.activities.update', 'uses' => 'Admin\ActivityController@update' ] );
    Route::delete( 'admin/activities/{activities}', [ 'as'=> 'admin.activities.destroy', 'uses' => 'Admin\ActivityController@destroy' ] );
    Route::get( 'admin/activities/{activities}', [ 'as'=> 'admin.activities.show', 'uses' => 'Admin\ActivityController@show' ] );
    Route::get( 'admin/activities/{language}/{model}', [ 'as'=> 'admin.activities.edit', 'uses' => 'Admin\ActivityController@edit' ] );

    Route::get( 'admin/activityRequests', [ 'as'=> 'admin.activityRequests.index', 'uses' => 'Admin\ActivityRequestController@index' ] );
    Route::post( 'admin/activityRequests', [ 'as'=> 'admin.activityRequests.store', 'uses' => 'Admin\ActivityRequestController@store' ] );
    Route::get( 'admin/activityRequests/create', [ 'as'=> 'admin.activityRequests.create', 'uses' => 'Admin\ActivityRequestController@create' ] );
    Route::put( 'admin/activityRequests/{activityRequests}', [ 'as'=> 'admin.activityRequests.update', 'uses' => 'Admin\ActivityRequestController@update' ] );
    Route::patch( 'admin/activityRequests/{activityRequests}', [ 'as'=> 'admin.activityRequests.update', 'uses' => 'Admin\ActivityRequestController@update' ] );
    Route::delete( 'admin/activityRequests/{activityRequests}', [ 'as'=> 'admin.activityRequests.destroy', 'uses' => 'Admin\ActivityRequestController@destroy' ] );
    Route::get( 'admin/activityRequests/{activityRequests}', [ 'as'=> 'admin.activityRequests.show', 'uses' => 'Admin\ActivityRequestController@show' ] );
    Route::get( 'admin/activityRequests/{activityRequests}/edit', [ 'as'=> 'admin.activityRequests.edit', 'uses' => 'Admin\ActivityRequestController@edit' ] );

    Route::get( 'admin/roomSeasons', [ 'as'=> 'admin.roomSeasons.index', 'uses' => 'Admin\RoomSeasonController@index' ] );
    Route::post( 'admin/roomSeasons', [ 'as'=> 'admin.roomSeasons.store', 'uses' => 'Admin\RoomSeasonController@store' ] );
    Route::get( 'admin/roomSeasons/create', [ 'as'=> 'admin.roomSeasons.create', 'uses' => 'Admin\RoomSeasonController@create' ] );
    Route::put( 'admin/roomSeasons/{roomSeasons}', [ 'as'=> 'admin.roomSeasons.update', 'uses' => 'Admin\RoomSeasonController@update' ] );
    Route::patch( 'admin/roomSeasons/{roomSeasons}', [ 'as'=> 'admin.roomSeasons.update', 'uses' => 'Admin\RoomSeasonController@update' ] );
    Route::delete( 'admin/roomSeasons/{roomSeasons}', [ 'as'=> 'admin.roomSeasons.destroy', 'uses' => 'Admin\RoomSeasonController@destroy' ] );
    Route::get( 'admin/roomSeasons/{roomSeasons}', [ 'as'=> 'admin.roomSeasons.show', 'uses' => 'Admin\RoomSeasonController@show' ] );
    Route::get( 'admin/roomSeasons/{roomSeasons}/edit', [ 'as'=> 'admin.roomSeasons.edit', 'uses' => 'Admin\RoomSeasonController@edit' ] );


    Route::get( 'admin/serviceCategories', [ 'as'=> 'admin.serviceCategories.index', 'uses' => 'Admin\ServiceCategoryController@index' ] );
    Route::post( 'admin/serviceCategories', [ 'as'=> 'admin.serviceCategories.store', 'uses' => 'Admin\ServiceCategoryController@store' ] );
    Route::get( 'admin/serviceCategories/create', [ 'as'=> 'admin.serviceCategories.create', 'uses' => 'Admin\ServiceCategoryController@create' ] );
    Route::put( 'admin/serviceCategories/{serviceCategories}', [ 'as'=> 'admin.serviceCategories.update', 'uses' => 'Admin\ServiceCategoryController@update' ] );
    Route::patch( 'admin/serviceCategories/{serviceCategories}', [ 'as'=> 'admin.serviceCategories.update', 'uses' => 'Admin\ServiceCategoryController@update' ] );
    Route::delete( 'admin/serviceCategories/{serviceCategories}', [ 'as'=> 'admin.serviceCategories.destroy', 'uses' => 'Admin\ServiceCategoryController@destroy' ] );
    Route::get( 'admin/serviceCategories/{language}/{model}', [ 'as'=> 'admin.serviceCategories.edit', 'uses' => 'Admin\ServiceCategoryController@edit' ] );


    // Route::get( 'admin/eventCategories', [ 'as'=> 'admin.eventCategories.index', 'uses' => 'Admin\EventCategoryController@index' ] );
    // Route::post( 'admin/eventCategories', [ 'as'=> 'admin.eventCategories.store', 'uses' => 'Admin\EventCategoryController@store' ] );
    // Route::get( 'admin/eventCategories/create', [ 'as'=> 'admin.eventCategories.create', 'uses' => 'Admin\EventCategoryController@create' ] );
    // Route::put( 'admin/eventCategories/{eventCategories}', [ 'as'=> 'admin.eventCategories.update', 'uses' => 'Admin\EventCategoryController@update' ] );
    // Route::patch( 'admin/eventCategories/{eventCategories}', [ 'as'=> 'admin.eventCategories.update', 'uses' => 'Admin\EventCategoryController@update' ] );
    // Route::delete( 'admin/eventCategories/{eventCategories}', [ 'as'=> 'admin.eventCategories.destroy', 'uses' => 'Admin\EventCategoryController@destroy' ] );
    // Route::get( 'admin/eventCategories/{eventCategories}', [ 'as'=> 'admin.eventCategories.show', 'uses' => 'Admin\EventCategoryController@show' ] );
    // Route::get( 'admin/eventCategories/{eventCategories}/edit', [ 'as'=> 'admin.eventCategories.edit', 'uses' => 'Admin\EventCategoryController@edit' ] );

    Route::get( 'admin/features', [ 'as'=> 'admin.features.index', 'uses' => 'Admin\FeatureController@index' ] );
    Route::post( 'admin/features', [ 'as'=> 'admin.features.store', 'uses' => 'Admin\FeatureController@store' ] );
    Route::get( 'admin/features/create', [ 'as'=> 'admin.features.create', 'uses' => 'Admin\FeatureController@create' ] );
    Route::put( 'admin/features/{features}', [ 'as'=> 'admin.features.update', 'uses' => 'Admin\FeatureController@update' ] );
    Route::patch( 'admin/features/{features}', [ 'as'=> 'admin.features.update', 'uses' => 'Admin\FeatureController@update' ] );
    Route::delete( 'admin/features/{features}', [ 'as'=> 'admin.features.destroy', 'uses' => 'Admin\FeatureController@destroy' ] );
    Route::get( 'admin/features/{features}', [ 'as'=> 'admin.features.show', 'uses' => 'Admin\FeatureController@show' ] );
    Route::get( 'admin/features/{language}/{model}', [ 'as'=> 'admin.features.edit', 'uses' => 'Admin\FeatureController@edit' ] );


    Route::get( 'admin/rates', [ 'as'=> 'admin.rates.index', 'uses' => 'Admin\RatesController@index' ] );
    Route::get( 'admin/rates/search', [ 'as'=> 'admin.rates.search', 'uses' => 'Admin\RatesController@search' ] );
    Route::post( 'admin/rates', [ 'as'=> 'admin.rates.store', 'uses' => 'Admin\RatesController@store' ] );
    Route::get( 'admin/rates/create', [ 'as'=> 'admin.rates.create', 'uses' => 'Admin\RatesController@create' ] );
    Route::put( 'admin/rates/{rates}', [ 'as'=> 'admin.rates.update', 'uses' => 'Admin\RatesController@update' ] );
    Route::patch( 'admin/rates/{rates}', [ 'as'=> 'admin.rates.update', 'uses' => 'Admin\RatesController@update' ] );
    Route::delete( 'admin/rates/{rates}', [ 'as'=> 'admin.rates.destroy', 'uses' => 'Admin\RatesController@destroy' ] );
    Route::get( 'admin/rates/{rates}', [ 'as'=> 'admin.rates.show', 'uses' => 'Admin\RatesController@show' ] );
    Route::get( 'admin/rates/{rates}/edit', [ 'as'=> 'admin.rates.edit', 'uses' => 'Admin\RatesController@edit' ] );


    Route::get( 'admin/packages', [ 'as'=> 'admin.packages.index', 'uses' => 'Admin\PackageController@index' ] );
    Route::post( 'admin/packages', [ 'as'=> 'admin.packages.store', 'uses' => 'Admin\PackageController@store' ] );
    Route::get( 'admin/packages/create', [ 'as'=> 'admin.packages.create', 'uses' => 'Admin\PackageController@create' ] );
    Route::put( 'admin/packages/{packages}', [ 'as'=> 'admin.packages.update', 'uses' => 'Admin\PackageController@update' ] );
    Route::patch( 'admin/packages/{packages}', [ 'as'=> 'admin.packages.update', 'uses' => 'Admin\PackageController@update' ] );
    Route::delete( 'admin/packages/{packages}', [ 'as'=> 'admin.packages.destroy', 'uses' => 'Admin\PackageController@destroy' ] );
    Route::get( 'admin/packages/{packages}', [ 'as'=> 'admin.packages.show', 'uses' => 'Admin\PackageController@show' ] );
    Route::get( 'admin/packages/{language}/{model}', [ 'as'=> 'admin.packages.edit', 'uses' => 'Admin\PackageController@edit' ] );

    //Activity Allies
    Route::get( 'admin/allies', [ 'as'=> 'admin.allies.index', 'uses' => 'Admin\AllyController@index' ] );
    Route::post( 'admin/allies', [ 'as'=> 'admin.allies.store', 'uses' => 'Admin\AllyController@store' ] );
    Route::get( 'admin/allies/create', [ 'as'=> 'admin.allies.create', 'uses' => 'Admin\AllyController@create' ] );
    Route::put( 'admin/allies/{allies}', [ 'as'=> 'admin.allies.update', 'uses' => 'Admin\AllyController@update' ] );
    Route::patch( 'admin/allies/{allies}', [ 'as'=> 'admin.allies.update', 'uses' => 'Admin\AllyController@update' ] );
    Route::delete( 'admin/allies/{allies}', [ 'as'=> 'admin.allies.destroy', 'uses' => 'Admin\AllyController@destroy' ] );
    Route::get( 'admin/allies/{allies}', [ 'as'=> 'admin.allies.show', 'uses' => 'Admin\AllyController@show' ] );
    Route::get( 'admin/allies/{allies}/edit', [ 'as'=> 'admin.allies.edit', 'uses' => 'Admin\AllyController@edit' ] );

    //Settings
    Route::get( 'admin/settings', [ 'as'=> 'admin.settings.index', 'uses' => 'Admin\SettingController@index' ] );
    Route::post( 'admin/settings', [ 'as'=> 'admin.settings.store', 'uses' => 'Admin\SettingController@store' ] );
    Route::get( 'admin/settings/create', [ 'as'=> 'admin.settings.create', 'uses' => 'Admin\SettingController@create' ] );
    Route::put( 'admin/settings/{settings}', [ 'as'=> 'admin.settings.update', 'uses' => 'Admin\SettingController@update' ] );
    Route::patch( 'admin/settings/{settings}', [ 'as'=> 'admin.settings.update', 'uses' => 'Admin\SettingController@update' ] );
    Route::delete( 'admin/settings/{settings}', [ 'as'=> 'admin.settings.destroy', 'uses' => 'Admin\SettingController@destroy' ] );
    Route::get( 'admin/settings/{settings}', [ 'as'=> 'admin.settings.show', 'uses' => 'Admin\SettingController@show' ] );
    Route::get( 'admin/settings/{settings}/edit', [ 'as'=> 'admin.settings.edit', 'uses' => 'Admin\SettingController@edit' ] );

    //Blog
    Route::get( 'admin/blogCategories', [ 'as'=> 'admin.blogCategories.index', 'uses' => 'Admin\BlogCategoryController@index' ] );
    Route::post( 'admin/blogCategories', [ 'as'=> 'admin.blogCategories.store', 'uses' => 'Admin\BlogCategoryController@store' ] );
    Route::get( 'admin/blogCategories/create', [ 'as'=> 'admin.blogCategories.create', 'uses' => 'Admin\BlogCategoryController@create' ] );
    Route::put( 'admin/blogCategories/{blogCategories}', [ 'as'=> 'admin.blogCategories.update', 'uses' => 'Admin\BlogCategoryController@update' ] );
    Route::patch( 'admin/blogCategories/{blogCategories}', [ 'as'=> 'admin.blogCategories.update', 'uses' => 'Admin\BlogCategoryController@update' ] );
    Route::delete( 'admin/blogCategories/{blogCategories}', [ 'as'=> 'admin.blogCategories.destroy', 'uses' => 'Admin\BlogCategoryController@destroy' ] );
    Route::get( 'admin/blogCategories/{language}/{model}', [ 'as'=> 'admin.blogCategories.edit', 'uses' => 'Admin\BlogCategoryController@edit' ] );


    Route::get( 'admin/blogs', [ 'as'=> 'admin.blogs.index', 'uses' => 'Admin\BlogController@index' ] );
    Route::post( 'admin/blogs', [ 'as'=> 'admin.blogs.store', 'uses' => 'Admin\BlogController@store' ] );
    Route::get( 'admin/blogs/create', [ 'as'=> 'admin.blogs.create', 'uses' => 'Admin\BlogController@create' ] );
    Route::put( 'admin/blogs/{blogs}', [ 'as'=> 'admin.blogs.update', 'uses' => 'Admin\BlogController@update' ] );
    Route::patch( 'admin/blogs/{blogs}', [ 'as'=> 'admin.blogs.update', 'uses' => 'Admin\BlogController@update' ] );
    Route::delete( 'admin/blogs/{blogs}', [ 'as'=> 'admin.blogs.destroy', 'uses' => 'Admin\BlogController@destroy' ] );
    Route::get( 'admin/blogs/{blogs}', [ 'as'=> 'admin.blogs.show', 'uses' => 'Admin\BlogController@show' ] );
    Route::get( 'admin/blogs/{language}/{model}', [ 'as'=> 'admin.blogs.edit', 'uses' => 'Admin\BlogController@edit' ] );

    //Room Category Features
    Route::get( 'admin/roomCategoryFeatures', [ 'as'=> 'admin.roomCategoryFeatures.index', 'uses' => 'Admin\RoomCategoryFeatureController@index' ] );
    Route::post( 'admin/roomCategoryFeatures', [ 'as'=> 'admin.roomCategoryFeatures.store', 'uses' => 'Admin\RoomCategoryFeatureController@store' ] );
    Route::get( 'admin/roomCategoryFeatures/create', [ 'as'=> 'admin.roomCategoryFeatures.create', 'uses' => 'Admin\RoomCategoryFeatureController@create' ] );
    Route::put( 'admin/roomCategoryFeatures/{roomCategoryFeatures}', [ 'as'=> 'admin.roomCategoryFeatures.update', 'uses' => 'Admin\RoomCategoryFeatureController@update' ] );
    Route::patch( 'admin/roomCategoryFeatures/{roomCategoryFeatures}', [ 'as'=> 'admin.roomCategoryFeatures.update', 'uses' => 'Admin\RoomCategoryFeatureController@update' ] );
    Route::delete( 'admin/roomCategoryFeatures/{roomCategoryFeatures}', [ 'as'=> 'admin.roomCategoryFeatures.destroy', 'uses' => 'Admin\RoomCategoryFeatureController@destroy' ] );
    Route::get( 'admin/roomCategoryFeatures/{roomCategoryFeatures}', [ 'as'=> 'admin.roomCategoryFeatures.show', 'uses' => 'Admin\RoomCategoryFeatureController@show' ] );
    Route::get( 'admin/roomCategoryFeatures/{roomCategoryFeatures}/edit', [ 'as'=> 'admin.roomCategoryFeatures.edit', 'uses' => 'Admin\RoomCategoryFeatureController@edit' ] );

    //Rooms Services
    Route::get( 'admin/roomsServices', [ 'as'=> 'admin.roomsServices.index', 'uses' => 'Admin\RoomsServiceController@index' ] );
    Route::post( 'admin/roomsServices', [ 'as'=> 'admin.roomsServices.store', 'uses' => 'Admin\RoomsServiceController@store' ] );
    Route::get( 'admin/roomsServices/create', [ 'as'=> 'admin.roomsServices.create', 'uses' => 'Admin\RoomsServiceController@create' ] );
    Route::put( 'admin/roomsServices/{roomsServices}', [ 'as'=> 'admin.roomsServices.update', 'uses' => 'Admin\RoomsServiceController@update' ] );
    Route::patch( 'admin/roomsServices/{roomsServices}', [ 'as'=> 'admin.roomsServices.update', 'uses' => 'Admin\RoomsServiceController@update' ] );
    Route::delete( 'admin/roomsServices/{roomsServices}', [ 'as'=> 'admin.roomsServices.destroy', 'uses' => 'Admin\RoomsServiceController@destroy' ] );
    Route::get( 'admin/roomsServices/{roomsServices}', [ 'as'=> 'admin.roomsServices.show', 'uses' => 'Admin\RoomsServiceController@show' ] );
    Route::get( 'admin/roomsServices/{roomsServices}/edit', [ 'as'=> 'admin.roomsServices.edit', 'uses' => 'Admin\RoomsServiceController@edit' ] );

    Route::get( 'admin/policies', [ 'as'=> 'admin.policies.index', 'uses' => 'Admin\policyController@index' ] );
    Route::post( 'admin/policies', [ 'as'=> 'admin.policies.store', 'uses' => 'Admin\policyController@store' ] );
    Route::get( 'admin/policies/create', [ 'as'=> 'admin.policies.create', 'uses' => 'Admin\policyController@create' ] );
    Route::put( 'admin/policies/{policies}', [ 'as'=> 'admin.policies.update', 'uses' => 'Admin\policyController@update' ] );
    Route::patch( 'admin/policies/{policies}', [ 'as'=> 'admin.policies.update', 'uses' => 'Admin\policyController@update' ] );
    Route::delete( 'admin/policies/{policies}', [ 'as'=> 'admin.policies.destroy', 'uses' => 'Admin\policyController@destroy' ] );
    Route::get( 'admin/policies/{policies}', [ 'as'=> 'admin.policies.show', 'uses' => 'Admin\policyController@show' ] );
    Route::get( 'admin/policies/{language}/{model}', [ 'as'=> 'admin.policies.edit', 'uses' => 'Admin\policyController@edit' ] );

    Route::get( 'admin/roomCategoryServices', [ 'as'=> 'admin.roomCategoryServices.index', 'uses' => 'Admin\RoomCategoryServiceController@index' ] );
    Route::post( 'admin/roomCategoryServices', [ 'as'=> 'admin.roomCategoryServices.store', 'uses' => 'Admin\RoomCategoryServiceController@store' ] );
    Route::get( 'admin/roomCategoryServices/create', [ 'as'=> 'admin.roomCategoryServices.create', 'uses' => 'Admin\RoomCategoryServiceController@create' ] );
    Route::put( 'admin/roomCategoryServices/{roomCategoryServices}', [ 'as'=> 'admin.roomCategoryServices.update', 'uses' => 'Admin\RoomCategoryServiceController@update' ] );
    Route::patch( 'admin/roomCategoryServices/{roomCategoryServices}', [ 'as'=> 'admin.roomCategoryServices.update', 'uses' => 'Admin\RoomCategoryServiceController@update' ] );
    Route::delete( 'admin/roomCategoryServices/{roomCategoryServices}', [ 'as'=> 'admin.roomCategoryServices.destroy', 'uses' => 'Admin\RoomCategoryServiceController@destroy' ] );
    Route::get( 'admin/roomCategoryServices/{roomCategoryServices}', [ 'as'=> 'admin.roomCategoryServices.show', 'uses' => 'Admin\RoomCategoryServiceController@show' ] );
    Route::get( 'admin/roomCategoryServices/{roomCategoryServices}/edit', [ 'as'=> 'admin.roomCategoryServices.edit', 'uses' => 'Admin\RoomCategoryServiceController@edit' ] );

    // Galerias
    Route::get( 'admin/gallerySliders', [ 'as'=> 'admin.gallerySliders.index', 'uses' => 'Admin\GallerySliderController@index' ] );
    Route::get( 'admin/gallerySliders/create', [ 'as'=> 'admin.gallerySliders.create', 'uses' => 'Admin\GallerySliderController@create' ] );
    Route::delete( 'admin/gallerySliders/{gallerySliders}', [ 'as'=> 'admin.gallerySliders.destroy', 'uses' => 'Admin\GallerySliderController@destroy' ] );
    Route::get( 'admin/gallerySliders/{language}/{model}/edit', [ 'as'=> 'admin.gallerySliders.edit', 'uses' => 'Admin\GallerySliderController@edit' ] );

    // General Informations
    Route::get('admin/generalInformations', ['as'=> 'admin.generalInformations.index', 'uses' => 'Admin\GeneralInformationController@index']);
    Route::post('admin/generalInformations', ['as'=> 'admin.generalInformations.store', 'uses' => 'Admin\GeneralInformationController@store']);
    Route::get('admin/generalInformations/create', ['as'=> 'admin.generalInformations.create', 'uses' => 'Admin\GeneralInformationController@create']);
    Route::put('admin/generalInformations/{generalInformations}', ['as'=> 'admin.generalInformations.update', 'uses' => 'Admin\GeneralInformationController@update']);
    Route::patch('admin/generalInformations/{generalInformations}', ['as'=> 'admin.generalInformations.update', 'uses' => 'Admin\GeneralInformationController@update']);
    Route::delete('admin/generalInformations/{generalInformations}', ['as'=> 'admin.generalInformations.destroy', 'uses' => 'Admin\GeneralInformationController@destroy']);
    Route::get('admin/generalInformations/{generalInformations}', ['as'=> 'admin.generalInformations.show', 'uses' => 'Admin\GeneralInformationController@show']);
    Route::get('admin/generalInformations/{generalInformations}/edit', ['as'=> 'admin.generalInformations.edit', 'uses' => 'Admin\GeneralInformationController@edit']);

});

Route::name( 'log-in')->get( 'log-in', function() {
    return redirect( 'clientLogin');
});

Route::get( '{any}', 'FrontController@index')->where( 'any','.*');



Route::get( 'password/reseted/{token}', function(){
    return;
})->name( 'password.reset');

// Route::get( 'admin/formDatas', [ 'as'=> 'admin.formDatas.index', 'uses' => 'Admin\FormDataController@index' ] );
// Route::post( 'admin/formDatas', [ 'as'=> 'admin.formDatas.store', 'uses' => 'Admin\FormDataController@store' ] );
// Route::get( 'admin/formDatas/create', [ 'as'=> 'admin.formDatas.create', 'uses' => 'Admin\FormDataController@create' ] );
// Route::put( 'admin/formDatas/{formDatas}', [ 'as'=> 'admin.formDatas.update', 'uses' => 'Admin\FormDataController@update' ] );
// Route::patch( 'admin/formDatas/{formDatas}', [ 'as'=> 'admin.formDatas.update', 'uses' => 'Admin\FormDataController@update' ] );
// Route::delete( 'admin/formDatas/{formDatas}', [ 'as'=> 'admin.formDatas.destroy', 'uses' => 'Admin\FormDataController@destroy' ] );
// Route::get( 'admin/formDatas/{formDatas}', [ 'as'=> 'admin.formDatas.show', 'uses' => 'Admin\FormDataController@show' ] );
// Route::get( 'admin/formDatas/{formDatas}/edit', [ 'as'=> 'admin.formDatas.edit', 'uses' => 'Admin\FormDataController@edit' ] );


// Route::get( 'admin/lockedRooms', [ 'as'=> 'admin.lockedRooms.index', 'uses' => 'Admin\LockedRoomController@index' ] );
// Route::post( 'admin/lockedRooms', [ 'as'=> 'admin.lockedRooms.store', 'uses' => 'Admin\LockedRoomController@store' ] );
// Route::get( 'admin/lockedRooms/create', [ 'as'=> 'admin.lockedRooms.create', 'uses' => 'Admin\LockedRoomController@create' ] );
// Route::put( 'admin/lockedRooms/{lockedRooms}', [ 'as'=> 'admin.lockedRooms.update', 'uses' => 'Admin\LockedRoomController@update' ] );
// Route::patch( 'admin/lockedRooms/{lockedRooms}', [ 'as'=> 'admin.lockedRooms.update', 'uses' => 'Admin\LockedRoomController@update' ] );
// Route::delete( 'admin/lockedRooms/{lockedRooms}', [ 'as'=> 'admin.lockedRooms.destroy', 'uses' => 'Admin\LockedRoomController@destroy' ] );
// Route::get( 'admin/lockedRooms/{lockedRooms}', [ 'as'=> 'admin.lockedRooms.show', 'uses' => 'Admin\LockedRoomController@show' ] );
// Route::get( 'admin/lockedRooms/{lockedRooms}/edit', [ 'as'=> 'admin.lockedRooms.edit', 'uses' => 'Admin\LockedRoomController@edit' ] );


//L

Route::get( 'admin/tagTranslations', [ 'as'=> 'admin.tagTranslations.index', 'uses' => 'Admin\TagTranslationController@index' ] );
Route::post( 'admin/tagTranslations', [ 'as'=> 'admin.tagTranslations.store', 'uses' => 'Admin\TagTranslationController@store' ] );
Route::get( 'admin/tagTranslations/create', [ 'as'=> 'admin.tagTranslations.create', 'uses' => 'Admin\TagTranslationController@create' ] );
Route::put( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.update', 'uses' => 'Admin\TagTranslationController@update' ] );
Route::patch( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.update', 'uses' => 'Admin\TagTranslationController@update' ] );
Route::delete( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.destroy', 'uses' => 'Admin\TagTranslationController@destroy' ] );
Route::get( 'admin/tagTranslations/{tagTranslations}', [ 'as'=> 'admin.tagTranslations.show', 'uses' => 'Admin\TagTranslationController@show' ] );
Route::get( 'admin/tagTranslations/{tagTranslations}/edit', [ 'as'=> 'admin.tagTranslations.edit', 'uses' => 'Admin\TagTranslationController@edit' ] );