<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post( 'storeMultimediaFront', 'API\UtilsAPIFrontController@storeMultimediaFront' );

Route::post( 'insertTagsIdiomas', 'API\LanguageAPIController@insertTagsIdiomas' );
Route::get( 'getLanguageAll','API\LanguageAPIController@getLanguageAll' );
Route::get( 'setLanguage/{language}','API\LanguageAPIController@setLanguage' );

Route::post( 'get_env', 'API\ConfigAPIController@getEnv' );

Route::get( 'tag_translations', 'API\Admin\TagTranslationAPIController@index' );
// Route::get( 'admin/tag_translations', 'API\Admin\TagTranslationAPIController@index' )->middleware( 'auth:api' );

/* Mostrar los detalles del usuario */
Route::post( 'show/user/details/{user}', 'API\Admin\UserAPIController@showUserDetails' )->middleware( 'auth:api' );
/* Editar los datos del usuario */
Route::post( 'update/user', 'API\Admin\UserAPIController@updateProfile' );
Route::post( 'profile/{user}', 'API\Admin\UserAPIController@updateProfile' );
/* Eliminar los datos del usuario */
Route::get( 'delete/user/{user}', 'API\Admin\UserAPIController@dropOutUser' )->middleware( 'auth:api' );
Route::post( 'delete/user', 'API\Admin\UserAPIController@dropOutUser' )/*->middleware( 'auth:api' )*/;
/* Cambio de password */
Route::post( 'change/password', 'API\Admin\UserAPIController@changePassword' )->middleware( 'auth:api' );


Route::post( 'contactus', 'API\UtilsAPIFrontController@contactUs' );
Route::post( 'newsletter', 'API\UtilsAPIFrontController@newsLetter' );
Route::post( 'experience', 'API\UtilsAPIFrontController@experienceForm' );
Route::get( 'unsubscribe/newsletter/{user}', 'API\UtilsAPIFrontController@unsubscribeNewsletter' );


Route::post( 'forgotPassword','Auth\ForgotPasswordController@sendResetLinkEmail' );
Route::post( 'password/reset','Auth\ResetPasswordController@reset' );


// Route::middleware( 'auth:api' )->get( '/user', function (Request $request) {
//     return $request->user();
// });


//  ____
// |  _ \    ___     ___    _ __ ___    ___
// | |_) |  / _ \   / _ \  | '_ ` _ \  / __|
// |  _ <  | (_) | | (_) | | | | | | | \__ \
// |_| \_\  \___/   \___/  |_| |_| |_| |___/
//


// RoomLocation (localidades)
Route::get( 'admin/room_locations', 'API\Admin\RoomLocationAPIController@index' );
Route::post( 'admin/room_locations', 'API\Admin\RoomLocationAPIController@store' );
Route::get( 'admin/room_locations/{room_locations}', 'API\Admin\RoomLocationAPIController@show' );
Route::put( 'admin/room_locations/{room_locations}', 'API\Admin\RoomLocationAPIController@update' );
Route::patch( 'admin/room_locations/{room_locations}', 'API\Admin\RoomLocationAPIController@update' );
Route::delete( 'admin/room_locations/{room_locations}', 'API\Admin\RoomLocationAPIController@destroy' );


// RoomCategory (casas)
Route::get( 'admin/room_categories', 'API\Admin\RoomCategoryAPIController@index' );
Route::get( 'admin/room_categories/{room_categories}', 'API\Admin\RoomCategoryAPIController@show' );
Route::post( 'admin/room_categories', 'API\Admin\RoomCategoryAPIController@store' );
Route::put( 'admin/room_categories/{room_category}', 'API\Admin\RoomCategoryAPIController@update' );
Route::delete( 'admin/room_categories/{room_category}/{image}', 'API\Admin\RoomCategoryAPIController@deleteImage' );
Route::get( 'admin/room_category_services/{room_category}', 'API\Admin\RoomCategoryAPIController@roomCategoryServices' ); // API get Room Services


// Room (habitaciones)
Route::post( 'admin/rooms', 'API\Admin\RoomAPIController@index' );// API Listado de ROOM (DISPONIBILIDAD)
Route::get( 'admin/roomAll/', 'API\Admin\RoomAPIController@roomAll' );
Route::post( 'admin/rooms/lock', 'API\Admin\RoomAPIController@lock' ); // API Bloquear temporalmente ROOM (BLOQUEO TEMPORAL)
Route::post( 'admin/rooms/unlock', 'API\Admin\RoomAPIController@unlock' ); // API Desbloquear ROOM (BLOQUEO TEMPORAL)
Route::get( 'admin/rooms/{room}', 'API\Admin\RoomAPIController@show' ); // API Obtener detalle de ROOM
Route::get( 'admin/room_services/{room}', 'API\Admin\RoomAPIController@roomServices' ); // API get Room Services
Route::delete( 'admin/rooms/{room}/{image}', 'API\Admin\RoomAPIController@deleteImage' );
Route::get( 'admin/rooms', 'API\Admin\RoomAPIController@all' ); // API Obtener detalle de ROOM
Route::put('admin/rooms/{room}', 'API\Admin\RoomAPIController@update');

// Room (Ruta API para crear )
// Se tuvo que crear en esa ruta porque ya estaba ocupada con la API de Listado de ROOM (DISPONIBILIDAD)
Route::post('admin/room', 'API\Admin\RoomAPIController@store');

// Booking
Route::get( 'admin/bookings/user/{user_id}', 'API\Admin\BookingAPIController@index' );
Route::post( 'admin/bookings', 'API\Admin\BookingAPIController@store' ); // (BOOKING)
Route::get( 'admin/bookings/{booking}', 'API\Admin\BookingAPIController@show' );
Route::put( 'admin/bookings/hide/{booking}', 'API\Admin\BookingAPIController@hide' );

Route::post( 'admin/bookings_calendar', 'API\Admin\BookingDetailAPIController@index' ); // (BOOKING)

Route::get( 'admin/room_seasons', 'API\Admin\RoomSeasonAPIController@index' );
Route::get( 'admin/room_seasons/search', 'API\Admin\RoomSeasonAPIController@searchOferte' );
Route::post( 'admin/room_seasons', 'API\Admin\RoomSeasonAPIController@store' );

Route::post( 'admin/room_seasons_calendar', 'API\Admin\RoomSeasonAPIController@calendar' );
// Route::post( 'admin/room_seasons', 'API\Admin\RoomSeasonAPIController@store' );
// Route::get( 'admin/room_seasons/{room_seasons}', 'API\Admin\RoomSeasonAPIController@show' );
Route::put( 'admin/room_seasons/{room_seasons}', 'API\Admin\RoomSeasonAPIController@update' );
Route::patch( 'admin/room_seasons/{room_seasons}', 'API\Admin\RoomSeasonAPIController@update' );
// Route::delete( 'admin/room_seasons/{room_seasons}', 'API\Admin\RoomSeasonAPIController@destroy' );

// Route::get( 'admin/newsletter_users', 'API\Admin\NewsletterUserAPIController@index' );
// Route::post( 'admin/newsletter_users', 'API\Admin\NewsletterUserAPIController@store' );
// Route::get( 'admin/newsletter_users/{newsletter_users}', 'API\Admin\NewsletterUserAPIController@show' );
// Route::put( 'admin/newsletter_users/{newsletter_users}', 'API\Admin\NewsletterUserAPIController@update' );
// Route::patch( 'admin/newsletter_users/{newsletter_users}', 'API\Admin\NewsletterUserAPIController@update' );
// Route::delete( 'admin/newsletter_users/{newsletter_users}', 'API\Admin\NewsletterUserAPIController@destroy' );


// Route::get( 'admin/client_requests', 'API\Admin\ClientRequestAPIController@index' );
Route::post( 'admin/client_requests', 'API\Admin\ClientRequestAPIController@store' );
// Route::get( 'admin/client_requests/{client_requests}', 'API\Admin\ClientRequestAPIController@show' );
// Route::put( 'admin/client_requests/{client_requests}', 'API\Admin\ClientRequestAPIController@update' );
// Route::patch( 'admin/client_requests/{client_requests}', 'API\Admin\ClientRequestAPIController@update' );
// Route::delete( 'admin/client_requests/{client_requests}', 'API\Admin\ClientRequestAPIController@destroy' );


Route::get( 'admin/request_categories', 'API\Admin\RequestCategoryAPIController@index' );
Route::post( 'admin/request_categories', 'API\Admin\RequestCategoryAPIController@store' );
Route::get( 'admin/request_categories/{request_categories}', 'API\Admin\RequestCategoryAPIController@show' );
Route::put( 'admin/request_categories/{request_categories}', 'API\Admin\RequestCategoryAPIController@update' );
Route::patch( 'admin/request_categories/{request_categories}', 'API\Admin\RequestCategoryAPIController@update' );
Route::delete( 'admin/request_categories/{request_categories}', 'API\Admin\RequestCategoryAPIController@destroy' );





Route::middleware( 'auth:api' )->get( '/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'auth'], function () {
    Route::post( 'login', 'API\Auth\AuthAPIController@login' );
    Route::post( 'signup', 'API\Auth\AuthAPIController@signup' );
    Route::get( 'signup/activate/{token}', 'API\Auth\AuthAPIController@signupActivate' );

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get( 'logout', 'API\Auth\AuthAPIController@logout' );
        Route::get( 'user', 'API\Auth\AuthAPIController@user' );
    });
});


/*Route::group(['namespace' => 'Auth', 'middleware' => 'auth:api', 'prefix' => 'password'], function () {
    //Envia correo con el token para el cambio de clave
    Route::post( 'send/reset', 'PasswordResetAPIController@create' );
    Route::get( 'find/{token}', 'PasswordResetAPIController@find' );
    Route::post( 'reset', 'PasswordResetAPIController@reset' );
});*/


Route::get( 'admin/rates', 'API\Admin\RatesAPIController@index' );
Route::post( 'admin/rates', 'API\Admin\RateAPIController@index' );
//Route::post( 'admin/rates', 'Admin\RatesAPIController@store' );
Route::get( 'admin/rates/{rates}', 'Admin\RatesAPIController@show' );
Route::put( 'admin/rates/{rates}', 'Admin\RatesAPIController@update' );
Route::patch( 'admin/rates/{rates}', 'Admin\RatesAPIController@update' );
Route::delete( 'admin/rates/{rates}', 'Admin\RatesAPIController@destroy' );

// para correos con ofertas
Route::post( 'admin/rates/get_rate/{token}', 'API\Admin\RateAPIController@getRate' );


Route::get( 'admin/packages', 'API\Admin\PackageAPIController@index' );
// Route::post( 'admin/packages', 'Admin\PackageAPIController@store' );
Route::get( 'admin/packages/{packages}', 'API\Admin\PackageAPIController@show' );
// Route::put( 'admin/packages/{packages}', 'Admin\PackageAPIController@update' );
// Route::patch( 'admin/packages/{packages}', 'Admin\PackageAPIController@update' );
// Route::delete( 'admin/packages/{packages}', 'Admin\PackageAPIController@destroy' );


Route::get( 'admin/activity_requests', 'Admin\ActivityRequestAPIController@index' );
Route::post( 'admin/activity_requests', 'API\Admin\ActivityRequestAPIController@store' );
Route::get( 'admin/activity_requests/{activity_requests}', 'Admin\ActivityRequestAPIController@show' );
Route::put( 'admin/activity_requests/{activity_requests}', 'Admin\ActivityRequestAPIController@update' );
Route::patch( 'admin/activity_requests/{activity_requests}', 'Admin\ActivityRequestAPIController@update' );
Route::delete( 'admin/activity_requests/{activity_requests}', 'Admin\ActivityRequestAPIController@destroy' );

Route::get( 'admin/activity_categories', 'API\Admin\ActivityCategoryAPIController@index' );
Route::get( 'admin/activities', 'API\Admin\ActivityAPIController@index' );
Route::get( 'admin/activity/{slug}', 'API\Admin\ActivityAPIController@show' );
Route::delete( 'admin/activities/{activity}/{image}', 'API\Admin\ActivityAPIController@deleteImage' );

Route::post( 'admin/activities', 'API\Admin\ActivityAPIController@store' );
Route::put( 'admin/activities/{id}', 'API\Admin\ActivityAPIController@update' );



Route::get( 'admin/allies', 'API\Admin\AllyAPIController@index' );
Route::post( 'admin/allies', 'API\Admin\AllyAPIController@store' );
Route::get( 'admin/allies/{allies}', 'API\Admin\AllyAPIController@show' );
Route::put( 'admin/allies/{allies}', 'API\Admin\AllyAPIController@update' );
Route::patch( 'admin/allies/{allies}', 'API\Admin\AllyAPIController@update' );
Route::delete( 'admin/allies/{allies}', 'API\Admin\AllyAPIController@destroy' );


Route::get( 'admin/settings', 'API\Admin\SettingAPIController@index' );
Route::post( 'admin/settings', 'Admin\SettingAPIController@store' );
Route::get( 'admin/settings/{settings}', 'Admin\SettingAPIController@show' );
Route::put( 'admin/settings/{settings}', 'Admin\SettingAPIController@update' );
Route::patch( 'admin/settings/{settings}', 'Admin\SettingAPIController@update' );
Route::delete( 'admin/settings/{settings}', 'Admin\SettingAPIController@destroy' );
Route::delete( 'admin/delet/{settings}', 'Admin\SettingAPIController@destroy' );


Route::get( 'admin/blog_categories', 'Admin\BlogCategoryAPIController@index' );
Route::post( 'admin/blog_categories', 'Admin\BlogCategoryAPIController@store' );
Route::get( 'admin/blog_categories/{blog_categories}', 'Admin\BlogCategoryAPIController@show' );
Route::put( 'admin/blog_categories/{blog_categories}', 'Admin\BlogCategoryAPIController@update' );
Route::patch( 'admin/blog_categories/{blog_categories}', 'Admin\BlogCategoryAPIController@update' );
Route::delete( 'admin/blog_categories/{blog_categories}', 'Admin\BlogCategoryAPIController@destroy' );


Route::get( 'admin/blogs', 'API\Admin\BlogAPIController@index' );
Route::post( 'admin/blogs', 'API\Admin\BlogAPIController@store' );
Route::get( 'admin/blogs/{blogs}', 'API\Admin\BlogAPIController@show' );
Route::put( 'admin/blogs/{blogs}', 'API\Admin\BlogAPIController@update' );
Route::patch( 'admin/blogs/{blogs}', 'API\Admin\BlogAPIController@update' );
Route::delete( 'admin/blogs/{blogs}', 'API\Admin\BlogAPIController@destroy' );


Route::get( 'admin/blog_comments', 'Admin\BlogCommentAPIController@index' );
Route::post( 'admin/blog_comments', 'Admin\BlogCommentAPIController@store' );
Route::get( 'admin/blog_comments/{blog_comments}', 'Admin\BlogCommentAPIController@show' );
Route::put( 'admin/blog_comments/{blog_comments}', 'Admin\BlogCommentAPIController@update' );
Route::patch( 'admin/blog_comments/{blog_comments}', 'Admin\BlogCommentAPIController@update' );
Route::delete( 'admin/blog_comments/{blog_comments}', 'Admin\BlogCommentAPIController@destroy' );


Route::get( 'admin/room_category_features', 'Admin\RoomCategoryFeatureAPIController@index' );
Route::post( 'admin/room_category_features', 'Admin\RoomCategoryFeatureAPIController@store' );
Route::get( 'admin/room_category_features/{room_category_features}', 'Admin\RoomCategoryFeatureAPIController@show' );
Route::put( 'admin/room_category_features/{room_category_features}', 'Admin\RoomCategoryFeatureAPIController@update' );
Route::patch( 'admin/room_category_features/{room_category_features}', 'Admin\RoomCategoryFeatureAPIController@update' );
Route::delete( 'admin/room_category_features/{room_category_features}', 'Admin\RoomCategoryFeatureAPIController@destroy' );


Route::get( 'admin/policies', 'API\Admin\policyAPIController@index' );
Route::post( 'admin/policies', 'API\Admin\policyAPIController@store' );
Route::get( 'admin/policies/{policies}', 'API\Admin\policyAPIController@show' );
Route::put( 'admin/policies/{policies}', 'API\Admin\policyAPIController@update' );
Route::patch( 'admin/policies/{policies}', 'API\Admin\policyAPIController@update' );
Route::delete( 'admin/policies/{policies}', 'API\Admin\policyAPIController@destroy' );


Route::get( 'admin/room_category_services', 'Admin\RoomCategoryServiceAPIController@index' );
Route::post( 'admin/room_category_services', 'Admin\RoomCategoryServiceAPIController@store' );
Route::get( 'admin/room_category_services/{room_category_services}', 'Admin\RoomCategoryServiceAPIController@show' );
Route::put( 'admin/room_category_services/{room_category_services}', 'Admin\RoomCategoryServiceAPIController@update' );
Route::patch( 'admin/room_category_services/{room_category_services}', 'Admin\RoomCategoryServiceAPIController@update' );
Route::delete( 'admin/room_category_services/{room_category_services}', 'Admin\RoomCategoryServiceAPIController@destroy' );

// Galerias
Route::get( 'admin/gallery_sliders', 'API\Admin\GallerySliderAPIController@index' );
Route::post( 'admin/gallery_sliders', 'API\Admin\GallerySliderAPIController@store' );
Route::get( 'admin/gallery_sliders/{gallery_sliders}', 'API\Admin\GallerySliderAPIController@show' );
Route::put( 'admin/gallery_sliders/{gallery_sliders}', 'API\Admin\GallerySliderAPIController@update' );
Route::patch( 'admin/gallery_sliders/{gallery_sliders}', 'API\Admin\GallerySliderAPIController@update' );
// Route::delete( 'admin/gallery_sliders/{gallery_sliders}', 'API\Admin\GallerySliderAPIController@destroy' );

//General Informations
Route::get('admin/general_informations', 'API\Admin\GeneralInformationAPIController@index');
Route::post('admin/general_informations', 'API\Admin\GeneralInformationAPIController@store');
Route::get('admin/general_informations/{general_informations}', 'API\Admin\GeneralInformationAPIController@show');
Route::put('admin/general_informations', 'API\Admin\GeneralInformationAPIController@update');
Route::patch('admin/general_informations/{general_informations}', 'API\Admin\GeneralInformationAPIController@update');
Route::delete('admin/general_informations/{general_informations}', 'API\Admin\GeneralInformationAPIController@destroy');