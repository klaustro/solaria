<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->text('slug');

            $table->text('image')->nullable()->default(null);

            $table->double('price', 12, 2)->nullable()->default(0.00);
            $table->double('iva', 12, 2)->nullable()->default(0.00);

            $table->integer('adults_quantity')->nullable()->default(null);
            $table->integer('children_quantity')->nullable()->default(null);

            $table->unsignedInteger('room_category_id');
            $table->foreign('room_category_id')->references('id')->on('room_categories')->onDelete('cascade');

            $table->unsignedInteger('status_id')->default(1);
            $table->foreign('status_id')->references('id')->on('statuses')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
