<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDetailPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_detail_prices', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('booking_detail_id');
            $table->foreign('booking_detail_id')->references('id')->on('booking_details');

            $table->text('type')->nullable()->default(null);
            $table->text('name')->nullable()->default(null);
            $table->double('price', 12, 2)->nullable()->default(null);
            $table->integer('nights')->nullable()->default(null);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_detail_prices');
    }
}
