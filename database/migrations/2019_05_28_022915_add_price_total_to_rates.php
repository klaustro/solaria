<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceTotalToRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'rates', function ( Blueprint $table ) {
            $table->double( 'price_total', 12, 2 )->nullable()->default( null );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'rates', function ( Blueprint $table ) {
            $table->dropColumn( 'price_total' );
        });
    }
}
