<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'invoices', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->string( 'code', 10 )->nullable()->default( null );

            $table->unsignedInteger( 'status_id' )->default( 4 );
            $table->foreign( 'status_id' )->references( 'id' )->on( 'statuses' )->onDelete( 'cascade' );

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'invoices' );
    }
}
