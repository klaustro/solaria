ç<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'rates', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->unsignedInteger( 'room_id' );
            $table->foreign( 'room_id' )->references( 'id' )->on( 'rooms' )->onDelete( 'cascade' );

            $table->date( 'start_date' );
            $table->date( 'end_date' );

            $table->unsignedInteger( 'package_id' )->nullable()->default( null );
            $table->foreign( 'package_id' )->references( 'id' )->on( 'packages' )->onDelete( 'cascade' );

            $table->integer( 'discount' )->nullable()->default( null );
            $table->double( 'price_temp', 12, 2 );
            $table->double( 'price', 12, 2 );
            $table->double( 'iva', 12, 2 )->default( 0.22 );
            $table->integer( 'num_people' )->nullable()->default( null );

            $table->string( 'email' )->nullable()->default( null );
            $table->text( 'comment' )->nullable()->default( null );

            $table->unsignedInteger( 'blog_id' )->nullable()->default( null );
            $table->foreign( 'blog_id' )->references( 'id' )->on( 'blogs' );

            $table->timestamps();
            $table->softDeletes();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'rates' );
    }
}
