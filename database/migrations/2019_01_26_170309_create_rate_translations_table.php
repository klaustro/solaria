<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'rate_translations', function ( Blueprint $table ) {
            $table->increments( 'id' );

            $table->unsignedInteger( 'rate_id' );
            $table->foreign( 'rate_id' )->references( 'id' )->on( 'rates' )->onDelete( 'cascade' );

            $table->unsignedInteger( 'language_id' );
            $table->foreign( 'language_id' )->references( 'id' )->on( 'languages' )->onDelete( 'cascade' );

            $table->string( 'title' )->nullable()->default( null );
            $table->string( 'subtitle' )->nullable()->default( null );
            $table->text( 'description' )->nullable()->default( null );

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'rate_translations' );
    }
}
