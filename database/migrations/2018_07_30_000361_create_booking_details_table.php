<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_details', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('booking_id');
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');

            $table->unsignedInteger('room_id');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');

            $table->double('room_price', 12, 2)->nullable()->default(null);
            $table->double('room_iva', 12, 2)->nullable()->default(null);
            $table->text('room_name')->nullable()->default(null);

            $table->integer('adults_quantity')->nullable()->default(null);
            $table->integer('children_quantity')->nullable()->default(null);

            $table->date('checkin_date');
            $table->date('checkout_date');

            // DATOS DEL RESPONSABLE
            $table->unsignedInteger('data_form_id')->nullable()->default(null)->comment('The data of the person responsible for this object model booked');
            $table->foreign('data_form_id')->references('id')->on('data_forms')->onDelete('cascade');

            $table->double('iva_item', 12, 2);
            $table->double('total_item', 12, 2);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_details');
    }
}
