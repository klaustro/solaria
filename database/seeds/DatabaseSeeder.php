<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OAuthSeeder::class,

            StatusesTableSeeder::class,
            LanguagesTableSeeder::class,
            StatusTranslationsTableSeeder::class,
            RolesTableSeeder::class,
            UsersTableSeeder::class,

            // TagTranslations
            ScreensCSVSeeder::class,
            FrontSectionsCSVSeeder::class,
            ScreensFrontSetionsCSVSeeder::class,
            TagTranslationCSVSeeder::class,

            RequestsTableSeeder::class,
            RoomLocationSeeder::class,
            ServicesTableSeeder::class,
            RoomCategorySeeder::class,
            // Product
            // ProductsTableSeeder::class,
            // Room
            RoomsTableSeeder::class,
            RatesTableseeder::class,
            ActivityCategorySeeder::class,
            AllyTableSeeder::class,
            ActivitySeeder::class,
            ActivityRequestSeeder::class,
            SettingTableSeeder::class,
            NewsletterUserTableSeeder::class,
            BlogTableSeeder::class,
            PolicyTableseeder::class,
            MethodPaymentTableseeder::class,
            RoomsSeasonCSVSeeder::class,
            RoomsSeasonTranslationsCSVSeeder::class,
            //BookingTableSeeder::class,
            //RoomSeasonTableSeeder::class,

            // Gallery
            GallerySliderTableSeeder::class,
            GeneralInformationTableSeeder::class,
        ]);
    }
}
