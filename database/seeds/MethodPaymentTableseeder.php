<?php

use Illuminate\Database\Seeder;

class MethodPaymentTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_methods')->insert([
            [
                'code' =>  001,
                'name' => 'Paypal',
            ],
            [
                'code' =>  002,
                'name' => 'Efficace',
            ],
            [
                'code' =>  003,
                'name' => 'Transferimento',
            ]
        ]);
    }
}
