<?php

use Illuminate\Database\Seeder;

class RatesTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table( 'packages' )->insert( [
            'date'       => '0,1,2,3,4,5,6',
            'min_nights' => '1',
            'max_nights' => '20'

        ] );

        DB::table( 'package_translations' )->insert( [
            'package_id'  => 1,
            'language_id' => 1,
            'name'        => 'Offerte Especiali',
            'description' => 'descrizione package all days'

        ] );

        /*DB::table( 'rates' )->insert( [
            'room_id'     =>1,
            'start_date'  => '2019/01/01',
            'end_date'    => '2021/06/15',
            'package_id'  => 1,
            'price'       => 50.00,
            'iva'         => 0.22,
            'num_people'  => 3
        ] );*/

    }
}
