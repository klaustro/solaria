<?php

use App\Models\Admin\Activity;
use App\Models\Admin\ActivityCategory;
use App\Models\Admin\ActivityTranslation;
use App\Models\Admin\Multimedia;
use App\Models\Admin\Row;
use App\Models\Admin\RowsMultimedia;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $experiences = [
            [
                'category' => 'spiagge',
                'italianTitle' => 'Spiagge',
                'italianDescription' => 'Budoni e San Teodoro sono certamente un punto di arrivo per il viaggiatore, ma rappresentano anche un ottimo punto di partenza per',
                'englishTitle' => 'Beaches',
                'englishDescription' => 'Budoni and San Teodoro are certainly a point of arrival for the traveler, but also represent an excellent starting point for',
                'gallery' => [
                    '/images/esperienze/solaria-vacanze-cala-brandinchi.jpg',
                    '/images/esperienze/solaria-vacanze-capo.jpg',
                    '/images/esperienze/solaria-vacanze-isuledda.jpg',
                    '/images/esperienze/solaria-vacanze-lacinta.jpg',
                ],
            ],
            [
                'category' => 'sport',
                'italianTitle' => 'Sport',
                'italianDescription' => 'I due paesi di Budoni e San Teodoro offrono molte possibilità di praticare sport come immersioni, windsurf, Trekking.equitazione, golf e tennis',
                'englishTitle' => 'Sport',
                'englishDescription' => 'The two villages of Budoni and San Teodoro offer many possibilities for practicing sports such as diving, windsurfing, trekking, horse riding, golf and tennis.',
                'gallery' => [
                    '/images/esperienze/solaria-vacanze-golf.jpg',
                    '/images/esperienze/solaria-vacanze-tennis.jpg',
                ],
            ],
            [
                'category' => 'escursioni',
                'italianTitle' => 'Escursioni',
                'italianDescription' => 'Avrai a disposizione tantissime escursioni con i migliori professionisti del `settore` => dalle gite in barca ai tour archeologici, dai percorsi in quad, bici e fuoristrada ai trekking',
                'englishTitle' => 'Escursion',
                'englishDescription' => 'You will have lots of excursions with the best professionals of the `sector` => from boat trips to archaeological tours, from quad routes, bikes and off-road to trekking',
                'gallery' => [
                    '/images/esperienze/solaria-vacanze-trekking.jpg'
                ],
            ],
            [
                'category' => 'nightlife',
                'italianTitle' => 'Nightlife',
                'englishTitle' => 'Nightlife',
                'italianDescription' => 'Le serate dell’estate Sarda offrono la risposta a tutte le esigenze. Se dopo una lunga giornata di mare vuoi mangiar fuori',
                'englishDescription' => 'Sardinian summer evenings offer the answer to all needs. If you want to eat out after a long day at sea',
                'gallery' => [
                    '/images/home/esperience-slider/solaria-vacanze-esperienze-nightlife-min.jpg',
                ],
            ],
            [
                'category' => 'enogastronomia',
                'italianTitle' => 'Enogastronomia',
                'italianDescription' => 'Come in molte parti della Sardegna, Budoni e San Teodoro offrono la possibilità di un viaggio tra i sapori antichi e moderni della regione',
                'englishTitle' => 'Food & Wine',
                'englishDescription' => 'As in many parts of Sardinia, Budoni and San Teodoro offer the possibility of a journey through the ancient and modern flavors of the region',
                'gallery' => [
                    '/images/home/esperience-slider/solaria-vacanze-esperienze-enogastronomia-min.jpg',
                ],
            ],
            [
                'category' => 'archeologia',
                'italianTitle' => 'archeologia',
                'italianDescription' => 'Come in molte parti della Sardegna, Budoni e San Teodoro offrono la possibilità di un viaggio tra i sapori antichi e moderni della regione',
                'englishTitle' => 'Archeology',
                'englishDescription' => 'As in many parts of Sardinia, Budoni and San Teodoro offer the possibility of a journey through the ancient and modern flavors of the region',
                'gallery' => [
                    '/images/home/esperience-slider/caprera.jpg',
                ],
            ]
        ];
            foreach ($experiences as $key => $experience) {
                $activity = factory(Activity::class)->create([
                    'activity_category_id' => $key + 1,
                    'slug' => Str::slug($experience['italianTitle']),
                ]);

                factory(ActivityTranslation::class)->create([
                    'activity_id' => $activity->id,
                    'title' => $experience['italianTitle'],
                    'description' => $experience['italianDescription'],
                    'language_id' => 1
                ]);

                factory(ActivityTranslation::class)->create([
                    'activity_id' => $activity->id,
                    'title' => $experience['englishTitle'],
                    'description' => $experience['englishDescription'],
                    'language_id' => 2
                ]);

                $row = new Row();
                $activity->row()->save($row);

                foreach ($experience['gallery'] as $key => $image) {
                    $multimedia = factory(Multimedia::class)->create([
                        'name' => basename($image),
                        'path' => str_replace(basename($image), '', $image)
                    ]);

                    $rows_multimedia = new RowsMultimedia();
                    $rows_multimedia->row_id = $row->id;
                    $rows_multimedia->multimedia_id = $multimedia->id;
                    $rows_multimedia->save();
                }
            }
    }
}


