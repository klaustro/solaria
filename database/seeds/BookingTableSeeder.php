<?php

use App\Models\Admin\Booking;
use App\Models\Admin\BookingDetail;
use App\Models\Admin\BookingDetailPrice;
use Illuminate\Database\Seeder;

class BookingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Booking::class, 200)->create()->each(function($booking){
            $detail = $booking->bookingDetails()->save(factory(BookingDetail::class)->make([
                'room_id' => rand(1, 16),
            ]));

            $detail->bookingDetailPrices()->save(factory(BookingDetailPrice::class)->make());
        });
    }
}
