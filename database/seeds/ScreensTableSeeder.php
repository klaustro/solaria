<?php

use Illuminate\Database\Seeder;

class ScreensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('screens')->insert([
            [
                'id' => 1,
                'code' => 'backoffice',
                'name' => 'backoffice'
            ]
        ]);
    }
}
