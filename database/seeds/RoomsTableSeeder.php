<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  __  __           _   _     _   _                          _   _
        // |  \/  |  _   _  | | | |_  (_) | |  _ __ ___     ___    __| | (_)   __ _
        // | |\/| | | | | | | | | __| | | | | | '_ ` _ \   / _ \  / _` | | |  / _` |
        // | |  | | | |_| | | | | |_  | | | | | | | | | | |  __/ | (_| | | | | (_| |
        // |_|  |_|  \__,_| |_|  \__| |_| |_| |_| |_| |_|  \___|  \__,_| |_|  \__,_|

            DB::table('multimedias')->insert([
                [//multimedias.id => 1
                    'name'          => '5-trilocale-piano-terra-con-ampio-giardino-minipiscina-idromassaggio-esterna-aria-condizionata-e-wifi--15.jpg',
                    'description'   => 'Villetta Stella Maris 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 2
                    'name'          => '5-trilocale-piano-terra-con-ampio-giardino-minipiscina-idromassaggio-esterna-aria-condizionata-e-wifi--16.jpg',
                    'description'   => 'Villetta Stella Maris 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 3
                    'name'          => '5-trilocale-piano-terra-con-ampio-giardino-minipiscina-idromassaggio-esterna-aria-condizionata-e-wifi--17.jpg',
                    'description'   => 'Villetta Stella Maris 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 4
                    'name'          => '5-trilocale-piano-terra-con-ampio-giardino-minipiscina-idromassaggio-esterna-aria-condizionata-e-wifi--19.jpg',
                    'description'   => 'Villetta Stella Maris 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 5
                    'name'          => '5-villetta-stella-maris-6.jpg',
                    'description'   => 'Villetta Stella Maris 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 6
                    'name'          => '5-villetta-stella-maris-7.jpg',
                    'description'   => 'Villetta Stella Maris 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 7
                    'name'          => '5-villetta-stella-maris-11.jpg',
                    'description'   => 'Villetta Stella Maris 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 8
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--1.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 9
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--2.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 10
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--3.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 11
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--4.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 12
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--5.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 13
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--6.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 13
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--7.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 14
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--8.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 15
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--9.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 16
                    'name'          => '4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--10.jpg',
                    'description'   => 'Residence Solaria 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 17
                    'name'          => '12-trilocale-piano-terra-con-giardino-solaria-2--7.jpg',
                    'description'   => 'Residence Solaria 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 18
                    'name'          => '12-trilocale-piano-terra-con-giardino-solaria-2--8.jpg',
                    'description'   => 'Residence Solaria 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 19
                    'name'          => '12-trilocale-piano-terra-con-giardino-solaria-2--9.jpg',
                    'description'   => 'Residence Solaria 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 20
                    'name'          => '13-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-3--7.jpg',
                    'description'   => 'Residence Solaria 3', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 21
                    'name'          => '13-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-3--8.jpg',
                    'description'   => 'Residence Solaria 3', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 22
                    'name'          => '14-solaria-4-trilocale-primo-piano-con-veranda-vista-mare-aria-condizionata-e-wifi--8.jpg',
                    'description'   => 'Residence Solaria 4', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 23
                    'name'          => '14-solaria-4-trilocale-primo-piano-con-veranda-vista-mare-aria-condizionata-e-wifi--9.jpg',
                    'description'   => 'Residence Solaria 4', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 24
                    'name'          => '14-solaria-4-trilocale-primo-piano-con-veranda-vista-mare-aria-condizionata-e-wifi--10.jpg',
                    'description'   => 'Residence Solaria 4', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 25
                    'name'          => '14-solaria-4-trilocale-primo-piano-con-veranda-vista-mare-aria-condizionata-e-wifi--11.jpg',
                    'description'   => 'Residence Solaria 4', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 26
                    'name'          => '14-solaria-4-trilocale-primo-piano-con-veranda-vista-mare-aria-condizionata-e-wifi--12.jpg',
                    'description'   => 'Residence Solaria 4', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 27
                    'name'          => '14-solaria-4-trilocale-primo-piano-con-veranda-vista-mare-aria-condizionata-e-wifi--13.jpg',
                    'description'   => 'Residence Solaria 4', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 28
                    'name'          => '20-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-5--7.jpg',
                    'description'   => 'Residence Solaria 5', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 29
                    'name'          => '20-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-5--8.jpg',
                    'description'   => 'Residence Solaria 5', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 30
                    'name'          => '20-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-5--9.jpg',
                    'description'   => 'Residence Solaria 5', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 31
                    'name'          => '20-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-5--10.jpg',
                    'description'   => 'Residence Solaria 5', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 32
                    'name'          => '20-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-5--11.jpg',
                    'description'   => 'Residence Solaria 5', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 33
                    'name'          => '21-trilocale-piano-terra-con-giardino-solaria-6--7.jpg',
                    'description'   => 'Residence Solaria 6', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 34
                    'name'          => '21-trilocale-piano-terra-con-giardino-solaria-6--8.jpg',
                    'description'   => 'Residence Solaria 6', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 35
                    'name'          => '21-trilocale-piano-terra-con-giardino-solaria-6--9.jpg',
                    'description'   => 'Residence Solaria 6', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 36
                    'name'          => '21-trilocale-piano-terra-con-giardino-solaria-6--10.jpg',
                    'description'   => 'Residence Solaria 6', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 37
                    'name'          => '21-trilocale-piano-terra-con-giardino-solaria-6--11.jpg',
                    'description'   => 'Residence Solaria 6', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 38
                    'name'          => '22-solaria-7-trilocale-piano-terra-con-giardino-aria-condizionata-e-wifi-12.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 39
                    'name'          => '22-solaria-7-trilocale-piano-terra-con-giardino-aria-condizionata-e-wifi-13.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 40
                    'name'          => '22-solaria-7-trilocale-piano-terra-con-giardino-aria-condizionata-e-wifi-14.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 41
                    'name'          => '22-solaria-7-trilocale-piano-terra-con-giardino-aria-condizionata-e-wifi-15.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 42
                    'name'          => '22-solaria-7-trilocale-piano-terra-con-giardino-aria-condizionata-e-wifi-16.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 43
                    'name'          => '22-solaria-7-trilocale-piano-terra-con-giardino-aria-condizionata-e-wifi-17.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 44
                    'name'          => '22-solaria-7-trilocale-piano-terra-con-giardino-aria-condizionata-e-wifi-18.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 45
                    'name'          => '22-solaria-7-trilocale-piano-terra-con-giardino-aria-condizionata-e-wifi-19.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 46
                    'name'          => '22-trilocale-piano-terra-con-giardino-ed-aria-condizionata-solaria-7--7.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 47
                    'name'          => '22-trilocale-piano-terra-con-giardino-ed-aria-condizionata-solaria-7--8.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 48
                    'name'          => '22-trilocale-piano-terra-con-giardino-ed-aria-condizionata-solaria-7--9.jpg',
                    'description'   => 'Residence Solaria 7', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 49
                    'name'          => '23-trilocale-primo-piano-con-veranda-vista-mare-solaria-8--7.jpg',
                    'description'   => 'Residence Solaria 8', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 50
                    'name'          => '23-trilocale-primo-piano-con-veranda-vista-mare-solaria-8--8.jpg',
                    'description'   => 'Residence Solaria 8', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 51
                    'name'          => '23-trilocale-primo-piano-con-veranda-vista-mare-solaria-8--9.jpg',
                    'description'   => 'Residence Solaria 8', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 52
                    'name'          => '19-trilocale-primo-piano-con-veranda-giardino-e-aria-condizionata-2.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 53
                    'name'          => '19-trilocale-primo-piano-con-veranda-giardino-e-aria-condizionata-17.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 54
                    'name'          => '19-trilocale-primo-piano-con-veranda-giardino-e-aria-condizionata-18.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 55
                    'name'          => '19-trilocale-primo-piano-con-veranda-vista-mare-giardino-wifi-e-aria-condizionata--12.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 56
                    'name'          => '19-trilocale-primo-piano-con-veranda-vista-mare-giardino-wifi-e-aria-condizionata--14.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 57
                    'name'          => '19-trilocale-primo-piano-con-veranda-vista-mare-giardino-wifi-e-aria-condizionata--19.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 58
                    'name'          => '19-trilocale-primo-piano-con-veranda-vista-mare-giardino-wifi-e-aria-condizionata--20.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 59
                    'name'          => '19-trilocale-primo-piano-con-veranda-vista-mare-giardino-wifi-e-aria-condizionata--21.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 60
                    'name'          => '19-trilocale-primo-piano-con-veranda-vista-mare-giardino-wifi-e-aria-condizionata--22.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 61
                    'name'          => '19-trilocale-primo-piano-con-veranda-vista-mare-giardino-wifi-e-aria-condizionata--23.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 62
                    'name'          => '19-trilocale-primo-piano-con-veranda-vista-mare-giardino-wifi-e-aria-condizionata--24.jpg',
                    'description'   => 'Villetta Bouganville 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 63
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-2.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 64
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-3.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 65
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-4.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 66
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-5.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 67
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-6.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 68
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-10.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 69
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-11.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 70
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-12.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 71
                    'name'          => '6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-13.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 72
                    'name'          => '6-trilocale-piano-terra-con-giardino-wifi-e-aria-condizionata-villino-bouganville-17.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 73
                    'name'          => '6-trilocale-piano-terra-con-giardino-wifi-e-aria-condizionata-villino-bouganville-18.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 74
                    'name'          => '6-trilocale-piano-terra-con-giardino-wifi-e-aria-condizionata-villino-bouganville-20.jpg',
                    'description'   => 'Villetta Bouganville 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 75
                    'name'          => '9-bilocale-5.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 76
                    'name'          => '9-bilocale-6.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 77
                    'name'          => '9-bilocale-7.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 78
                    'name'          => '9-bilocale-8.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 79
                    'name'          => '9-bilocale-10.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 80
                    'name'          => '9-bilocale-13.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 81
                    'name'          => '9-bilocale-con-giardino-vista-mare-wifi-aria-condizionata-15-min.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 82
                    'name'          => '9-bilocale-con-giardino-vista-mare-wifi-aria-condizionata-16.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 83
                    'name'          => '9-bilocale-con-giardino-vista-mare-wifi-aria-condizionata-17.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 84
                    'name'          => '9-bilocale-con-giardino-vista-mare-wifi-aria-condizionata-18.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 85
                    'name'          => '9-bilocale-con-giardino-vista-mare-wifi-aria-condizionata-19.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 86
                    'name'          => '9-bilocale-con-giardino-vista-mare-wifi-aria-condizionata-20.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 87
                    'name'          => '9-bilocale-con-giardino-vista-mare-wifi-aria-condizionata-21-min.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 88
                    'name'          => '9-bilocale-con-giardino-vista-mare-wifi-aria-condizionata-22-min.jpg',
                    'description'   => 'Villa ludduí 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 89
                    'name'          => '11-quadrilocale-con-ampia-veranda-vista-mare-e-minipiscina-idromassaggio-wifi-e-aria-condizionata--48.jpg',
                    'description'   => 'Villa ludduí 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 90
                    'name'          => '11-quadrilocale-con-ampia-veranda-vista-mare-e-minipiscina-idromassaggio-wifi-e-aria-condizionata--64.jpg',
                    'description'   => 'Villa ludduí 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 91
                    'name'          => '11-quadrilocale-con-ampia-veranda-vista-mare-wifi-e-aria-condizionata--63.jpg',
                    'description'   => 'Villa ludduí 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 92
                    'name'          => '11-quadrilocale-con-giardino-e-veranda-vista-mare-rif--l--48.jpg',
                    'description'   => 'Villa ludduí 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 93
                    'name'          => '11-quadrilocale-con-giardino-e-veranda-vista-mare-rif--l--30.jpg',
                    'description'   => 'Villa ludduí 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 94
                    'name'          => '10-trilocale-1.jpg',
                    'description'   => 'Villa ludduí 3', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 95
                    'name'          => '10-trilocale-10.jpg',
                    'description'   => 'Villa ludduí 3', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 96
                    'name'          => '10-trilocale-con-terrazza-vista-mare-wifi-e-aria-condizionata-11.jpg',
                    'description'   => 'Villa ludduí 3', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 97
                    'name'          => '10-trilocale-con-terrazza-vista-mare-wifi-e-aria-condizionata-13.jpg',
                    'description'   => 'Villa ludduí 3', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 98
                    'name'          => '10-trilocale-con-terrazza-vista-mare-wifi-e-aria-condizionata-16.jpg',
                    'description'   => 'Villa ludduí 3', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 99
                    'name'          => '17-bilocale-primo-piano-con-veranda-giardino-wifi-e-aria-condizionata-11-min.jpg',
                    'description'   => 'Residence Budoni Centro 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 100
                    'name'          => '17-bilocale-primo-piano-con-veranda-giardino-wifi-e-aria-condizionata-12.jpg',
                    'description'   => 'Residence Budoni Centro 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 101
                    'name'          => '17-bilocale-primo-piano-con-veranda-giardino-wifi-e-aria-condizionata-14.jpg',
                    'description'   => 'Residence Budoni Centro 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 102
                    'name'          => '17-bilocale-primo-piano-con-veranda-giardino-wifi-e-aria-condizionata-17.jpg',
                    'description'   => 'Residence Budoni Centro 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 103
                    'name'          => '17-bilocale-primo-piano-con-veranda-giardino-wifi-e-aria-condizionata-15.jpg',
                    'description'   => 'Residence Budoni Centro 1', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 104
                    'name'          => '27-bilocale-primo-piano-con-veranda-rif--bc-5--2.jpg',
                    'description'   => 'Residence Budoni Centro 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 105
                    'name'          => '27-bilocale-primo-piano-con-veranda-rif--bc-5--3.jpg',
                    'description'   => 'Residence Budoni Centro 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 106
                    'name'          => '27-bilocale-primo-piano-con-veranda-rif--bc-5--4.jpg',
                    'description'   => 'Residence Budoni Centro 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 107
                    'name'          => '18-trilocale-piano-terra-rif--bc-3--2.jpg',
                    'description'   => 'Residence Budoni Centro 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 108
                    'name'          => '18-trilocale-piano-terra-rif--bc-3--3.jpg',
                    'description'   => 'Residence Budoni Centro 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 109
                    'name'          => '18-trilocale-piano-terra-rif--bc-3--7.jpg',
                    'description'   => 'Residence Budoni Centro 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 110
                    'name'          => '18-trilocale-piano-terra-rif--bc-3--4.jpg',
                    'description'   => 'Residence Budoni Centro 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ],
                [//multimedias.id => 111
                    'name'          => '18-trilocale-piano-terra-rif--bc-3--8.jpg',
                    'description'   => 'Residence Budoni Centro 2', // title de la imagen
                    'path'          => '/storage/multimedia/casa/'
                ]
            ]);

        //  ____
        // |  _ \    ___     ___    _ __ ___    ___
        // | |_) |  / _ \   / _ \  | '_ ` _ \  / __|
        // |  _ <  | (_) | | (_) | | | | | | | \__ \
        // |_| \_\  \___/   \___/  |_| |_| |_| |___/
        //

            //rooms.id => 1
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'villetta-stella-maris',
                    'image'                 => '',
                    'room_category_id'      => 1,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 1, //incremental
                    'language_id'   => 1,
                    'name'          => 'Villetta Stella Maris 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati per poter mangiare all\'aperto, Minipiscina Idromassaggio esterna, solarium. Dispone di WiFi, aria condizionata, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto. Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 1, //incremental
                    'language_id'   => 2,
                    'name'          => 'Stella Maris small villa 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden for outdoor dining, outdoor hot tub, solarium. It has WiFi, air conditioning, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 1, //incremental
                    'language_id'   => 3,
                    'name'          => 'Kleines villa Stella Maris 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten zum Speisen im Freien, Whirlpool im Freien, Solarium. Es verfügt über WLAN, Klimaanlage, Waschmaschine, Spülmaschine, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 1,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 1,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 1,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 1,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 1,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 1,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 1,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 1
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 1 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 1, //id del rows.id
                    'multimedia_id' => 1  //id del multimedias.id
                ],
                [
                    'row_id'        => 1, //id del rows.id
                    'multimedia_id' => 2  //id del multimedias.id
                ],
                [
                    'row_id'        => 1, //id del rows.id
                    'multimedia_id' => 3  //id del multimedias.id
                ],
                [
                    'row_id'        => 1, //id del rows.id
                    'multimedia_id' => 4  //id del multimedias.id
                ],
                [
                    'row_id'        => 1, //id del rows.id
                    'multimedia_id' => 5  //id del multimedias.id
                ],
                [
                    'row_id'        => 1, //id del rows.id
                    'multimedia_id' => 6  //id del multimedias.id
                ],
                [
                    'row_id'        => 1, //id del rows.id
                    'multimedia_id' => 7  //id del multimedias.id
                ]
            ]);


            //rooms.id => 2
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'solaria1',
                    'image'                 => '',
                    'room_category_id'      => 2,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 2, //incremental
                    'language_id'   => 1,
                    'name'          => 'Solaria 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto. Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 2, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Solaria 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 2, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Solaria 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Spülmaschine, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 2,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 2,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 2,
                    'service_id'    => 29,
                    'info'          => ''
                ],
                [
                    'room_id'       => 2,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 2,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 2,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 2,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 2,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 2
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 2 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 2, //id del rows.id
                    'multimedia_id' => 1  //id del multimedias.id
                ],
                [
                    'row_id'        => 2, //id del rows.id
                    'multimedia_id' => 2  //id del multimedias.id
                ],
                [
                    'row_id'        => 2, //id del rows.id
                    'multimedia_id' => 3  //id del multimedias.id
                ],
                [
                    'row_id'        => 2, //id del rows.id
                    'multimedia_id' => 4  //id del multimedias.id
                ],
                [
                    'row_id'        => 2, //id del rows.id
                    'multimedia_id' => 5  //id del multimedias.id
                ]
            ]);


            //rooms.id => 3
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'solaria2',
                    'image'                 => '',
                    'room_category_id'      => 2,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 3, //incremental
                    'language_id'   => 1,
                    'name'          => 'Solaria 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata,Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 3, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Solaria 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 3, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Solaria 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Geschirrspüler, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 3,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 3,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 3,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 3,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 3,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 3,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 3,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 3
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 3 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 3, //id del rows.id
                    'multimedia_id' => 17  //id del multimedias.id
                ],
                [
                    'row_id'        => 3, //id del rows.id
                    'multimedia_id' => 18  //id del multimedias.id
                ],
                [
                    'row_id'        => 3, //id del rows.id
                    'multimedia_id' => 19 //id del multimedias.id
                ],
                [
                    'row_id'        => 3, //id del rows.id
                    'multimedia_id' => 20 //id del multimedias.id
                ],
                [
                    'row_id'        => 3, //id del rows.id
                    'multimedia_id' => 21 //id del multimedias.id
                ]
            ]);


            //rooms.id => 4
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'solaria3',
                    'image'                 => '',
                    'room_category_id'      => 2,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 4, //incremental
                    'language_id'   => 1,
                    'name'          => 'Solaria 3',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata,Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 4, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Solaria 3',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 4, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Solaria 3',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Geschirrspüler, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 4,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 4,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 4,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 4,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 4,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 4,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 4,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 4
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 4 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 4, //id del rows.id
                    'multimedia_id' => 20  //id del multimedias.id
                ],
                [
                    'row_id'        => 4, //id del rows.id
                    'multimedia_id' => 21 //id del multimedias.id
                ],
                [
                    'row_id'        => 4, //id del rows.id
                    'multimedia_id' => 12  //id del multimedias.id
                ],
                [
                    'row_id'        => 4, //id del rows.id
                    'multimedia_id' => 14 //id del multimedias.id
                ],
                [
                    'row_id'        => 4, //id del rows.id
                    'multimedia_id' => 15 //id del multimedias.id
                ]
            ]);

            //rooms.id => 5
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'solaria4',
                    'image'                 => '',
                    'room_category_id'      => 2,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 5, //incremental
                    'language_id'   => 1,
                    'name'          => 'Solaria 4',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata,Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 5, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Solaria 4',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 5, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Solaria 4',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Geschirrspüler, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 5,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 5,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 5,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 5,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 5,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 5,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 5
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 5 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 5, //id del rows.id
                    'multimedia_id' => 29  //id del multimedias.id
                ],
                [
                    'row_id'        => 5, //id del rows.id
                    'multimedia_id' => 30 //id del multimedias.id
                ],
                [
                    'row_id'        => 5, //id del rows.id
                    'multimedia_id' => 12  //id del multimedias.id
                ],
                [
                    'row_id'        => 5, //id del rows.id
                    'multimedia_id' => 14 //id del multimedias.id
                ],
                [
                    'row_id'        => 5, //id del rows.id
                    'multimedia_id' => 15 //id del multimedias.id
                ]
            ]);

            //rooms.id => 6
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'solaria5',
                    'image'                 => '',
                    'room_category_id'      => 2,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 6, //incremental
                    'language_id'   => 1,
                    'name'          => 'Solaria 5',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata,Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 6, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Solaria 5',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 6, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Solaria 5',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Geschirrspüler, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 6,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 6,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 6,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 6,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 6,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 6,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 6,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 6
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 6 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 6, //id del rows.id
                    'multimedia_id' => 33  //id del multimedias.id
                ],
                [
                    'row_id'        => 6, //id del rows.id
                    'multimedia_id' => 34 //id del multimedias.id
                ],
                [
                    'row_id'        => 6, //id del rows.id
                    'multimedia_id' => 12  //id del multimedias.id
                ],
                [
                    'row_id'        => 6, //id del rows.id
                    'multimedia_id' => 14 //id del multimedias.id
                ],
                [
                    'row_id'        => 6, //id del rows.id
                    'multimedia_id' => 15 //id del multimedias.id
                ]
            ]);

            //rooms.id => 7
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'solaria6',
                    'image'                 => '',
                    'room_category_id'      => 2,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 7, //incremental
                    'language_id'   => 1,
                    'name'          => 'Solaria 6',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata,Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 7, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Solaria 6',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 7, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Solaria 6',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Geschirrspüler, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 7,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 7,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 7,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 7,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 7,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 7,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 7
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 7 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 7, //id del rows.id
                    'multimedia_id' => 38  //id del multimedias.id
                ],
                [
                    'row_id'        => 7, //id del rows.id
                    'multimedia_id' => 39 //id del multimedias.id
                ],
                [
                    'row_id'        => 7, //id del rows.id
                    'multimedia_id' => 12  //id del multimedias.id
                ],
                [
                    'row_id'        => 7, //id del rows.id
                    'multimedia_id' => 14 //id del multimedias.id
                ],
                [
                    'row_id'        => 7, //id del rows.id
                    'multimedia_id' => 15 //id del multimedias.id
                ]
            ]);

            //rooms.id => 8
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'solaria7',
                    'image'                 => '',
                    'room_category_id'      => 2,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 8, //incremental
                    'language_id'   => 1,
                    'name'          => 'Solaria 7',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata,Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 8, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Solaria 7',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 8, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Solaria 7',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Geschirrspüler, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 8,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 8,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 8,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 8,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 8,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 8,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 8
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 8 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 8, //id del rows.id
                    'multimedia_id' => 38  //id del multimedias.id
                ],
                [
                    'row_id'        => 8, //id del rows.id
                    'multimedia_id' => 39  //id del multimedias.id
                ],
                [
                    'row_id'        => 8, //id del rows.id
                    'multimedia_id' => 15  //id del multimedias.id
                ],
                [
                    'row_id'        => 8, //id del rows.id
                    'multimedia_id' => 16  //id del multimedias.id
                ],
                [
                    'row_id'        => 8, //id del rows.id
                    'multimedia_id' => 17  //id del multimedias.id
                ]
            ]);

            //rooms.id => 9
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'solaria8',
                    'image'                 => '',
                    'room_category_id'      => 2,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 9, //incremental
                    'language_id'   => 1,
                    'name'          => 'Solaria 8',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata,Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 9, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Solaria 8',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 9, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Solaria 8',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Geschirrspüler, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 9,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 9,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 9,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 9,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 9,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 9,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 9
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 9 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 9, //id del rows.id
                    'multimedia_id' => 49  //id del multimedias.id
                ],
                [
                    'row_id'        => 9, //id del rows.id
                    'multimedia_id' => 50  //id del multimedias.id
                ],
                [
                    'row_id'        => 9, //id del rows.id
                    'multimedia_id' => 15  //id del multimedias.id
                ],
                [
                    'row_id'        => 9, //id del rows.id
                    'multimedia_id' => 16  //id del multimedias.id
                ],
                [
                    'row_id'        => 9, //id del rows.id
                    'multimedia_id' => 17  //id del multimedias.id
                ],
            ]);

            //rooms.id => 10
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'Bouganville1',
                    'image'                 => '',
                    'room_category_id'      => 3,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 10, //incremental
                    'language_id'   => 1,
                    'name'          => 'Bouganville 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 10, //incremental
                    'language_id'   => 2,
                    'name'          => 'Bouganville 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 10, //incremental
                    'language_id'   => 3,
                    'name'          => 'Bouganville 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Spülmaschine, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 10,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 10,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 10,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 10,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 10,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 10,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 10,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 10
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 10 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 10, //id del rows.id
                    'multimedia_id' => 52  //id del multimedias.id
                ],
                [
                    'row_id'        => 10, //id del rows.id
                    'multimedia_id' => 53  //id del multimedias.id
                ],
                [
                    'row_id'        => 10, //id del rows.id
                    'multimedia_id' => 54 //id del multimedias.id
                ],
                [
                    'row_id'        => 10, //id del rows.id
                    'multimedia_id' => 55 //id del multimedias.id
                ],
                [
                    'row_id'        => 10, //id del rows.id
                    'multimedia_id' => 56 //id del multimedias.id
                ]
            ]);

            //rooms.id => 11
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'Bouganville2',
                    'image'                 => '',
                    'room_category_id'      => 3,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 11, //incremental
                    'language_id'   => 1,
                    'name'          => 'Bouganville 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 11, //incremental
                    'language_id'   => 2,
                    'name'          => 'Bouganville 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 11, //incremental
                    'language_id'   => 3,
                    'name'          => 'Bouganville 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Spülmaschine, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 11,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 11,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 11,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 11,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 11,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 11,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 11,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 11
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 11 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 11, //id del rows.id
                    'multimedia_id' => 63  //id del multimedias.id
                ],
                [
                    'row_id'        => 11, //id del rows.id
                    'multimedia_id' => 64  //id del multimedias.id
                ],
                [
                    'row_id'        => 11, //id del rows.id
                    'multimedia_id' => 65  //id del multimedias.id
                ],
                [
                    'row_id'        => 11, //id del rows.id
                    'multimedia_id' => 66  //id del multimedias.id
                ],
                [
                    'row_id'        => 11, //id del rows.id
                    'multimedia_id' => 67  //id del multimedias.id
                ]
            ]);

            //rooms.id => 12
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'Luddui1',
                    'image'                 => '',
                    'room_category_id'      => 4,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 12, //incremental
                    'language_id'   => 1,
                    'name'          => 'Ludduì 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, terrazza attrezzata e abitabile per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, doccetta esterna, posto auto.
                    Consumi luce, acqua, gas inclusi.'
                ],
                [
                    'room_id'       => 12, //incremental
                    'language_id'   => 2,
                    'name'          => 'Ludduì 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, terrace and living space to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, outdoor shower, parking space. Consumption of electricity, water, gas included.'
                ],
                [
                    'room_id'       => 12, //incremental
                    'language_id'   => 3,
                    'name'          => 'Ludduì 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Terrasse und Wohnbereich im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Außendusche, Parkplatz. Verbrauch von Strom, Wasser, Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 12,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 12,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 12,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 12,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 12,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 12,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 12,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 12
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 12 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 12, //id del rows.id
                    'multimedia_id' => 81  //id del multimedias.id
                ],
                [
                    'row_id'        => 12, //id del rows.id
                    'multimedia_id' => 82 //id del multimedias.id
                ],
                [
                    'row_id'        => 12, //id del rows.id
                    'multimedia_id' => 83  //id del multimedias.id
                ],
                [
                    'row_id'        => 12, //id del rows.id
                    'multimedia_id' => 84  //id del multimedias.id
                ],
                [
                    'row_id'        => 12, //id del rows.id
                    'multimedia_id' => 85  //id del multimedias.id
                ]
            ]);

            //rooms.id => 13
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'Luddui2',
                    'image'                 => '',
                    'room_category_id'      => 4,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 13, //incremental
                    'language_id'   => 1,
                    'name'          => 'Luddui 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, terrazza attrezzata e abitabile per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, doccetta esterna, posto auto.
                    Consumi luce, acqua, gas inclusi.'
                ],
                [
                    'room_id'       => 13, //incremental
                    'language_id'   => 2,
                    'name'          => 'Luddui 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, terrace and living space to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, outdoor shower, parking space. Consumption of electricity, water, gas included.'
                ],
                [
                    'room_id'       => 13, //incremental
                    'language_id'   => 3,
                    'name'          => 'Luddui 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, terrazza attrezzata e abitabile per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, doccetta esterna, posto auto.
                    Consumi luce, acqua, gas inclusi'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 13,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 13,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 13,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 13,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 13,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 13,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 13,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 13
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 13 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 13, //id del rows.id
                    'multimedia_id' => 89  //id del multimedias.id
                ],
                [
                    'row_id'        => 13, //id del rows.id
                    'multimedia_id' => 90 //id del multimedias.id
                ],
                [
                    'row_id'        => 13, //id del rows.id
                    'multimedia_id' => 91  //id del multimedias.id
                ],
                [
                    'row_id'        => 13, //id del rows.id
                    'multimedia_id' => 92  //id del multimedias.id
                ],
                [
                    'row_id'        => 13, //id del rows.id
                    'multimedia_id' => 93  //id del multimedias.id
                ]
            ]);

            //rooms.id => 14
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'Luddui 3',
                    'image'                 => '',
                    'room_category_id'      => 4,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 14, //incremental
                    'language_id'   => 1,
                    'name'          => 'Luddui 3',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '6+2 posti letto, 3 camere da letto + soppalco, 2 bagni con box doccia, ampio soggiorno, cucina e spaziose verande attrezzate e abitabili per poter mangiare all\'aperto, Minipiscina Idromassaggio esterna, Solarium, giardino posteriore. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, lavastoviglie, barbecue, doccetta esterna, posti auto.
                    Consumi luce, acqua, gas inclusi.'
                ],
                [
                    'room_id'       => 14, //incremental
                    'language_id'   => 2,
                    'name'          => 'Luddui 3',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '6 + 2 beds, 3 bedrooms + mezzanine, 2 bathrooms with shower, large living room, kitchen and spacious verandas equipped and habitable to eat outdoors, outdoor hot tub, solarium, back garden. It has WiFi, air conditioning, satellite TV, washing machine, dishwasher, barbecue, outdoor shower, parking spaces. Consumption of electricity, water, gas included.'
                ],
                [
                    'room_id'       => 14, //incremental
                    'language_id'   => 3,
                    'name'          => 'Luddui 3',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '6 + 2 Betten, 3 Schlafzimmer + Mezzanine, 2 Bäder mit Dusche, großes Wohnzimmer, Küche und geräumige Veranden ausgestattet und bewohnbar, um im Freien zu essen, Whirlpool im Freien, Solarium, Garten. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Spülmaschine, Grill, Außendusche, Parkplätze. Verbrauch von Strom, Wasser, Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 14,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 14,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 14,
                    'service_id'    => 29,
                    'info'          => ''
                ],
                [
                    'room_id'       => 14,
                    'service_id'    => 30,
                    'info'          => ''
                ],
                [
                    'room_id'       => 14,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 14,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 14,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 14,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 14
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 14 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 14, //id del rows.id
                    'multimedia_id' => 94  //id del multimedias.id
                ],
                [
                    'row_id'        => 14, //id del rows.id
                    'multimedia_id' => 95 //id del multimedias.id
                ],
                [
                    'row_id'        => 14, //id del rows.id
                    'multimedia_id' => 96  //id del multimedias.id
                ],
                [
                    'row_id'        => 14, //id del rows.id
                    'multimedia_id' => 97  //id del multimedias.id
                ],
                [
                    'row_id'        => 14, //id del rows.id
                    'multimedia_id' => 98  //id del multimedias.id
                ]
            ]);

            //rooms.id => 15
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'Residence-Budoni-Centro1',
                    'image'                 => '',
                    'room_category_id'      => 5,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 15, //incremental
                    'language_id'   => 1,
                    'name'          => 'Residence Budoni Centro 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4+1 posti letto, 2 camere da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, barbecue, doccetta esterna, posto auto.
                    Consumi luce, acqua, gas inclusi.'
                ],
                [
                    'room_id'       => 15, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Budoni Centro 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 beds, 2 bedrooms, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, barbecue, outdoor shower, parking space. Consumption of electricity, water, gas included.'
                ],
                [
                    'room_id'       => 15, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residence Budoni Centro 1',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '4 + 1 Betten, 2 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar zum Essen im Freien. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Grill, Außendusche, Parkplatz. Verbrauch von Strom, Wasser, Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 15,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 15,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 15,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 15,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 15,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 15,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 15
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 15 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 15, //id del rows.id
                    'multimedia_id' => 99  //id del multimedias.id
                ],
                [
                    'row_id'        => 15, //id del rows.id
                    'multimedia_id' => 100  //id del multimedias.id
                ],
                [
                    'row_id'        => 15, //id del rows.id
                    'multimedia_id' => 101  //id del multimedias.id
                ],
                [
                    'row_id'        => 15, //id del rows.id
                    'multimedia_id' => 102  //id del multimedias.id
                ],
                [
                    'row_id'        => 15, //id del rows.id
                    'multimedia_id' => 103  //id del multimedias.id
                ]
            ]);

            //rooms.id => 16
            DB::table('rooms')->insert(
                [
                    'slug'                  => 'Residence-Budoni-Centro2',
                    'image'                 => '',
                    'room_category_id'      => 5,
                    'adults_quantity'       => 4,
                    'children_quantity'     => 1
                ]
            );
            DB::table('room_translations')->insert([
                [
                    'room_id'       => 16, //incremental
                    'language_id'   => 1,
                    'name'          => 'Residence Budoni Centro 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '2+1 posti letto, 1 camera da letto, 1 bagno con box doccia, soggiorno con angolo cucina, veranda e giardino attrezzati e abitabili per poter mangiare all\'aperto. Dispone di WiFi, aria condizionata, Tv-Sat, lavatrice, doccetta esterna, posto auto.
                    Consumi luce, acqua e gas inclusi.'
                ],
                [
                    'room_id'       => 16, //incremental
                    'language_id'   => 2,
                    'name'          => 'Residence Budoni Centro 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '2 + 1 beds, 1 bedroom, 1 bathroom with shower, living room with kitchenette, veranda and garden equipped and habitable to eat outdoors. It has WiFi, air conditioning, satellite TV, washing machine, outdoor shower, parking space. Consumption of electricity, water and gas included.'
                ],
                [
                    'room_id'       => 16, //incremental
                    'language_id'   => 3,
                    'name'          => 'Residenz Budoni Centro 2',
                    'subtitle1'     => '',
                    'subtitle2'     => '',
                    'description'   => '2 + 1 Betten, 1 Schlafzimmer, 1 Badezimmer mit Dusche, Wohnzimmer mit Küchenzeile, Veranda und Garten ausgestattet und bewohnbar, um im Freien zu essen. Es verfügt über WLAN, Klimaanlage, Sat-TV, Waschmaschine, Außendusche, Parkplatz. Verbrauch von Strom, Wasser und Gas inklusive.'
                ]
            ]);
            DB::table('rooms_services')->insert([
                [
                    'room_id'       => 16,
                    'service_id'    => 26,
                    'info'          => '2'
                ],
                [
                    'room_id'       => 16,
                    'service_id'    => 27,
                    'info'          => '1'
                ],
                [
                    'room_id'       => 16,
                    'service_id'    => 31,
                    'info'          => ''
                ],
                [
                    'room_id'       => 16,
                    'service_id'    => 32,
                    'info'          => ''
                ],
                [
                    'room_id'       => 16,
                    'service_id'    => 33,
                    'info'          => ''
                ],
                [
                    'room_id'       => 16,
                    'service_id'    => 39,
                    'info'          => ''
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 16
                [
                    'rowable_type' => 'App\Models\Admin\Room',
                    'rowable_id'   => 16 //incremental room
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 16, //id del rows.id
                    'multimedia_id' => 104  //id del multimedias.id
                ],
                [
                    'row_id'        => 16, //id del rows.id
                    'multimedia_id' => 105  //id del multimedias.id
                ],
                [
                    'row_id'        => 16, //id del rows.id
                    'multimedia_id' => 106  //id del multimedias.id
                ],
                [
                    'row_id'        => 16, //id del rows.id
                    'multimedia_id' => 107  //id del multimedias.id
                ],
                [
                    'row_id'        => 16, //id del rows.id
                    'multimedia_id' => 108  //id del multimedias.id
                ]
            ]);


        //RoomCategorisMultimedia

            DB::table('rows')->insert( //rows.id => 17
                [
                    'rowable_type' => 'App\Models\Admin\RoomCategory',
                    'rowable_id'   => 1 //incremental roomCategory
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 17, //id del rows.id
                    'multimedia_id' => 104  //id del multimedias.id
                ],
                [
                    'row_id'        => 17, //id del rows.id
                    'multimedia_id' => 105  //id del multimedias.id
                ],
                [
                    'row_id'        => 17, //id del rows.id
                    'multimedia_id' => 106  //id del multimedias.id
                ],
                [
                    'row_id'        => 17, //id del rows.id
                    'multimedia_id' => 107  //id del multimedias.id
                ],
                [
                    'row_id'        => 17, //id del rows.id
                    'multimedia_id' => 108  //id del multimedias.id
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 17
                [
                    'rowable_type' => 'App\Models\Admin\RoomCategory',
                    'rowable_id'   => 2 //incremental roomCategory
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 18, //id del rows.id
                    'multimedia_id' => 104  //id del multimedias.id
                ],
                [
                    'row_id'        => 18, //id del rows.id
                    'multimedia_id' => 105  //id del multimedias.id
                ],
                [
                    'row_id'        => 18, //id del rows.id
                    'multimedia_id' => 106  //id del multimedias.id
                ],
                [
                    'row_id'        => 18, //id del rows.id
                    'multimedia_id' => 107  //id del multimedias.id
                ],
                [
                    'row_id'        => 18, //id del rows.id
                    'multimedia_id' => 108  //id del multimedias.id
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 17
                [
                    'rowable_type' => 'App\Models\Admin\RoomCategory',
                    'rowable_id'   => 3 //incremental roomCategory
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 19, //id del rows.id
                    'multimedia_id' => 104  //id del multimedias.id
                ],
                [
                    'row_id'        => 19, //id del rows.id
                    'multimedia_id' => 105  //id del multimedias.id
                ],
                [
                    'row_id'        => 19, //id del rows.id
                    'multimedia_id' => 106  //id del multimedias.id
                ],
                [
                    'row_id'        => 19, //id del rows.id
                    'multimedia_id' => 107  //id del multimedias.id
                ],
                [
                    'row_id'        => 19, //id del rows.id
                    'multimedia_id' => 108  //id del multimedias.id
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 17
                [
                    'rowable_type' => 'App\Models\Admin\RoomCategory',
                    'rowable_id'   => 4 //incremental roomCategory
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 20, //id del rows.id
                    'multimedia_id' => 104  //id del multimedias.id
                ],
                [
                    'row_id'        => 20, //id del rows.id
                    'multimedia_id' => 105  //id del multimedias.id
                ],
                [
                    'row_id'        => 20, //id del rows.id
                    'multimedia_id' => 106  //id del multimedias.id
                ],
                [
                    'row_id'        => 20, //id del rows.id
                    'multimedia_id' => 107  //id del multimedias.id
                ],
                [
                    'row_id'        => 20, //id del rows.id
                    'multimedia_id' => 108  //id del multimedias.id
                ]
            ]);
            DB::table('rows')->insert( //rows.id => 17
                [
                    'rowable_type' => 'App\Models\Admin\RoomCategory',
                    'rowable_id'   => 5 //incremental roomCategory
                ]
            );
            DB::table('rows_multimedias')->insert([
                [
                    'row_id'        => 21, //id del rows.id
                    'multimedia_id' => 104  //id del multimedias.id
                ],
                [
                    'row_id'        => 21, //id del rows.id
                    'multimedia_id' => 105  //id del multimedias.id
                ],
                [
                    'row_id'        => 21, //id del rows.id
                    'multimedia_id' => 106  //id del multimedias.id
                ],
                [
                    'row_id'        => 21, //id del rows.id
                    'multimedia_id' => 107  //id del multimedias.id
                ],
                [
                    'row_id'        => 21, //id del rows.id
                    'multimedia_id' => 108  //id del multimedias.id
                ]
            ]);
    }
}
