<?php

use App\Models\Admin\Multimedia;
use App\Models\Admin\Row;
use Illuminate\Database\Seeder;

class GallerySliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1 - Gallery Home
            DB::table( 'gallery_sliders' )->insert(
                [
                    'code'      => 'gallery-header',
                    'slug'      => 'gallery-header',
                    'status_id' => 1,
                ]
            );
            DB::table( 'gallery_slider_translations' )->insert(
                [
                    'gallery_slider_id'     => 1,
                    'language_id'           => 1,
                    'title'                 => 'Gallery Header',
                ]
            );

            $row = new Row();
            $row->rowable_type = 'App\Models\Admin\GallerySlider';
            $row->rowable_id   = 1; // incremental
            $row->save();

            $multimedias = [];

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria--header01-min.jpg';
            $multimedia->path   = '/images/home/header-slider/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria--header02-min.jpg';
            $multimedia->path   = '/images/home/header-slider/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria--header03-min.jpg';
            $multimedia->path   = '/images/home/header-slider/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            foreach ( $multimedias as $multimed ) {
                DB::table( 'rows_multimedias' )->insert(
                    [
                        'row_id'        => $row->id,
                        'multimedia_id' => $multimed->id
                    ]
                );
            }

        // 2 - Gallery Chi Siamo
            DB::table( 'gallery_sliders' )->insert(
                [
                    'code'      => 'gallery-landing',
                    'slug'      => 'gallery-landing',
                    'status_id' => 1,
                ]
            );
            DB::table( 'gallery_slider_translations' )->insert(
                [
                    'gallery_slider_id'     => 2,
                    'language_id'           => 1,
                    'title'                 => 'Gallery Landing',
                ]
            );

            $row = new Row();
            $row->rowable_type = 'App\Models\Admin\GallerySlider';
            $row->rowable_id   = 2; // incremental
            $row->save();

            $multimedias = [];

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria-gallery01-min.jpg';
            $multimedia->path   = '/images/home/gallery-slider-map/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria-gallery02-min.jpg';
            $multimedia->path   = '/images/home/gallery-slider-map/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria-gallery03-min.jpg';
            $multimedia->path   = '/images/home/gallery-slider-map/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria-vacanze-slider1.jpg';
            $multimedia->path   = '/images/home/gallery-slider-map/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria-vacanze-slider2.jpg';
            $multimedia->path   = '/images/home/gallery-slider-map/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            $multimedia         = new Multimedia();
            $multimedia->name   = 'solaria-vacanze-slider3.jpg';
            $multimedia->path   = '/images/home/gallery-slider-map/';
            $multimedia->width  = 1920;
            $multimedia->height = 800;
            $multimedia->save();
            $multimedias[] = $multimedia;

            foreach ( $multimedias as $multimed ) {
                DB::table( 'rows_multimedias' )->insert(
                    [
                        'row_id'        => $row->id,
                        'multimedia_id' => $multimed->id
                    ]
                );
            }
    }
}
