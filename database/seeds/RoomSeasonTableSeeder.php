<?php

use App\Models\Admin\Room;
use App\Models\Admin\RoomSeason;
use App\Models\Admin\RoomSeasonTranslation;
//use App\Models\Admin\Language;
use Illuminate\Database\Seeder;

class RoomSeasonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Villetta Stella Maris
        DB::table('room_seasons')->insert([
            [
                'room_id'    => 1,
                'start_date' => '02/03/2019',
                'end_date'   => '09/03/2019',
                'price'      => 350.00,
                'iva'        => 0.22
            ]
        ]);

        DB::table('room_season_translations')->insert([
            [
                'room_season_id' => 1,
                'name'           => 'Marzo settimana 1',
                'description'    => 'Marzo settimana 1',
                'language_id'    => 1

            ]
        ]);
    }
}