<?php

use App\Models\Admin\Ally;
use Illuminate\Database\Seeder;

class AllyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Ally::class, 20)->create();
    }
}
