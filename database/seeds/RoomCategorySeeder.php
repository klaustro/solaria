<?php

use App\Models\Admin\RoomCategory;
use App\Models\Admin\RoomCategoryTranslation;
use App\Models\Admin\RoomLocation;
use Illuminate\Database\Seeder;

class RoomCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //feature
            //feature.id => 1
            DB::table('features')->insert([
                'iconblack' => '/storage/multimedia/icons/002-sunsetblack.svg',
                'iconwhite' => '/storage/multimedia/icons/002-sunset.svg',
                'icongreen' => '/storage/multimedia/icons/002-sunsetgreen.svg'

            ]);
            DB::table('feature_translations')->insert([
                [
                    'feature_id'   => 1,
                    'language_id'   => 1,
                    'name'          => 'Spiaggia'
                ],
                [
                    'feature_id'   => 1,
                    'language_id'   => 2,
                    'name'          => 'Beach'
                ],
                [
                    'feature_id'   => 1,
                    'language_id'   => 3,
                    'name'          => 'Strand'
                ]
            ]);

            //feature.id => 2
            DB::table('features')->insert([
                'iconblack' => '/storage/multimedia/icons/001-placeholderblack.svg',
                'iconwhite' => '/storage/multimedia/icons/001-placeholder.svg',
                'icongreen' => '/storage/multimedia/icons/001-placeholdergreen.svg'
            ]);
            DB::table('feature_translations')->insert([
                [
                    'feature_id'   => 2,
                    'language_id'   => 1,
                    'name'          => 'Distanza della città'
                ],
                [
                    'feature_id'   => 2,
                    'language_id'   => 2,
                    'name'          => 'City distance'
                ],
                [
                    'feature_id'   => 2,
                    'language_id'   => 3,
                    'name'          => 'Entfernung zur Stadt'
                ]
            ]);

            //feature.id => 3
            DB::table('features')->insert([
                'iconblack' => '/storage/multimedia/icons/leafblack.svg',
                'iconwhite' => '/storage/multimedia/icons/leaf.svg',
                'icongreen' => '/storage/multimedia/icons/leafgreen.svg'
            ]);
            DB::table('feature_translations')->insert([
                [
                    'feature_id'   => 3,
                    'language_id'   => 1,
                    'name'          => 'Giardino'
                ],
                [
                    'feature_id'   => 3,
                    'language_id'   => 2,
                    'name'          => 'Garden'
                ],
                [
                    'feature_id'   => 3,
                    'language_id'   => 3,
                    'name'          => 'Garten'
                ]
            ]);


            // Budoni(id) => 1
            // Loddui(id) => 2

        //RoomCategory

            //RoomCategory.id => 1
            DB::table('room_categories')->insert([
                'status_id'             => 1,
                'image'                 => '/storage/multimedia/casa/1-villetta-stella-maris-1_list-img.jpg',
                'room_location_id'      => 1
            ]);
            DB::table('room_category_features')->insert([
                [
                    'room_category_id'     => 1,
                    'feature_id'             => 1,
                    'value'                  => rand(500, 100)
                ],
                [
                    'room_category_id'     => 1,
                    'feature_id'             => 2,
                    'value'                  => rand(800, 1500),
                ],
                [
                    'room_category_id'     => 1,
                    'feature_id'             => 3,
                    'value'                  =>'Si'
                ]
            ]);
            DB::table('rooms_categories_services')->insert([
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 1,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 2,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 3,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 4,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 5,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 6,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 7,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 8,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 9,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 10,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 11,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 12,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 13,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 14,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 15,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 16,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 17,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 18,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 19,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 20,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 21,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 22,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 23,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 24,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 1,
                    'service_id'            => 25,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'       => 1,
                    'service_id'    => 34,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 1,
                    'service_id'    => 35,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 1,
                    'service_id'    => 36,
                    'info'          => 'Giardino'
                ],
                [
                    'room_categories_id'       => 1,
                    'service_id'    => 37,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 1,
                    'service_id'    => 38,
                    'info'          => '100'
                ]
            ]);
            DB::table('room_category_translations')->insert([
                [
                    'room_category_id'      => 1, //incremental
                    'language_id'           => 1, //Italiano
                    'name'                  => 'Villetta Stella Maris',
                    'description'           => 'Immersa in un ridente giardino adornato da ulivi e melograni, la Villetta Stella Maris è pensata per chi cerca relax, tranquillità e privacy.Dotata di tutti i comfort e arredata con cura e passione nei minimi dettagli per far sentire gli ospiti amati e coccolati.A un passo dal centro di Budoni e poco distante dalla meravigliosa spiaggia Salamaghe, è la soluzione ideale per scoprire che "casa" è ovunque portiamo il cuore e la famiglia.'
                ],
                [
                    'room_category_id'      => 1, //incremental
                    'language_id'           => 2, //English
                    'name'                  => 'Stella Maris small villa',
                    'description'           => 'Immersed in a lovely garden decorated with olive and pomegranate trees, Villetta Stella Maris is designed for those seeking relaxation, tranquility and privacy. Equipped with all the comforts and furnished with care and passion down to the smallest details, all to make guests feel loved and pampered. Just a short step from the centre of Budoni and not far from the wonderful Salamaghe beach, it\'s the ideal solution to discover that "home" is wherever we bring our hearts and family.'
                ],
                [
                    'room_category_id'      => 1, //incremental
                    'language_id'           => 3, //Aleman
                    'name'                  => 'Kleines villa Stella Maris',
                    'description'           => 'Umgeben von einem bezaubernden Garten mit Olivenhainen und Granatapfelbäumen ist die kleine Villa Villetta Stella Maris für Entspannung, Ruhe und Privatsphäre bestens geeignet, Mit allem Komfort ausgestattet und mit Liebe und Leidenschaft bis ins kleinste Detail geplant und eingerichtet ist es ein Ort wo sich die Gäste gut aufgehoben und wohl fühlen. Nur wenige Meter vom Zentrum von Budoni entfernt und unweit des wunderschönen Strandes gelegen ist die Villa die perfekte Lösung für das Wohlbefinden der Seele. Zuhause ist schließlich dort, wo sich Seele und Familie wohl fühlen.'
                ]
            ]);


            //RoomCategory.id => 2
            DB::table('room_categories')->insert(
            [
                'status_id'             => 1,
                'image'                 => '/storage/multimedia/casa/4-trilocale-piano-terra-con-giardino-e-aria-condizionata-solaria-1--1.jpg',
                'room_location_id'      => 1
            ]);
            DB::table('room_category_features')->insert([
                [
                    'room_category_id'     => 2,
                    'feature_id'             => 1,
                    'value'                  => rand(500, 100)
                ],
                [
                    'room_category_id'     => 2,
                    'feature_id'             => 2,
                    'value'                  => rand(800, 1500),
                ],
                [
                    'room_category_id'     => 2,
                    'feature_id'             => 3,
                    'value'                  =>'Si'
                ]
            ]);
            DB::table('rooms_categories_services')->insert([
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 1,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 2,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 3,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 4,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 5,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 6,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 7,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 8,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 9,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 10,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 11,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 12,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 13,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 14,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 15,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 16,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 17,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 18,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 22,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 23,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 24,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 2,
                    'service_id'            => 25,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'       => 2,
                    'service_id'    => 34,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 2,
                    'service_id'    => 35,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 2,
                    'service_id'    => 36,
                    'info'          => 'Giardino'
                ],
                [
                    'room_categories_id'       => 2,
                    'service_id'    => 37,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 2,
                    'service_id'    => 38,
                    'info'          => '100'
                ]
            ]);
            DB::table('room_category_translations')->insert([
                [
                    'room_category_id'      => 2, //incremental
                    'language_id'           => 1, //Italiano
                    'name'                  => 'Residence Solaria',
                    'description'           => 'Immersa in un ridente giardino adornato da ulivi e melograni, la Villetta Stella Maris è pensata per chi cerca relax, tranquillità e privacy. Dotata di tutti i comfort e arredata con cura e passione nei minimi dettagli per far sentire gli ospiti amati e coccolati. A un passo dal centro di Budoni e poco distante dalla meravigliosa spiaggia Salamaghe, è la soluzione ideale per scoprire che "casa" è ovunque portiamo il cuore e la famiglia.'
                ],
                [
                    'room_category_id'      => 2, //incremental
                    'language_id'           => 2, //English
                    'name'                  => 'Residence Solaria',
                    'description'           => 'Surrounded by gardens and shaded by olive and pomegranate trees, Residence Solaria offers serenity and relaxation. Close to the sea and a few steps from the picturesque centre of Budoni, it\'s also a convenient starting point for exploring the many wonders of Sardinia. Recently built, the Villas of the Residence are equipped with all home comforts, and cared for in the details, furnished with love and passion by searching for the traditions of old Sardinia in our colours and shapes. The complex consists of 2 beautiful first-floor apartments with sea-view veranda and 6 ground floor apartments with veranda and garden. All the verandas are furnished and equipped for outdoor dining, grilling at the barbecue and enjoying the breathtaking sunsets that unspoiled nature offers in this wonderful part of Sardinia.'
                ],
                [
                    'room_category_id'      => 2, //incremental
                    'language_id'           => 3, //Aleman
                    'name'                  => 'Residence Solaria',
                    'description'           => 'Umgeben von Gärten, Olivenhainen und Granatapfelbäumen bietet das Apartmenhaus Residence Solaria Entspannung und Ruhe. In unmittelbarer Nähe zum Strand und nur wenige Meter vom bezauberndem Zentrum der Ortschaft Budoni entfernt ist das Apartmenthaus ein bequemer Startpunkt für die Entdeckungsreise der unzähligen Schönheiten Sardiniens.  Die modernen kleinen Villen der Apartmentanlage sind mit allem Komfort ausgestattet und bis ins kleinste Detail mit Liebe und Leidenschaft geplant und eingerichtet. Dabei wurde großes Augenmerk auf unsere Traditionen und auf die Farben und Formen des altertümlichen Sardiniens gelegt. Die Anlage besteht aus 2 wunderschönen Stöcken mit einer Veranda mit Meerblick und weitere 6 Erdgeschösse bieten eine Veranda und einen Garten. Alle Veranden sind so ausgestattet, dass sie Ihre Mahlzeiten im Freien genießen können: Grillen Sie köstliches Fleisch und Gemüse und lassen Sie dabei die atemberaubende Sonnenaufgänge und Sonnenuntergänge auf sich wirken. Die unberührte Natur Sardiniens macht es möglich!'
                ]
            ]);


            //RoomCategory.id => 3
            DB::table('room_categories')->insert(
            [
                'status_id'             => 1,
                'image'                 => '/storage/multimedia/casa/6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-2.jpg',
                'room_location_id'      => 1
            ]);
            DB::table('room_category_features')->insert([
                [
                    'room_category_id'     => 3,
                    'feature_id'             => 1,
                    'value'                  => rand(500, 100)
                ],
                [
                    'room_category_id'     => 3,
                    'feature_id'             => 2,
                    'value'                  => rand(800, 1500),
                ],
                [
                    'room_category_id'     => 3,
                    'feature_id'             => 3,
                    'value'                  =>'Si'
                ]
            ]);
            DB::table('rooms_categories_services')->insert([
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 1,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 2,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 3,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 4,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 5,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 6,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 7,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 8,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 9,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 10,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 11,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 12,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 13,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 14,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 15,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 16,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 17,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 18,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 22,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 23,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 24,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 3,
                    'service_id'            => 25,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'       => 3,
                    'service_id'    => 34,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 3,
                    'service_id'    => 35,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 3,
                    'service_id'    => 36,
                    'info'          => 'Giardino'
                ],
                [
                    'room_categories_id'       => 3,
                    'service_id'    => 37,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 3,
                    'service_id'    => 38,
                    'info'          => '100'
                ]
            ]);
            DB::table('room_category_translations')->insert([
                [
                    'room_category_id'      => 3, //incremental
                    'language_id'           => 1, //Italiano
                    'name'                  => 'Villetta Bouganville',
                    'description'           => 'Immersa in un ridente giardino adornato da ulivi e melograni, la Villetta Stella Maris è pensata per chi cerca relax, tranquillità e privacy. Dotata di tutti i comfort e arredata con cura e passione nei minimi dettagli per far sentire gli ospiti amati e coccolati. A un passo dal centro di Budoni e poco distante dalla meravigliosa spiaggia Salamaghe, è la soluzione ideale per scoprire che "casa" è ovunque portiamo il cuore e la famiglia.'
                ],
                [
                    'room_category_id'      => 3, //incremental
                    'language_id'           => 2, //English
                    'name'                  => 'Bouganville small villa',
                    'description'           => 'Composed of two independent apartments, Villetta Bouganville is surrounded by a flourishing garden, evergreen hedges and multicolored flowers. Adorned with a bouganville of a velvety violet color that frames the spacious entrances and the cozy verandas. Strategically located close to the sea and the centre of Budoni, for those who want to park the car and \'forget it\' for the rest of their stay. Equipped with all the comforts and cared for in both the interior and exterior furnishings. From the first floor, between the blue curtains of the veranda, you can admire the sea and enjoy the sunset while sipping a good Vermentino di Gallura.'
                ],
                [
                    'room_category_id'      => 3, //incremental
                    'language_id'           => 3, //Aleman
                    'name'                  => 'Kleines villa Bouganville',
                    'description'           => 'Das Einfamilienhaus Villino Bouganville besteht aus zwei getrennt begehbaren Apartments und ist umgeben von einem blühenden Garten, immergrünen Hecken und farbenfrohen Blumen. Mit der violetten Pflanze Bougainvillea, auch als Drillingsblume bekannt, geschmückt entsteht ein romantischer Rahmen für die großzügigen Eingänge und Veranden. Die strategische Lage in unmittelbarer Nähe zum Meer und zum Zentrum von Budoni ermöglicht das eigene Auto in Sicherheit zu parken und es für den gesamten Aufenthalt zu "vergessen". Mit allem Komfort ausgestattet und mit viel Liebe und Leidenschaft eingerichtet bietet das Haus die Möglichkeit, durch die schneeweißen Vorhängen hindurch, aufs Meer zu blicken und den Sonnenuntergang mit einem köstlichen Gläschen Vermentino di Gallura zu genießen.'
                ]
            ]);


            //RoomCategory.id => 4
            DB::table('room_categories')->insert(
            [
                'status_id'             => 1,
                'image'                 => '/storage/multimedia/casa/6-trilocale-piano-terra-con-giardino-e-aria-condizionata-villino-bouganville-2.jpg',
                'room_location_id'      => 2
            ]);
            DB::table('room_category_features')->insert([
                [
                    'room_category_id'     => 4,
                    'feature_id'             => 1,
                    'value'                  => rand(500, 100)
                ],
                [
                    'room_category_id'     => 4,
                    'feature_id'             => 2,
                    'value'                  => rand(800, 1500),
                ],
                [
                    'room_category_id'     => 4,
                    'feature_id'             => 3,
                    'value'                  =>'Si'
                ]
            ]);
            DB::table('rooms_categories_services')->insert([
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 1,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 2,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 3,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 4,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 5,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 6,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 7,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 8,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 9,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 10,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 11,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 12,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 13,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 14,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 15,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 16,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 17,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 18,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 19,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 20,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 21,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 22,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 23,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 24,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 4,
                    'service_id'            => 25,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'       => 4,
                    'service_id'    => 34,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 4,
                    'service_id'    => 35,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 4,
                    'service_id'    => 36,
                    'info'          => 'Giardino'
                ],
                [
                    'room_categories_id'       => 4,
                    'service_id'    => 37,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 4,
                    'service_id'    => 38,
                    'info'          => '100'
                ]
            ]);
            DB::table('room_category_translations')->insert([
                [
                    'room_category_id'      => 4, //incremental
                    'language_id'           => 1, //Italiano
                    'name'                  => 'Villa Ludduì',
                    'description'           => 'Nel centro del grazioso paese di Budoni, vicino a tutti i servizi e a poca distanza dalla finissima spiaggia di Salamaghe, il Residence è il perfetto punto di partenza per chi preferisce rigenerarsi scoprendo ogni giorno un luogo diverso della nostra splendida Isola.'
                ],
                [
                    'room_category_id'      => 4, //incremental
                    'language_id'           => 2, //English
                    'name'                  => 'Villa Ludduì',
                    'description'           => 'In the middle of the pretty village of Budoni, close to all amenities and within easy reach of the wonderful Salamaghe beach, the Residence is the perfect starting point for those who prefer to discover a new place every day on our beautiful island. There are several types of apartments from 2-room to 3-room located on the ground floor or on the first floor.'
                ],
                [
                    'room_category_id'      => 4, //incremental
                    'language_id'           => 3, //Aleman
                    'name'                  => 'Kleines villa Luddui',
                    'description'           => 'Im Zentrum der hübschen Stadt Budoni, nah an allen Einrichtungen und nicht weit vom feinen Strand von Salamaghe entfernt, bildet die Wohnanlage der ideale Ausgangspunkt für diejenigen, die jeden Tag an einem anderen Ort auf unserer schönen Insel relaxen wollen. Es gibt verschiedene Arten von Wohnungen, Zweizimmer- oder Dreizimmer-Wohnungen, die im Erdgeschoss oder im ersten Stock positioniert sind.'
                ]
            ]);


            //RoomCategory.id => 5
            DB::table('room_categories')->insert(
            [
                'status_id'             => 1,
                'image'                 => '/storage/multimedia/casa/9-bilocale-13.jpg',
                'room_location_id'      => 1
            ]);
            DB::table('room_category_features')->insert([
                [
                    'room_category_id'     => 5,
                    'feature_id'             => 1,
                    'value'                  => rand(500, 100),
                ],
                [
                    'room_category_id'     => 5,
                    'feature_id'             => 2,
                    'value'                  => rand(800, 1500),
                ],
                [
                    'room_category_id'     => 5,
                    'feature_id'             => 3,
                    'value'                  =>'Si'
                ]
            ]);
            DB::table('rooms_categories_services')->insert([
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 1,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 2,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 3,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 4,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 5,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 6,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 7,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 10,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 11,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 12,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 13,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 14,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 15,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 16,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 17,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 19,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 22,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 23,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 24,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'    => 5,
                    'service_id'            => 25,
                    'info'                  => 'Sì'
                ],
                [
                    'room_categories_id'       => 5,
                    'service_id'    => 34,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 5,
                    'service_id'    => 35,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 5,
                    'service_id'    => 36,
                    'info'          => 'Giardino'
                ],
                [
                    'room_categories_id'       => 5,
                    'service_id'    => 37,
                    'info'          => '100'
                ],
                [
                    'room_categories_id'       => 5,
                    'service_id'    => 38,
                    'info'          => '100'
                ]
            ]);
            DB::table('room_category_translations')->insert([
                [
                    'room_category_id'      => 5, //incremental
                    'language_id'           => 1, //Italiano
                    'name'                  => 'Residence Budoni Centro',
                    'description'           => 'Nel centro del grazioso paese di Budoni, vicino a tutti i servizi e a poca distanza dalla finissima spiaggia di Salamaghe, il Residence è il perfetto punto di partenza per chi preferisce rigenerarsi scoprendo ogni giorno un luogo diverso della nostra splendida Isola.'
                ],
                [
                    'room_category_id'      => 5, //incremental
                    'language_id'           => 2, //English
                    'name'                  => 'Residence Budoni Centre',
                    'description'           => 'In the middle of the pretty village of Budoni, close to all amenities and within easy reach of the wonderful Salamaghe beach, the Residence is the perfect starting point for those who prefer to discover a new place every day on our beautiful island. There are several types of apartments from 2-room to 3-room located on the ground floor or on the first floor.'
                ],
                [
                    'room_category_id'      => 5, //incremental
                    'language_id'           => 3, //Aleman
                    'name'                  => 'Residence Budoni Zentrum',
                    'description'           => 'Im Zentrum der hübschen Stadt Budoni, nah an allen Einrichtungen und nicht weit vom feinen Strand von Salamaghe entfernt, bildet die Wohnanlage der ideale Ausgangspunkt für diejenigen, die jeden Tag an einem anderen Ort auf unserer schönen Insel relaxen wollen. Es gibt verschiedene Arten von Wohnungen, Zweizimmer- oder Dreizimmer-Wohnungen, die im Erdgeschoss oder im ersten Stock positioniert sind.'
                ]
            ]);
    }
}
