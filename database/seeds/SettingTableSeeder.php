<?php

use App\Models\Admin\Setting;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Setting::class)->create([
            'name' => 'Tempo di prenotazione (minuti)',
            'key' => 'booking_time',
            'value' => '5',
        ]);

        factory(Setting::class)->create([
            'name' => 'ID cliente Paypal',
            'key' => 'paypal_client_id',
            'value' => 'AWqNB7O2Uf_Iak3B5Ybz7bLccKISGX7RzrIKXJcX7t9hLQwYMtUdPJMxrc0EyPN2LSJ70b_pHHVv3Llg',

        ]);

        factory(Setting::class)->create([
            'name' => 'Modalità Paypal',
            'key' => 'paypal_mode',
            'value' => 'sandbox',
        ]);

        factory(Setting::class)->create([
            'name' => 'Giorni di prenotazione minimi',
            'key' => 'booking_min_days',
            'value' => '7',
        ]);

        factory(Setting::class)->create([
            'name' => 'Chiave del sito di recaptcha',
            'key' => 'recaptcha_site_key',
            'value' => '6LcPb40UAAAAACQJq6MA6T-oJKGq9Egyg8kLD8Jm',
        ]);

        factory(Setting::class)->create([
            'name' => 'URL Facebook',
            'key' => 'facebook_url',
            'value' => 'https://www.facebook.com/SolariaHomeHoliday/?ref=br_rs',
        ]);

        factory(Setting::class)->create([
            'name' => 'URL Instagram',
            'key' => 'instagram_url',
            'value' => 'https://www.instagram.com/solariahomeholiday/',
        ]);

        factory(Setting::class)->create([
            'name' => 'Whatsapp Number',
            'key' => 'whatsapp_number',
            'value' => '393408831917',
        ]);

        factory(Setting::class)->create([
            'name' => 'Telefono',
            'key' => 'telefono',
            'value' => '393408831917',
        ]);

        factory(Setting::class)->create([
            'name' => 'URL Youtube',
            'key' => 'youtube_url',
            'value' => 'https://www.youtube.com/channel/UCjDiQmnUMWkaCrs9q-3761w/featured',
        ]);

        factory(Setting::class)->create([
            'name' => 'Booking Email',
            'key' => 'booking_email',
            'value' => 'booking@solariahomeholiday.com',
        ]);

        factory(Setting::class)->create([
            'name' => 'Contact Email',
            'key' => 'contact_email',
            'value' => 'info@solariahomeholiday.com'
        ]);

        factory(Setting::class)->create([
            'name' => 'Address',
            'key' => 'address',
            'value' => 'Via Papa Giovanni XXIII, 7'
        ]);

        factory(Setting::class)->create([
            'name' => 'Google Map',
            'key' => 'iframe_google_map',
            'value' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4820.174114515201!2d9.698173053021549!3d40.7099472647752!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12dec55fd57942f9%3A0x5a8d19f11c61d207!2sSolaria+Vacanze!5e0!3m2!1ses!2sve!4v1557170032149!5m2!1ses!2sve" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'
        ]);
    }
}
