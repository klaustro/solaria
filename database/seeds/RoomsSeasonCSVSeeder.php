<?php

use Illuminate\Database\Seeder;
use Flynsarmy\CsvSeeder\CsvSeeder;


class RoomsSeasonCSVSeeder extends CsvSeeder
{
    public function __construct()
    {
        $this->table = 'room_seasons';
        $this->filename = base_path().'/database/seeds/csvs/room_seasons.csv';
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        //DB::table($this->table)->truncate();

        parent::run();
    }
}
