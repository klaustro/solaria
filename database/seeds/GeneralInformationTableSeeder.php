<?php

use Illuminate\Database\Seeder;

class GeneralInformationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //id => 1
        DB::table('general_informations')->insert([
            'slug' => 'informazioni-generali'
        ]);

        DB::table('general_information_translations')->insert([
            [
                'name'                    => 'Informazioni Generali',
                'description'             => '<div class="row justify-content-center"><div class="col-12 mt-3"><h3 class="text-uppercase font-weight-bold text-center mb-0">A tutti i clienti offriamo il Set Cortesia</h3> <hr></div> <div class="col-12"><div class="row"><div class="col-12 col-md-4"><li class="text-capitalize pl-5">2 rotoli carta igienica</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 saponetta mani</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">bottiglia acqua in frigo</li></div></div></div><div class="col-12 mt-3"><h3 class="text-uppercase font-weight-bold text-center mb-0 mt-4">Welcome Kit Pulizia</h3> <hr></div> <div class="col-12"><div class="row"><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Spugna per piatti</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Panno microfibra</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 Detersivo Piatti in Flacone da 50ml</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 Detergente Piatti in Flacone da 50ml</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">3 Pastiglie per lavatrice</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">3 Pastiglie per lavatoviglie</li></div></div></div></div>',
                'language_id'             => 1,
                'general_information_id'  => 1
            ],
            [
                'name'                    => 'Informazioni Generali',
                'description'             => '<div class="row justify-content-center"><div class="col-12 mt-3"><h3 class="text-uppercase font-weight-bold text-center mb-0">All Customers we offer the courtesy Set</h3> <hr></div> <div class="col-12"><div class="row"><div class="col-12 col-md-4"><li class="text-capitalize pl-5">2 toilet paper rolls</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 mani soap</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Water bottle in fridge</li></div></div></div><div class="col-12 mt-3"><h3 class="text-uppercase font-weight-bold text-center mb-0 mt-4"> Welcome cleaning Kit </h3> <hr></div> <div class="col-12"><div class="row"><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Sponge for Dishes</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Microfibre cloth</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 dish detergent in bottle of 50ml</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 dish detergent in bottle of 50ml</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">3 washer pads</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">3 pads for Lavatoviglie</li></div></div></div></div>',
                'language_id'             => 2,
                'general_information_id'  => 1
            ],
            [
                'name'                    => 'Informazioni Generali',
                'description'             => '<div class="row justify-content-center"><div class="col-12 mt-3"><h3 class="text-uppercase font-weight-bold text-center mb-0">Alle Kunden bieten wir das Höflichkeitsset</h3> <hr></div> <div class="col-12"><div class="row"><div class="col-12 col-md-4"><li class="text-capitalize pl-5">2 Toilettenpapierrollen</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 Mani-Seife</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Wasserflasche im Kühlschrank</li></div></div></div><div class="col-12 mt-3"><h3 class="text-uppercase font-weight-bold text-center mb-0 mt-4">Welcome cleaning Kit</h3> <hr></div> <div class="col-12"><div class="row"><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Sponge für die Gerichte</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Mikrofasertuch</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 dish Waschmittel in Flasche 50ml</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 dish Waschmittel in Flasche 50ml</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">3 Waschmaschinen</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">3 Pads for Lavatoviglie</li></div></div></div></div>',
                'language_id'             => 3,
                'general_information_id'  => 1
            ],
            [
                'name'                    => 'Informazioni Generali',
                'description'             => '<div class="row justify-content-center"><div class="col-12 mt-3"><h3 class="text-uppercase font-weight-bold text-center mb-0">A todos los clientes les ofrecemos el conjunto de cortesía.</h3> <hr></div> <div class="col-12"><div class="row"><div class="col-12 col-md-4"><li class="text-capitalize pl-5">2 rollos de papel higiénico</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">1 jabón de mani</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5">Botella de agua en nevera.</li></div></div></div><div class="col-12 mt-3"><h3 class="text-uppercase font-weight-bold text-center mb-0 mt-4"> kit de limpieza de bienvenida </h3> <hr></div> <div class="col-12"><div class="row"><div class="col-12 col-md-4"><li class="text-capitalize pl-5"> esponja para platos</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5"> paño de microfibra</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5"> 1 detergente en botella de 50ml</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5"> 1 detergente en botella de 50ml</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5"> 3 lavadoras</li></div><div class="col-12 col-md-4"><li class="text-capitalize pl-5"> 3 pads para Lavatoviglie</li></div></div></div></div>',
                'language_id'             => 4,
                'general_information_id'  => 1
            ]
        ]);
    }
}
