<?php

use Illuminate\Database\Seeder;

class ScreensFrontSectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Home / front_sections*/
        DB::table('screens_front_sections')->insert([
            [
                'screen_id' => 1,
                'front_section_id' => 1
            ]
        ]);
    }
}
