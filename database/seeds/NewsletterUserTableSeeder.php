<?php

use App\Models\Admin\NewsletterUser;
use Illuminate\Database\Seeder;

class NewsletterUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(NewsletterUser::class, 10)->create();
    }
}
