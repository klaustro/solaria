<?php

use App\Models\Admin\ActivityRequest;
use Illuminate\Database\Seeder;

class ActivityRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ActivityRequest::class, 20)->create();
    }
}
