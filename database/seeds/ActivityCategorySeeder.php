<?php

use App\Models\Admin\ActivityCategory;
use App\Models\Admin\ActivityCategoryTranslation;
use Illuminate\Database\Seeder;

class ActivityCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activity_categories = factory(ActivityCategory::class)->create(['image' => 'images/home/esperience-slider/solaria-vacanze-esperienze-spiagge-min.jpg']);
        $activity_categories = factory(ActivityCategory::class)->create(['image' => 'images/home/esperience-slider/solaria-vacanze-esperienze-escursionit-min.jpg']);
        $activity_categories = factory(ActivityCategory::class)->create(['image' => 'images/home/esperience-slider/solaria-vacanze-esperienze-sport-min.jpg']);
        $activity_categories = factory(ActivityCategory::class)->create(['image' => 'images/home/esperience-slider/solaria-vacanze-esperienze-nightlife-min.jpg']);
        $activity_categories = factory(ActivityCategory::class)->create(['image' => 'images/home/esperience-slider/solaria-vacanze-esperienze-enogastronomia-min.jpg']);
        $activity_categories = factory(ActivityCategory::class)->create(['image' => 'images/home/esperience-slider/caprera.jpg']);

        //Italian
        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 1,
            'name' => 'Spiagge',
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 2 ,
            'name' => 'Sport',
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 3,
            'name' => 'Escursioni',
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 4 ,
            'name' => 'Nightlife',
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 5 ,
            'name' => 'Enogastronomia',
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 6 ,
            'name' => 'Archeologia',
        ]);

        //English
        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 1,
            'name' => 'Beaches',
            'language_id' => 2,
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 2,
            'name' => 'Excursion',
            'language_id' => 2,
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 3 ,
            'name' => 'Sports',
            'language_id' => 2,
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 4 ,
            'name' => 'Nightlife',
            'language_id' => 2,
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 5 ,
            'name' => 'Food & Wine',
            'language_id' => 2,
        ]);

        factory(ActivityCategoryTranslation::class)->create([
            'activity_category_id' => 6 ,
            'name' => 'Archeologia',
            'language_id' => 2,
        ]);
    }
}
