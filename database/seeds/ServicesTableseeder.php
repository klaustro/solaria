<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			//  ____                             ____           _                                   _
			// / ___|    ___   _ __  __   __    / ___|   __ _  | |_    ___    __ _    ___    _ __  (_)   ___   ___
			// \___ \   / _ \ | '__| \ \ / /   | |      / _` | | __|  / _ \  / _` |  / _ \  | '__| | |  / _ \ / __|
			//  ___) | |  __/ | |     \ V /    | |___  | (_| | | |_  |  __/ | (_| | | (_) | | |    | | |  __/ \__ \
			// |____/   \___| |_|      \_/      \____|  \__,_|  \__|  \___|  \__, |  \___/  |_|    |_|  \___| |___/
			//                                                               |___/
			    DB::table('service_categories')->insert([
			        ['status_id'  => 1], //service_categories.id => 1 Servizi per Booking
			        ['status_id'  => 1],  //service_categories.id => 2 Dettagli Servizi
                    ['status_id'  => 1]  //service_categories.id => 3 Caratteristiche Casa
			    ]);

			    DB::table('service_category_translations')->insert([
			        [
			            'service_category_id'   => 1,
			            'language_id'           => 1,
			            'name'                  => 'Servizi per Booking',
			            'description'           => 'Servizi per Booking'
			        ],
			        [
			            'service_category_id'   => 2,
			            'language_id'           => 1,
			            'name'                  => 'Dettagli Servizi',
			            'description'           => 'Dettagli Servizi'
			        ],
                    [
                        'service_category_id'   => 3,
                        'language_id'           => 1,
                        'name'                  => 'Caratteristiche Casa',
                        'description'           => 'Caratteristiche Casa'
                    ]
			    ]);

			//  ____                          _                       _             ____
			// / ___|    ___   _ __  __   __ (_)   ___    ___      __| |   ___     / ___|   __ _   ___    __ _
			// \___ \   / _ \ | '__| \ \ / / | |  / __|  / _ \    / _` |  / _ \   | |      / _` | / __|  / _` |
			//  ___) | |  __/ | |     \ V /  | | | (__  |  __/   | (_| | |  __/   | |___  | (_| | \__ \ | (_| |
			// |____/   \___| |_|      \_/   |_|  \___|  \___|    \__,_|  \___|    \____|  \__,_| |___/  \__,_|

			    DB::table('services')->insert( //services.id => 1
			        [
			            'status_id'             => 1,
			            'service_category_id'   => 2,
			            'ico'                   => '/storage/multimedia/icons/bagno_con_doccia.svg'
			        ]
			    );
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 1,
			            'language_id'       => 1,
			            'name'              => 'Bagno con doccia',
			            'description'       => 'Bagno con doccia',
			        ]
			    );
			    DB::table('services')->insert( //services.id => 2
			        [
			            'status_id'             => 1,
			            'service_category_id'   => 2,
			            'ico'                   => '/storage/multimedia/icons/aria_condizionata.svg'
			        ]
			    );
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 2,
			            'language_id'       => 1,
			            'name'              => 'Aria condizionata',
			            'description'       => 'Aria condizionata',
			        ]
			    );
			    DB::table('services')->insert( //services.id => 3
			        [
			            'status_id'             => 1,
			            'service_category_id'   => 2,
			            'ico'                   => '/storage/multimedia/icons/satellite.svg'
			        ]
			    );
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 3,
			            'language_id'       => 1,
			            'name'              => 'TV sat',
			            'description'       => 'TV sat',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/giardino.svg']); //services.id => 4
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 4,
			            'language_id'       => 1,
			            'name'              => 'Giardino',
			            'description'       => 'Giardino',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/barbecue.svg']); //services.id => 5
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 5,
			            'language_id'       => 1,
			            'name'              => 'Barbecue',
			            'description'       => 'Barbecue',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1,'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/ferro_da_stiro.svg']); //services.id => 6
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 6,
			            'language_id'       => 1,
			            'name'              => 'Ferro da stiro',
			            'description'       => 'Ferro da stiro',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/lavatrice.svg']); //services.id => 7
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 7,
			            'language_id'       => 1,
			            'name'              => 'Lavatrice',
			            'description'       => 'Lavatrice',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/lavastoviglie.svg']); //services.id => 8
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 8,
			            'language_id'       => 1,
			            'name'              => 'Lavastoviglie',
			            'description'       => 'Lavastoviglie',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/frigo.svg']); //services.id => 9
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 9,
			            'language_id'       => 1,
			            'name'              => 'Frigo',
			            'description'       => 'Frigo',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/freezer.svg']); //services.id => 10
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 10,
			            'language_id'       => 1,
			            'name'              => 'Freezer',
			            'description'       => 'Freezer',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1,'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/seggiolone.svg']); //services.id => 11
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 11,
			            'language_id'       => 1,
			            'name'              => 'Seggiolone ( su richiesta)',
			            'description'       => 'Seggiolone ( su richiesta)',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/forno.svg']); //services.id => 12
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 12,
			            'language_id'       => 1,
			            'name'              => 'Forno',
			            'description'       => 'Forno',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/cucina_elettrica_gas.svg']); //services.id => 13
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 13,
			            'language_id'       => 1,
			            'name'              => 'Cucina elettrica/gas',
			            'description'       => 'Cucina elettrica/gas',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/stoviglie.svg']); //services.id => 14
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 14,
			            'language_id'       => 1,
			            'name'              => 'Stoviglie',
			            'description'       => 'Stoviglie',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/stendibiancheria.svg']); //services.id => 15
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 15,
			            'language_id'       => 1,
			            'name'              => 'Stendibiancheria',
			            'description'       => 'Stendibiancheria',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1,'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/asse_da_stiro.svg']); //services.id => 16
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 16,
			            'language_id'       => 1,
			            'name'              => 'Asse da stiro',
			            'description'       => 'Asse da stiro',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/doccia_esterna.svg']); //services.id => 17
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 17,
			            'language_id'       => 1,
			            'name'              => 'Doccia esterna',
			            'description'       => 'Doccia esterna',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/arredamento-da_esterno.svg']); //services.id => 18
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 18,
			            'language_id'       => 1,
			            'name'              => 'Arredamento da esterno',
			            'description'       => 'Arredamento da esterno',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/tv_lcd_32pollici.svg']); //services.id => 19
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 19,
			            'language_id'       => 1,
			            'name'              => 'Tv LCD 32 pollici',
			            'description'       => 'Tv LCD 32 pollici',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/satellite.svg']); //services.id => 20
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 20,
			            'language_id'       => 1,
			            'name'              => 'Satellite',
			            'description'       => 'Satellite',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1,'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/riserva_idrica.svg']); //services.id => 21
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 21,
			            'language_id'       => 1,
			            'name'              => 'Riserva Idrica',
			            'description'       => 'Riserva Idrica',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/wi_fi_gratuito.svg']); //services.id => 22
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 22,
			            'language_id'       => 1,
			            'name'              => 'Wi-fi gratuito',
			            'description'       => 'Wi-fi gratuito',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/biancheria_letto.svg']); //services.id => 23
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 23,
			            'language_id'       => 1,
			            'name'              => 'Biancheria letto (optional/extra)',
			            'description'       => 'Biancheria letto (optional/extra)',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/asciugamani.svg']); //services.id => 24
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 24,
			            'language_id'       => 1,
			            'name'              => 'Asciugamani (optional/extra)',
			            'description'       => 'Asciugamani (optional/extra)',
			        ]
			    );
			    DB::table('services')->insert(['status_id'  => 1, 'service_category_id' => 2, 'ico' => '/storage/multimedia/icons/animali_ammessi.svg']); //services.id => 25
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 25,
			            'language_id'       => 1,
			            'name'              => 'Animali ammessi (optional/extra)',
			            'description'       => 'Animali ammessi (optional/extra)',
			        ]
			    );

			//  ____                          _          _                 _            _               _       _   _                    _
			// / ___|    ___   _ __  __   __ (_)   ___  (_)   ___       __| |   ___    | |__     __ _  | |__   (_) | |_    __ _    ___  (_)   ___    _ __     ___   ___
			// \___ \   / _ \ | '__| \ \ / / | |  / __| | |  / _ \     / _` |  / _ \   | '_ \   / _` | | '_ \  | | | __|  / _` |  / __| | |  / _ \  | '_ \   / _ \ / __|
			// ___ ) | |  __/ | |     \ V /  | | | (__  | | | (_) |   | (_| | |  __/   | | | | | (_| | | |_) | | | | |_  | (_| | | (__  | | | (_) | | | | | |  __/ \__ \
			// |____/   \___| |_|      \_/   |_|  \___| |_|  \___/     \__,_|  \___|   |_| |_|  \__,_| |_.__/  |_|  \__|  \__,_|  \___| |_|  \___/  |_| |_|  \___| |___/
			    //services.id => 26
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/camere_da_letto.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 26,
			            'language_id'       => 1,
			            'name'              => 'Camere da letto',
			            'description'       => 'Camere da letto',
			        ]
			    );
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 26,
			            'language_id'       => 2,
			            'name'              => 'Bedrooms',
			            'description'       => 'Bedrooms',
			        ]
			    );


			    //services.id => 27
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/bagni.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 27,
			            'language_id'       => 1,
			            'name'              => 'Bagni',
			            'description'       => 'Bagni',
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 27,
			            'language_id'       => 2,
			            'name'              => 'Bathrooms',
			            'description'       => 'Bathrooms',
			        ]
			    );


                //services.id => 28
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/posti-letto.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 28,
			            'language_id'       => 1,
			            'name'              => 'Posti letto',
			            'description'       => 'Posti letto',
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 28,
			            'language_id'       => 2,
			            'name'              => 'Beds',
			            'description'       => 'Beds',
			        ]
			    );


                //services.id => 29
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/giardino.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 29,
			            'language_id'       => 1,
			            'name'              => 'Giardino',
			            'description'       => 'Giardino',
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 29,
			            'language_id'       => 2,
			            'name'              => 'Garden',
			            'description'       => 'Garden',
			        ]
			    );


                //services.id => 30
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/lavatrice.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 30,
			            'language_id'       => 1,
			            'name'              => 'Lavatrice',
			            'description'       => 'Lavatrice',
			        ]
			    );
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 30,
			            'language_id'       => 2,
			            'name'              => 'Washing machine',
			            'description'       => 'Washing machine',
			        ]
			    );


                //services.id => 31
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/climatizzata.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 31,
			            'language_id'       => 1,
			            'name'              => 'Climatizzata',
			            'description'       => 'Climatizzata',
			        ]
			    );
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 31,
			            'language_id'       => 2,
			            'name'              => 'Air-conditioned',
			            'description'       => 'Air-conditioned',
			        ]
			    );


                //services.id => 32
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/tv_lcd_40pollici.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 32,
			            'language_id'       => 1,
			            'name'              => 'televisione satellitare',
			            'description'       => 'televisione satellitare',
			        ]
			    );
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 32,
			            'language_id'       => 2,
			            'name'              => 'Tv-Sat',
			            'description'       => 'Tv-Sat',
			        ]
			    );


                //services.id => 33
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/posti-auto.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 33,
			            'language_id'       => 1,
			            'name'              => 'Posto auto',
			            'description'       => 'Posto auto',
			        ]
			    );


			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 33,
			            'language_id'       => 2,
			            'name'              => 'Parking space',
			            'description'       => 'Parking space',
			        ]
				);
				
				//Categoria 3

				//services.id => 34
			    DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 3,
			            'ico' => '/storage/multimedia/icons/002-sunsetgreen.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 34,
			            'language_id'       => 1,
			            'name'              => 'distanza dalla spiaggia',
			            'description'       => 'distanza dalla spiaggia',
			        ]
				);
				//services.id => 35
				DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 3,
			            'ico' => '/storage/multimedia/icons/001-placeholdergreen.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 35,
			            'language_id'       => 1,
			            'name'              => 'distanza centro',
			            'description'       => 'distanza centro',
			        ]
				);
				//services.id => 36
				DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 3,
			            'ico' => '/storage/multimedia/icons/leafgreen.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 36,
			            'language_id'       => 1,
			            'name'              => 'Giardino',
			            'description'       => 'Giardino',
			        ]
				);
				//services.id => 37
				DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 3,
			            'ico' => '/storage/multimedia/icons/aeroporto.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 37,
			            'language_id'       => 1,
			            'name'              => 'aeroporto',
			            'description'       => 'aeroporto',
			        ]
				);
				//services.id => 38
				DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 3,
			            'ico' => '/storage/multimedia/icons/porto.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 38,
			            'language_id'       => 1,
			            'name'              => 'porto',
			            'description'       => 'porto',
			        ]
				);

				//services.id => 39
				DB::table('services')->insert(
			        [
			            'status_id'  => 1,
			            'service_category_id' => 1,
			            'ico' => '/storage/multimedia/icons/metros-cuadrados.svg'
			        ]);
			    DB::table('service_translations')->insert(
			        [
			            'service_id'        => 39,
			            'language_id'       => 1,
			            'name'              => 'MQ',
			            'description'       => 'MQ',
			        ]
				);

    }
}
