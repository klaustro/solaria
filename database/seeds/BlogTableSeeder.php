<?php

use App\Models\Admin\Blog;
use App\Models\Admin\BlogCategory;
use App\Models\Admin\BlogCategoryTranslation;
use App\Models\Admin\BlogTranslation;
use Illuminate\Database\Seeder;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blog_categories = factory(BlogCategory::class, 3)->create();

        foreach ($blog_categories as $key => $category) {
            factory(BlogCategoryTranslation::class)->create([
                'blog_category_id' => $category->id
            ]);

            $blogs = factory(Blog::class, 3)->create([
                'blog_category_id' => $category->id
            ]);

            foreach ($blogs as $key => $blog) {
                factory(BlogTranslation::class)->create([
                    'blog_id' => $blog->id
                ]);
            }
        }
    }
}
