<?php

use App\Models\Admin\ActivityCategory;
use Faker\Generator as Faker;

$factory->define(ActivityCategory::class, function (Faker $faker) {
    return [
        'code' => $faker->numerify('C###'),
        'image' => $faker->imageUrl,
    ];
});
