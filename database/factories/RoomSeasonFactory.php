<?php

use App\Models\Admin\Room;
use App\Models\Admin\RoomSeason;
use Faker\Generator as Faker;

$factory->define(RoomSeason::class, function (Faker $faker) {
    return [
        'room_id' => function(){
            return firstOrFactory(Room::class)->id;
        },
        'start_date' => $faker->dateTimeThisYear,
        'end_date' => $faker->dateTimeThisYear,
        'price' => rand(1, 60),
        'iva' => 0.22,
    ];
});
