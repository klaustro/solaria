<?php

use App\Models\Admin\Multimedia;
use Faker\Generator as Faker;

$factory->define(Multimedia::class, function (Faker $faker) {
    $image = $faker->imageUrl;
    return [
        'name' => basename($image),
        'path' => str_replace(basename($image), '', $image),
    ];
});
