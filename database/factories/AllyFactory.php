<?php

use App\Models\Admin\Ally;
use Faker\Generator as Faker;

$factory->define(Ally::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'website' => $faker->url,
    ];
});
