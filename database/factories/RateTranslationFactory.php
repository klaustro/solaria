<?php

use App\Models\Admin\Language;
use App\Models\Admin\Rate;
use App\Models\Admin\RateTranslation;
use Faker\Generator as Faker;

$factory->define(RateTranslation::class, function (Faker $faker) {
    return [
        'rate_id' => function(){
            return firstOrFactory(Rate::class)->id;
        },
        'language_id' => function(){
            return firstOrFactory(Language::class)->id;
        },
        'title' => 'Offerte ' . $faker->word,
        'subtitle' => $faker->sentence,
        'description' => $faker->sentence,
    ];
});
