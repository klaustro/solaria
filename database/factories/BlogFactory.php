<?php

use App\Models\Admin\Blog;
use App\Models\Admin\BlogCategory;
use App\User;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'slug' => str_slug($faker->sentence(3)),
        'blog_category_id' => function () {
            return firstOrFactory(BlogCategory::class)->id;
        },
        'user_id' => function () {
            return firstOrFactory(User::class)->id;
        },
        'image' => $faker->imageUrl,
    ];
});
