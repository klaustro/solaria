<?php

use App\Models\Admin\Setting;
use Faker\Generator as Faker;

$factory->define(Setting::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'key' => $faker->word,
        'value' => $faker->word,
    ];
});
