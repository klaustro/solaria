<?php

use App\Models\Admin\Booking;
use App\Models\Admin\DataForm;
use App\Models\Admin\PaymentMethod;
use App\Models\Admin\Status;
use App\User;
use Faker\Generator as Faker;

$factory->define(Booking::class, function (Faker $faker) {
    return [
        'code' => $faker->realText(10),
        'user_id' => function(){
            return firstOrFactory(User::class)->id;
        },
        'subtotal' => rand(1, 20),
        'iva' => rand(1, 5),
        'total' => rand(1, 25),
        'data_form_id' => function(){
            return factory(DataForm::class)->create()->id;
        },
        'show_to_user' => function(){
            return firstOrFactory(User::class)->id;
        },
        'status_id' => function(){
            return firstOrFactory(Status::class)->id;
        },
        'payment_method_id' => function(){
            return firstOrFactory(PaymentMethod::class)->id;
        },
        'code_payment' => rand(1, 100),
    ];
});
