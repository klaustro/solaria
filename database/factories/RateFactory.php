<?php

use App\Models\Admin\Rate;
use App\Models\Admin\Room;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Rate::class, function (Faker $faker) {
    $start_date = $faker->dateTimeThisYear('2020-01-01');
    $end_date = Carbon::instance($start_date)->addDays(rand(2, 10));
    return [
        'room_id' => rand(1, 16),
        'start_date'  => $start_date,
        'end_date'    => $end_date,
        'price'       => rand(10, 50),
        'iva'         => rand(1, 5),
        'num_people'  => rand(1, 5)
    ];
});
