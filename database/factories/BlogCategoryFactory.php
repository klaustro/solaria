<?php

use App\Models\Admin\BlogCategory;
use Faker\Generator as Faker;

$factory->define(BlogCategory::class, function (Faker $faker) {
    return [
        'code' => $faker->word(3),
    ];
});
