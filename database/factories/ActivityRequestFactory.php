<?php

use App\Models\Admin\Activity;
use App\Models\Admin\ActivityRequest;
use Faker\Generator as Faker;

$factory->define(ActivityRequest::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'activity_id' => function() {
            return firstOrFactory(Activity::class)->id;
        },
        'adult' => rand(1, 5),
        'children' => rand(1, 5),
        'pets' => rand(1, 5),
        'message' => $faker->sentence,
    ];
});
