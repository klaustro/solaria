<?php

use App\Models\Admin\Activity;
use App\Models\Admin\ActivityCategory;
use App\Models\Admin\Ally;
use Faker\Generator as Faker;

$factory->define(Activity::class, function (Faker $faker) {
    return [
        'slug' => $faker->word,
        'activity_category_id' => function() {
            return firstOrFactory(ActivityCategory::class)->id;
        },
        'ally_id' => function() {
            return firstOrFactory(Ally::class)->id;
        },
    ];
});
