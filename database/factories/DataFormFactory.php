<?php

use App\Models\Admin\DataForm;
use Faker\Generator as Faker;

$factory->define(DataForm::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'fiscal_code' => $faker->ean13,
    ];
});
