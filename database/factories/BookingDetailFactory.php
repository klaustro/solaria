<?php

use App\Models\Admin\Booking;
use App\Models\Admin\BookingDetail;
use App\Models\Admin\DataForm;
use App\Models\Admin\Room;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(BookingDetail::class, function (Faker $faker) {
    $checkin = $faker->dateTimeThisYear('2020-01-01');
    $checkout = Carbon::instance($checkin)->addDays(rand(2, 10));

    return [
        'booking_id' => function(){
            return firstOrFactory(Booking::class)->id;
        },
        'room_id' => function(){
            return firstOrFactory(Room::class)->id;
        },
        'room_price' => rand(1, 20),
        'room_iva' => rand(1, 5),
        'room_name' => rand(1, 20),
        'adults_quantity' => rand(1, 8),
        'children_quantity' => rand(1, 5),
        'checkin_date' => $checkin,
        'checkout_date' => $checkout,
        'data_form_id' => function(){
            return firstOrFactory(DataForm::class)->id;
        },
        'iva_item' => rand(1, 5),
        'total_item' => rand(1, 20),
    ];
});
