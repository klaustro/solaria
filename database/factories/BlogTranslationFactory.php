<?php

use App\Models\Admin\BlogTranslation;
use App\Models\Admin\Language;
use Faker\Generator as Faker;

$factory->define(BlogTranslation::class, function (Faker $faker) {
    return [
        'blog_id' => $faker->word,
        'language_id' => function () {
            return firstOrFactory(Language::class)->id;
        },
        'title' => $faker->sentence(3),
        'subtitle' => $faker->sentence(5),
        'description' => $faker->paragraph(6),
    ];
});
