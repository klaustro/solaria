<?php

use App\Models\Admin\Activity;
use App\Models\Admin\ActivityTranslation;
use App\Models\Admin\Language;
use Faker\Generator as Faker;

$factory->define(ActivityTranslation::class, function (Faker $faker) {
    return [
        'activity_id' => function () {
            return firstOrFactory(Activity::class)->id;
        },
        'language_id' => function () {
            return firstOrFactory(Language::class)->id;
        },
        'title' => $faker->word,
        'description' => $faker->sentence(1),
    ];
});
