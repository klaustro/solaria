<?php

use App\Models\Admin\BookingDetail;
use App\Models\Admin\BookingDetailPrice;
use Faker\Generator as Faker;

$factory->define(BookingDetailPrice::class, function (Faker $faker) {
    return [
        'booking_detail_id' => function(){
            return firstOrFactory(BookingDetail::class)->id;
        },
        'type' => 'season',
        'name' => "Prezzo dell'Appartamento",
        'price' => rand(1, 20),
        'nights' => rand(1, 20),
    ];
});
