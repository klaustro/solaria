<?php

use App\Models\Admin\Room;
use App\Models\Admin\RoomCategory;
use App\Models\Admin\Status;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    return [
        'slug' => $faker->word,
        'image' => $faker->imageUrl,
        'price' => rand(1, 100),
        'adults_quantity' => 8,
        'children_quantity' => 5,
        'room_category_id' => function(){
            return firstOrFactory(RoomCategory::class)->id;
        },
        'status_id' => function(){
            return firstOrFactory(Status::class)->id;
        },
    ];
});
