<?php

use App\Models\Admin\Language;
use App\Models\Admin\RoomSeason;
use App\Models\Admin\RoomSeasonTranslation;
use Faker\Generator as Faker;

$factory->define(RoomSeasonTranslation::class, function (Faker $faker) {
    return [
        'room_season_id' => function(){
            return firstOrFactory(RoomSeason::class)->id;
        },
        'language_id' => function(){
            return firstOrFactory(Language::class)->id;
        },
        'name' => $faker->word,
        'description' => $faker->sentence,
    ];
});
