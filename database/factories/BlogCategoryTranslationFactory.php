<?php

use App\Models\Admin\BlogCategory;
use App\Models\Admin\BlogCategoryTranslation;
use App\Models\Admin\Language;
use Faker\Generator as Faker;

$factory->define(BlogCategoryTranslation::class, function (Faker $faker) {
    return [
        'blog_category_id' => function () {
            return firstOrFactory(BlogCategory::class)->id;
        },
        'language_id' => function () {
            return firstOrFactory(Language::class)->id;
        },
        'name' => $faker->word,
        'description' => $faker->text(20),
    ];
});
