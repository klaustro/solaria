<?php

use App\Models\Admin\ActivityCategory;
use App\Models\Admin\ActivityCategoryTranslation;
use App\Models\Admin\Language;
use Faker\Generator as Faker;

$factory->define(ActivityCategoryTranslation::class, function (Faker $faker) {
    return [
        'activity_category_id' => function () {
            return firstOrFactory(ActivityCategory::class)->id;
        },
        'language_id' => function () {
            return firstOrFactory(Language::class)->id;
        },
        'name' => $faker->word,
        'description' => $faker->paragraph,
    ];
});
