<?php

use App\Models\Admin\NewsletterUser;
use Faker\Generator as Faker;

$factory->define(NewsletterUser::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
    ];
});
