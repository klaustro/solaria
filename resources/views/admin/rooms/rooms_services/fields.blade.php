@if( @$roomsService )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.roomsServices.update', ['roomsServices'=>$roomsService->id]) }}" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.roomsServices.store') }}" accept-charset="UTF-8">
@endif

    @csrf

    <!-- Room Field -->
    <div class="form-group {{$errors->has('room_id') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('room_id', tags( 'back_rooms_room_title' )) !!}
        {!! Form::select('room_id', $rooms,  @$roomsService->room_id, ['class' => 'form-control', 'placeholder' => tags( 'general_select' )]) !!}
        @if ($errors->has('room_id'))
            <span class="help-block">{{ $errors->first('room_id') }}</span>
        @endif
    </div>

    <!-- Service Field -->
    <div class="form-group {{$errors->has('service_id') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('service_id', tags( 'back_menu_services' )) !!}
        {{-- {!! Form::select('service_id', $services,  @$roomsService->service_id, ['class' => 'form-control', 'placeholder' => tags( 'general_select' )]) !!} --}}
        <select id="service_id" name="service_id" class="form-control">
            <option value="0" >Seleziona un'opzione</option>
            @foreach ($services as $service)
            <option value="{{ $service['id'] }}" {{ $service['id'] == @$roomsService->service_id ? 'selected': '' }}>
                {{ $service['name'] }}  -  {{ $service->serviceCategory['name'] }}
            </option>
            @endforeach
        </select>
        @if ($errors->has('service_id'))
            <span class="help-block">{{ $errors->first('service_id') }}</span>
        @endif
    </div>

    <!-- Name Field -->
    <div class="form-group {{$errors->has('info') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('info', tags( 'general_value' )) !!}

        @if(@$roomsService)
            {!! Form::text('info', $roomsService->info ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('info', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('info'))
            <span class="help-block">{{ $errors->first('info') }}</span>
        @endif
    </div>

</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model' ]) !!}
    <a href="{!! route('admin.roomsServices.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
@endpush
