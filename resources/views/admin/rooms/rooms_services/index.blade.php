@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_rooms_services_title' ),
        'subtitle'      => tags( 'general_index' ),
        'button'        => tags( 'general_addnew' ),
        'route'         => 'admin.roomsServices.create',
        'breadcrumb'    => [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_rooms_services_title' ),
            ]
        ]
    ] )
    @include('admin.rooms.rooms_services.table')
@endsection
