<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="roomCategories-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th class="text-center">{{ tags( 'general_image' ) }}</th>
            <th>{{ tags( 'general_name' ) }}</th>
            {{-- <th>{{ tags( 'general_description' ) }}</th> --}}
            <th>{{ tags( 'back_rooms_room_location_title' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            @include('utils.language.add_table_th')

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $roomCategories as $roomCategory )
        <tr>
            <td class="text-center">{!! $roomCategory->id !!}</td>
            <td class="text-center no-padding">

                <div class="gallery gallery-widget" data-toggle="lightbox-gallery">
                    <div class="row">
                        <div class="dz-preview dz-image-preview text-center" style="padding: 2px;">
                            <a href="{!! $roomCategory->image !!}" class="gallery-link dz-image border-gray" title="Immagine">
                                <img src="{!! $roomCategory->image !!}" alt="image" style="height: 70px; max-width: 120px;">
                            </a>
                        </div>
                    </div>
                </div>

                {{-- <img src="{!! $roomCategory->image !!}" height="50"> --}}

            </td>
            <td>{!! $roomCategory->name !!}</td>
            {{-- <td>{!! $roomCategory->description !!}</td> --}}
            <td>{!! $roomCategory->location->name !!}</td>
            {{-- <td>{!! $roomCategory->status->name !!}</td> --}}

            @include('utils.language.add_table_tbody', [
                'translations' => $roomCategory->translations,
                'id' => $roomCategory->id,
                'route' => 'admin.roomCategories.edit'
            ])

            <td class="text-center">
                {!! Form::open(['route' => ['admin.roomCategories.destroy', $roomCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{-- <a href="{!! route('admin.roomCategories.edit', [$roomCategory->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-edit"></i></a> --}}
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push( 'css' )
    <style type="text/css">
        .no-padding {
            padding: 0 !important;
        }
    </style>
@endpush

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
