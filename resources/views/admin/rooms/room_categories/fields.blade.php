<div id="room-categories">
    <room-category-form
        data="{{ isset($roomCategory) ? json_encode($roomCategory) : null }}"
        name="{{ isset($roomCategory) ?  $roomCategory->itemByLanguage($translation->code)->name : '' }}"
        description="{{ isset($roomCategory) ? $roomCategory->itemByLanguage($translation->code)->description : '' }}"
        :room-locations="{{ $roomLocations }}"
        :services="{{ $services }}"
        :translation="{{ json_encode($translation) }}"
    />
</div>
