<room-form
        data="{{ isset($room) ? json_encode($room) : null }}"
        :translation="{{ json_encode($translation) }}"
        :room-categories="{{ $roomCategories }}"
/>