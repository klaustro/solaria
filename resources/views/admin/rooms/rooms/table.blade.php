<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="rooms-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }} </th>
            <th>{{ tags( 'general_name' ) }} {{tags( 'back_rooms_room_title' )}}</th>
            {{-- <th>{{ tags( 'general_description' ) }}</th> --}}
            <th>{{ tags( 'back_rooms_room_category_title' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            @include('utils.language.add_table_th')

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $rooms as $room )
        <tr>
            <td class="text-center">{!! $room->id !!}</td>
            <td>{!! $room->name !!}</td>
            {{-- <td>{!! $room->description !!}</td> --}}
            <td>{!! $room->roomCategory->name !!}</td>
            {{-- <td>{!! $room->status->name !!}</td> --}}

            @include('utils.language.add_table_tbody', [
                'translations' => $room->translations,
                'id' => $room->id,
                'route' => 'admin.rooms.edit'
            ])

            <td class="text-center">
                {!! Form::open(['route' => ['admin.rooms.destroy', $room->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {{-- <a href="{!! route('admin.rooms.edit', [$room->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-edit"></i></a> --}}
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush

