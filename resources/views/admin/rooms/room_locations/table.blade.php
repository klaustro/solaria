<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="roomLocations-table">
    <thead>
        <tr>
            <th class="text-left text-center" style="padding-left: 2px">{{ tags( 'general_id' ) }}</th>
            <th class="text-left" style="padding-left: 2px">{{ tags( 'back_rooms_room_location_title' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            {{-- @include('utils.language.add_table_th') --}}

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $roomLocations as $roomLocation )
        <tr>
            <td class="text-center">{!! $roomLocation->id !!}</td>
            <td>{!! $roomLocation->name !!}</td>
            {{-- <td>{!! $roomLocation->status->name !!}</td> --}}

            {{-- @include('utils.language.add_table_tbody', [
                'translations' => $roomLocation->translations,
                'id' => $roomLocation->id,
                'route' => 'admin.roomLocations.edit'
            ]) --}}

            <td class="text-center">
                {!! Form::open(['route' => ['admin.roomLocations.destroy', $roomLocation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.roomLocations.edit', [$roomLocation->id]) !!}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
