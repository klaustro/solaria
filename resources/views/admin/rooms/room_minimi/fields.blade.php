@if( @$room )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.roomPrezzi.update', ['rooms'=>$room->id]) }}" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="form-model" name="form-model" class="form-model" method="POST" action="{{ route('admin.roomPrezzi.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
@endif

    @csrf

    <!-- RoomCategory Field -->
    <div class="form-group {{$errors->has('room_category_id') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('room_category_id', tags( 'back_rooms_room_category_title' )) !!}
        {!! Form::label('room_category_id', $room->roomCategory->name,['class' => 'form-control']) !!}
        {!! Form::hidden('room_category_id', $room->roomCategory->id) !!}
        {{-- {!! Form::select('room_category_id', $roomCategories,  $room->room_category_id, ['class' => 'form-control', 'placeholder' => tags( 'general_select' )]) !!} --}}
        @if ($errors->has('room_category_id'))
            <span class="help-block">{{ $errors->first('room_category_id') }}</span>
        @endif
        <span id="error-room_category_id" class="help-block" style="color:red"></span>
    </div>

    <!-- Name Field -->
    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('name', tags( 'general_name' )) !!}

        @if(@$room)
            {!! Form::label('name', $room->name, ['class' => 'form-control']) !!}
        @else
            {!! Form::label('name', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
        <span id="error-name" class="help-block" style="color:red"></span>
    </div>

    <!-- Price Field -->
    <div class="form-group {{$errors->has( 'price' ) ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'price', tags( 'back_rates_price' ) . ':' ) !!}
        {!! Form::number( 'price', @$room->price, [ 'class' => 'form-control', 'min' => '0.00', 'step' => '0.01' ] ) !!}
        @if ( $errors->has( 'price' ) )
            <span class="help-block">{{ $errors->first( 'price' ) }}</span>
        @endif
    </div>

    <!-- Iva Field -->
    <div class="form-group {{ $errors->has( 'iva' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'iva', tags( 'general_tax' ) . ':' ) !!}
        {!! Form::number( 'iva', @$room->iva, [ 'class' => 'form-control iva_id', 'min' => '0.00', 'step' => '0.01' ] ) !!}
        @if ( $errors->has( 'price' ) )
            <span class="help-block">{{ $errors->first( 'price' ) }}</span>
        @endif
    </div>
</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model' ]) !!}
    <a href="{!! route('admin.roomPrezzi.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@prepend( 'css' )
<style type="text/css">
.multiselect__spinner:before{position:absolute;
  border-color:rgb(73, 73, 73) !important;
}
.multiselect__tag{position:relative;
  background:rgb(73, 73, 73) !important;
}
.multiselect__option--highlight{
  background:rgb(73, 73, 73) !important;
}
.multiselect__option--highlight:after{content:attr(data-select);
  background:rgb(73, 73, 73) !important;
}
.multiselect__tag-icon:hover{
  background: #888888 !important;
}
.multiselect__tag-icon{
  color: #888888 !important;
}
.multiselect__tag-icon:after {
    color: #d4d4d4 !important;
}
</style>
@endprepend
