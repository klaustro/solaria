<div class="table-responsive" style="margin-bottom: 30px;">
    <table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="rooms-table">
        <thead>
            <tr>
                <th class="text-center">{{ tags( 'general_id' ) }} </th>
                <th>{{ tags( 'general_name' ) }} {{tags( 'back_rooms_room_title' )}}</th>
                <th>{{ tags( 'back_rooms_room_category_title' ) }}</th>
                <th class="text-center">{{ tags( 'back_rooms_room_season_price' ) }}</th>
                <th class="text-center">{{ tags( 'back_rooms_room_season_iva' ) }}</th>
                <th class="text-center">{{ tags( 'general_action' ) }}</th>
            </tr>
        </thead>
        <tbody class="widget-extra-full">
        @foreach ( $rooms as $room )
            <tr>
                <td class="text-center">{!! $room->id !!}</td>
                <td>{!! $room->name !!}</td>
                <td>{!! $room->roomCategory->name !!}</td>
                <td class="text-right">{!! number_format($room->price,2,',',' ') !!}</td>
                <td class="text-right">{!! number_format($room->iva,2,',',' ') !!}</td>
                <td class="text-center">
                    <div class='btn-group'>
                        <a href="{!! route('admin.roomPrezzi.edit', [$room->id]) !!}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush

