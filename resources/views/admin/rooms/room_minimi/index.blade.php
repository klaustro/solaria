@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_rooms_room_minimi_title' ),
        'subtitle'      => tags( 'general_index' ),
        'icon'          =>   'fa fa-cogs',
        'breadcrumb'    => array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_rooms_room_minimi_title' ),
            )
        )
    ] )

    @include('admin.rooms.room_minimi.table')
@endsection
