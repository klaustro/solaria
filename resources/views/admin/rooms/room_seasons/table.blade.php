<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="roomSeasons-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'back_rooms_room_title' ) }}</th>
            <th>{{ tags( 'back_rooms_room_season_start_date' ) }}</th>
            <th>{{ tags( 'back_rooms_room_season_end_date' ) }}</th>
            <th>{{ tags( 'back_rooms_room_season_price' ) }}</th>
            <th>{{ tags( 'back_rooms_room_season_iva' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $roomSeasons as $roomSeason )
        <tr>
            <td class="text-center">{!! $roomSeason->id !!}</td>
            <td>{!! $roomSeason->room_name !!}</td>
            <td>{!! date_format($roomSeason->start_date, 'd/m/Y') !!}</td>
            <td>{!! date_format($roomSeason->end_date, 'd/m/Y') !!}</td>
            <td>{!! number_format($roomSeason->price, 2, ',', '' ) !!}</td>
            <td>{!! number_format($roomSeason->iva, 2, ',', '' ) !!}</td>
            {{-- <td>{!! $roomSeason->status->name !!}</td> --}}

            <td class="text-center">
                {!! Form::open(['route' => ['admin.roomSeasons.destroy', $roomSeason->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{{ route('admin.roomSeasons.edit', $roomSeason->id) }}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                </div>
                <div class='btn-group'>
                    {{-- <a href="{!! route('admin.roomSeasons.edit', [$roomSeason->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-edit"></i></a> --}}
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
