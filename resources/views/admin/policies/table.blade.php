<div class="table-responsive" style="margin-bottom: 30px;">
    <table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="rates-table">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>{{ tags( 'general_name' ) }}</th>

                    @include('utils.language.add_table_th')

                <th class="text-center">{{ tags( 'general_action' ) }}</th>
            </tr>
        </thead>
        <tbody class="widget-extra-full">
            @foreach($policies as $policy)
                <tr>
                    <td class="text-center">{!! $policy->id !!}</td>
                    <td>{!! $policy->name !!}</td>

                    @include('utils.language.add_table_tbody', [
                            'translations' => $policy->policyTranslations,
                            'id' => $policy->id,
                            'route' => 'admin.policies.edit'
                        ])

                    <td class="text-center">
                        {!! Form::open(['route' => ['admin.policies.destroy', $policy->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@push( 'css' )
    <style type="text/css">
        .label-primary {
            display: inline-block;
            padding: 2px;
        }
    </style>
@endpush

{{--@push('scripts')
     Para inicializar el datatable
    <script>
        $(function(){
            TablesDatatables.productsInit();
        });
    </script>
@endpush --}}
