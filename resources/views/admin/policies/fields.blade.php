<div>
    @if( @$policy )
        <form id="form-model" name="form-model" method="POST" action="{{ route('admin.policies.update', ['policy'=>$policy->id]) }}" accept-charset="UTF-8" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PATCH">
    @else
        <form id="form-model" name="form-model" method="POST" action="{{ route('admin.policies.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
    @endif
        @csrf
            <!-- Language Id Field -->
            <div class="form-group col-sm-6 col-sm-offset-3 text-center">
                <h1>{!! tags( 'general_language' ).': <b>'.$translation->name.'</b>' !!}</h1>
                {!! Form::hidden('language_id', $translation->id) !!}
            </div>

            <!-- Name Field -->
            <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
                {!! Form::label('name', tags( 'general_name' )) !!}
                {!! Form::text('name', isset($policy) ? $policy->policyTranslation($translation->id)->name ?? '' : '', ['class' => 'form-control']) !!}
                @if ($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <!-- Description Field -->
            <div class="form-group {{$errors->has('description') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
                {!! Form::label('description', tags( 'general_description' )) !!}
                {!! Form::textarea('description', isset($policy) ? $policy->policyTranslation($translation->id)->description ?? '' : '', ['class' => 'form-control ckeditor']) !!}
                @if ($errors->has('description'))
                    <span class="help-block">{{ $errors->first('description') }}</span>
                @endif
            </div>
        </form>
</div>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
       {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model' ]) !!}
        <a href="{!! route('admin.policies.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
    </div>