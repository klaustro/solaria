<div class="col-sm-6 col-sm-offset-3">
    @if(isset($single_select))
        @if(!empty(@$form))
            <label>Sostituisci {{ tags( 'general_image' ) }}</label>
            {{-- <label>{{ tags( 'general_image' ) }}</label> --}}
        @else
            <label>{{ tags( 'general_image' ) }} @if ( $quantity ?? null ) <small>(quantità {{ $quantity }})</small> @endif</label>
        @endif
    @else
        <label>{{ tags( 'general_image' ) }}</label>
    @endif
    <hr>
</div>

{{-- IMAGENES YA CARGADAS --}}
{{-- <div class="row">
    @if ( isset( $model->gallery ) )
        <div class="col-sm-6 col-sm-offset-3">
            @foreach ( $model->gallery as $image )
                <div class="dz-preview dz-image-preview" id="image-{{$image['id']}}">

                    <!-- imagen -->
                    <div class="dz-image">
                        <img src="{{ $image['url']}}" class="dz-image"  alt> <!--  width="120" height="120" -->
                    </div>

                    <!-- boton de eliminar -->
                    <button class="btn btn-xs btn-danger" onclick="deleteImage( {{$model->id}}, {{$image['id']}} )" title="Drop image">
                        <i class="fa fa-trash"></i>
                    </button>

                    <!-- radio de seleccionar principal -->
                    <button class="btn btn-xs btn-danger" onclick="deleteImage( {{$model->id}}, {{$image['id']}} )" title="Drop image">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
            @endforeach
        </div>
    @endif

    <div class="previews">
    </div>
</div> --}}

{{-- PREVISUALIZADOR DE DROPZONE --}}
<div class="form-group {{$errors->has('images') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
    <div class="#previews" id="showhere"
        style='
            width:100%;
            min-height:150px;
            text-align:center;
            background-color: #f9f9f9;
            border-color: #f2f2f2;
            position: relative;
            border: 2px dashed #eaedf1;
            background-color: #f9fafc;
            padding: 10px;
        '>

        <div class="dz-default dz-message">
            <span>Rilascia qui i file da caricare.</span>
        </div>
    </div>
<span id="error-file" class="help-block" style="color:red"></span>
<span id="error-immagine" class="help-block" style="color:red"></span>
</div>
{{-- IMAGENES YA CARGADAS --}}
@if ( isset( $model->gallery ) )
    <div id="gallery" class="col-sm-6 col-sm-offset-3">
        <div class="gallery gallery-widget" data-toggle="lightbox-gallery">
            <div class="row">
                @foreach ( $model->gallery as $image )
                    <div class="dz-preview dz-image-preview text-center">
                        <a href="{{ $image['url'] }}" class="gallery-link dz-image border-gray" id="image-{{$image['id']}}" title="Immagine">
                            <img src="{{ $image['url'] }}" alt="image" class="min-custom">
                        </a>

                        @if (isset($single_select))
                            <!-- Boton de Eliminar Single -->
                            <!-- <button id="btn-remove" type="button" class="btn btn-xs btn-danger" onclick="deleteSingleImage()" title="Drop image">
                                <i class="fa fa-trash"></i> Rimuovere
                            </button> -->
                        @elseif (!isset($single_select))
                            <!-- Boton de Eliminar Multi -->
                            <button class="btn btn-xs btn-danger" onclick="deleteImage( {{$model->id}}, {{$image['id']}} )" title="Drop image">
                                <i class="fa fa-trash"></i>
                            </button>


                            @if ( @$model->image )
                                <!-- radio de seleccionar principal -->
                                <input type="radio" name="image" value="{{ $image['url'] }}" <?= ( $model->image === $image['url'] ) ? 'checked' : '' ?> />
                            @endif
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif

@push( 'css' )
<style type="text/css">
    .dropzone .dz-preview:hover .dz-image img {
         -webkit-filter: blur(0px);
         filter: blur(0px);
    }
    .border-gray {
        border: 1px solid #888888;
    }
    .min-custom {
        min-height: 100%;
    }
</style>
@endpush

@push( 'scripts' )
    <script>
        $( document ).ready( function () {
            $( document ).on( 'submit', 'form#myDropzone', function ( e ) {
                e.preventDefault();
            })
        });

        Dropzone.options.myDropzone = {
            autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads:10,
            maxFilezise: 10,
            maxFiles: {{ isset($single_select) ? 1 : 100 }},
            previewsContainer: '.previews',
            addRemoveLinks: true,
            previewsContainer: "#showhere",
            clickable: "#showhere",
            chunking: true,
            //Translates
            ictCancelUpload: 'Cancel il fil',
            dictCancelUploadConfirmation: 'asd',
            dictRemoveFile: 'Rimuovi il file',
            capture: 'camera',
            dictDefaultMessage: 'Rilascia qui i file da caricare.',

            accept: function ( file, done ) {

                var mimes = [
                    'image/gif',
                    'image/png',
                    'image/jpeg',
                    'image/bmp',
                    'image/webp',
                    'image/vnd.microsoft.icon',
                    'image/svg+xml'
                ];

                if ( mimes.indexOf( file.type ) == -1 ) {
                    done( "Formato non consentito." );
                }
                else {
                    done();
                }
            },


            init: function() {
                var submitBtn = document.querySelector("#submit");
                var submitBtnSame = $( '#submit' );
                myDropzone = this;
                var creating = window.location.pathname.includes("create")
                var url = ({!! json_encode($url) !!})

                submitBtn.addEventListener("click", function(e){

                    // iniciar modo Loading...
                    NProgress.start();
                    submitBtnSame.button( 'loading' );

                    if (creating) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    if (myDropzone.getQueuedFiles().length > 0) {
                        myDropzone.processQueue();
                    } else {
                        var blob = new Blob();
                        blob.upload = { 'chunked': this.chunking };
                        myDropzone.uploadFile(blob);
                    }

                    //myDropzone.processQueue();
                });
                this.on("addedfile", function(file) {

                });

                this.on("complete", function(file) {
                    // detener modo Loading...
                    $('html, body').animate( { scrollTop: 0 }, 1000 );
                    NProgress.done();
                    submitBtnSame.button( 'reset' );

                    myDropzone.removeFile(file);
                });

                this.on("success", function(file) {
                    $('.help-block').empty()
                    window.location.href = url + '?edited=1'
                });

                this.on("error", function(error){
                    $('.help-block').empty()
                    let response = JSON.parse(error.xhr.response)
                    for(let i in response.errors){
                        $('#error-'+i).html(response.errors[i])
                    }

                });
            }
        };

        function deleteSingleImage() {
            if (confirm("Vuoi far cadere questa immagine?")) {
                $("#gallery").remove();
            }
        }

        function deleteImage( model_id, multimedia_id ){
            event.preventDefault();

            var r = confirm("Vuoi far cadere questa immagine?");
            if (r == true) {
                var url = ({!! json_encode($url) !!})
                $.ajax({
                    url: '/api' + url + '/' + model_id + '/' + multimedia_id,
                    type: 'DELETE',
                    dataType: 'json',
                })
                .done(function(data) {
                    $('#image-' + multimedia_id).html('<img src="/images/image-deleted.png" width="120" alt>')
                })
                .fail(function(error) {
                    console.log(error)
                });
            }
        }
    </script>
@endpush

@push( 'css' )
<style type="text/css">
    #myDropzone {
        background-color: #fff;
        border: none;
    }
</style>
@endpush
