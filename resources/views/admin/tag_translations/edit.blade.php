@extends('layouts.admin.app')

@section('content')
   @include('layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags('general_tagsTranslation'),
       'subtitle'      =>   tags('general_edit'),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   [
                               [
                                   'title' => tags('general_home'),
                                   'route' => 'home'
                               ],
                               [
                                   'title' => tags('general_tagsTranslation'),
                                   'route' => 'admin.tagTranslations.index'
                               ],
                               [
                                   'title' => tags('general_edit'),
                               ]
                            ]
   ])

   <div class="block full">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row" >
                   {!! Form::model($tagTranslation, ['route' => ['admin.tagTranslations.update', $tagTranslation->id], 'method' => 'patch', 'files' => true ]) !!}

                        @include('admin.tag_translations.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection