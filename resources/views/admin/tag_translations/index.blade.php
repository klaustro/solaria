@extends('layouts.admin.app')

@section('content')
@include('layouts.admin.partials.dashboard-header-top', [
    'title'         => tags('general_tagsTranslation'),
    'subtitle'      => tags('general_index'),
    'icon'          =>   'fa fa-cogs',
    'breadcrumb'    => [
        [
            'title' => tags('general_home'),
            'route' => 'home'
        ],
        [
            'title' => tags('general_tagsTranslation'),
        ]
    ]
])

<div class="row">
    <div class="widget">
        @include('admin.tag_translations.table')
    </div>
</div>
@endsection


