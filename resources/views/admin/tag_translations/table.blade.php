<div class="table-responsive" style="margin-bottom: 30px;">
    <table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="products-table">
        <thead>
            <tr>
                <th>#</th>
                <th class="text-center">{{ tags('general_tags') }}</th>
                <th class="text-center">{{ tags('general_value') }}</th>
                <th class="text-center">{{ tags('general_language') }}</th>
                <th class="text-center">{{ tags('general_action') }}</th>
            </tr>
        </thead>
        <tbody class="widget-extra">
            @foreach($tagTranslations as $tagTranslation)
                <tr>
                    <td class="text-center">{!! $tagTranslation['id'] !!}</td>
                    <td>{!! $tagTranslation['tag'] !!}</td>
                    <td>{!! $tagTranslation['value'] !!}</td>
                    {{-- {{ dd($tagTranslation->language->name) }} --}}
                    <td>{!! $tagTranslation->language->name !!}</td>


                    {{--
                        {{ dd($tagTranslation) }}
                        @include('admin.utils.language.add_table_tbody', [
                            'translations' => $tagTranslation['language_id'],
                            'id' => $tagTranslation['id'],
                            'route' => 'admin.tagTranslations.edit'
                        ])
                    --}}

                    <td class="text-center">
                        {!! Form::open(['route' => ['admin.tagTranslations.destroy', $tagTranslation['id']], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                <a href="{!! route('admin.tagTranslations.edit', [$tagTranslation['id']]) !!}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                            </div>
                        {!! Form::close() !!}

                    {{--
                        <td class="text-center">
                        {!! Form::open(['route' => ['admin.tagTranslations.destroy', $tagTranslation['id']], 'method' => 'delete']) !!}
                            <div class='btn-group'>
                                {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                            </div>
                        {!! Form::close() !!}
                    --}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function(){
            TablesDatatables.productsInit();
        });
    </script>
@endpush
