<!-- Language Id Field -->
<div class="form-group text-center col-sm-6 col-sm-offset-3">
    <h1>{!! tags('general_language').': <b>'.@$tagTranslation->language->name.'</b>' !!}</h1>

</div>

<!-- Tag Field -->
<div class="form-group {{$errors->has('tag') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
    {!! Form::label('tag', tags('general_tags')) !!}
    {!! Form::text('tag', isset($tagTranslation) ? $tagTranslation->tag ?? '' : '', ['class' => 'form-control','readonly']) !!}
    @if ($errors->has('tag'))
        <span class="help-block">{{ $errors->first('tag') }}</span>
    @endif
</div>

<!-- Value Field -->
<div class="form-group {{$errors->has('value') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
    {!! Form::label('value', tags('general_value')) !!}
    {!! Form::textarea('value', isset($tagTranslation) ? $tagTranslation->value ?? '' : '', ['class' => 'form-control']) !!}
    @if ($errors->has('value'))
        <span class="help-block">{{ $errors->first('value') }}</span>
    @endif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-6 col-sm-offset-3">
    {!! Form::submit(tags('general_save'), ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.tagTranslations.index') !!}" class="btn btn-default">{{ tags('general_cancel') }}</a>
</div>