<div class="table-responsive" style="margin-bottom: 30px;">
    <table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="rates-table">
        <thead>
            <tr>
                <th class="text-center">
                <th>{{ tags( 'general_days' ) }}</th>
                <th>{{ tags( 'general_name' ) }}</th>
                    @include('utils.language.add_table_th')

                <th class="text-center">{{ tags( 'general_action' ) }}</th>
            </tr>
        </thead>
        <tbody class="widget-extra-full">
        @foreach($packages as $package)
            <tr>
                <td class="text-center">{!! $package->id !!}</td>
                <td>
                    @foreach( $package->date_array as $day )
                        <span class="label label-primary">{{ $day[ 'label' ] }}</span>
                    @endforeach
                </td>
                <td>{!! $package->name !!}</td>

                @include('utils.language.add_table_tbody', [
                    'translations' => $package->translations,
                    'id' => $package->id,
                    'route' => 'admin.packages.edit'
                ])

                <td class="text-center">
                    {!! Form::open(['route' => ['admin.packages.destroy', $package->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@push( 'css' )
    <style type="text/css">
        .label-primary {
            display: inline-block;
            padding: 2px;
        }
    </style>
@endpush

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            // TablesDatatables.tableGeneral();
        });
    </script>
@endpush
