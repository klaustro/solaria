<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $package->id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $package->date !!}</p>
</div>

<!-- Min Nights Field -->
<div class="form-group">
    {!! Form::label('min_nights', 'Min Nights:') !!}
    <p>{!! $package->min_nights !!}</p>
</div>

<!-- Max Nights Field -->
<div class="form-group">
    {!! Form::label('max_nights', 'Max Nights:') !!}
    <p>{!! $package->max_nights !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $package->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $package->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $package->deleted_at !!}</p>
</div>

