@extends('layouts.admin.app')

@section('content')
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_packages_title' ),
       'subtitle'      =>   tags( 'general_edit' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_packages_title' ),
                'route' => 'admin.packages.index'
            ),
            array (
                'title' => tags( 'general_edit' ),
            )
        )
    ] )

    <div class="block full">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                     @include('admin.packages.fields')
                </div>
            </div>
        </div>
    </div>
@endsection