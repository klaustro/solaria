<div id="app">
    @if( @$package )
        <form id="form-model" name="form-model" method="POST" action="{{ route('admin.packages.update', ['packages'=>$package->id]) }}" accept-charset="UTF-8">
        <input name="_method" type="hidden" value="PATCH">
        <input type="hidden" ref="packageId" value="{{ @$package->id }}">
    @else
        <form id="form-model" name="form-model" method="POST" action="{{ route('admin.packages.store') }}" accept-charset="UTF-8">
    @endif

        @csrf

        <!-- Language Id Field -->
        <div class="form-group col-sm-6 col-sm-offset-3 text-center">
            <h1>{!! tags( 'general_language' ).': <b>'.$translation->name.'</b>' !!}</h1>
            {!! Form::hidden('language_id', $translation->id) !!}
        </div>

        <!-- Name Field -->
        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
            {!! Form::label('name', tags( 'general_name' )) !!}

            @if(@$package)
                {!! Form::text('name', $package->itemByLanguage($translation->code)->name ?? '', ['class' => 'form-control']) !!}
            @else
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @endif

            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>

        <!-- Description Field -->
        <div class="form-group {{$errors->has('description') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
            {!! Form::label('description', tags( 'general_description' )) !!}

            @if(@$package)
                {!! Form::textarea('description', @$package->itemByLanguage($translation->code)->description, ['class' => 'form-control']) !!}
            @else
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            @endif

            @if ($errors->has('description'))
                <span class="help-block">{{ $errors->first('description') }}</span>
            @endif
        </div>

        <!-- Date Field -->
        <div class="form-group col-sm-6 col-sm-offset-3">
            <label for="services">{{ tags( 'general_days' ) }}</label>
            <vue-multiselect
                v-model="packageDays"
                :options="days"
                :multiple="true"
                :clear-on-select="false"
                :hide-selected="true"
                :searchable="true"
                placeholder="{{ tags( 'general_multiple_select' ) }}"
                select-label="{{ tags( 'general_enter_to_select' ) }}"
                label="label"
                track-by="label"
                :taggable="true"
            ></vue-multiselect>
            <input type="hidden" name="date" :value="packageIds">
        </div>
        <!-- Max Nights Field -->
        <div class="form-group col-sm-6 col-sm-offset-3">
            {!! Form::checkbox('notificate', 'value', false) !!}
            {!! Form::label('max_nights', tags( 'back_packages_notify' )) !!}
        </div>

        <!-- Submit Field -->
        <div class="col-sm-6 col-sm-offset-3">
            {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model' ]) !!}
            <a href="{!! route('admin.packages.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
        </div>

    </form>
</div>
@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
    <script>
    Vue.component('vue-multiselect', window.VueMultiselect.default)
    var app = new Vue({
        el: '#app',
        data: {
            packageDays: [],
            days: [
                {
                    value: 0,
                    label:  'domenica'
                },
                {
                    value: 1,
                    label:  'lunedi'
                },
                {
                    value: 2,
                    label:  'martedì'
                },
                {
                    value: 3,
                    label:  'mercoledì'
                },
                {
                    value: 4,
                    label:  'giovedi'
                },
                {
                    value: 5,
                    label:  'venerdì'
                },
                {
                    value: 6,
                    label:  'sabato'
                }
            ],
        },
        mounted(){
            this.getRoomServices()
        },
        methods: {
            getRoomServices(){
                if (this.packageId) {
                    axios.get('/api/admin/packages/' + this.packageId)
                        .then(response => {
                            let daysSelected = response.data.data.date_array;

                            this.packageDays = daysSelected;

                        })
                        .catch(error => console.log(error));
                }
            }
        },
        computed: {
            packageIds(){
                if (!this.packageDays) {
                    return []
                }
                let ids = []
                for(let i in this.packageDays){
                    ids.push(this.packageDays[i].value)
                }
                return ids
            },
            packageId(){
                return this.$refs.packageId ? this.$refs.packageId.value : null
            }
        }
    });
    </script>
@endpush
@prepend( 'css' )
<style type="text/css">
.multiselect__spinner:before{position:absolute;
  border-color:rgb(73, 73, 73) !important;
}
.multiselect__tag{position:relative;
  background:rgb(73, 73, 73) !important;
}
.multiselect__option--highlight{
  background:rgb(73, 73, 73) !important;
}
.multiselect__option--highlight:after{content:attr(data-select);
  background:rgb(73, 73, 73) !important;
}
.multiselect__tag-icon:hover{
  background: #888888 !important;
}
.multiselect__tag-icon{
  color: #888888 !important;
}
.multiselect__tag-icon:after {
    color: #d4d4d4 !important;
}
</style>
@endprepend