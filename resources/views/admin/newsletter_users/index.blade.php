@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_module_utils_newsletter_title' ),
        'subtitle'      => tags( 'general_index' ),
        'icon'          =>   'fa fa-cogs',
        'breadcrumb'    => array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_module_utils_newsletter_title' ),
            )
        )
    ] )

    @include('admin.newsletter_users.table')
@endsection

