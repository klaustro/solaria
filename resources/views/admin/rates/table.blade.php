<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="rates-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'back_rooms_room_title' ) }}</th>
            <th>{{ tags( 'general_date' ) }}</th>
            <th>{{ tags( 'general_email' ) }}</th>
            {{-- <th>{{ tags( 'back_packages_title' ) }}</th> --}}
            <th>{{ tags( 'general_discount' ) }}</th>
            <th>{{ tags( 'back_rates_price' ) }}</th>
            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $rates as $rate )
        <tr>
            <td class="text-center">{!! $rate->id!!}</td>
            <td>{!! $rate->room_name !!}</td>

            <td>{{ $rate->start_date_italian }} a {{ $rate->end_date_italian }}</td>

            <td>{!! $rate->email !!}</td>
            {{-- <td>{!! $rate->package_name !!}</td> --}}
            <td>{!! number_format( $rate->discount, 2, ',', '' ) !!}</td>
            <td>{!! number_format( $rate->price_total, 2, ',', '' ) !!}</td>
            {{-- <td>{!! $rate->status->name !!}</td> --}}
            <td class="text-center">
                {!! Form::open( [ 'route' => [ 'admin.rates.destroy', $rate->id ], 'method' => 'delete' ] ) !!}

                <!-- RESERVAR -->
                <div class='btn-group'>
                    <a class='btn btn-success btn-xs'
                        onclick="event.preventDefault(); document.getElementById( 'form-search-{{ $rate->id }}' ).submit();">
                        <i class="fa fa-car"></i>
                    </a>
                </div>

                <!-- EDITAR -->
                <div class='btn-group'>
                    <a href="{{ route('admin.rates.edit', $rate->id) }}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                </div>

                <!-- ELIMINAR -->
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}

                <form id="form-search-{{ $rate->id }}" method="get" action="{{ route('admin.bookings.searchRoom') }}" accept-charset="UTF-8">
                    <input type="hidden" name="checkin" value="{{ $rate->start_date_italian }}">
                    <input type="hidden" name="checkout" value="{{ $rate->end_date_italian }}">
                    <input type="hidden" name="email" value="{{ $rate->email }}">
                    <input type="hidden" name="rate_room_id" value="{{ $rate->room_id }}">
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
