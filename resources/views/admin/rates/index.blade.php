@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_rates_title' ),
        'subtitle'      => tags( 'general_index' ),
        'button'        => tags( 'general_addnew' ),
        'route'         => 'admin.rates.create',
        'breadcrumb'    => [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_rates_title' ),
            ]
        ]
    ] )
        <a title="Visualizza Tavolo" class="btn btn-primary btn-xs {{ isset(request()->calendar) ? '' : 'active'}}" href="{{route('admin.rates.index')}}"><i class="fa fa-th-large" aria-hidden="true"></i></a>
        <a title="Visualizza Calendario" class="btn btn-primary btn-xs {{ isset(request()->calendar) ? 'active' : ''}}" href="{{route('admin.rates.index', ['calendar' => 1])}}"><i class="fa fa-calendar" aria-hidden="true"></i></a>
        <br>
        <br>
    @if(request()->calendar == 1)
        @include( 'admin.rooms.calendar' )
    @else
        @include('admin.rates.table')
    @endif
@endsection