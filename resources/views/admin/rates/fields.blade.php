{{-- @include( 'admin.rates.searchForm' ) --}}
    <div class="row">
        <!-- Campos de rango de fecha -->
        <div class="form-group col-sm-6 col-sm-offset-3">
            <label class="control-label" for="start_date">Data dell'offerta</label>
             <input type="text" readonly class="form-control text-center" value="{{ @$start_date }} - {{ @$end_date }}" />
        </div>
        <!-- Cancel Button -->
        <div class="col-sm-6 col-sm-offset-3">
            <a href="{!! route('admin.rates.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
        </div>
    </div>

<hr>

@if( @$rate )

    <form id="form-model" name="form-model" method="POST" action="{{ route( 'admin.rates.update', [ 'rates'=>$rate->id] ) }}" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="PATCH">

@else

    <form id="form-model" name="form-model" method="POST" action="{{ route( 'admin.rates.store' ) }}" accept-charset="UTF-8">

@endif

    @csrf

    <input type="hidden" name="start_date" value="{{ @$start_date }}">
    <input type="hidden" name="end_date" value="{{ @$end_date }}">

    @if ( $errors->has( 'rooms' ) )
        <div class="has-error">
            <span class="help-block">{{ $errors->first( 'rooms' ) }}</span>
        </div>
    @endif

    <div class="row">
        <table id="general-table" class="table table-vcenter table-condensed table-bordered">{{-- table-hover --}}
            <thead>
                <tr>
                    <th style="width: 80px;" class="text-center">
                        {{-- <input type="checkbox" /> --}}
                    </th>
                    <th>{{ tags( 'back_rooms_room1_title' ) }}</th>
                    <th>Prezzo totale</th>
                    <th style="width: 150px;" class="text-center">{{ tags( 'general_discount' ) }}</th>
                    <th class="text-center">{{ tags( 'general_resulting_price' ) }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ( $rooms as $room )
                    @if ( $room[ 'availability' ] === 1 &&  $room[ 'price_request' ] > 0 )
                        <tr>
                            <td class="text-center">
                                <input type="checkbox"
                                    name="rooms[{{ $room[ 'id' ] }}][selected]"
                                    {{ @array_key_exists( 'selected', old( 'rooms' )[ $room[ 'id' ] ] ) ? 'checked' : '' }}>
                            </td>
                            <td>
                                {{ $room[ 'name' ] }}
                            </td>
                            <td>
                                € <span id="price-{{ $room[ 'id' ] }}">{{ number_format( $room[ 'price_request' ], 2, ',', '' ) }}</span>
                            </td>
                            <td class="{{ $errors->has( 'rooms.' . $room[ 'id' ] . '.discount' ) ? 'has-error' : '' }}">
                                <input type="number"
                                    name="rooms[{{ $room[ 'id' ] }}][discount]"
                                    value="{{ @array_key_exists( 'selected', old( 'rooms' )[ $room[ 'id' ] ] ) ? old( 'rooms' )[ $room[ 'id' ] ][ 'discount' ] : '' }}"
                                    data-room="{{ $room[ 'id' ] }}"
                                    class="form-control input-discount"
                                    max="100"
                                    min="0"
                                    step="1"
                                    autocomplete="off" />
                            </td>
                            <td class="text-center">
                                <strong>€ <span id="resulting-price-{{ $room[ 'id' ] }}">{{ number_format( $room[ 'price_request' ], 2, ',', '' ) }}</span></strong>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>

    <hr>

    <div class="row">
        <!-- Email Field -->
        <div class="form-group {{ $errors->has( 'email' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
            <input type="radio"
                name="emailOrPost"
                value="email"
                id="emailOrPost-email"
                {{ old( 'emailOrPost' ) === 'email' ? 'checked' : '' }} />
            {!! Form::label( 'emailOrPost-email', tags( 'general_email' ) ) !!}

            <div class="input-email" style="display: none;">
                @if( @$rate )
                    {!! Form::text( 'email', $user->email ?? '', [ 'class' => 'form-control', 'placeholder' => 'Email' ] ) !!}
                @else
                    {!! Form::text( 'email', null, [ 'class' => 'form-control' , 'placeholder' => 'Email' ] ) !!}
                @endif

                @if ( $errors->has( 'email' ) )
                    <span class="help-block">{{ $errors->first( 'email' ) }}</span>
                @endif

                {!! Form::textarea( 'comment', @$rate->comment, [ 'class' => 'form-control', 'placeholder' => 'Commento' ] ) !!}
                @if ( $errors->has( 'comment' ) )
                    <span class="help-block">{{ $errors->first( 'comment' ) }}</span>
                @endif
            </div>
        </div>

        <!-- Post in Blog -->
        <div class="form-group {{ $errors->has( 'post' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
            <input type="radio"
                name="emailOrPost"
                value="post"
                id="emailOrPost-post"
                placeholder="E-mail"
                {{ old( 'emailOrPost' ) === 'post' ? 'checked' : '' }} />
            {!! Form::label( 'emailOrPost-post', tags( 'back_rates_blog' ) ) !!}

            <div class="input-post" style="display: none;">
                {!! Form::text( 'title', @$rate->title, [ 'class' => 'form-control', 'placeholder' => 'Titolo' ] ) !!}
                @if ( $errors->has( 'title' ) )
                    <span class="help-block">{{ $errors->first( 'title' ) }}</span>
                @endif

                {!! Form::text( 'subtitle', @$rate->subtitle, [ 'class' => 'form-control', 'placeholder' => 'Sototitolo' ] ) !!}
                @if ( $errors->has( 'subtitle' ) )
                    <span class="help-block">{{ $errors->first( 'subtitle' ) }}</span>
                @endif

                {!! Form::textarea( 'description', @$rate->description, [ 'class' => 'form-control', 'placeholder' => 'Descripzione' ] ) !!}
                @if ( $errors->has( 'description' ) )
                    <span class="help-block">{{ $errors->first( 'description' ) }}</span>
                @endif
            </div>

            @if ( $errors->has( 'emailOrPost' ) )
                <span class="help-block" style="color: #e74c3c;">{{ $errors->first( 'emailOrPost' ) }}</span>
            @endif
        </div>

    </div>
</form>

<div class="row">
    <!-- Submit Field -->
    <div class="col-sm-6 col-sm-offset-3">
        {!! Form::submit( tags( 'general_save' ), [ 'class' => 'btn btn-primary', 'form' => 'form-model' ] ) !!}
        <a href="{!! route( 'admin.rates.index' ) !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
    </div>
</div>

@push( 'scripts' )
    <script>
        /*
        $( '.datepicker' ).datepicker( {
            format: 'dd/mm/yyyy'
        } );
        */
    </script>

    <script>
        $( document ).ready( function () {
            var checkEmail = $( '#emailOrPost-email' )[ 0 ];
            var checkPost = $( '#emailOrPost-post' )[ 0 ];

            if ( checkEmail.checked ) {
                $( '.input-email' ).toggle();
            }

            if ( checkPost.checked ) {
                $( '.input-post' ).toggle();
            }
        } );

        $( document ).on( 'ifChanged', 'input[name=emailOrPost]', function() {
            if ( $( this ).val() === 'email' ) {
                $( '.input-email' ).toggle();
            }

            if ( $( this ).val() === 'post' ) {
                $( '.input-post' ).toggle();
            }
        } );
    </script>

    <script>
        // Get Rooms List
        var rooms = {!! json_encode( $rooms ) !!};

        $( document ).on( 'keyup change wheel', 'input.input-discount', function() {
            var roomId = $( this ).data( 'room' );
            // get values
            var discount = $( this ).val();
            // var price  = parseFloat( $( 'span#price-' + roomId ).text() );
            var price = rooms.find( item => item.id === roomId ).price_request || 0;

            // validation
            if ( discount > 100 ) {
                $( this ).val( 100 );
                discount = 100;
            }
            else if ( discount < 0 ) {
                $( this ).val( 0 );
                discount = 0;
            }

            // set resulting price
            var precentage = ( discount / 100 ) * price;
            var resultingPrice = price - precentage;

            // Display resulting price
            $( 'span#resulting-price-' + roomId ).text( parseFloat( resultingPrice ).toFixed( 2 ).toString().replace( '.', ',' ) );
        } );
    </script>
@endpush

@push( 'css' )
<style type="text/css">
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=number] { -moz-appearance:textfield; }
</style>
@endpush