@extends('layouts.admin.app')

@section('content')
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_rates_title' ),
       'subtitle'      =>   tags( 'general_edit' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_rates_title' ),
                'route' => 'admin.rates.index'
            ],
            [
                'title' => tags( 'general_edit' ),
            ]
        ]
    ] )

    <div class="block full">
        <div class="box box-primary">
            <div class="box-body">

                <form id="form-model" name="form-model" method="POST" action="{{ route( 'admin.rates.update', [ 'rates'=> $rate->id ] ) }}" accept-charset="UTF-8">
                    <div class="row">
                        <input name="_method" type="hidden" value="PATCH">
                        @csrf

                        <!-- Room Name Field -->
                        <div class="form-group {{ $errors->has( 'room_id' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
                            {!! Form::label( 'room_id', tags( 'back_rooms_room_title' ) ) !!}
                            <input class="form-control" type="text" name="room_name" value="{{ $rate->room->translation()->name }}" readonly="">
                            <input type="hidden" name="room_id" value="{{ $rate->room_id }}">
                            @if ( $errors->has( 'room_id' ) )
                                <span class="help-block">{{ $errors->first( 'room_id' ) }}</span>
                            @endif
                        </div>

                        <!-- Start Date Field -->
                        <div class="form-group {{ $errors->has( 'start_date' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
                            {!! Form::label( 'start_date', tags( 'general_datein' ).':' ) !!}
                            {!! Form::text( 'start_date', isset( $rate->start_date ) ? date_format(date_create( $rate->start_date ), 'd/m/Y' ) : '', [ 'class' => 'form-control', 'readonly' => 'true' ] ) !!}
                        </div>

                        <!-- End Date Field -->
                        <div class="form-group {{ $errors->has( 'end_date' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
                            {!! Form::label( 'end_date', tags( 'general_dateout' ) . ':' ) !!}
                            {!! Form::text( 'end_date', isset( $rate->end_date ) ? date_format(date_create( $rate->end_date ), 'd/m/Y' ) : '', [ 'class' => 'form-control', 'readonly' => 'true' ] ) !!}
                        </div>

                        <!-- Price Temp Field -->
                        <div class="form-group {{ $errors->has( 'price_temp' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
                            {!! Form::label( 'price_temp', 'Prezzo totale:' ) !!}
                            {!! Form::number( 'price_temp', @$rate->price_temp, [ 'class' => 'form-control', 'readonly' => 'true', 'min' => '0.00', 'step' => '0.01' ] ) !!}
                            @if ( $errors->has( 'price_temp' ) )
                                <span class="help-block">{{ $errors->first( 'price_temp' ) }}</span>
                            @endif
                        </div>

                        <!-- Discount Field -->
                        <div class="form-group {{ $errors->has( 'discount' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
                            {!! Form::label( 'discount', tags( 'general_discount' ) . ':' ) !!}
                            {!! Form::number( 'discount', @$rate->discount, [ 'class' => 'form-control', 'id' => 'discount', 'min' => '0.00', 'step' => '0.01' ] ) !!}
                            @if ( $errors->has( 'discount' ) )
                                <span class="help-block">{{ $errors->first( 'discount' ) }}</span>
                            @endif
                        </div>

                        <!-- Price Total Field -->
                        <div class="form-group {{ $errors->has( 'price_total' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
                            {!! Form::label( 'price_total', tags( 'back_rates_price' ) . ':' ) !!}
                            {!! Form::number( 'price_total', @$rate->price_total, [ 'class' => 'form-control', 'id' => 'price_total', 'readonly' => true, 'min' => '0.00', 'step' => '0.01' ] ) !!}
                            @if ( $errors->has( 'price_total' ) )
                                <span class="help-block">{{ $errors->first( 'price_total' ) }}</span>
                            @endif
                        </div>

                        <!-- Iva Field -->
                        <div class="form-group {{ $errors->has( 'iva' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
                            {!! Form::label( 'iva', tags( 'general_tax' ) . ':' ) !!}
                            {!! Form::number( 'iva', @$rate->iva, [ 'class' => 'form-control iva_id', 'min' => '0.00', 'step' => '0.01' ] ) !!}
                            @if ( $errors->has( 'iva' ) )
                                <span class="help-block">{{ $errors->first( 'iva' ) }}</span>
                            @endif
                        </div>

                        <!-- Comment Field -->
                        <div class="form-group {{ $errors->has( 'comment' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
                            {!! Form::label( 'comment', tags( 'booking_2_comment' ) . ':' ) !!}
                            {!! Form::textarea( 'comment', @$rate->comment, [ 'class' => 'form-control', 'readonly' => 'true','min' => '0.00', 'step' => '0.01' ] ) !!}
                            @if ( $errors->has( 'comment' ) )
                                <span class="help-block">{{ $errors->first( 'comment' ) }}</span>
                            @endif
                        </div>
                    </div>
                </form>

                <div class="row">
                    <!-- Submit Field -->
                    <div class="col-sm-6 col-sm-offset-3">
                        {!! Form::submit( tags( 'general_save' ), [ 'class' => 'btn btn-primary', 'form' => 'form-model' ] ) !!}
                        <a href="{!! route( 'admin.rates.index' ) !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    const rate = {!! json_encode($rate); !!}

    $( document ).on( 'keyup wheel', '#discount', function() {
        let discount = $(this).val();
        // validation
        if (discount > 100) {
            $(this).val(100);
            discount = 100;
        } else if (discount < 0) {
            $(this).val(0);
            discount = 0;
        }

        // set resulting price
        const precentage = ( discount / 100 ) * rate.price_temp;
        const resultingPrice = rate.price_temp - precentage;

        // Display resulting price
        $('#price_total').val(resultingPrice.toFixed(2));
    })
</script>
@endpush
