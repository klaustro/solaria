@extends( 'layouts.admin.app' )
@section( 'content' )
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_rates_title' ),
       'subtitle'      =>   tags( 'general_create' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_rates_title' ),
                'route' => 'admin.rates.index'
            ],
            [
                'title' => tags( 'general_create' ),
            ]
        ]
    ] )

    <div class="block full">
        <div class="box box-primary">
            <div class="box-body">

                  @include( 'admin.rates.fields' )

            </div>
        </div>
    </div>
@endsection
