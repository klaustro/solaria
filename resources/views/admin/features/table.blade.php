<div class="table-responsive" style="margin-bottom: 30px;">
    <table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="rates-table">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th>{{ tags( 'general_Icon' ) }}</th>
                <th>{{ tags( 'general_Value' ) }}</th>
                    @include('utils.language.add_table_th')
                <th class="text-center">{{ tags( 'general_action' ) }}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($features as $feature)
            <tr>
                <td class="text-center">{!! $feature->id !!}</td>
                <td class="text-center"><img src="{!! $feature->iconblack !!}" alt="images" class="animation-pulseSlow " width="62px"></td>
                <td>{!! $feature->name !!}</td>

                @include('utils.language.add_table_tbody', [
                    'translations' => $feature->featureTranslations,
                    'id' => $feature->id,
                    'route' => 'admin.features.edit'
                ])

                <td class="text-center">
                    {!! Form::open(['route' => ['admin.features.destroy', $feature->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        {{-- <a href="{!! route('admin.features.edit', [$feature->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a> --}}
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
