@if( @$roomCategoryFeature )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.roomCategoryFeatures.update', ['roomCategoryFeatures'=>$roomCategoryFeature->id]) }}" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.roomCategoryFeatures.store') }}" accept-charset="UTF-8">
@endif

    @csrf

    <!-- RoomCategory Field -->
    <div class="form-group {{$errors->has('room_category_id') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('room_category_id', tags( 'back_rooms_room_category_title' )) !!}
        {!! Form::select('room_category_id', $room_categories,  @$roomCategoryFeature->room_category_id, ['class' => 'form-control', 'placeholder' => tags( 'general_select' )]) !!}
        @if ($errors->has('room_category_id'))
            <span class="help-block">{{ $errors->first('room_category_id') }}</span>
        @endif
    </div>

    <!-- Feature Field -->
    <div class="form-group {{$errors->has('feature_id') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('feature_id', tags( 'back_features_title' )) !!}
        {!! Form::select('feature_id', $features,  @$roomCategoryFeature->feature_id, ['class' => 'form-control', 'placeholder' => tags( 'general_select' )]) !!}
        @if ($errors->has('feature_id'))
            <span class="help-block">{{ $errors->first('feature_id') }}</span>
        @endif
    </div>

    <!-- Name Field -->
    <div class="form-group {{$errors->has('value') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('value', tags( 'general_value' )) !!}

        @if(@$roomCategoryFeature)
            {!! Form::text('value', $roomCategoryFeature->value ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('value', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('value'))
            <span class="help-block">{{ $errors->first('value') }}</span>
        @endif
    </div>

</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model' ]) !!}
    <a href="{!! route('admin.roomCategoryFeatures.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
@endpush
