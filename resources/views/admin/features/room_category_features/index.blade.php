@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_room_category_features_title' ),
        'subtitle'      => tags( 'general_index' ),
        'button'        => tags( 'general_addnew' ),
        'route'         => 'admin.roomCategoryFeatures.create',
        'breadcrumb'    => [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_room_category_features_title' ),
            ]
        ]
    ] )
    @include('admin.features.room_category_features.table')
@endsection
