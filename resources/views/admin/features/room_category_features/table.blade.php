<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="roomCategoryFeatures-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'back_rooms_room_category_title' ) }}</th>
            <th>{{ tags( 'back_features_title' ) }}</th>
            <th>{{ tags( 'general_value' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}


            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $roomCategoryFeatures as $roomCategoryfeature )
        <tr>
            <td class="text-center">{!! $roomCategoryfeature->id !!}</td>
            <td>{!! $roomCategoryfeature->roomCategory->name !!}</td>
            <td>{!! $roomCategoryfeature->feature->name !!}</td>
            <td>{!! $roomCategoryfeature->value !!}</td>
            {{-- <td>{!! $roomCategoryfeature->status->name !!}</td> --}}

            <td>
                {!! Form::open(['route' => ['admin.roomCategoryFeatures.destroy', $roomCategoryfeature->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{{ route('admin.roomCategoryFeatures.edit', $roomCategoryfeature->id) }}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                </div>
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush

{{--
<table class="table table-responsive" id="roomCategoryFeatures-table">
    <thead>
        <tr>
            <th>Room Category Id</th>
        <th>Feature Id</th>
        <th>Value</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($roomCategoryFeatures as $roomCategoryFeature)
        <tr>
            <td>{!! $roomCategoryFeature->room_category_id !!}</td>
            <td>{!! $roomCategoryFeature->feature_id !!}</td>
            <td>{!! $roomCategoryFeature->value !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.roomCategoryFeatures.destroy', $roomCategoryFeature->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.roomCategoryFeatures.show', [$roomCategoryFeature->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.roomCategoryFeatures.edit', [$roomCategoryFeature->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
--}}
