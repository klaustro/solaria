<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $roomCategoryFeature->id !!}</p>
</div>

<!-- Room Category Id Field -->
<div class="form-group">
    {!! Form::label('room_category_id', 'Room Category Id:') !!}
    <p>{!! $roomCategoryFeature->room_category_id !!}</p>
</div>

<!-- Feature Id Field -->
<div class="form-group">
    {!! Form::label('feature_id', 'Feature Id:') !!}
    <p>{!! $roomCategoryFeature->feature_id !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $roomCategoryFeature->value !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $roomCategoryFeature->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $roomCategoryFeature->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $roomCategoryFeature->deleted_at !!}</p>
</div>

