@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_features_title' ),
        'subtitle'      => tags( 'general_index' ),
        'button'        => tags( 'general_addnew' ),
        'route'         => 'admin.features.create',
        'breadcrumb'    => array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_features_title' ),
            )
        )
    ] )
    @include('admin.features.table')
@endsection


