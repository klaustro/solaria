@if( @$feature )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.features.update', ['features'=>$feature->id]) }}" accept-charset="UTF-8" enctype="multipart/form-data">
    <input name="_method" type="hidden" value="PATCH">
    <input type="hidden" ref="featureId" value="{{ @$feature->id }}">
@else
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.features.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
@endif

    @csrf

        <!-- Language Id Field -->
        <div class="form-group col-sm-6 col-sm-offset-3 text-center">
            <h1>{!! tags( 'general_language' ).': <b>'.$translation->name.'</b>' !!}</h1>
            {!! Form::hidden('language_id', $translation->id) !!}
        </div>

        <!-- Icon Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('iconblack', tags( 'general_iconblack' )) !!}
            {!! Form::file('iconblack', ['class' => 'form-control']) !!}
            {{-- {!! Form::file('iconblack', @$feature->iconblack, ['class' => 'form-control']) !!} --}}
        </div>

        <!-- Icon Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('iconwhite', tags( 'general_iconwhite' )) !!}
            {!! Form::file('iconwhite', ['class' => 'form-control']) !!}
            {{-- {!! Form::file('iconwhite', @$feature->iconwhite, ['class' => 'form-control']) !!} --}}
        </div>

        <!-- Icon Field -->
        <div class="form-group col-sm-12 col-lg-12">
            {!! Form::label('icongreen', tags( 'general_icongreen' )) !!}
            {!! Form::file('icongreen', ['class' => 'form-control']) !!}
            {{-- {!! Form::file('icongreen', @$feature->icongreen, ['class' => 'form-control']) !!} --}}
        </div>

        <!-- Name Field -->
        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
            {!! Form::label('name', tags( 'general_name' )) !!}

            @if(@$feature)
                {!! Form::text('name', @$feature->itemByLanguage($translation->code)->name ?? '', ['class' => 'form-control']) !!}
            @else
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            @endif

            @if ($errors->has('name'))
                <span class="help-block">{{ $errors->first('name') }}</span>
            @endif
        </div>

        <!-- Submit Field -->
        <div class="form-group col-sm-12">
           {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model' ]) !!}
            <a href="{!! route('admin.features.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
        </div>
</form>