@php
if( isset( $quantityVariable ) === false ) {
    $quantityVariable = 'nights';
}
@endphp

@if( $detail !== null && empty( $detail ) === false )
    <tr class="tr-sub">
        <th colspan="3" class="text-center">
            @if ( $name == 'Prezzo dell\'Appartamento' )
                {{ $name }}
            @else
                @php
                    $nameRate = explode(" ", $name );
                @endphp
                {{ $title }}: {{ tags( 'general_offer' ) }} {{ $nameRate[2] }}
            @endif
        </th>
    </tr>
    <tr class="tr-sub">
        <td width="33%" class="text-center">{!! $detail[ $quantityVariable ] !!}</td>
        <td width="33%" class="text-center">{!! number_format( (float)$detail[ 'price' ], 2, ',', '.' ) !!}</td>
        @php
            $importo = (float)$detail[ 'price' ] * $detail[ $quantityVariable ];

            // solo para cuando la reserva ya esta hecha
            if ( isset( $total_item ) === true && $importo > $total_item ) {
                $importo = $total_item;
            }
        @endphp
        <td width="33%" class="text-center">{!! number_format( $importo, 2, ',', '.' ) !!}</td>
    </tr>
@endif
