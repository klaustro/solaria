<table class="table table-responsive widget table-bordered" id="bookingDetails-table"  style="margin-bottom: 30px;">
    <thead>
        {{-- <tr>
            <th width="20%">{{ tags( 'back_rooms_room_title' ) }}</th>
            <th width="16.66%" class="text-center"> Notti </th>
            <th width="16.66%" class="text-center"> Prezzo </th>
            <th width="16.66%" class="text-center"> Importo </th>
        </tr> --}}
    </thead>
    <tbody class="widget-extra-full ">
        <!-- item 1 -->
        @foreach ($bookingDetails as $bookingDetail)
            <tr>
                <td  colspan="4">
                    <div style="display: flex; margin: 10px 20px;">
                        <div style="width: 40%; min-width: 220px;">
                            <div>
                                <strong>{{ tags('order_email_reserved_room') }}:</strong> {{ $bookingDetail->room_name }}
                            </div>
                            <div>
                                <strong>{{ tags('order_email_people') }}:</strong> {{ $bookingDetail->adults_quantity }}
                            </div>
                            <div>
                                    <strong>{{ tags('booking_3_location') }}:</strong> {{ $bookingDetail->room->roomCategory->location->translations[0]->name }}
                                </div>
                                <div>
                                    <strong>{{ tags('booking_3_room_responsible') }} </strong>
                                </div>
                                <div>
                                    <strong>{{ tags('general_name') }}:</strong> {{ $bookingDetail->dataForms->name }}
                                </div>
                                <div>
                                    <strong>{{ tags('general_phone') }}:</strong> {{ $bookingDetail->dataForms->phone }}
                                </div>
                                <div>
                                    <strong>{{ tags('general_email')}}:</strong> {{ $bookingDetail->dataForms->email }}
                                </div>
                        </div>
                    </div>
                </td>
            </tr>
            {{-- recorro los season --}}
            @if( @$bookingDetail->price_detail[ 'season' ] )
                @foreach( $bookingDetail->price_detail[ 'season' ] as $key =>  $season )
                    <tr class="text-center">
                        <th colspan="3" class="text-center p-0 bg-light">
                            <strong>{{ tags( 'booking_3_stagione' )}}:</strong> {{ $season[ 0 ]['name'] }}
                        </th>
                    </tr>
                    <tr class="text-center">
                        <td class="text-center" style="width: 50%; min-width: 220px;"><strong>{{ tags('booking_3_nights') }}</strong></td>
                        {{-- <td class="text-center"><strong>{{ tags('booking_3_price') }}</strong></td> --}}
                        <td class="text-center" colspan="2"><strong>{{ tags('booking_3_amount') }}</strong></td>
                    </tr>
                    <tr class="text-center">
                        <td>
                            {{$season[ 0 ]['nights']}}
                        </td>
                        {{-- <td>
                            {!! number_format( (float)$season[ 0 ]['price'], 2, ',', '.' ) !!}
                        </td> --}}
                        <td colspan="2">
                            @php
                            $importo = (float)$season[ 0 ]['price'] *  $season[ 0 ]['nights'];

                            // solo para cuando la reserva ya esta hecha
                            if ( isset( $total_item ) === true && $importo > $total_item ) {
                                $importo = $total_item;
                            }
                            @endphp
                            {!! number_format( $importo, 2, ',', '.' ) !!}
                        </td>
                    </tr>
                @endforeach
            @endif
            {{-- recorro los rates --}}
            @if( @$bookingDetail->price_detail[ 'rate' ] )
                @foreach( $bookingDetail->price_detail[ 'rate' ] as $key => $rate )
                    {{-- {{ dd($rate[ 0 ]) }} --}}
                    <tr class="text-center">
                            <th colspan="3" class="text-center p-0 bg-light">
                            {{-- {{ dd( ($rate[ 0 ]['name'] == )back_rates_title) }} --}}
                                <strong>{{ tags( 'booking_3_stagione' )}}:</strong> {{  tags( 'back_rates_title' )  }}
                            </th>
                        </tr>
                        <tr class="text-center">
                            <td class="text-center" style="width: 28%; min-width: 220px;"><strong>{{ tags('booking_3_nights') }}</strong></td>
                            {{-- <td class="text-center"><strong>{{ tags('booking_3_price') }}</strong></td> --}}
                            <td class="text-center" colspan="2"><strong>{{ tags('booking_3_amount') }}</strong></td>
                        </tr>
                        <tr class="text-center">
                            <td>
                                {{$rate[ 0 ]['nights']}}
                            </td>
                            {{-- <td>
                                {!! number_format( (float)$rate[ 0 ]['price'], 2, ',', '.' ) !!}
                            </td> --}}
                            <td colspan="2">
                                @php
                                    $importo = (float)$rate[ 0 ]['price'] * $rate[ 0 ]['nights'];

                                    // solo para cuando la reserva ya esta hecha
                                    if ( isset( $total_item ) === true && $importo > $total_item ) {
                                        $importo = $total_item;
                                    }
                                @endphp
                                {!! number_format( $importo, 2, ',', '.' ) !!}
                            </td>
                        </tr>
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>
    {{-- @foreach ( $bookingDetails as $bookingDetail )
        <tr>

            Apartamento
            <td width="50%" style="padding-left: 10px;">
                <strong>{!! $bookingDetail->room_name !!}</strong>
                <small>
                    @if ( $bookingDetail->adults_quantity )
                        <br>
                        <strong>Persone:</strong>
                        {{ $bookingDetail->adults_quantity }}
                        @if ( $bookingDetail->children_quantity )
                            + {{ $bookingDetail->children_quantity }}
                        @endif
                    @endif

                    @if( $bookingDetail->personResponsible )
                        <br>
                        <strong>Responsabile della stanza</strong> <br>
                        <strong>Nome:</strong> {{ $bookingDetail->personResponsible->name }} <br>
                        <strong>Telefono:</strong> {{ $bookingDetail->personResponsible->phone }} <br>
                        <strong>E-mail:</strong> {{ $bookingDetail->personResponsible->email }} <br>
                    @endif
                </small>
            </td>


            @php
            $checkin    = $bookingDetail->bookingDate[ 'checkin' ];
            $checkout   = $bookingDetail->bookingDate[ 'checkout' ];
            $nights     = count( getNightsInRange( $checkin, $checkout ) );

            // dd( $bookingDetail->price_detail );
            @endphp

            <td colspan="3">
                <table width="99%">

                    recorro los season
                    @if( @$bookingDetail->price_detail[ 'season' ] )
                        @foreach( $bookingDetail->price_detail[ 'season' ] as $key =>  $season )
                            {{ dd( @$bookingDetail->price_detail[ 'season' ] ) }}
                            @include( 'admin.bookings.booking_details.price_details', [
                                'name' => $key,
                                'detail' => $season[ 0 ],
                                'title' => 'Stagione',
                                'total_item' => $bookingDetail->total_item
                            ] )
                        @endforeach
                    @endif

                    recorro los rates
                    @if( @$bookingDetail->price_detail[ 'rate' ] )
                        @foreach( $bookingDetail->price_detail[ 'rate' ] as $key => $rate )
                            @include( 'admin.bookings.booking_details.price_details', [
                                'name' => $key,
                                'detail' => $rate[ 0 ],
                                'title' => 'Promozione',
                                'total_item' => $bookingDetail->total_item
                            ] )
                        @endforeach
                    @endif
                </table>
            </td>



            {{-- Notti --}}
            {{-- @php
            $checkin    = $bookingDetail->bookingDate[ 'checkin' ];
            $checkout   = $bookingDetail->bookingDate[ 'checkout' ];
            $nights     = count( getNightsInRange( $checkin, $checkout ) );
            @endphp
            <td class="text-center">{!! $nights !!}</td> --}}

            {{-- Prezzo --}}
            {{-- <td>{!! number_format( $bookingDetail->room_price, 2 ) !!}</td> --}}

            {{-- Importo --}}
            {{-- <td>{!! number_format( $bookingDetail->total_item, 2 ) !!}</td> --}}
        {{-- </tr>
    @endforeach --}}

    {{-- </tbody>
</table> --}}

@push( 'css' )
<style type="text/css">
    #bookingDetails-table {
        font-size: 15px;
        /*font-weight: 600;*/
    }
    #summary-table {
        font-size: 15px;
    }
    .table>tbody>tr>td {
        /*border-right: 1px solid #ddd;*/
        padding-bottom: 8px;
        padding-top: 8px;
        padding-left: 0px;
        padding-right: 0px;

        border-top: 2px solid #ebeaeb;
    }
    #bookingDetails-table>tbody>tr>td {
        line-height: 1.3;
    }
    .sombra {
        background-color: #ebeaeb;
    }
    .sombra strong {
        font-weight: 800;
        color: #000;
    }

    .tr-sub {
        height: 35px;
    }
</style>
@endpush



















{{-- <div class="table-responsive">
<table class="table table-vcenter table-condensed table-bordered table-hover" id="bookingDetails-table">
    <thead class="widget-extra themed-background-dark">
        <tr class="widget-content-light">
            <th>Checkin Date</th>
            <th>Checkout Date</th>
            <th>Persons Amount</th>
            <th>Booking Id</th>
            <th>Row Id</th>
            <th>Payment Method Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody class="widget-extra">
    @foreach($bookingDetails as $bookingDetail)
        <tr>
            <td>{!! $bookingDetail->checkin_date !!}</td>
            <td>{!! $bookingDetail->checkout_date !!}</td>
            <td>{!! $bookingDetail->persons_amount !!}</td>
            <td>{!! $bookingDetail->booking_id !!}</td>
            <td>{!! $bookingDetail->row_id !!}</td>
            <td>{!! $bookingDetail->payment_method_id !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.bookingDetails.destroy', $bookingDetail->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.bookingDetails.show', [$bookingDetail->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-eye"></i></a>
                    <a href="{!! route('admin.bookingDetails.edit', [$bookingDetail->id]) !!}" class='btn btn-default btn-xs'><i class="fa fa-edit"></i></a>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div> --}}
