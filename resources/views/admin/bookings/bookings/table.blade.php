<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="bookings-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_code' ) }}</th>
            <th>{{ tags( 'general_name' ) }}</th>
            <th>{{ tags( 'general_date' ) }}</th>
            <th>{{ tags( 'general_total' ) }}</th>
            <th>{{ tags( 'general_status' ) }}</th>
            {{-- <th>{{ tags( 'order_email_payment_method' ) }}</th> --}}
            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $bookings as $booking )
        <tr>
            <td class="text-center">{!! $booking->code !!}</td>
            <td>{!! $booking->personResponsible->name !!}</td>
            <td>{{ @$booking->bookingDetails->first()->checkin_date_italian }} a {{ @$booking->bookingDetails->first()->checkout_date_italian }}</td>
            <td>{!! number_format( (float)$booking->total, 2, ',', '.' ) !!}</td>
            <td>{!! $booking->status->name !!}</td>
            {{-- <td>{!! $booking->bookingDetails->paymentMethod->name !!}</td> --}}
            <td class="text-center">
                <div class='btn-group'>
                    <a href="{!! route('admin.bookings.show', [$booking->id]) !!}" class='btn btn-default btn-xs' title="Dettaglio"><i class="fa fa-eye"></i></a>

                    <a data-target="#changeStatusOrder-{{ $booking->id }}" data-toggle="modal"  class='btn btn-default btn-xs' title="{{ tags( 'general_change_status' ) }}"><i class="fa fa-toggle-on"></i></a>

                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

{{-- ESTE ES EL MODAL PARA CAMBIAR DE ESTATUS LA ORDEN. --}}
@foreach ( $bookings as $booking )
    <div id="changeStatusOrder-{{ $booking->id }}" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- begin::Modal content-->
            <form name="editOrder" action="{{ route('admin.bookings.update', ['booking' => $booking->id]) }}" method="POST">
                <input name="_method" type="hidden" value="PUT">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Cambia stato - Booking #{{ $booking->code }}</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="form-group col-sm-6">
                                {!! Form::label('status_id', tags( 'general_status' )) !!}
                                {!! Form::select('status_id', $statuses,  $booking->status_id, ['class' => 'chosen select-chosen', 'data-placeholder' => tags( 'general_select' ), 'placeholder' => tags( 'general_select' ), 'required' => true]) !!}
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ tags( 'general_back' ) }}</button>
                        <button type="submit" class="btn btn-primary" onclick=" return confirm('{{ tags( 'general_confirm_ask' ) }}')"
                        >{{ tags( 'general_save' ) }}</button>
                    </div>
                </div>
            </form>
            <!-- end::Modal content-->
        </div>
    </div>
@endforeach

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $( function ( ) {
            TablesDatatables.tableGeneral();
        } );
    </script>
@endpush
