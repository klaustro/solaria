<form id="form-model" name="form-model" method="POST" action="{{ route( 'admin.bookings.store' ) }}" accept-charset="UTF-8">
    @csrf

    <!-- Campos de rango de fecha SOLO LECTURA -->
    <div class="form-group col-sm-6 col-sm-offset-3 {{ $errors->has( 'checkin_date' ) ? 'has-error' : '' }} {{ $errors->has( 'checkout_date' ) ? 'has-error' : '' }}">
        <label class="control-label" for="checkin_date">Data di prenotazione</label>
        <div class="input-group" data-date-format="dd/mm/yyyy">
            <input type="text" class="form-control text-center" name="checkin_date" id="checkin_date" placeholder="a partire dal" autocomplete="off" value="{{ @$checkin }}" readonly />
            <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
            <input type="text" class="form-control text-center" name="checkout_date" id="checkout_date" placeholder="fino a" autocomplete="off" value="{{ @$checkout }}" readonly />
        </div>

        @if ( $errors->has( 'checkin_date' ) )
            <span class="help-block">{{ $errors->first( 'checkin_date' ) }}</span>
        @endif
        @if ( $errors->has( 'checkout_date' ) )
            <span class="help-block">{{ $errors->first( 'checkout_date' ) }}</span>
        @endif

        <input type="hidden" name="checkin_ymd" id="checkin_ymd" value="{{ $checkin_ymd }}">
        <input type="hidden" name="checkout_ymd" id="checkout_ymd" value="{{ $checkout_ymd }}">
    </div>

    <!-- Casa Vacazan Field -->
    <div class="form-group {{ $errors->has( 'room_id' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'room_id', tags( 'back_rooms_room_title' ) ) !!}
        <select id="room_id" name="room_id" class="form-control data-placeholder">
            <option value="0">{{ tags( 'general_select' ) }}</option>
            @foreach ( $rooms as $room )
                {{ var_dump( $room[ 'price_detail_request' ] ) }}

                @php
                    $selected = '';
                    if ( old( 'room_id' ) !== null && $room[ 'id' ] == old( 'room_id' ) ) {
                        $selected = 'selected';
                    }

                    $disabled = '';
                    if ( $room[ 'availability' ] === 0 ) {
                        $disabled = 'disabled';
                    }
                @endphp

                <option value="{{ $room[ 'id' ] }}" {{ $selected }} {{ $disabled }}>{{ $room[ 'name' ] }} - Prezzo: {{ $room[ 'price_request' ] }}</option>
            @endforeach
        </select>
        @if ( $errors->has( 'room_id' ) )
            <span id="error-room_id" class="help-block">{{ $errors->first( 'room_id' ) }}</span>
        @endif
        <span id="error-room_id" class="help-block" style="color:red"></span>
    </div>

    <!-- Price Field -->
    <div class="form-group {{ $errors->has( 'price_backoffice' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        <!-- label -->
        <label for="price">Dettaglio del prezzo</label><br>

        <!-- tabla de detalle -->
        @foreach ( $rooms as $room )
            <table class="table table-responsive widget table-details" id="table-{{ $room[ 'id' ] }}">
                @if( isset( $room[ 'price_detail_request' ][ 'season' ] ) === true )
                    @foreach( $room[ 'price_detail_request' ][ 'season' ] as $key => $season )
                        @include( 'admin.bookings.booking_details.price_details', [
                            'name' => $key,
                            'detail' => $season,
                            'title' => 'Stagione',
                            'quantityVariable' => 'quantity'
                        ] )
                    @endforeach
                @endif

                @if( isset( $room[ 'price_detail_request' ][ 'rate' ] ) === true )
                    @foreach( $room[ 'price_detail_request' ][ 'rate' ] as $key => $rate )
                        @include( 'admin.bookings.booking_details.price_details', [
                            'name' => $key,
                            'detail' => $rate,
                            'title' => 'Promozione ',
                            'quantityVariable' => 'quantity'
                        ] )
                    @endforeach
                @endif
            </table>
        @endforeach

        <!-- input -->
        <input type="checkbox"name="price_selected" id="price_selected"> Indicare un prezzo personalizzato
        <input id="price_backoffice" name="price_backoffice" type="number" class="form-control" step="0.01" disabled>
        @if ( $errors->has( 'price_backoffice' ) )
            <span id="error-price_backoffice" class="help-block">{{ $errors->first( 'price_backoffice' ) }}</span>
        @endif
        <span id="error-price_backoffice" class="help-block" style="color:red"></span>
    </div>

    <!-- Name Field -->
    <div class="form-group {{ $errors->has( 'name' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'name',  tags( 'general_name' ) ) !!}
        {!! Form::text( 'name', null, [ 'class' => 'form-control' ] ) !!}
        @if ( $errors->has( 'name' ) )
            <span id="error-name" class="help-block">{{ $errors->first( 'name' ) }}</span>
        @endif
        <span id="error-name" class="help-block" style="color:red"></span>
    </div>

    <!-- Lastname Field -->
    <div class="form-group {{ $errors->has( 'lastname' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'lastname', tags( 'general_lastname' ) ) !!}
        {!! Form::text( 'lastname', null, [ 'class' => 'form-control' ] ) !!}
        @if ( $errors->has( 'lastname' ) )
            <span id="error-lastname" class="help-block">{{ $errors->first( 'lastname' ) }}</span>
        @endif
        <span id="error-lastname" class="help-block" style="color:red"></span>
    </div>

    <!-- Email Field -->
    <div class="form-group {{ $errors->has( 'email' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'email', tags( 'general_email' ) ) !!}
        <input name="email" type="email" id="email" class="form-control" value="{{ isset( $email ) ? $email : '' }}" {{ isset( $email ) ? 'readonly' : '' }}>
        @if ( $errors->has( 'email' ) )
            <span id="error-email" class="help-block">{{ $errors->first( 'email' ) }}</span>
        @endif
        <span id="error-email" class="help-block" style="color:red"></span>
    </div>

    <!-- fiscalCode Field -->
    <div class="form-group {{ $errors->has( 'fiscalCode' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'fiscalCode', tags( 'order_email_fiscal_code' ) ) !!}
        {!! Form::text('fiscalCode', null, ['class' => 'form-control text-uppercase']) !!}
        @if ( $errors->has( 'fiscalCode' ) )
            <span id="error-fiscalCode" class="help-block">{{ $errors->first( 'fiscalCode' ) }}</span>
        @endif
        <span id="error-fiscalCode" class="help-block" style="color:red"></span>
    </div>

    <!-- adults_quantity Field -->
    <div class="form-group {{ $errors->has( 'adults_quantity' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'adults_quantity', tags( 'quantity_adults_back' ) ) !!}
        {!! Form::number( 'adults_quantity', null, [ 'class' => 'form-control', 'min' => '0', 'step' => '0' ] ) !!}
        @if ( $errors->has( 'adults_quantity' ) )
            <span id="error-adults_quantity" class="help-block">{{ $errors->first( 'adults_quantity' ) }}</span>
        @endif
        <span id="error-adults_quantity" class="help-block" style="color:red"></span>
    </div>

    <!-- children_quantity Field -->
    <div class="form-group {{ $errors->has( 'children_quantity' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'children_quantity', tags( 'quantity_children' ) ) !!}
        {!! Form::number( 'children_quantity', null, [ 'class' => 'form-control', 'min' => '0', 'step' => '0' ] ) !!}
        @if ( $errors->has( 'children_quantity' ) )
            <span id="error-children_quantity" class="help-block">{{ $errors->first( 'children_quantity' ) }}</span>
        @endif
        <span id="error-children_quantity" class="help-block" style="color:red"></span>
    </div>

    <!-- Phone Field -->
    <div class="form-group {{ $errors->has( 'phone' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'phone', tags( 'general_phone' ) ) !!}
        {!! Form::text( 'phone', null, [ 'class' => 'form-control' ] ) !!}
        @if ( $errors->has( 'phone' ) )
            <span id="error-phone" class="help-block">{{ $errors->first( 'phone' ) }}</span>
        @endif
        <span id="error-phone" class="help-block" style="color:red"></span>
    </div>

    <!-- Status Field -->
    <div class="form-group {{ $errors->has( 'status_id' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'status_id', tags( 'general_status' ) ) !!}
        {!! Form::select( 'status_id', $statuses,  null, [ 'class' => 'chosen select-chosen', 'data-placeholder' => tags( 'general_select' ), 'placeholder' => tags( 'general_select' ) ] ) !!}
        @if ( $errors->has( 'status_id' ) )
            <span id="error-status_id" class="help-block">{{ $errors->first( 'status_id' ) }}</span>
        @endif
        <span id="error-status_id" class="help-block" style="color:red"></span>
    </div>

    <!-- Metodo de pago Field -->
    <div class="form-group {{ $errors->has( 'method_payment' ) ? 'has-error' : '' }} col-sm-6 col-sm-offset-3">
        {!! Form::label( 'method_payment', tags( 'order_email_payment_method' ) ) !!}
        <select name="method_payment" class="chosen select-chosen data-placeholder">
                <option value="">{{ tags( 'general_select' ) }}</option>
                @foreach ( $method_paymenties as $method_payment )
                    <option value="{{ $method_payment->id }}" {{ $method_payment->id == old('method_payment') ? 'selected' : '' }}>{{ $method_payment[ 'name' ] }}</option>
                @endforeach
        </select>
        @if ( $errors->has( 'method_payment' ) )
            <span id="error-method_payment" class="help-block">{{ $errors->first( 'method_payment' ) }}</span>
        @endif
        <span id="error-method_payment" class="help-block" style="color:red"></span>
    </div>
</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), [ 'class' => 'btn btn-primary', 'form' => 'form-model' ] ) !!}
    <a href="{!! route( 'admin.bookings.create' ) !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@push( 'scripts' )
    <script src="{{ asset( 'admin/js/vendor/moment-timezone.min.js' ) }}"></script>
    <script src="{{ asset( 'admin/js/vendor/moment-timezone-with-data-2012-2022.min.js' ) }}"></script>
    <script src="{{ asset( 'admin/js/vendor/moment-timezone-with-data.min.js' ) }}"></script>
    <script src="{{ asset( 'admin/js/sweetalert.min.js' ) }}"></script>
    <script>
        $( '.datepicker' ).datepicker( {
            format: 'dd/mm/yyyy'
        } );

        var previousValue = 0;
        // Array de Habitaciones
        var rooms = {!! json_encode($rooms->toArray()) !!};

        $( document ).ready( function () {

            // eventon change
            $( document ).on( 'change', '#room_id', function ( e ) {
                // loading
                loading( $( this ) );

                // se intenta cambiar la habitacion seleccionada
                changeSelectedRoom( $( this ).val(), previousValue, $( this ) );
            } );

            // en caso de que venga desde oferta especial (icono de carrito)
            var rateRoomId = {{ empty( $rate_room_id ) === false ? $rate_room_id : 'null' }};
            if ( rateRoomId !== null ) {
                changeSelectedRoom( rateRoomId, previousValue, $( '#room_id' ) )
            }

            // eventon click de precio personalizado
            $( document ).on( 'ifChecked ifUnchecked', '#price_selected', function ( e ) {
                $( '#price_backoffice' ).prop( 'disabled', function( i, v ) { return !v; } );
            } );
        } );

        function changeSelectedRoom( newValue, previousValue, select ) {
            // Asignamos el Precio al campo para poder editar
            var roomId =  parseInt( newValue );
            var room = rooms.find( room => room.id === roomId );
            if ( room ) {
                console.log( 'price: ', room.price_request.toFixed( 2 ) );
                $( '#price_backoffice' ).val( room.price_request.toFixed( 2 ) );

                // mostrar detalle
                $( '.table-details' ).hide();
                $( '#table-' + roomId ).show();
            }

            // para bloquear la habitacion seleccionada
            var dataLock = {
                roomId          : newValue,
                checkinDate     : $( '#checkin_ymd' ).val(),
                checkoutDate    : $( '#checkout_ymd' ).val(),
                datetime        : moment().tz( 'Europe/Rome' ).format( 'YYYY-MM-DD HH:mm:ss' )
            };

            // para desbloquear la habitacion deseleccionada
            var dataUnlock = {
                roomId          : previousValue,
                checkinDate     : $( '#checkin_ymd' ).val(),
                checkoutDate    : $( '#checkout_ymd' ).val()
            };

            // envio solicitud de bloqueo para la room recien seleccionada
            lockRoom( select, dataLock, dataUnlock );
        }

        // poner bloqueo temporal a la habitacion
        function lockRoom( select, data, dataUnlock ) {
            // ajax
            $.ajax( {
                type: 'POST',
                url: '/api/admin/rooms/lock',
                data: data,
                success: function ( response ) {

                    // si se pudo bloquear
                    if ( response.data !== null ) {

                        // mensaje de habitacion desbloqueada
                        var message = 'L\'appartamento ' + response.data.room.name + ' è stato selezionato correttamente.';
                        var type = 'success';
                        console.log( message );

                        // actualizar valor previousValue
                        previousValue = select.val();

                        // rectifico que el valor del select sea el recien seleccionado
                        select.val( data.roomId );
                        select.trigger( 'chosen:updated' );

                        // envio solicitud de desbloqueo para la room anterior
                        unlockRoom( select, dataUnlock );
                    } else {
                        message = 'Non è possibile selezionare questo appartamento, è bloccato.';
                        var type = 'error';
                        console.log( message );

                        // vuelvo a poner el valor anterior
                        select.val( dataUnlock.roomId );
                        select.trigger( 'chosen:updated' );

                        // quitar loading
                        loaded( select );
                    }

                    // notificacion
                    // growlNotification( message, type );
                    sweetAlertNotification( message, type );
                },
                error: function ( jqXHR, textStatus, errorThrown ) {
                    // envio solicitud de desbloqueo para la room anterior
                    unlockRoom( select, dataUnlock );

                    // quitar loading
                    loaded( select );
                },
                headers: {
                    'X-CSRF-TOKEN': $( 'meta[name="csrf-token"]' ).attr( 'content' )
                }
            } );
        }

        // quitar bloqueo temporal a la habitacion
        function unlockRoom( select, dataUnlock ) {
            // ajax
            $.ajax( {
                type: 'POST',
                url: '/api/admin/rooms/unlock',
                data: dataUnlock,
                success: function ( response ) {

                    // mensaje de habitacion desbloqueada
                    var message = 'L\'appartamento ' + response.data.room.name + ' è stato sbloccato correttamente.';
                    var type = 'info';
                    console.log( message );

                    // quitar loading
                    loaded( select );

                    // notificacion
                    // growlNotification( message, type );
                    sweetAlertNotification( message, type );
                },
                error: function ( jqXHR, textStatus, errorThrown ) {
                    // quitar loading
                    loaded( select );
                },
                headers: {
                    'X-CSRF-TOKEN': $( 'meta[name="csrf-token"]' ).attr( 'content' )
                }
            } );
        }

        // poner loading
        function loading( select ) {
            NProgress.start();
            select.attr( 'disabled', true );
            select.trigger( 'chosen:updated' );
        }

        // quitar loading
        function loaded( select ) {

            // si esta deshabilitado
            if ( select.attr( 'disabled' ) !== undefined ) {

                // habilito
                $( select ).attr( 'disabled', false );
                $( select ).trigger( 'chosen:updated' );

                // quito loading
                NProgress.done();
            }
        }

        /*
         * Grawl Notifications with Bootstrap-growl plugin,
         * check out more examples at http://ifightcrime.github.io/bootstrap-growl/
         */
        function growlNotification( message, type ) {

            var type = type || 'info';

            $.bootstrapGrowl( '<p>' + message + '</p>', {
                type: type,
                delay: 2500,
                allow_dismiss: true
            } );

            $( this ).prop( 'disabled', true );
        }

        /*
         * Sweet Alert Notification
         */
        function sweetAlertNotification( message, type ) {
            swal( {
                text: message,
                icon: type,
            } );
        }
    </script>
@endpush

@push( 'css' )
    <style type="text/css">
        .table-details {
            margin-bottom: 0;
            display: none;
        }
    </style>
@endpush