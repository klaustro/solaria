@extends( 'layouts.admin.app' )

@section( 'content' )
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_module_bookings_boking_title' ),
        'subtitle'      => tags( 'general_index' ),
        'button'        => tags('general_addnew'),
        'route'         => 'admin.bookings.create',
        'breadcrumb'    => array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_module_bookings_boking_title' ),
            )
        )
    ] )
        <a title="Visualizza Tavolo" class="btn btn-primary btn-xs {{ isset(request()->calendar) ? '' : 'active'}}" href="{{route('admin.bookings.index')}}"><i class="fa fa-th-large" aria-hidden="true"></i></a>
        <a title="Visualizza Calendario" class="btn btn-primary btn-xs {{ isset(request()->calendar) ? 'active' : ''}}" href="{{route('admin.bookings.index', ['calendar' => 1])}}"><i class="fa fa-calendar" aria-hidden="true"></i></a>
        <br>
        <br>
    @if(request()->calendar == 1)
        @include( 'admin.bookings.bookings.calendar' )
    @else
        @include( 'admin.bookings.bookings.table' )
    @endif
@endsection

