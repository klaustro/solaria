@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         =>   tags( 'back_bookings_booking_title' ) . ' #' . $booking->code,
        'subtitle'      =>   tags( 'general_show' ),
        'icon'          =>   'fa fa-cogs',
        'breadcrumb'    =>   array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_module_bookings_boking_title' ),
                'route' => 'admin.bookings.index'
            ),
            array (
                'title' => tags( 'general_show' ),
            )
        )
    ] )

    @include('admin.bookings.bookings.partials.single_booking')
@endsection
