<div class="table-responsive" style="margin-bottom: 30px;">

    <!-- begin::datos del usuario de la orden -->
    @if( $booking->exists )
        <div class="col-md-12" style="padding: 0;">
            <div class="block">
                <div class="block-title">
                    <h2><strong>{{ tags( 'Dettagli della Prenotazione' ) }}</strong></h2>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <span><strong>Cliente:</strong> {!! $booking->personResponsible->name !!}</span><br>
                        <span><strong>E-mail:</strong> {!! $booking->personResponsible->email !!}</span><br>
                        <span><strong>Codice Fiscale:</strong> {!! strtoupper( $booking->personResponsible->fiscal_code ) !!}</span><br>
                        <span><strong>Telefono:</strong> {!! $booking->personResponsible->phone !!}</span><br>
                        <span><strong>{{ tags( 'order_email_payment_method' ) }}:</strong> {!! $booking->paymentMethod->name !!}</span><br>
                    </div>

                    <div class="col-md-6">
                        <span><strong>{{ tags( 'general_date' ) }}:</strong> {!! $booking->created_at_date !!}</span><br>
                        <span><strong>{{ tags( 'general_time' ) }}:</strong> {!! $booking->created_at_time !!}</span><br>

                        <br>

                        @php
                        $checkin    = $booking->bookingDetails[ 0 ]->bookingDate[ 'checkin' ];
                        $checkout   = $booking->bookingDetails[ 0 ]->bookingDate[ 'checkout' ];
                        $nights     = count( getNightsInRange( $checkin, $checkout ) );
                        @endphp

                        <span><strong>Arrivo:</strong> {{ $booking->bookingDetails->first()->checkin_date_italian }}</span><br>
                        <span><strong>Partenza:</strong> {{ $booking->bookingDetails->first()->checkout_date_italian}}</span><br>
                        <span><strong>Durata totale del soggiorno:</strong> {{ $nights }} notti</span><br>
                    </div>
                </div>

                <br>
            </div>
        </div>
    @endif
    <!-- end::datos del usuario de la orden -->

    <!-- begin::listado de los produntos de la orden -->
    @include('admin.bookings.booking_details.table')
    <!-- end::listado de los produntos de la orden -->

    <div class=""  style="overflow-x: hidden;">
        <div class="row " style="background: white;">
            {{-- begin: Comment --}}
            <div class="col-md-6">
                <div class="form-group" style="margin: 5px;">
                    <h4><label><strong>Commento</strong></label></h4>
                    <textarea class="form-control" disabled cols="30" rows="3">{{$booking['comment']}}</textarea>
                </div>
            </div>
            {{-- end: Comment --}}
            <!-- begin::tabla de montos de la orden -->
            <div class="col-md-6">
                <table class="table table-responsive table-hover widget" id="summary-table">
                    <tbody class="widget-extra-full">
                        <tr>
                            <th class="summary-title">{!! tags( 'booking_3_amount' ) !!}</th>
                            <td class="text-left summary-value">€ {!! number_format( (float)$booking->subtotal, 2, ',', '.' ) !!}</td>
                        </tr>
                        <tr>
                            <th class="summary-title">{!! tags( 'general_tax' ) !!}</th>
                            <td class="text-left summary-value">€ {!! number_format( (float)$booking->iva, 2, ',', '.' ) !!}</td>
                        </tr>
                        <tr class="summary-total">
                            <th class="summary-title">{!! tags( 'general_total' ) !!}</th>
                            <td class="text-left summary-value"><strong>€ {!! number_format( (float)$booking->total, 2, ',', '.' ) !!}</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- end::tabla de montos de la orden -->
        </div>
    </div>
</div>

@push( 'css' )
    <style type="text/css">
        .summary-title {
            padding-left: 60px !important;
            width: 50%;
        }
        .summary-value {
            border-top: 1px solid #f2f2f2 !important;
        }
        .summary-total {
            font-size: 16px;
        }
    </style>
@endpush
