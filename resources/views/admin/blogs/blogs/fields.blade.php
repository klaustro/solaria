<div>
    @if( @$blog )
        <form id="myDropzone" name="myDropzone" method="POST" class="dropzone" action="{{ route('admin.blogs.update', ['blogs'=>$blog->id]) }}" accept-charset="UTF-8" enctype="multipart/form-data">
        <input name="_method" type="hidden" value="PATCH">
    @else
        <form id="myDropzone" name="myDropzone" method="POST" class="dropzone" action="{{ route('admin.blogs.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
    @endif

            @csrf

            <!-- Language Id Field -->
            <div class="form-group col-sm-6 col-sm-offset-3 text-center">
                <h1>{!! tags( 'general_language' ).': <b>'.$translation->name.'</b>' !!}</h1>
                {!! Form::hidden('language_id', $translation->id) !!}
            </div>

            <!-- Title Field -->
            <div class="form-group {{$errors->has('title') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
                {!! Form::label('title', tags( 'general_title' )) !!}
                {!! Form::text('title', isset($blog) ? $blog->itemByLanguage($translation->id)->title ?? '': '', ['class' => 'form-control']) !!}
                @if ($errors->has('title'))
                    <span class="help-block">{{ $errors->first('title') }}</span>
                @endif
            </div>

            <!-- Subtitle Field -->
            <div class="form-group {{$errors->has('subtitle') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
                {!! Form::label('subtitle', tags( 'general_subtitle' )) !!}
                {!! Form::text('subtitle', isset($blog) ? $blog->itemByLanguage($translation->id)->subtitle ?? '': '', ['class' => 'form-control']) !!}
                @if ($errors->has('subtitle'))
                    <span class="help-block">{{ $errors->first('subtitle') }}</span>
                @endif
            </div>

            <!-- Description Field -->
            <div class="form-group {{$errors->has('description') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
                {!! Form::label('description', tags( 'general_description' )) !!}
                {!! Form::textarea('description', isset($blog) ? $blog->itemByLanguage($translation->id)->description ?? '' : '', ['class' => 'form-control', 'id' => 'description']) !!}
                @if ($errors->has('description'))
                    <span class="help-block">{{ $errors->first('description') }}</span>
                @endif
            </div>

            <!-- Informazioni -->
            <div class="col-sm-6 col-sm-offset-3" >
                <label>Informazioni</label>
                <span>
                <ul>
                    <li>Dimensione massima dell'immagine: 1 MB</li>
                    <li>Dimensione raccomandata in pixel: 658 x 280</li>
                    <li>Numero di immagini da caricare: 1</li>
                    <li>
                        Utilizzato questo link per comprimere le tue immagini: <a href="https://imagecompressor.com/"  target="_blank">https://imagecompressor.com</a>
                    </li>
                </ul>
            </span>
            </div>

            <!-- Single Images view -->
            @include('admin.images.index', [
                'model' => @$blog,
                'url' => '/admin/blogs',
                'single_select' => true,
                'form' => isset($blog) ? $blog->itemByLanguage($translation->id)->title ?? '' : '',
           ])
        </form>
</div>
           <br>
           <br>
<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    <br>
    <br>
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'myDropzone', 'id' => 'submit' ]) !!}
    <a href="{!! route('admin.blogs.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>
