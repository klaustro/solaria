@extends('layouts.admin.app')
@section('content')
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_blogs_blogs_title' ),
       'subtitle'      =>   tags( 'general_create' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_blogs_blogs_title' ),
                'route' => 'admin.blogs.index'
            ),
            array (
                'title' => tags( 'general_create' ),
            )
        )
    ] )

    <div class="block full">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">

                  @include('admin.blogs.blogs.fields')

                </div>
            </div>
        </div>
    </div>
@endsection