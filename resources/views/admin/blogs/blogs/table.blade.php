<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="blogs-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'general_title' ) }}</th>
           {{-- <th>{{ tags( 'general_subtitle' ) }}</th>
            <th>{{ tags( 'general_description' ) }}</th>--}}
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            @include('utils.language.add_table_th')

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $blogs as $blog )
        <tr>
            <td class="text-center">{!! $blog->id !!}</td>
            <td>{!! $blog->title !!}</td>
            {{-- <td>{!! $blog->subtitle !!}</td>
            <td>{!! $blog->description !!}</td> --}}
            {{-- <td>{!! $blog->status->name !!}</td> --}}

            @include('utils.language.add_table_tbody', [
                'translations' => $blog->translations,
                'id' => $blog->id,
                'route' => 'admin.blogs.edit'
            ])

            <td class="text-center">
                {!! Form::open(['route' => ['admin.blogs.destroy', $blog->id], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
