@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_blogs_blog_categories_title' ),
        'subtitle'      => tags( 'general_index' ),
        'button'        => tags( 'general_addnew' ),
        'route'         => 'admin.blogCategories.create',
        'breadcrumb'    => [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_blogs_blog_categories_title' ),
            ]
        ]
    ] )
    @include('admin.blogs.blog_categories.table')
@endsection

