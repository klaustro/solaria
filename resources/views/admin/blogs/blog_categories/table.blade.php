<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="blogCategories-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'general_name' ) }}</th>
            <th>{{ tags( 'general_description' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            @include('utils.language.add_table_th')

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $blogCategories as $blogCategory )
        <tr>
            <td class="text-center">{!! $blogCategory->id !!}</td>
            <td>{!! $blogCategory->name !!}</td>
            <td>{!! $blogCategory->description !!}</td>
            {{-- <td>{!! $blogCategory->status->name !!}</td> --}}

            @include('utils.language.add_table_tbody', [
                'translations' => $blogCategory->translations,
                'id' => $blogCategory->id,
                'route' => 'admin.blogCategories.edit'
            ])

            <td>
                {!! Form::open(['route' => ['admin.blogCategories.destroy', $blogCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
