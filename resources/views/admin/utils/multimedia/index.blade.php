<!-- begin::Carga masiva imagenes - Dropzone -->
<div class="form-group col-sm-6 col-sm-offset-3" id="div-dropzone">
    {!! Form::label('images_quantity', 'Imaggini') !!}

    {{-- imagenes ya cargadas --}}
    @if(!empty($filesDB) && !empty($object))
        @include('admin.modal_galery.galery', [
            'galery' => $object->galeryImages(),
            'ruta' => 'admin.modal_galery.galery.deleteGalery',
            'language' => $language,
            'id' => $object->id
        ])
    @endif

    {{-- dropzone para cargar las fotos --}}
    <form id="image_jumperr" action="{{ route('admin.modal_galery.galery.saveGalery') }}" class="dropzone">
        {{ csrf_field() }}
    </form>
</div>

<input type="hidden" id="route_model_index" value="{{ route('admin.products.index') }}">
<input type="hidden" id="images_quantity" name="images_quantity" value="0" form="{{ 'form-product' }}">
<!-- end::Carga masiva imagenes - Dropzone -->

@push('scripts')
    <script src="{{ asset('admin/js/ProductForm.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            var myDropzone;

            Dropzone.options.imageJumperr = {
                autoProcessQueue: false,
                // autoQueue: false,
                addRemoveLinks: true,
                parallelUploads: 1000,

                // traducciones
                dictCancelUpload: '123',
                dictCancelUploadConfirmation: 'asd',
                dictRemoveFile: 'Rimuovi il file',
                capture: 'camera',
                dictDefaultMessage: 'Rilascia qui i file da caricare.',

                paramName: "image_jumperr", // The name that will be used to transfer the file
                maxFilesize: 15, // MB
                accept: function(file, done) {

                    var mimes = [
                        'image/gif',
                        'image/png',
                        'image/jpeg',
                        'image/bmp',
                        'image/webp',
                        'image/vnd.microsoft.icon'
                    ];

                    if ( mimes.indexOf(file.type) == -1 ) {
                        done("Formato non consentito.");
                    }
                    else {
                        done();
                    }
                },

                init: function (e) {
                    myDropzone = this;

                    ProductForm.storeProduct( myDropzone );
                },

                success: function(file, response) {
                    location.href = $('#route_model_index').val() + '?m=1';
                }
            };
        });
    </script>
@endpush