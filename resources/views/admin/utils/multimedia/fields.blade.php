<div class="row">
    <!-- Name Field - Module -->
    <div class="form-group col-sm-6 col-sm-offset-3">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::select('name', $modules, null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    {!! Form::open(['route' => 'admin.multimedia.store', 'class' => "dropzone form-group col-sm-6 col-sm-offset-3", "id" => "multimedia-dropzone"]) !!}
    {!! Form::close() !!}
</div>

<div class="row">
    <!-- Submit Field -->
    <div class="form-group col-sm-6 col-sm-offset-3">
        {!! Form::submit('Save', ['class' => 'btn btn-primary submit-form']) !!}
        <a href="{!! route('admin.multimedia.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">
        $(function(){
            let name = document.querySelector("#name").value;
            Dropzone.options.multimediaDropzone = {
                parallelUploads: 100,
                maxFilesize: 4,
                acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg,.webp,.vnd.microsoft.icon",
                dictCancelUpload: 'Cancelar la subida',
                dictCancelUploadConfirmation: 'Desea cancelar la carga?',
                dictDefaultMessage: 'Arrastra los archivos que desea subir.',
                capture: 'camera',
                autoProcessQueue: false,
                dictResponseError: 'Server not Configured',
                url: '/api/storeMultimediaFront',
                init:function(){
                    var self = this;
                    // config
                    self.options.addRemoveLinks = true;
                    self.options.dictRemoveFile = 'Remover archivo';

                    //New file added
                    self.on("addedfile", function (file) {
                        // console.log('new file added ', file);
                    });

                    // Send file starts
                    self.on("sending", function (file, xhr, data) {
                        // console.log('upload started', file);
                        data.append("name", name);
                        $('.meter').show();
                    });
                  
                    // File upload Progress
                    self.on("totaluploadprogress", function (progress) {
                        // console.log("progress ", progress);
                        $('.roller').width(progress + '%');
                    });

                    self.on("queuecomplete", function (progress) {
                        $('.meter').delay(999).slideUp(999);
                    });
                  
                    // On removing file
                    self.on("removedfile", function (file) {
                        // console.log(file);
                    });

                    self.on("success", function (file, response) {
                        // console.log('success ', file);
                        // console.log('success ', response);
                    });

                    document.querySelector(".submit-form").addEventListener("click", function(e) {
                        e.preventDefault();
                        self.processQueue();
                    })
                }
            };

            // Dropzone.options.multimediaDropzone = {
            //     paramName: "file",
            //     maxFilesize: 10,
            //     url: '/api/storeMultimediaFront',
            //     uploadMultiple: true,
            //     parallelUploads: 5,
            //     maxFiles: 20,
            //     init: function() {
            //         var cd;
            //         this.on("success", function(file, response) {
            //             $('.dz-progress').hide();
            //             $('.dz-size').hide();
            //             $('.dz-error-mark').hide();
            //             console.log(response);
            //             console.log(file);
            //             cd = response;
            //         });
            //         this.on("addedfile", function(file) {
            //             var removeButton = Dropzone.createElement("<a href=\"#\">Remove file</a>");
            //             var _this = this;
            //             removeButton.addEventListener("click", function(e) {
            //                 e.preventDefault();
            //                 e.stopPropagation();
            //                 _this.removeFile(file);
            //                 var name = "largeFileName=" + cd.pi.largePicPath + "&smallFileName=" + cd.pi.smallPicPath;
            //                 $.ajax({
            //                     type: 'POST',
            //                     url: 'DeleteImage',
            //                     data: name,
            //                     dataType: 'json'
            //                 });
            //             });
            //             file.previewElement.appendChild(removeButton);
            //         });
            //     }
            // };
        })
    </script>
@endpush

{{--
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('width', 'Width:') !!}
    {!! Form::number('width', null, ['class' => 'form-control']) !!}
</div>

<!-- Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('height', 'Height:') !!}
    {!! Form::number('height', null, ['class' => 'form-control']) !!}
</div>

<!-- Size Field -->
<div class="form-group col-sm-6">
    {!! Form::label('size', 'Size:') !!}
    {!! Form::number('size', null, ['class' => 'form-control']) !!}
</div>

<!-- Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('path', 'Path:') !!}
    {!! Form::text('path', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.multimedia.index') !!}" class="btn btn-default">Cancel</a>
</div>
--}}