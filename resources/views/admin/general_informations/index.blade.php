@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'general_general_informations' ),
        'subtitle'      => tags( 'general_index' ),
        'icon'          =>   'fa fa-cogs',
        'breadcrumb'    => [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'general_general_informations' ),
            ]
        ]
    ] )
   <div class="block full">
       <div class="box box-primary">
           <div class="box-body">
               <div class="row" >
                    @include('admin.general_informations.fields')
               </div>
           </div>
       </div>
   </div>
@endsection