@if( @$user )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.users.update', ['id'=>$user->id]) }}" accept-charset="UTF-8" enctype="multipart/form-data">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.users.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
@endif

    @csrf
    <!-- Name Field -->
    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('name', tags( 'general_name' )) !!}

        @if(@$user)
            {!! Form::text('name', $user->name ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>

    <!-- LastName Field -->
    <div class="form-group {{$errors->has('lastname') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('lastname', tags( 'general_lastname' )) !!}

        @if(@$user)
            {!! Form::text('lastname', $user->lastname ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('lastname'))
            <span class="help-block">{{ $errors->first('lastname') }}</span>
        @endif
    </div>

    <!-- Email Field -->
    <div class="form-group {{$errors->has('email') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('email', tags( 'general_email' )) !!}

        @if(@$user)
            {!! Form::text('email', $user->email ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('email'))
            <span class="help-block">{{ $errors->first('email') }}</span>
        @endif
    </div>
    @if(! @$user)
        <!-- Password Field -->
        <div class="form-group {{$errors->has('password') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
            {!! Form::label('password', tags( 'general_password' )) !!}

            {!! Form::password('password', ['class' => 'form-control']) !!}

            @if ($errors->has('password'))
                <span class="help-block">{{ $errors->first('password') }}</span>
            @endif
        </div>

        <!-- Password Field -->
        <div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
            {!! Form::label('password_confirmation', tags( 'general_password_confirmation' )) !!}

            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}

            @if ($errors->has('password_confirmation'))
                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
            @endif
        </div>
    @endif
    {{-- {{ dd( $role ) }} --}}
    <!-- Roles Field -->
        <div class="form-group {{$errors->has('role_id') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
            {!! Form::label('role_id', 'Roli ') !!}
            {{-- {!! Form::select('role_id', $role,  @$user->role_id,['class' => 'form-control', 'placeholder' => tags( 'general_select' )]) !!} --}}

            <select name="role_id" id="role_id" class="form-control">
                <option value="0" disabled="">{{ tags( 'general_select' ) }}</option>
                @foreach ( $role as $id => $rol )
                    <option value="{{ $id }}" {{ $id == $user->roles[0]->id ? 'selected' : '' }}>{{ $rol }}</option>
                @endforeach
            </select>

            @if ($errors->has('role_id'))
                <span class="help-block">{{ $errors->first('role_id') }}</span>
            @endif
        </div>
    {{-- @include('admin.users.users.search') --}}
</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model', 'id' => 'submit' ]) !!}
    <a href="{!! route('admin.users.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
@endpush