<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="users-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'general_name' ) }}</th>
            {{-- <th>{{ tags( 'general_lastname' ) }}</th> --}}
            <th>{{ tags( 'general_email' ) }}</th>
            <th>{{ tags( 'back_module_users_status_title' ) }}</th>
            <th>{{ tags( 'back_module_users_tipo_title' ) }}</th>

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
  {{--   {{ dd($users) }} --}}
    @foreach ( $users as $user )

        <tr class="{{ $user->active ? '' : 'inactive-user'}}"
            title="{{$user->active ? tags('general_status_active') : tags('general_status_inactive')}}">
            <td class="text-center">{!! $user->id !!}</td>
            <td>{!! $user->name !!} {!! $user->lastname !!}</td>
            {{-- <td>{!! $user->lastname !!}</td> --}}
            <td>{!! $user->email !!}</td>
            <td>
                @if ($user->active )
                    {{ tags( 'general_status_active' ) }}
                @else
                    Non confermato
                @endif
            </td>
            <td>
                @foreach($user->roles as $role)
                    {!! $role->name !!}
                @endforeach
            </td>

            <td class="text-center">
                {!! Form::open(['route' => ['admin.users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{{ route('admin.users.edit', $user->id) }}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                </div>
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
@push('css')
    <style type="text/css">
        .inactive-user {
            color: red;
        }
    </style>
@endpush
