@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_module_users_user_title' ),
        'subtitle'      => tags( 'general_index' ),
        'icon'          =>   'fa fa-cogs',
        'breadcrumb'    => [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_module_users_user_title' ),
            ]
        ]
    ] )
    @include('admin.users.users.table')
@endsection

@push('scripts')
    <script type="text/javascript">
        $('.permissionstorole').on('click', function() {
            $(':checkbox').prop('checked',false);
            $('.icheckbox_square-blue').removeClass('checked');
            var input = $(this).find('.role_slug').val();
            $.get('/lookuppermissionsofrole/'+input, function(data, status){
                console.log(data)
                if(data){
                    $.each(data, function( index, value ) {
                        if($('#permissionstorole'+value).val()==value){
                            $('#permissionstorole'+value).prop('checked',true);
                            $('#permissionstorole'+value).parent().addClass('checked');
                        }
                    });
                }
            });
        });

        $('.searchuser').on('click', function(event) {
            var data = {email: $(this).data('email'),
                        view: $(this).data('view'),
                        _token: $('meta[name="csrf-token"]').attr('content')},
                url  = '/searchuser';

            var send = $.post( url, data);

            send.done(function( data ) {
                $('#render-result').empty().append(data);
            });
        });
    </script>
@endpush

@push('css')
    <style type="text/css">
        .pointer {cursor: pointer;}
    </style>
@endpush

