@extends('layouts.admin.app')

@section('content')
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_module_users_user_title' ),
       'subtitle'      =>   tags( 'general_edit' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_module_users_user_title' ),
                'route' => 'admin.users.index'
            ],
            [
                'title' => tags( 'general_edit' ),
            ]
        ]
    ] )

    <div class="block full">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">

                        @include( 'admin.users.users.fields' )

                </div>
            </div>
        </div>
    </div>
@endsection