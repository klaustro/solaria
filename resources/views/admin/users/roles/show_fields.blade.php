@php $module = null; @endphp
<div class="row">
    <div class="form-group col-sm-6 col-sm-offset-3 ">
        <!-- Id Field -->
        <div class="form-group">
            {!! Form::label('name', 'Nome:') !!}
            {!! $role->name !!}
        </div>
    </div>
    <div class="form-group col-sm-6 col-sm-offset-3">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('description', 'Descrizione:') !!}
            {!! $role->description !!}
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="text-center">
            {{-- <a href="{!! route('admin.permission.to.role', $role->id) !!}" class="btn btn-default">Agregar Permisos</a> --}}
            <a href="{!! route('admin.role.index') !!}" class="btn btn-default">{{ tags('general_back') }}</a>
        </div>
    </div>
</div>


<div class="row">
@if(!$role->permissionRoles->isEmpty())
    @foreach($role->permissionRoles as $permission)
        <div class="col-sm-6 col-sm-offset-3">
            @if($permission->permission->module->id != $module)
                @php $module = $permission->permission->module->id; @endphp
                <h3 class="text-center">{{ $permission->permission->module->name }}</h3>
                <hr />
            @endif
            <!-- Name Field -->
            <div class="form-group">
                {!! Form::label('permission', 'Permission:') !!}
                {!! $permission->permission->name !!}
            </div>
        </div>
    @endforeach
@endif
</div>