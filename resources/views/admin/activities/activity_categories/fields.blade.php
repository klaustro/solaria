
@if( @$activityCategory )
    <form id="myDropzone" name="myDropzone" method="POST" class="dropzone" action="{{ route('admin.activityCategories.update', ['activityCategories'=>$activityCategory->id]) }}" accept-charset="UTF-8" enctype="multipart/form-data">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="myDropzone" name="myDropzone" method="POST" class="dropzone" action="{{ route('admin.activityCategories.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
@endif

    @csrf

    <!-- Language Id Field -->
    <div class="form-group col-sm-6 col-sm-offset-3 text-center">
        <h1>{!! tags( 'general_language' ).': <b>'.$translation->name.'</b>' !!}</h1>
        {!! Form::hidden('language_id', $translation->id) !!}
    </div>

    <!-- Name Field -->
    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('name', tags( 'general_name' )) !!}

        @if(@$activityCategory)
            {!! Form::text('name', $activityCategory->itemByLanguage($translation->code)->name ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>

    <!-- Description Field -->
    {{-- <div class="form-group {{$errors->has('description') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('description', tags( 'general_description' )) !!}

        @if(@$activityCategory)
            {!! Form::textarea('description', @$activityCategory->itemByLanguage($translation->code)->description, ['class' => 'form-control']) !!}
        @else
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('description'))
            <span class="help-block">{{ $errors->first('description') }}</span>
        @endif
    </div> --}}
        <!-- Information -->
    <div class="col-sm-6 col-sm-offset-3" >
        <label>Informazioni</label>
        <ul>
            <li>Dimensione massima dell'immagine: 1 MB</li>
            <li>Dimensione raccomandata in pixel: 1080 x 720</li>
            <li>Caricamento di immagini 10 in 10</li>
            <li>
                Utilizzato questo link per comprimere le tue immagini: <a href="https://imagecompressor.com/"  target="_blank">https://imagecompressor.com</a>
            </li>
        </ul>
    </div>

    <!-- Single Images view -->
    @include('admin.images.index', [
        'model' => @$activityCategory,
        'url' => '/admin/activityCategories',
        'single_select' => true,
        'form' => isset($activityCategory) ? $activityCategory->itemByLanguage($translation->code)->name ?? '' : '',
    ])


</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'myDropzone', 'id' => 'submit' ]) !!}
    <a href="{!! route('admin.activityCategories.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>
