<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="activities-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'general_title' ) }}</th>
            {{-- <th>{{ tags( 'general_subtitle' ) }}</th> --}}
            {{-- <th>{{ tags( 'general_description' ) }}</th> --}}
            <th>{{ tags( 'back_activities_activity_categories_title' ) }}</th>
            {{-- <th>{{ tags( 'back_activities_activity_slug' ) }}</th> --}}
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            @include('utils.language.add_table_th')

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $activities as $activity )
        <tr>
            <td class="text-center">{!! $activity->id !!}</td>
            <td>{!! $activity->title !!}</td>
            {{-- <td>{!! $activity->subtitle !!}</td> --}}
            {{-- <td>{!! $activity->description !!}</td> --}}
            <td>{!! $activity->activity_category_name !!}</td>
            {{-- <td>{!! $activity->slug !!}</td> --}}
            {{-- <td>{!! $activity->status->name !!}</td> --}}

            @include('utils.language.add_table_tbody', [
                'translations' => $activity->translations,
                'id' => $activity->id,
                'route' => 'admin.activities.edit'
            ])

            <td class="text-center">
                {!! Form::open(['route' => ['admin.activities.destroy', $activity->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
