<experience-form
    data="{{ isset($activity) ? json_encode($activity) : null }}"
    :translation="{{ json_encode($translation) }}"
    :categories="{{ @$activity_categories }}"
    :allies="{{ @$allies }}"
    title="{{ isset($activity) ? $activity->itemByLanguage($translation->code)->title : '' }}"
    description="{{ isset($activity) ? $activity->itemByLanguage($translation->code)->description : '' }}"
/>