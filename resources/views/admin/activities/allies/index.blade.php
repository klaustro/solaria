@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_activities_allies_title' ),
        'subtitle'      => tags( 'general_index' ),
        'button'        => tags( 'general_addnew' ),
        'route'         => 'admin.allies.create',
        'breadcrumb'    => [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_activities_allies_title' ),
            ]
        ]
    ] )
    @include('admin.activities.allies.table')
@endsection

