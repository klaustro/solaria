<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="allies-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'general_name' ) }}</th>
            <th>{{ tags( 'general_address' ) }}</th>
            <th>{{ tags( 'general_phone' ) }}</th>
            <th>{{ tags( 'general_email' ) }}</th>
            <th>{{ tags( 'general_website' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $allies as $ally )
        <tr>
            <td class="text-center">{!! $ally->id !!}</td>
            <td>{!! $ally->name !!}</td>
            <td>{!! $ally->address !!}</td>
            <td>{!! $ally->phone !!}</td>
            <td>{!! $ally->email !!}</td>
            <td>{!! $ally->website !!}</td>
            {{-- <td>{!! $ally->status->name !!}</td> --}}

            <td class="text-center">
                {!! Form::open(['route' => ['admin.allies.destroy', $ally->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{{ route('admin.allies.edit', $ally->id) }}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                </div>
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
