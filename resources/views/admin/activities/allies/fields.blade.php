@if( @$ally )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.allies.update', ['id'=>$ally->id]) }}" accept-charset="UTF-8" enctype="multipart/form-data">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.allies.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
@endif

    @csrf

    <!-- Name Field -->
    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('name', tags( 'general_name' )) !!}

        @if(@$ally)
            {!! Form::text('name', $ally->name ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>


    <!-- Address Field -->
    <div class="form-group {{$errors->has('address') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('address', tags( 'general_address' )) !!}

        @if(@$ally)
            {!! Form::textarea('address', $ally->address ?? '', ['class' => 'form-control', 'rows' => '3']) !!}
        @else
            {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('address'))
            <span class="help-block">{{ $errors->first('address') }}</span>
        @endif
    </div>

    <!-- Phone Field -->
    <div class="form-group {{$errors->has('phone') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('phone', tags( 'general_phone' )) !!}

        @if(@$ally)
            {!! Form::text('phone', $ally->phone ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('phone'))
            <span class="help-block">{{ $errors->first('phone') }}</span>
        @endif
    </div>

    <!-- Email Field -->
    <div class="form-group {{$errors->has('email') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('email', tags( 'general_email' )) !!}

        @if(@$ally)
            {!! Form::text('email', $ally->email ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('email'))
            <span class="help-block">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <!-- Website Field -->
    <div class="form-group {{$errors->has('website') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('website', tags( 'general_website' )) !!}

        @if(@$ally)
            {!! Form::text('website', $ally->website ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('website', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('website'))
            <span class="help-block">{{ $errors->first('website') }}</span>
        @endif
    </div>
</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model', 'id' => 'submit' ]) !!}
    <a href="{!! route('admin.allies.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
@endpush