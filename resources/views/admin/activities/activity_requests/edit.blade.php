@extends('layouts.admin.app')

@section('content')
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_activities_activity_requests_title' ),
       'subtitle'      =>   tags( 'general_edit' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_activities_activity_requests_title' ),
                'route' => 'admin.activityRequests.index'
            ],
            [
                'title' => tags( 'general_edit' ),
            ]
        ]
    ] )

    <div class="block full">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">

                        @include( 'admin.activities.activity_requests.fields' )

                </div>
            </div>
        </div>
    </div>
@endsection