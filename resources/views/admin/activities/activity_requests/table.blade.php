<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="activity_requests-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'general_name' ) }}</th>
            <th>{{ tags( 'general_phone' ) }}</th>
            <th>{{ tags( 'general_email' ) }}</th>
            <th>{{ tags( 'back_activities_activity_title' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}
            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $activityRequests as $activity_request )
        <tr
            class="{{ $activity_request->status_id == 6 ? 'font-weight-bold' : ''}}"
            title="{{ tags( $activity_request->status->name ) }}"
        >
            <td class="text-center">{!! $activity_request->id !!}</td>
            <td>{!! $activity_request->name !!}</td>
            <td>{!! $activity_request->phone !!}</td>
            <td>{!! $activity_request->email !!}</td>
            <td>{!! $activity_request->activity->title !!}</td>
            {{-- <td>{!! $activity_request->status->name !!}</td> --}}
            <td class="text-center">
                {!! Form::open(['route' => ['admin.activityRequests.destroy', $activity_request->id], 'method' => 'delete']) !!}

                <div class='btn-group'>
                    <a href="{{ route('admin.activityRequests.edit', $activity_request->id) }}" class='btn btn-success btn-xs'><i class="fa fa-eye"></i></a>
                </div>
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
@push('css')
    <style type="text/css">
        .font-weight-bold {
            font-weight: bold;
        }
    </style>
@endpush

