@extends('layouts.admin.app')

@section('content')
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_activities_activity_requests_title' ),
        'subtitle'      => tags( 'general_index' ),
        'icon'        => 'fa fa-envelope',
        'breadcrumb'    => [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_activities_activity_requests_title' ),
            ]
        ]
    ] )
    @include('admin.activities.activity_requests.table')
@endsection

