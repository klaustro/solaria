@if( @$activityRequest )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.activityRequests.update', ['activities'=>$activityRequest->id]) }}" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.activityRequests.store') }}" accept-charset="UTF-8">
@endif

    @csrf

    <!-- Name Field -->
    <div class="form-group {{$errors->has('activity_title') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('activity_title', tags( 'back_activities_activity_title' )) !!}

        @if(@$activityRequest)
            {!! Form::text('activity_title', $activityRequest->activity_title ?? '', ['class' => 'form-control', 'disabled' => '']) !!}
        @else
            {!! Form::text('activity_title', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('activity_title'))
            <span class="help-block">{{ $errors->first('activity_title') }}</span>
        @endif
    </div>

    <!-- Name Field -->
    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('name', tags( 'general_name' )) !!}

        @if(@$activityRequest)
            {!! Form::text('name', $activityRequest->name ?? '', ['class' => 'form-control', 'disabled' => '']) !!}
        @else
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>

    <!-- Phone Field -->
    <div class="form-group {{$errors->has('phone') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('phone', tags( 'general_phone' )) !!}

        @if(@$activityRequest)
            {!! Form::text('phone', $activityRequest->phone ?? '', ['class' => 'form-control', 'disabled' => '']) !!}
        @else
            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('phone'))
            <span class="help-block">{{ $errors->first('phone') }}</span>
        @endif
    </div>

    <!-- Email Field -->
    <div class="form-group {{$errors->has('email') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('email', tags( 'general_email' )) !!}

        @if(@$activityRequest)
            {!! Form::text('email', $activityRequest->email ?? '', ['class' => 'form-control', 'disabled' => '']) !!}
        @else
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('email'))
            <span class="help-block">{{ $errors->first('email') }}</span>
        @endif
    </div>

    <!-- Adults Field -->
    <div class="form-group {{$errors->has('adult') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('adult', tags( 'general_adult' )) !!}

        @if(@$activityRequest)
            {!! Form::text('adult', $activityRequest->adult ?? '', ['class' => 'form-control', 'disabled' => '']) !!}
        @else
            {!! Form::text('adult', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('adult'))
            <span class="help-block">{{ $errors->first('adult') }}</span>
        @endif
    </div>

    <!-- Children Field -->
    <div class="form-group {{$errors->has('children') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('children', tags( 'general_children' )) !!}

        @if(@$activityRequest)
            {!! Form::text('children', $activityRequest->children ?? '', ['class' => 'form-control', 'disabled' => '']) !!}
        @else
            {!! Form::text('children', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('children'))
            <span class="help-block">{{ $errors->first('children') }}</span>
        @endif
    </div>

    <!-- Pets Field -->
    <div class="form-group {{$errors->has('pets') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('pets', tags( 'general_pets' )) !!}

        @if(@$activityRequest)
            {!! Form::text('pets', $activityRequest->pets ?? '', ['class' => 'form-control', 'disabled' => '']) !!}
        @else
            {!! Form::text('pets', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('pets'))
            <span class="help-block">{{ $errors->first('pets') }}</span>
        @endif
    </div>

    <!-- Message Field -->
    <div class="form-group {{$errors->has('message') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('message', tags( 'general_message' )) !!}

        @if(@$activityRequest)
            {!! Form::textarea('message', @$activityRequest->message, ['class' => 'form-control', 'disabled' => '']) !!}
        @else
            {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('message'))
            <span class="help-block">{{ $errors->first('description') }}</span>
        @endif
    </div>

</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    <a href="{!! route('admin.activityRequests.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
@endpush