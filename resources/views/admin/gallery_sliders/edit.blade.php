@extends( 'layouts.admin.app' )
@section( 'content' )
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_module_gallery_sliders_title' ),
        'subtitle'      => tags( 'general_edit' ),
        'icon'          => 'fa fa-cogs',
        'breadcrumb'    => array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_module_gallery_sliders_title' ),
                'route' => 'admin.gallerySliders.index'
            ),
            array (
                'title' => tags( 'general_edit' ),
            ),
        )
    ] )
    @include( 'adminlte-templates::common.errors' )
   <div class="block full">
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                   {{-- {!! Form::model( $gallerySlider, [ 'route' => [ 'admin.gallerySliders.update', $gallerySlider->id ], 'method' => 'patch' ] ) !!} --}}

                        @include( 'admin.gallery_sliders.fields' )

                   {{-- {!! Form::close() !!} --}}
                </div>
            </div>
        </div>
    </div>
@endsection