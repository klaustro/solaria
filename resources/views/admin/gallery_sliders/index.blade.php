@extends( 'layouts.admin.app' )

@section( 'content' )
    @include( 'layouts.admin.partials.dashboard-header-top', [
        'title'         => tags( 'back_module_gallery_sliders_title' ),
        'subtitle'      => tags( 'general_index' ),
        'icon'          => 'fa fa-cogs',
        'breadcrumb'    => array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_module_gallery_sliders_title' ),
            ),
        ),
    ] )

    @include( 'flash::message' )
    <div class="row">
        <div class="widget">
             @include( 'admin.gallery_sliders.table' )
        </div>
    </div>
@endsection