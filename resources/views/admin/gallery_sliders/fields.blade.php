<gallery-slider-form
    default-message="Rilascia qui i file da caricare <br> <i class='fa fa-2x fa-cloud-upload text-muted'></i>"
    data="{{ isset( $gallerySlider ) ? json_encode( $gallerySlider ) : null }}"
    translation="{{ json_encode( $translation ) }}"
    language="{{ tags( 'general_language' ) }}"
/>

@push( 'scripts' )
    <script>
    </script>
@endpush