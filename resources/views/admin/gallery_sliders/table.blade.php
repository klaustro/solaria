<table class="table table-responsive tableGeneral" id="gallerySliders-table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>{{ tags( 'general_code' ) }}</th>
            <th>{{ tags( 'general_title' ) }}</th>

            {{-- @include( 'utils.language.add_table_th' ) --}}

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach ( $gallerySliders as $gallerySlider )
        <tr>
            <td class="text-center">{!! $gallerySlider->id !!}</td>
            <td>{!! $gallerySlider->code !!}</td>
            <td>{!! $gallerySlider->title !!}</td>

            {{-- @include( 'utils.language.add_table_tbody', [
                'translations' => $gallerySlider->translations,
                'id' => $gallerySlider->id,
                'route' => 'admin.gallerySliders.edit'
            ] ) --}}

            <td class="text-center">
                {!! Form::open( [ 'route' => [ 'admin.gallerySliders.destroy', $gallerySlider->id ], 'method' => 'delete' ] ) !!}
                <div class='btn-group'>
                    <a href="{!! route( 'admin.gallerySliders.edit', [ 'it', $gallerySlider->id ] ) !!}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                    {!! Form::button( '<i class="fa fa-trash"></i>', [ 'type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm( 'Sei sicuro?' )" ] ) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@push( 'scripts' )
    {{-- Para inicializar el datatable --}}
    <script>
        $( function () {
            TablesDatatables.tableGeneral();
        } );
    </script>
@endpush
