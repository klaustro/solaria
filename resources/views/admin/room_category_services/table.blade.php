<div class="table-responsive" style="margin-bottom: 30px;">
    <table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="roomsServices-table">
        <thead>
            <tr>
                <th class="text-center">{{ tags( 'general_id' ) }}</th>
                <th>{{ tags( 'back_rooms_room_category_title' ) }}</th>
                <th>{{ tags( 'back_menu_services' ) }}</th>
                <th>categoria</th>
                <th>{{ tags( 'general_value' ) }}</th>
                <th class="text-center">{{ tags( 'general_action' ) }}</th>
            </tr>
        </thead>
        <tbody class="widget-extra-full">
        @foreach($roomCategoryServices as $roomCategoryService)
            <tr>
                <td class="text-center">{!! $roomCategoryService->id !!}</td>
                <td>{!! $roomCategoryService->roomCategories->name !!}</td>
                <td>{!! $roomCategoryService->service->name !!}</td>
                <td>{!! $roomCategoryService->service->serviceCategory->name !!}</td>
                <td>{!! $roomCategoryService->info !!}</td>
                <td class="text-center">
                    {!! Form::open(['route' => ['admin.roomCategoryServices.destroy', $roomCategoryService->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('admin.roomCategoryServices.edit', [$roomCategoryService->id]) !!}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                        {{-- <a href="{!! route('admin.roomCategoryServices.edit', [$roomCategoryService->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a> --}}
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
