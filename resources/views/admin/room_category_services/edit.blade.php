@extends('layouts.admin.app')
@section('content')
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_rooms_room_category_service_title' ),
       'subtitle'      =>   tags( 'general_edit' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_rooms_room_category_service_title' ),
                'route' => 'admin.roomCategoryServices.index'
            ],
            [
                'title' => tags( 'general_edit' ),
            ]
        ]
    ] )

<div class="block full">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">

                        @include('admin.room_category_services.fields')

                </div>
            </div>
        </div>
    </div>
@endsection