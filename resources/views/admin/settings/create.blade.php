@extends('layouts.admin.app')
@section('content')
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_settings_setting_title' ),
       'subtitle'      =>   tags( 'general_create' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   array (
            array (
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ),
            array (
                'title' => tags( 'back_settings_setting_title' ),
                'route' => 'admin.settings.index'
            ),
            array (
                'title' => tags( 'general_create' ),
            )
        )
    ] )

    <div class="block full">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">

                  @include('admin.settings.fields')

                </div>
            </div>
        </div>
    </div>
@endsection
