@if( @$setting )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.settings.update', ['settings'=>$setting->id]) }}" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.settings.store') }}" accept-charset="UTF-8">
@endif

    @csrf

    <!-- Name Field -->
    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('name', tags( 'general_name' )) !!}

        @if(@$setting)
            {!! Form::text('name', $setting->name ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>

    <!-- Key Field -->
    <div class="form-group {{$errors->has('key') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('key', tags( 'back_settings_setting_key' )) !!}

        @if(@$setting)
            {!! Form::text('key', @$setting->key, ['class' => 'form-control', 'readonly' => true,]) !!}
        @else
            {!! Form::text('key', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('key'))
            <span class="help-block">{{ $errors->first('key') }}</span>
        @endif
    </div>

    <!-- Value Field -->
    <div class="form-group {{$errors->has('value') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('value', tags( 'back_settings_setting_value' )) !!}

        @if(@$setting)
            {!! Form::text('value', @$setting->value, ['class' => 'form-control']) !!}
        @else
            {!! Form::text('value', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('value'))
            <span class="help-block">{{ $errors->first('value') }}</span>
        @endif
    </div>

</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model' ]) !!}
    <a href="{!! route('admin.settings.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
@endpush
