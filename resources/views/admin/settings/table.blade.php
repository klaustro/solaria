<div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="settings-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th>{{ tags( 'general_name' ) }}</th>
            <th>{{ tags( 'back_settings_setting_key' ) }}</th>
            <th>{{ tags( 'back_settings_setting_value' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $settings as $setting )
        <tr>
            <td class="text-center">{!! $setting->id !!}</td>
            <td>{!! $setting->name !!}</td>
            <td>{!! $setting->key !!}</td>
            <td>{!! $setting->value !!}</td>
            {{-- <td>{!! $setting->status->name !!}</td> --}}
            <td class="text-center">
                {!! Form::open(['route' => ['admin.settings.destroy', $setting->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{{ route('admin.settings.edit', $setting->id) }}" class='btn btn-success btn-xs' data-toggle='tooltip' data-placement='top' title='Modificare'><i class="fa fa-edit"></i></a>
                </div>
                {{-- <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div> --}}
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            TablesDatatables.tableGeneral();
        });
    </script>
@endpush
{{--
<table class="table table-responsive" id="settings-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Key</th>
        <th>Value</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($settings as $setting)
        <tr>
            <td>{!! $setting->name !!}</td>
            <td>{!! $setting->key !!}</td>
            <td>{!! $setting->value !!}</td>
            <td>
                {!! Form::open(['route' => ['admin.settings.destroy', $setting->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.settings.show', [$setting->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.settings.edit', [$setting->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
--}}
