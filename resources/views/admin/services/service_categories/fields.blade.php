@if( @$serviceCategory )
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.serviceCategories.update', ['serviceCategories'=>$serviceCategory->id]) }}" accept-charset="UTF-8">
    <input name="_method" type="hidden" value="PATCH">
@else
    <form id="form-model" name="form-model" method="POST" action="{{ route('admin.serviceCategories.store') }}" accept-charset="UTF-8">
@endif

    @csrf

    <!-- Language Id Field -->
    <div class="form-group col-sm-6 col-sm-offset-3 text-center">
        <h1>{!! tags( 'general_language' ).': <b>'.$translation->name.'</b>' !!}</h1>
        {!! Form::hidden('language_id', $translation->id) !!}
    </div>

    <!-- Name Field -->
    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('name', tags( 'general_name' )) !!}

        @if(@$serviceCategory)
            {!! Form::text('name', $serviceCategory->itemByLanguage($translation->code)->name ?? '', ['class' => 'form-control']) !!}
        @else
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('name'))
            <span class="help-block">{{ $errors->first('name') }}</span>
        @endif
    </div>

    <!-- Description Field -->
    <div class="form-group {{$errors->has('description') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
        {!! Form::label('description', tags( 'general_description' )) !!}

        @if(@$serviceCategory)
            {!! Form::textarea('description', @$serviceCategory->itemByLanguage($translation->code)->description, ['class' => 'form-control']) !!}
        @else
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        @endif

        @if ($errors->has('description'))
            <span class="help-block">{{ $errors->first('description') }}</span>
        @endif
    </div>

</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'form-model' ]) !!}
    <a href="{!! route('admin.serviceCategories.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>

@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
@endpush
