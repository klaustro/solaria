<div><div class="table-responsive" style="margin-bottom: 30px;">
<table class="table table-vcenter table-condensed table-bordered table-hover widget tableGeneral" id="services-table">
    <thead>
        <tr>
            <th class="text-center">{{ tags( 'general_id' ) }}</th>
            <th class="text-center">{{ tags( 'general_image' ) }}</th>
            <th>{{ tags( 'general_name' ) }}</th>
            <th>{{ tags( 'back_services_service_categories_title' ) }}</th>
            {{-- <th>{{ tags( 'general_status' ) }}</th> --}}

            @include('utils.language.add_table_th')

            <th class="text-center">{{ tags( 'general_action' ) }}</th>
        </tr>
    </thead>
    <tbody class="widget-extra-full">
    @foreach ( $services as $service )
        <tr>
            <td>{!! $service->id !!}</td>
            <td><img src="{!! $service->ico !!}" height="50"></td>
            <td style="    text-align: left;">{!! $service->name !!}</td>
            <td style="    text-align: left;">{!! $service->service_category_name !!}</td>
            {{-- <td>{!! $service->status->name !!}</td> --}}

            @include('utils.language.add_table_tbody', [
                'translations' => $service->translations,
                'id' => $service->id,
                'route' => 'admin.services.edit'
            ])

            <td class="text-center">
                {!! Form::open(['route' => ['admin.services.destroy', $service->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','data-toggle'=>'tooltip', 'data-placement'=>'top', 'title'=>'Elimina', 'onclick' => "return confirm('" . tags( 'general_confirm_ask' ) . "')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    {{-- Para inicializar el datatable --}}
    <script>
        $(function() {
            // TablesDatatables.tableGeneral();
        });
    </script>
@endpush
