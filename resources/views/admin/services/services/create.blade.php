@extends('layouts.admin.app')
@section('content')
   @include( 'layouts.admin.partials.dashboard-header-top', [
       'title'         =>   tags( 'back_services_service_title' ),
       'subtitle'      =>   tags( 'general_create' ),
       'icon'          =>   'fa fa-cogs',
       'breadcrumb'    =>   [
            [
                'title' => tags( 'general_home' ),
                'route' => 'home'
            ],
            [
                'title' => tags( 'back_services_service_title' ),
                'route' => 'admin.services.index'
            ],
            [
                'title' => tags( 'general_create' ),
            ]
        ]
    ] )

    <div class="block full">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">

                  @include('admin.services.services.fields')

                </div>
            </div>
        </div>
    </div>
@endsection