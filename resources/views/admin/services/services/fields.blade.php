@if( @$service )
    <form id="myDropzone" name="myDropzone" method="POST" class="dropzone" action="{{ route('admin.services.update', ['services'=>$service->id]) }}" accept-charset="UTF-8" enctype="multipart/form-data">
    <input name="_method" type="hidden" value="PATCH">
    @else
    <form id="myDropzone" name="myDropzone" method="POST" class="dropzone" action="{{ route('admin.services.store') }}" accept-charset="UTF-8" enctype="multipart/form-data">
@endif

@csrf

<!-- Language Id Field -->
<div class="form-group col-sm-6 col-sm-offset-3 text-center">
    <h1>{!! tags( 'general_language' ).': <b>'.$translation->name.'</b>' !!}</h1>
    {!! Form::hidden('language_id', $translation->id) !!}
</div>

<!-- ServiceCategory Field -->
<div class="form-group {{$errors->has('service_category_id') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
    {!! Form::label('service_category_id', tags( 'back_services_service_categories_title' )) !!}
    {!! Form::select('service_category_id', $service_categories,  @$service->service_category_id, ['class' => 'form-control', 'placeholder' => tags( 'general_select' )]) !!}
    @if ($errors->has('service_category_id'))
        <span class="help-block">{{ $errors->first('service_category_id') }}</span>
    @endif
    <span id="error-service_category_id" class="help-block" style="color:red"></span>
</div>

<!-- Name Field -->
<div class="form-group {{$errors->has('name') ? 'has-error' : ''}} col-sm-6 col-sm-offset-3">
    {!! Form::label('name', tags( 'general_name' )) !!}

    @if(@$service)
        {!! Form::text('name', $service->itemByLanguage($translation->code)->name ?? '', ['class' => 'form-control']) !!}
    @else
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @endif

    @if ($errors->has('name'))
        <span class="help-block">{{ $errors->first('name') }}</span>
    @endif
    <span id="error-name" class="help-block" style="color:red"></span>
</div>

<!-- Single Images view -->
@include('admin.images.index', [
    'model' => @$service,
    'url' => '/admin/services',
    'single_select' => true,
    'form' => isset($service) ? $service->itemByLanguage($translation->code)->name ?? '' : '',
    'quantity' => 1,
])

@if ( $errors->has( 'file' ) )
    <span class="help-block">{{ $errors->first( 'file' ) }}</span>
@endif

</form>

<!-- Submit Field -->
<div class="col-sm-6 col-sm-offset-3">
    {!! Form::submit( tags( 'general_save' ), ['class' => 'btn btn-primary', 'form' => 'myDropzone', 'id' => 'submit' ]) !!}
    <a href="{!! route('admin.services.index') !!}" class="btn btn-default">{{ tags( 'general_back' ) }}</a>
</div>
@push( 'scripts' )
    {{-- <script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script> --}}
@endpush
