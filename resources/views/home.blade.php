@extends('layouts.admin.app')

@section('content')
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-3 hidden-xs hidden-sm">
                    <h1>{{ tags( 'back_dashboard_greeting' ) }} <strong>{{ auth()->user()->name }}*</strong></h1>
                </div>
                <!-- END Main Title -->

                <!-- Top Stats -->
                <div class="col-md-9">
                    <div class="row text-center">
                        <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                <strong>{{ $users }}</strong><br>
                                <small><i class="fa fa-users"></i> {{ tags( 'back_dashboard_user' ) }}</small>
                            </h2>
                        </div>
                        <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                <strong>{{ $newsletterUser }}</strong><br>
                                <small><i class="fa fa-newspaper-o"></i> {{ tags( 'back_dashboard_newsletter' ) }}</small>
                            </h2>
                        </div>
                        <!-- We hide the last stat to fit the other 3 on small devices -->
                        <div class="col-sm-3 hidden-xs">
                            <h2 class="animation-hatch">
                                <strong>{{ $bookings }}</strong><br>
                                <small><i class="fa fa-credit-card"></i> {{ tags( 'back_bookings_bookings_title' ) }}</small>
                            </h2>
                        </div>
                        <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                <strong>{{ $unreadActivityRequests }}</strong><br>
                                <small><i class="fa fa-envelope"></i> {{ tags( 'back_dashboard_unread_activity_requests' ) }}</small>
                            </h2>
                        </div>
                    </div>
                </div>
                <!-- END Top Stats -->
            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="/images/playa.jpg" alt="header image" class="">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="{{ route('admin.users.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-users"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>{{ tags( 'back_dashboard_user' ) }}</strong><br>
                        <small>Total: {{ $users }}</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="{{ route('admin.newsletterUsers.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="fa fa-newspaper-o"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        <strong>{{ $newsletterUser }}</strong><br>
                        <small>{{ tags( 'back_dashboard_newsletter' ) }}</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="{{ route('admin.bookings.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                        <i class="fa fa-credit-card"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        {{ $bookings }} <strong>{{ tags( 'back_bookings_bookings_title' ) }}</strong>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="{{ route('admin.activityRequests.index') }}" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                        <i class="gi gi-envelope"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        {{ $unreadActivityRequests }} <strong>{{ tags( 'back_dashboard_new_messages' ) }}</strong>
                        <small>{{ tags( 'back_dashboard_activity_requests' ) }}</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="row">
            <custom-calendar type="price"></custom-calendar>
        </div>
    </div>
    <!-- END Mini Top Stats Row -->
</div>
<!-- END Page Content -->
@endsection