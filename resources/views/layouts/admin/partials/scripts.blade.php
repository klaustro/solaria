<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="{{ asset('admin/js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/js/plugins.js') }}"></script>
<script src="{{ asset('admin/js/app.js') }}"></script>

<!-- Google Maps API Key (you will have to obtain a Google Maps API key to use Google Maps) -->
<!-- For more info please have a look at https://developers.google.com/maps/documentation/javascript/get-api-key#key -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key="></script> -->
<script src="{{ asset('admin/js/helpers/gmaps.min.js') }}"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="{{ asset('admin/js/pages/index.js') }}"></script>
<script src="{{ asset('admin/js/helpers/ckeditor/ckeditor.js') }}"></script>

<!-- iCheck -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });


        $('.app_logout').on('click', function(e) {
            $.ajax({
                type: "POST",
                url: '/logout',
                data: $(this).serialize(),

                success: function( response ) {
                    window.location.href = '/';
                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( jqXHR );
                    console.log( textStatus );
                    console.log( errorThrown );
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    });
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="{{ asset('admin/js/pages/tablesDatatables.js') }}"></script>
<script>$(function(){ TablesDatatables.init(); });</script>

<!-- Vue Ctk Date Time Picker -->
<script src="https://cdn.jsdelivr.net/npm/vue-ctk-date-time-picker@2.0.9/dist/vue-ctk-date-time-picker.umd.min.js"></script>

<!-- Vue -->
<script src="{{ asset('admin/js/main.js') }}"></script>

@stack('scripts')

<!-- para los alerts -->
<script type="text/javascript">
	function alerts(growlType, message){
		console.info(growlType+' '+message);
		if(growlType != null){
		    $.bootstrapGrowl(message, {
		        type: growlType,
		        delay: 2500,
		        allow_dismiss: true
		    });
		}
	};
</script>

<!-- para los botones de submit -->
<script type="text/javascript">
    $( document ).ready( function () {

        jQuery.fn.extend({ getPath: function() { var pathes = []; this.each(function(index, element) { var path, $node = jQuery(element); while ($node.length) { var realNode = $node.get(0), name = realNode.localName; if (!name) { break; } name = name.toLowerCase(); var parent = $node.parent(); var sameTagSiblings = parent.children(name); if (sameTagSiblings.length > 1) { allSiblings = parent.children(); var index = allSiblings.index(realNode) +1; if (index > 0) { name += ':nth-child(' + index + ')'; } } path = name + (path ? ' > ' + path : ''); $node = parent; } pathes.push(path); }); return pathes.join(','); } });

        var submitBtnSame = $( 'input[type=submit]' );
        var form = $( 'form:not(#profile_user)' );

        var cssForms = form.getPath();

        $( document ).on( 'submit', cssForms, function () {
            // iniciar modo Loading...
            NProgress.start();
            submitBtnSame.button( 'loading' );
        } );

        // detener modo Loading...
        // $('html, body').animate( { scrollTop: 0 }, 1000 );
        // NProgress.done();
        // submitBtnSame.button( 'reset' );
    } );
</script>

@push('scripts')
    <script type="text/javascript">

    </script>
@endpush