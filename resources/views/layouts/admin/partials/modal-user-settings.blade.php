<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"><i class="fa fa-pencil"></i> Impostazioni</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                {{-- {!! Form::open(['url' => ['/api/profile', Auth::user()->id], 'class' => 'form-horizontal form-bordered user-form', 'id' => 'profile_user']) !!} --}}
                <form id="profile_user" method="POST" action="{{ url( '/api/profile', [ Auth::user()->id ] ) }}" accept-charset="UTF-8" class="form-horizontal form-bordered user-form" enctype="multipart/form-data">
                    @csrf

                    <!-- Block Tabs -->
                    <div class="block">
                        <!-- Block Tabs Title -->
                        <div class="block-title">
                            <ul class="nav nav-tabs" data-toggle="tabs" style="margin: 0; padding: 0;">
                                <li class="active"><a href="#user_user">Utente</a></li>
                                <li><a href="#user_photo">Immagine</a></li>
                                <li><a href="#user_password">Password</a></li>
                                <li><a href="#user_details">Dettagli</a></li>
                                {{-- <li><a href="#user_themes">Temi</a></li> --}}
                            </ul>
                        </div>
                        <!-- END Block Tabs Title -->

                        <!-- Tabs Content -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="user_user">
                                <fieldset>
                                    <legend>Informazioni Generali</legend>
                                    <div class="form-group col-sm-6 {{$errors->has('name') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label">Nome</label>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="name" name="name" class="form-control" value="{{ Auth::user()->name }}">
                                            @if ($errors->has('name'))
                                                <span class="help-block">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6 {{$errors->has('lastname') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label">Cognome</label>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="lastname" name="lastname" class="form-control" value="{{ Auth::user()->lastname }}">
                                            @if ($errors->has('lastname'))
                                                <span class="help-block">{{ $errors->first('lastname') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-12 {{$errors->has('email') ? 'has-error' : ''}}">
                                        <label class="col-md-2 control-label" for="user-settings-email">Email</label>
                                        <div class="col-md-10">
                                            <input type="email" autocomplete="email" name="email" class="form-control" value="{{ Auth::user()->email }}">
                                            @if ($errors->has('email'))
                                                <span class="help-block">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="tab-pane" id="user_photo">
                                <fieldset>
                                    <legend>Immagine del profilo</legend>
                                    <div class="form-group {{ $errors->has('user_image') ? 'has-error' : '' }}">
                                        <label class="col-md-4 control-label" for="user_image">Nuova Immagine</label>
                                        <div class="col-md-8">
                                            <input type="file" id="user_image" />

                                            <input type="hidden" name="image[base64]" id="b64">
                                            <input type="hidden" name="image[filename]" id="filename">

                                            <img id="img" height="150" src="{{ Auth::user()->image }}">

                                            @if ( $errors->has( 'user_image' ) )
                                                <span class="help-block">{{ $errors->first( 'user_image' ) }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="tab-pane" id="user_password">
                                <fieldset>
                                    <legend>Aggiornamento della password</legend>
                                    <div class="form-group {{$errors->has('actual') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label" for="current-password">Corrente Password</label>
                                        <div class="col-md-8">
                                            <input id="current-password" type="password" autocomplete="off" name="actual" class="form-control">
                                            @if ($errors->has('actual'))
                                                <span class="help-block">{{ $errors->first('actual') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label" for="new-password">Nuova Password</label>
                                        <div class="col-md-8">
                                            <input id="new-password" type="password" autocomplete="off" name="password" class="form-control" placeholder="Si prega di scegliere uno complesso ..">
                                            @if ($errors->has('password'))
                                                <span class="help-block">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label" for="confirm-password">Conferma la nuova password</label>
                                        <div class="col-md-8">
                                            <input id="confirm-password" type="password" autocomplete="off" name="password_confirmation" class="form-control" placeholder="..e confermalo!!">
                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="tab-pane" id="user_details">
                                <fieldset>
                                    <legend>Dettagli Utente</legend>
                                    <div class="form-group col-sm-6 {{$errors->has('phone') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label">Telefono</label>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="off" name="phone" class="form-control" value="{{ !isset(Auth::user()->userDetails->phone) ? old('phone') : Auth::user()->userDetails->phone }}">
                                            @if ($errors->has('phone'))
                                                <span class="help-block">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6 {{$errors->has('fiscalCode') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label">CAP</label>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="off" name="fiscalCode" class="form-control" value="{{ !isset(Auth::user()->userDetails->fiscal_code) ?  old('fiscalCode') : Auth::user()->userDetails->fiscal_code }}">
                                            @if ($errors->has('fiscalCode'))
                                                <span class="help-block">{{ $errors->first('fiscalCode') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6 {{$errors->has('azienda') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label">Società</label>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="off" name="azienda" class="form-control" value="{{ !isset(Auth::user()->userDetails->empresa) ?  old('azienda') : Auth::user()->userDetails->empresa }}">
                                            @if ($errors->has('azienda'))
                                                <span class="help-block">{{ $errors->first('azienda') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6 {{$errors->has('viaNum') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label">Número Civico</label>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="off" name="viaNum" class="form-control" value="{{ !isset(Auth::user()->userDetails->num_civic) ?  old('viaNum') : Auth::user()->userDetails->num_civic }}">
                                            @if ($errors->has('viaNum'))
                                                <span class="help-block">{{ $errors->first('viaNum') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6 {{$errors->has('province') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label">Provincia</label>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="off" name="province" class="form-control" value="{{ !isset(Auth::user()->userDetails->province) ?  old('province') : Auth::user()->userDetails->province }}">
                                            @if ($errors->has('province'))
                                                <span class="help-block">{{ $errors->first('province') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6 {{$errors->has('city') ? 'has-error' : ''}}">
                                        <label class="col-md-4 control-label">Città</label>
                                        <div class="col-md-8">
                                            <input type="text" autocomplete="off" name="city" class="form-control" value="{{ !isset(Auth::user()->userDetails->city) ?  old('city') : Auth::user()->userDetails->city }}">
                                            @if ($errors->has('city'))
                                                <span class="help-block">{{ $errors->first('city') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-12 {{$errors->has('address') ? 'has-error' : ''}}">
                                        <label class="col-md-2 control-label" for="address">Indirizzo</label>
                                        <div class="col-md-10">
                                            <input type="text" autocomplete="off" name="address" class="form-control" value="{{ !isset(Auth::user()->userDetails->address) ?  old('address') : Auth::user()->userDetails->address }}">
                                            @if ($errors->has('address'))
                                                <span class="help-block">{{ $errors->first('address') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    {{-- <div class="form-group col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="viaEmail" {{ isset(Auth::user()->userDetails->viaEmail) ? 'checked' : '' }}>

                                            <label class="form-check-label" for="viaEmail">
                                                Envia Email
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="viaSms" {{ isset(Auth::user()->userDetails->viaSms) ? 'checked' : '' }}>

                                            <label class="form-check-label" for="viaSms">
                                                Envia Sms
                                            </label>
                                        </div>
                                    </div> --}}

                                    {{-- <div class="form-group col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="terms" {{ isset(Auth::user()->userDetails->terms) ? 'checked' : '' }}>

                                            <label class="form-check-label" for="terms">
                                                Terminos y Condiciones
                                            </label>
                                        </div>
                                    </div> --}}
                                </fieldset>
                            </div>
                            {{-- <div class="tab-pane" id="user_themes">
                                <fieldset>
                                    <legend>Temi</legend>
                                    @include('layouts.admin.partials.themes')
                                </fieldset>
                            </div> --}}
                        </div>
                        <!-- END Tabs Content -->
                    </div>
                    <!-- END Block Tabs -->

                    <p id="flash"></p>

                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Indietro</button>
                            <button type="submit" form="profile_user" class="btn btn-sm btn-primary">Salvare</button>
                        </div>
                    </div>
                </form>
                {{-- {!! Form::close() !!} --}}

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- END User Settings -->

@push('scripts')
<script type="text/javascript">

    var base64 = '';

    $('#profile_user').on('submit', function(e) {
        e.preventDefault();
        clear_inputs()

        // Verificación de Contraseña
        var currentPassword = $("#current-password").val();
        var newPassword = $("#new-password").val();
        var confirmPassword = $("#confirm-password").val();

        if ((currentPassword.length === 0 && newPassword  > 0 && confirmPassword > 0) ||
           (currentPassword.length > 0 && (newPassword.length === 0 || confirmPassword.length === 0))) {
            // mensaje de error
            let message = 'Se si desidera aggiornare la password, è necessario indicare tutti i campi nella scheda \'password\', altrimenti lasciarli vuoti.';
            $('#flash').html('<div class="alert alert-danger" role="alert">'+ message +'</div>');

            return false;
        }

        $.ajax({
            type: "POST",
            url: $( this ).attr( "action" ),
            data: $(this).serialize(),

            success: function( response ) {
                $('#flash').html('<div class="alert alert-success" role="alert">'+ response.message +'</div>')
                setTimeout(function(){
                    $('#modal-user-settings').modal('hide');
                    $('#flash').empty();
                }, 1000);

                // actualizar imagen de perfil
                if ( base64 !== '' ) {
                    var avatars = document.getElementsByClassName( "main_avatar" );
                    for ( let avatar of avatars ) {
                        avatar.src = base64;
                    }
                }
            },
            error: function( jqXHR, textStatus, errorThrown ) {
                data = jqXHR.responseJSON.errors;
                $.each(data,function(i,error){
                    var element = $(document).find('[name="'+i+'"]');
                    element.after($('<span class="help-block">'+error[0]+'</span>'));
                    element.parents('.form-group').addClass('has-error');
                });
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    $( 'input#user_image' ).on( 'change', function(e) {
        var element = this;

        // limpiar imagen actual
        document.getElementById( "img" ).src = base64;
        document.getElementById( "b64" ).value = base64;

        if ( element.files && element.files[ 0 ] ) {
            var FR = new FileReader();

            FR.addEventListener( "load", function ( e ) {
                base64 = e.target.result;
                document.getElementById( "img" ).src = base64;
                document.getElementById( "b64" ).value = base64;
                document.getElementById( "filename" ).value = getFileName( "user_image" );
            } );

            FR.readAsDataURL( element.files[ 0 ] );
        }
    } );

    function getFileName( id ) {
        var fullPath = document.getElementById( id ).value;
        if (fullPath) {
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            return filename;
        }
    }

    function clear_inputs() {
        $('.help-block').html('');
        $('.form-group').removeClass('has-error');
    }
</script>
@endpush