{{-- home --}}
@php
    $homeOpen = (
    Request::is( 'home*' ) ||
    Request::is( 'admin/home*' ) ) ? true : false;
@endphp
<li>
    <a href="{!! route( 'home' ) !!}" class="{{ ($homeOpen)?'open':'' }}">
        <i class="fa fa-tachometer sidebar-nav-icon"></i>
        <span class="sidebar-nav-mini-hide">{{ tags( 'back_dashboard_title' )}}</span>
    </a>
</li>

{{-- rooms --}}
@php
    $roomsOpen = (
    Request::is( 'admin/roomLocations*' ) ||
    Request::is( 'admin/roomCategories*' ) ||
    Request::is( 'admin/service*' ) ||
    Request::is( 'admin/features*' ) ||
    Request::is( 'admin/roomCategoryServices*' ) ||
    Request::is( 'admin/roomCategoryFeatures*' ) ||
    Request::is('admin/generalInformations*') ||
    Request::is( 'admin/rooms*' ) ) ? true : false;
@endphp
<li>
    <a href="javascript:void(0)" class="sidebar-nav-menu {{ $roomsOpen === true ? 'open' : '' }}">
        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
        <i class="gi gi-saw_blade sidebar-nav-icon"></i>
        <span class="sidebar-nav-mini-hide">Configurazioni</span>
    </a>
    <ul style="display: {{ $roomsOpen === true ? 'block' : 'none' }};">
        <li>
            <a class="{{ Request::is( 'admin/roomLocations*' ) ? 'active' : '' }}" href="{!! route( 'admin.roomLocations.index' ) !!}"><span>{{ tags( 'back_rooms_room_location_title' ) }}</span></a>
        </li>

        {{-- <li>
            <a class="{{ Request::is( 'admin/serviceCategories*' ) ? 'active' : '' }}" href="{!! route( 'admin.serviceCategories.index' ) !!}"><span>{{ tags( 'back_services_service_categories_title' ) }}</span></a>
        </li> --}}


        <li>
            <a class="{{ Request::is( 'admin/roomCategories*' ) ? 'active' : '' }}" href="{!! route( 'admin.roomCategories.index' ) !!}"><span>{{ tags( 'back_rooms_room_category_title' ) }}</span></a>
        </li>
        <li>
            <a class="{{ Request::is( 'admin/rooms' ) ? 'active' : '' }}" href="{!! route( 'admin.rooms.index' ) !!}"><span>{{ tags( 'back_rooms_room_title' ) }}</span></a>
        </li>
        <li>
            <a class="{{ Request::is( 'admin/services*' ) ? 'active' : '' }}" href="{!! route( 'admin.services.index' ) !!}"><span>{{ tags( 'back_services_service_title' ) }}</span></a>
        </li>

        <li >
            <a class="{{ Request::is( 'admin/roomCategoryServices*' ) ? 'active' : '' }}" href="{!! route( 'admin.roomCategoryServices.index' ) !!}"><span>{{ tags( 'back_rooms_room_category_service_title' )}}</span></a>
        </li>
        {{-- <li>
            <a class="{{ Request::is( 'admin/features*' ) ? 'active' : '' }}" href="{!! route( 'admin.features.index' ) !!}"><span>{{ tags( 'back_features_title' ) }}</span></a>
        </li>

        <li>
            <a class="{{ Request::is( 'admin/roomCategoryFeatures*' ) ? 'active' : '' }}" href="{!! route( 'admin.roomCategoryFeatures.index' ) !!}"><span>{{ tags( 'back_room_category_features_title' ) }}</span></a>
        </li> --}}
        <li>
            <a class="{{ Request::is( 'admin/roomsServices*' ) ? 'active' : '' }}" href="{!! route( 'admin.roomsServices.index' ) !!}"><span>{{ tags( 'back_rooms_services_title' ) }}</span></a>
        </li>

        <li>
            <a class="{{ Request::is('admin/generalInformations*') ? 'active' : '' }}" href="{!! route('admin.generalInformations.index') !!}"><span>{{ tags('general_general_informations') }}</span></a>
        </li>

    </ul>
</li>

{{-- Gestion de Precio --}}
@php
    $gestioniPrecioOpen = (
    Request::is( 'admin/roomSeasons*' ) ||
    Request::is( 'admin/packages*' ) ||
    Request::is( 'admin/rate*' )) ? true : false;
@endphp
<li>
    <a href="javascript:void(0)" class="sidebar-nav-menu {{ $gestioniPrecioOpen === true ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-saw_blade sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Gestione Prezzi</span></a>
    <ul style="display: {{ $gestioniPrecioOpen === true ? 'block' : 'none' }};">
        <li>
            <a class="{{ Request::is( 'admin/roomSeasons*' ) ? 'active' : '' }}" href="{!! route( 'admin.roomSeasons.index' ) !!}"><span>{{ tags( 'back_module_rooms_room_season_title' ) }}</span></a>
        </li>
        {{-- <li>
            <a class="{{ Request::is( 'admin/packages*' ) ? 'active' : '' }}" href="{!! route( 'admin.packages.index' ) !!}"><span>{{ tags( 'back_packages_title' ) }}</span></a>
        </li> --}}
        <li>
            <a class="{{ Request::is( 'admin/rates*' ) ? 'active' : '' }}" href="{!! route( 'admin.rates.index' ) !!}"><span>{{ tags( 'back_rates_title' ) }}</span></a>
        </li>
    </ul>
</li>

{{-- bookings --}}
@php
    $bookingsOpen = (
    Request::is( 'admin/bookings*' ) ) ? true : false;
@endphp
<li>
    <a href="javascript:void(0)" class="sidebar-nav-menu {{ $bookingsOpen === true ? 'open' : '' }}">
        <i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>
        <i class="gi gi-saw_blade sidebar-nav-icon"></i>
        <span class="sidebar-nav-mini-hide">{{ tags( 'back_bookings_bookings_title' ) }}</span>
    </a>
    <ul style="display: {{ $bookingsOpen === true ? 'block' : 'none' }};">
        <li>
            <a class="{{ Request::is( 'admin/bookings*' ) ? 'active' : '' }}" href="{!! route( 'admin.bookings.index' ) !!}"><span>{{ tags( 'back_bookings_bookings_title' ) }}</span></a>
        </li>
    </ul>
</li>

{{-- galerias --}}
@php
    $offersOpen = (
    Request::is( 'admin/gallerySliders*' ) ) ? true : false;
@endphp
<li>
    <a href="{!! route( 'admin.gallerySliders.index' ) !!}" class="{{ ( $offersOpen ) ? 'open' : '' }}">
        <i class="gi gi-saw_blade sidebar-nav-icon"></i>
        <span class="sidebar-nav-mini-hide">{{ tags( 'back_module_gallery_sliders_title' ) }}</span>
    </a>
</li>

{{-- utils --}}
@php
    $utilsOpen = (
    Request::is( 'admin/multimedia*' ) ||
    Request::is( 'admin/seos*' ) ||
    Request::is( 'admin/tagTranslations*' ) ||
    Request::is( 'admin/settings*' ) ||
    Request::is( 'admin/policies*' ) ||
    Request::is( 'admin/clientRequests*' ) ) ? true : false;
@endphp
<li>
    <a href="javascript:void(0)" class="sidebar-nav-menu {{ $utilsOpen === true ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-saw_blade sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">{{ tags( 'back_menu_utils' ) }}</span></a>
    <ul style="display: {{ $utilsOpen === true ? 'block' : 'none' }};">
        <li>
            <a class="{{ Request::is( 'admin/settings*' ) ? 'active' : '' }}" href="{!! route( 'admin.settings.index' ) !!}"><span> {{ tags( 'back_settings_setting_title' ) }}</span></a>
        </li>
        <li>
            <a class="{{ Request::is( 'admin/tagTranslations*' ) ? 'active' : '' }}" href="{!! route( 'admin.tagTranslations.index' ) !!}"> <span>{{ tags( 'back_menu_utils_tag_translation' ) }}</span></a>
        </li>
        <li>
            <a class="{{ Request::is( 'admin/policies*' ) ? 'active' : '' }}" href="{!! route( 'admin.policies.index' ) !!}"><span>{{ tags( 'back_policy_title' ) }}</span></a>
        </li>
    </ul>
</li>

{{-- newsletter --}}
@php
    $newslettersOpen = (
    Request::is( 'admin/newsletterUsers*' ) ) ? true : false;
@endphp
<li>
    <a href="javascript:void(0)" class="sidebar-nav-menu {{ $newslettersOpen === true ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-saw_blade sidebar-nav-icon"></i> <span class="sidebar-nav-mini-hide">{{ tags( 'back_menu_newsletter' ) }}</span></a>
    <ul style="display: {{ $newslettersOpen === true ? 'block' : 'none' }};">

        <li>
            <a class="{{ Request::is( 'admin/newsletterUsers*' ) ? 'active' : '' }}" href="{!! route( 'admin.newsletterUsers.index' ) !!}"><span> {{ tags( 'back_menu_newsletter' ) }}</span></a>
        </li>

    </ul>
</li>

{{-- users --}}
@php
    $usersOpen = (
    Request::is( 'admin/role*' ) ||
    Request::is( 'admin/users*' ) ||
    Request::is( 'admin/search/user*' ) ) ? true : false;
@endphp
<li>
    <a href="javascript:void(0)" class="sidebar-nav-menu {{ $usersOpen === true ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-saw_blade sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">{{ 'Gestione Utenti' }}</span></a>
    <ul style="display: {{ $usersOpen === true ? 'block' : 'none' }};">
        <li>
            <a class="{{ Request::is( 'admin/users*' ) ? 'active' : '' }}" href="{!! route( 'admin.users.index' ) !!}"> <span>{{ tags( 'back_module_users_user_title' ) }}</span></a>
        </li>
        @if( Auth::user()->isRole( 'admin' ) )
            <li>
                <a href="{!! route( 'admin.role.index' ) !!}" class="{{ Request::is( 'admin/role*' ) ? 'active' : '' }}"><span>Ruoli</span></a>
            </li>
            {{-- <li>
                <a href="{!! route( 'admin.user.get.search' ) !!}" class="{{ Request::is( 'admin/search/user*' ) ? 'active' : '' }}"><span>Asignar Roles</span></a>
            </li> --}}
        @endif
        <li>
            <a href="#modal-user-settings" data-toggle="modal">Profilo</i></a>
        </li>
    </ul>
</li>

{{-- activities --}}
@php
    $activitiesOpen = (
    Request::is( 'admin/activities*' ) ||
    Request::is( 'admin/activityCategories*' ) ||
    Request::is( 'admin/allies*' ) ||
    Request::is( 'admin/activityRequests*' ) ) ? true : false;
@endphp
<li>
    <a href="javascript:void(0)" class="sidebar-nav-menu {{ $activitiesOpen === true ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-saw_blade sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">{{ tags( 'back_activities_activity_title' ) }}</span></a>
    <ul style="display: {{ $activitiesOpen === true ? 'block' : 'none' }};">
        <li>
            <a class="{{ Request::is( 'admin/activities*' ) ? 'active' : '' }}" href="{!! route( 'admin.activities.index' ) !!}"><span>{{ tags( 'back_activities_activity_title' ) }}</span></a>
        </li>

        <li>
            <a class="{{ Request::is( 'admin/activityCategories*' ) ? 'active' : '' }}" href="{!! route( 'admin.activityCategories.index' ) !!}"><span>{{ tags( 'back_activities_activity_categories_title' ) }}</span></a>
        </li>

        <li>
            <a class="{{ Request::is( 'admin/activityRequests*' ) ? 'active' : '' }}" href="{!! route( 'admin.activityRequests.index' ) !!}"><span>{{ tags( 'back_activities_activity_requests_title' ) }}</span>  <span class="label label-danger">{{ \App\Models\Admin\ActivityRequest::where( 'status_id', 6 )->count() }}</span></a>
        </li>

        <li>
            <a class="{{ Request::is( 'admin/allies*' ) ? 'active' : '' }}" href="{!! route( 'admin.allies.index' ) !!}"><span>{{ tags( 'back_activities_allies_title' ) }}</span></a>
        </li>
    </ul>
</li>

{{-- Blogs --}}
@php
    $blogsOpen = (
    Request::is( 'admin/blog*' ) ||
    Request::is( 'admin/blogCategories*' )) ? true : false;
@endphp
<li>
    <a href="javascript:void(0)" class="sidebar-nav-menu {{ $blogsOpen === true ? 'open' : '' }}"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-saw_blade sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">{{ tags( 'back_blogs_blogs_title' ) }}</span></a>
    <ul style="display: {{ $blogsOpen === true ? 'block' : 'none' }};">
        {{-- <li>
            <a class="{{ Request::is( 'admin/blogCategories*' ) ? 'active' : '' }}" href="{!! route( 'admin.blogCategories.index' ) !!}"><span>{{ tags( 'back_blogs_blog_categories_title' ) }}</span></a>
        </li> --}}
        <li>
            <a class="{{ Request::is( 'admin/blog*' ) ? 'active' : '' }}" href="{!! route( 'admin.blogs.index' ) !!}"><span>{{ tags( 'back_blogs_blogs_title' ) }}</span></a>
        </li>
    </ul>
</li>


@push( 'scripts' )
    <script type="text/javascript">
        $( '#menu_tag_translations' ).on( 'click', function() {
            console.log( '{{ Auth::user()->api_token }}' );

            $.ajax({
                type: "GET",
                url: $( this ).data( 'route' ),
                data: $(this).serialize(),

                success: function( response ) {
                    console.log( response );
                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log( jqXHR.responseJSON );
                },
                headers: {
                    'X-CSRF-TOKEN': $( 'meta[name="csrf-token"]' ).attr( 'content' )
                },
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader( 'Authorization', 'BEARER ' + '{{ Auth::user()->api_token }}' );
                },
            });
        });
    </script>
@endpush
