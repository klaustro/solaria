
<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <!--{!! SEO::generate() !!}-->

    <meta name="author" content="Solaria Home Holiday">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- begin::META-ETIQUETAS PARA FACEBOOK --}}
    <meta property="og:type"            content="article" />
    @php
    try{
        $blog = App\Models\Admin\Blog::where('slug', explode('/', Request::path())[1])->first();
        $blogTranslation = $blog->translations->where('language_id', 1)->first();
    @endphp
    @if( explode('/', Request::path())[0] == 'promozioni' )

    <meta property="og:url"             content="{{ Request::url() }}" />
    <meta property="og:title"           content="{{ $blogTranslation->title }}" />
    <meta property="og:description"     content="{{ substr($blogTranslation->description, 0, 50) }}" />
    <meta property="og:image"           content="{{ asset($blog->image) }}" />
    @endif
    @php
    } catch(\ErrorException $e) {
    }
    @endphp
    {{-- end::META-ETIQUETAS PARA FACEBOOK --}}

    <title>{{ config('app.name', 'Solaria Home Holiday') }}</title>
    <meta name="description" content="Solaria Home Holiday è un’agenzia immobiliare specializzata nella locazione di ville, case e appartamenti per vacanza nel nord-est della Sardegna. Esperienza, professionalità e profonda conoscenza del territorio sono le basi su cui, da oltre 20 anni, si fonda la nostra attività.">
    <link rel="shortcut icon" type="image/png" href="{{asset('images/logos/solaria-vacanze-affitto-ville-appartamenti-sardegna-favicon.png')}}"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Playfair+Display:400,900" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer></script>

    @stack('css')
</head>
<body>
    <div id="app">
        <main class="">
            @yield('content')
        </main>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('admin/js/vendor/jquery.min.js') }}"></script>
    @stack('scripts')

</body>
</html>
