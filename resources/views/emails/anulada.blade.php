<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
</head>
<!--BODY-->
<body
style="
-moz-box-sizing: border-box;
-ms-text-size-adjust: 100%;
-webkit-box-sizing: border-box;
-webkit-text-size-adjust: 100%;
margin: 0;
background: #cecece !important;
box-sizing: border-box;
color: #0a0a0a;
font-family: Helvetica,Arial,sans-serif;
font-size: 16px;
font-weight: 400;
line-height: 1.3;
margin: 0;
min-width: 100%;
padding: 10px;
text-align: left;
width: 100% !important"
>

<div style="background-color: #fff; border-radius: 0px 0px 20px 20px;">
    <table style="width: 100%; background-color: #fff; color: #fff; padding: 15px 20px; border-bottom: 4px solid #4fcaa5; text-align: center">
        <tr>
            <td> <img src="{{ asset('/images/logos/solaria-logo.png') }}" alt="logo"> </td>
        </tr>
    </table>

    <table style="width: 100%; padding: 0px 10px;">
        <tr>
            <td>
                <h3 style="text-align: center;">
                    {{ tags('order_email_greeting') }}
                    <strong>{{ $data['personResponsible']['name'] }}</strong>,
                    {{-- {{ tags('order_email_thanks') }} --}}
                    {{ tags('order_email_subject_anulado') }}: {{ $data['code'] }}
                </h3>
            </td>
        </tr>
    </table>
@php
    $start  = $data['rooms'][0]['bookingDate']['checkin'];
    $end    = $data['rooms'][0]['bookingDate']['checkout'];

    $nights = count( getNightsInRange( $start,$end ) );

    // dd( $nights );
@endphp
    <table style="width: 100%; margin: 5px 0px; padding: 0px 25px;">
        <tr>
            <td>
                <p style="text-align: left; margin: 0px;">
                    <h3 class="font-weight-bold text-dark">{{ tags('order_email_order') }}: {{ $data['code'] }}</h3>
                </p>

                <p class="d-block text-dark" style="margin: 0px 0px 0px; font-size: 12px;">{{ tags('general_date') }}: {{ Carbon\Carbon::createFromFormat( 'Y-m-d H:i:s', $data['created_at'])->format( 'd/m/Y' ) }} </p>
                <p class="d-block text-dark" style="margin: 0px 0px 0px; font-size: 12px;">{{ tags('booking_3_time') }}: {{ Carbon\Carbon::createFromFormat( 'Y-m-d H:i:s', $data['created_at'])->format( 'H:i:s' ) }} </p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 0px 0px 0px; font-size: 12px;"><strong>{{ tags('general_name') }}:</strong> {{ $data['personResponsible']['name'] }}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 0px 0px 0px; font-size: 12px;"><strong>{{ tags('general_email') }}:</strong> {{ $data['personResponsible']['email'] }}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 0px 0px 0px; font-size: 12px;"><strong>{{ tags('order_email_fiscal_code') }}:</strong> {{ $data['personResponsible']['fiscal_code'] }}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 0px 0px 0px; font-size: 12px;"><strong>{{ tags('general_phone') }}</strong> {{ $data['personResponsible']['phone'] }}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 0px 0px 0px; font-size: 12px;"><strong>{{ tags('order_email_arrival') }}:</strong> {{ Carbon\Carbon::createFromFormat( 'Y-m-d', $start)->format( 'd/m/Y' ) }}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 0px 0px 0px; font-size: 12px;"><strong>{{ tags('order_email_departure') }}: </strong> {{ Carbon\Carbon::createFromFormat( 'Y-m-d', $end)->format( 'd/m/Y' ) }}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin: 0px 0px 0px; font-size: 12px;"><strong>{{ tags('order_email_total_duration')}}: </strong>
                {{ $nights }} notte.

                </p>
            </td>
        </tr>
    </table>

    <div style="padding: 0px 10px;">
            <table style="width: 100%; padding-bottom: 0px; border-bottom: 2px solid #ccc;">
                {{-- <tr style="background: #ccc;">
                    <th>{{ tags('back_rooms_room_title') }}</th>
                    <th>{{ tags('booking_3_nights') }}</th>
                    <th>{{ tags('back_rooms_room_season_price') }}</th>
                    <th>{{ tags('order_email_amount') }}</th>
                </tr>        --}}
                @foreach ($data['rooms'] as $room )
                <tr style="background: #ccc;">
                        <th>{{-- {{ tags('back_rooms_room_title') }} --}}
                                Dettagli
                        </th>
                </tr>
                <tr>
                    <td>
                        <div>
                            <strong>{{ tags('back_rooms_room1_title') }}:</strong>
                            <br>
                            {{ $room['name'] }}
                        </div>
                        {{--<div >
                            <strong>{{ tags('order_email_people') }}:</strong>
                            {{ $room['persons_quantity'] }}
                        </div> --}}
                        {{-- <div >
                            <strong>{{ tags('order_email_room_responsable') }}</strong>
                        </div> --}}
                        <div>
                            {{-- <strong>{{ tags('general_name') }}: </strong> --}}
                            <div><strong>Responsabile della stanza</strong></div>
                            {{ $room['personResponsible']['name'] }}
                        </div>
                        <div>
                            {{-- <strong>{{ tags('general_phone') }}:</strong> --}}
                            {{ $room['personResponsible']['phone'] }}
                        </div>
                        <div>
                            {{-- <strong>{{ tags('general_email')}}:</strong> --}}
                            {{ $room['personResponsible']['email'] }}
                        </div>
                        <div>
                             <p style="margin: 0; padding: 0;"><strong>Adulti:</strong> {{ $room['adults_quantity'] }}</p>
                        </div>
                        @if(empty( $room['children_quantity'] ) == false )
                            <div >
                                {{-- <strong>{{ tags('general_email')}}:</strong> --}}
                                <p style="margin: 0; padding: 0;"><strong>Bambini:</strong> {{ $room['children_quantity'] }}</p>

                            </div>
                        @endif
                    </td>
                </tr>

                <tr>
                    <table style="width: 100%;">
                            <tr>
                                <td colspan="3">

                                    <table style="width: 100%;">

                                        @if(isset($room['price_detail'][ 'season' ]))
                                            @foreach($room['price_detail'][ 'season' ]  as $key => $season)
                                                <tr>
                                                    <th colspan="3" style="text-align: center; text-transform: uppercase; font-weight: bold;">
                                                        <p style="margin: 0px; padding: 5px 0px 10px; font-size: 12px">
                                                            @if ( $key == 'Prezzo dell\'Appartamento' )
                                                                {{ $key }}
                                                            @else
                                                                Stagione: {{ $key }}
                                                            @endif
                                                        </p>
                                                    </th>
                                                </tr>

                                                <tr style="background: #f1f1f1; font-size: 12px">
                                                    <th>{{ tags('booking_3_nights') }}</th>
                                                    {{-- <th>{{ tags('back_rooms_room_season_price') }}</th> --}}
                                                    <th colspan="2" style="width: 50%;">{{ tags('order_email_amount') }}</th>
                                                </tr>

                                                <tr>
                                                    <td style="font-size: 12px;">
                                                        {{ $season[ 0 ][ 'nights' ] }}
                                                    </td>
                                                    {{-- <td style="font-size: 12px;">
                                                            € {{ number_format( $season[ 0 ][ 'price' ], 2, ',', '.' ) }}
                                                    </td> --}}
                                                    <td style="font-size: 12px;">
                                                            €  {{ number_format( $season[ 0 ][ 'price' ] * $season[ 0 ][ 'nights' ], 2, ',', '.' ) }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        {{-- array_key_exists( 'rate', $room['price_detail'] richard--}}
                                        @if(isset($room['price_detail']['rate']))
                                            <tr>
                                                <td colspan="3">
                                                    <table style="width:100%;">
                                                        @foreach($room['price_detail']['rate'] as $key => $rate)
                                                            <tr>
                                                                <td colspan="3" style="text-align: center; text-transform: uppercase; font-weight: bold;">
                                                                    <p style="margin: 0px; padding: 5px 0px 10px; font-size: 12px">
                                                                          {{-- {{ $key }} --}} {{  tags( 'back_rates_title' )  }}
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr style="background: #f1f1f1; font-size: 12px">
                                                                <th>{{ tags('booking_3_nights') }}</th>
                                                                {{-- <th>{{ tags('back_rooms_room_season_price') }}</th> --}}
                                                                <th colspan="2" style="width: 50%;">{{ tags('order_email_amount') }}</th>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p style="margin: 0px; padding: 5px 0px 10px; font-size: 12px">
                                                                    {{ $rate[ 0 ][ 'nights' ] }}
                                                                    </p>
                                                                </td>
                                                                {{-- <td style="">
                                                                    <p style="margin: 0px; padding: 5px 0px 10px; font-size: 12px">
                                                                            € {{ number_format( $rate[ 0 ][ 'price' ], 2, ',', '.' ) }}
                                                                    </p>
                                                                </td> --}}
                                                                <td style="">
                                                                    <p style="margin: 0px; padding: 5px 0px 10px; font-size: 12px">
                                                                            €  {{ number_format( $rate[ 0 ][ 'price' ] * $rate[ 0 ][ 'nights' ], 2, ',', '.' ) }}
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </td>
                                            </tr>
                                        @endif

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <br>
                                </td>
                            </tr>
                    </table>
                </tr>

            @endforeach

                <br>
            </table>
        </div>
    <div style="max-width: 100%; margin: 10px 20px; text-align: right; text-transform: uppercase; padding-bottom: 10px;">
        <p>
            {{ tags('order_email_subtotal')}}: <span style="display: inline-block; width: 100px; color: brown">{{ number_format( $data['subtotal'], 2, ',', '.' ) }}€</span>
        </p>
        <p>
            IVA: <span style="display: inline-block; width: 100px; color: brown">{{ number_format( $data['iva'], 2, ',', '.' ) }}€</span>
        </p>
        <p>
            <strong>{{ tags('order_email_total')}}: <span style="display: inline-block; width: 100px; color: brown">{{ number_format( $data['total'], 2, ',', '.' ) }}€</span></strong>
        </p>
    </div>
</div>


<div style="border-radius: 20px; padding: 20px; background-color: #fff; margin-top: 30px; margin-bottom: 30px;">
    {{-- <p style="text-align: center;">
        <a href="#" style="display: inline-block; width: 50px; height: 50px; background-color: #222; border-radius: 50px; margin: 0px 10px;">
            <img src="red.png" alt="Red social">
        </a>
        <a href="#" style="display: inline-block; width: 50px; height: 50px; background-color: #222; border-radius: 50px; margin: 0px 10px;">
            <img src="red.png" alt="Red social">
        </a>
        <a href="#" style="display: inline-block; width: 50px; height: 50px; background-color: #222; border-radius: 50px; margin: 0px 10px;">
            <img src="red.png" alt="Red social">
        </a>
    </p> --}}
    <p style="text-align: center;">
        <strong>Solaria Home Holiday</strong>
        <br>
        {{getConfig('address')}}
    </p>
    <p style="text-align: center; font-size: 24px; margin: 0px;">
        <a href="tel:{{getConfig('telefono')}}" style="text-decoration: none; color:#222;">{{getConfig('telefono')}}</a>
    </p>


    <div style="width: 90%; margin: auto; border-top: 1px solid #ccc; display: flex; justify-content: space-between; padding: 10px 0px;">
        <small>Copyright {{ date("Y") }} Solaria Home Holiday</small>
        <img width="100" height="30" src="{{ asset('/images/home/logojumperr.png') }}" alt="Jumperr">
    </div>
</div>


</body>

</html>
