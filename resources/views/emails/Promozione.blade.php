
@extends('emails.template')

@section('content')

<table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
    <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-left:16px;padding-right:16px;padding-top:16px;text-align:left;width:564px">
                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tbody>
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">

                                <h2 class="text-center" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:32px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center;word-wrap:normal">
                                    {{ tags('back_rates_title') }}
                                </h2>
                                <p style="margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                    @php
                                        $fecha_start = strtotime($data['rates'][0]['start_date']);
                                        $fecha_start_format = date("d-m-Y", $fecha_start);

                                        $fecha_end = strtotime($data['rates'][0]['end_date']);
                                        $fecha_end_format = date("d-m-Y", $fecha_end);
                                    @endphp
                                    <p style="margin:0;margin-bottom:0px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:25px;font-weight:bold;line-height:1.3;margin-bottom:0px;padding:0;text-align:center;">
                                        Dal {{ $fecha_start_format }}
                                        al {{ $fecha_end_format }}
                                        <div style="background: #000; width: 100px; height: 2px; margin: auto;"></div>
                                    </p>
                                    @php
                                        $DeferenceInDays = \Carbon\Carbon::parse($data['rates'][0]['end_date'])->diffInDays($data['rates'][0]['start_date'])
                                    @endphp
                                </p>
                                @foreach ($data['rates'] as $rate )

                                        <div style="position: relative;display: block;min-width: 0;word-wrap: break-word;background-color: #fff;background-clip: border-box;border: 1px solid rgba(0, 0, 0, 0.125);border-radius: 0.25rem;margin-bottom: 2.5rem;box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;">
                                            <p style="margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                                <img src="{{ asset($rate['room']['gallery'][0]['url'] ) }}" alt="" style="width: 100%">
                                            </p>
                                            <p style="margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:22px;font-weight:bold;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                            @foreach ($roomCategories as $roomCategory)
                                                @if($rate['room']['room_category_id'] == $roomCategory['id'])

                                                        {{ $roomCategory->translations->first()->name }} -
                                                @endif
                                            @endforeach

                                                {{ $rate['room']['name'] }}
                                            </p>
                                            <p style="margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                            @foreach ($roomCategories as $roomCategory)
                                                @if($rate['room']['room_category_id'] == $roomCategory['id'])
                                                    @foreach ($roomLocationes as $roomLocation)
                                                        @if($roomCategory['room_location_id'] == $roomLocation['id'])
                                                            {{ $roomLocation->translations->first()->name }} -
        {{--                                             {{ $roomCategory['address'] }}
                                                        {{ $roomCategory->translations->first()->name }} -
         --}}                                           @endif
                                                    @endforeach
                                                    {{ $roomCategory['address'] }}


{{--                                                     <a href="{{ url('/village') }}/{{str_slug($roomCategory->translations->first()->name)}}" style="text-decoration: none; color:#222;">
                                                        {{ $roomCategory->translations->first()->name }}
                                                    </a>
                                                    <br>
                                                    <a href="{{ url('/village') }}/{{str_slug($roomCategory->translations->first()->name)}}" style="text-decoration: none; color:#222;">
 --}}
                                                @endif
                                            @endforeach
                                            </p>
                                            {{-- <p style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                                {{ number_format( $rate['price'], 2, ',', '.' ) }} € a notte
                                            </p> --}}

                                            @if ( $rate['discount'] > 0)
                                            <p style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:1.3;padding:0;text-align:center;color:#3490dc;">
                                                {{ number_format( $rate['discount'], 2, ',', '.' ) }} % sconto
                                            </p>

                                            {{-- <p style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:400;line-height:1.3;padding:0;text-align:center; text-decoration:line-through">
                                                {{ number_format( $rate['price_temp'], 2, ',', '.' ) }} € per notte
                                            </p> --}}
                                            @endif

                                            <p style="margin:10px 0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                                @php
                                                    $total = $rate['price'] * ($DeferenceInDays);
                                                @endphp
                                                Totale:  {{  number_format( $rate['price_total'], 2, ',', '.' ) }} €
                                            </p>
                                            <p style="margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                                @foreach ($roomCategories as $roomCategory)
                                                    @if($rate['room']['room_category_id'] == $roomCategory['id'])
                                                        <a href="{{ url('/village') }}/{{str_slug($roomCategory->translations->first()->name)}}" style="text-decoration: none; color:#fff; display: inline-block; padding: 8px 15px; background: blue; margin: auto; font-size: 12px; border-radius: 50px;">
                                                            Maggiori informazioni
                                                        </a>
                                                    @endif
                                                @endforeach

                                                @foreach ($roomCategories as $roomCategory)
                                                    @if($rate['room']['room_category_id'] == $roomCategory['id'])
                                                        <a href="{{ url('/booking') }}2/{{$rate['token']}}" style="text-decoration: none; color:#fff; display: inline-block; padding: 8px 15px; background: #f6993f; margin: auto; font-size: 12px; border-radius: 50px;">
                                                            Prenota Ora
                                                        </a>
                                                    @endif
                                                @endforeach
                                            </p>


                                            {{-- <p style="margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                                @foreach ($roomCategories as $roomCategory)
                                                    @if($rate['room']['room_category_id'] == $roomCategory['id'])
                                                        <a href="{{ url('/booking') }}2/{{$rate['token']}}" style="text-decoration: none; color:#fff; display: inline-block; padding: 8px 15px; background: blue; margin: auto; font-size: 12px; border-radius: 50px;">
                                                            Prenota Ora
                                                        </a>
                                                    @endif
                                                @endforeach
                                            </p>--}}


                                        </div>
                                    @endforeach
                                <br>
                                @if($data['rates'][0]['comment'])
                                    <p style="margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin-bottom:10px;padding:0;text-align:justify;">
                                        Nota:
                                        <br>
                                        {{ $data['rates'][0]['comment'] }}
                                    </p>
                                @endif
                                <br>
                                <p style="margin:0;margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:18px;font-weight:400;line-height:1.3;margin-bottom:10px;padding:0;text-align:center;">
                                    Contattaci per prenotare:
                                    <br>
                                    <br>
                                    <a href="https://api.whatsapp.com/send?phone= + {{getConfig('whatsapp_number')}} + &text=Ciao%2C%20" style="text-decoration: none; color:#222;" >
                                    <img src="{{ asset('/images/iconos/whatsapp.png') }}" alt="whatsapp" width="28">
                                    {{getConfig('whatsapp_number')}}
                                    </a>
                                    <br>
                                    <a href="mailto:{{getConfig('contact_email')}}" style="text-decoration: none; color:#222;" >
                                    <img src="{{ asset('/images/iconos/043-email-1.png') }}" alt="whatsapp" width="28">
                                    {{getConfig('contact_email')}}
                                    </a>
                                </p>
                                <br>
                                <br>

                            </th>
                        </tr>
                    </tbody>
                </table>
            </th>
        </tr>
    </tbody>
</table>

@endsection