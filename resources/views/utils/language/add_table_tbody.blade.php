@php
    $translationsIds = array_column( $translations->toArray(), 'language_id' )
@endphp
@foreach ( $languages as $language )
	@if ( in_array( $language->id, $translationsIds ) )
        <td class="text-center">
        	<a href="{!! route( $route, [ 'language' => $language->code, 'model' => $id] ) !!}" class='btn btn-success btn-xs' data-toggle="tooltip" data-placement="top" title="Modificare"><i class="fa fa-edit"></i></a>
		</td>
	@else
		<td class="text-center">
        	<a href="{!! route( $route, [ 'language' => $language->code, 'model' => $id ] ) !!}" class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Modificare"><i class="fa fa-plus-square-o"></i></a>
		</td>
	@endif
@endforeach
