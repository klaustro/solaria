<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'Zu`Pietru') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/_all-skins.min.css">

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css" scoped>
        .background {
            widows: 100%;
            min-height: 420px;
            padding-top: 30px;
            background-image: url(/images/solaria-login-backoffice.jpg);
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
        }
        .has-error{
            color: #e74c3c;
            border-color: #e74c3c;
        }
        .alert{
            display: none;
        }
        .my-alert{
            position: fixed;
            top: 5px;
            left:2%;
            width: 96%;
        }
        .shadow{
            box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
        }
    </style>
</head>
<body class="hold-transition login-page background">
<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body shadow">
        <div class="login-logo">
            {{-- <a href="{{ url('/home') }}"><b>InfyOm </b>Generator</a> --}}
            <img src="{{ asset('/images/logos/solaria-logo-login.png') }}"  width="256px">
        </div>

        <form method="POST" id="app_login" action="{{ url('/login') }}">
            @csrf

            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="{{ tags( 'general_email' ) }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ tags( 'general_password' ) }}">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

            </div>
            <div class="row text-center">
                <div class="col-xs-7">
                    <div class="checkbox icheck">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                    </button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
        {{-- <a href="{{ url('/register') }}" class="text-center">Register a new membership</a> --}}

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#app_login').on('submit', function(e) {
                e.preventDefault();
                clear_input();

                $.ajax({
                    type: "POST",
                    url: $( this ).attr( "action" ),
                    data: $(this).serialize(),

                    success: function( response ) {
                        console.log( response );
                        window.location.href = '/home';
                    },
                    error: function( jqXHR, textStatus, errorThrown ) {
                        data = jqXHR.responseJSON.errors;
                       if(errorThrown==='Unauthorized')
                            $('.info_form').html('Usted no esta autorizado para ingresar.');

                        $.each(data,function(i,error){
                            var element = $(document).find('[name="'+i+'"]');
                            element.after($('<span class="has-error help">'+error[0]+'</span>'));
                            element.parents('.form-group').addClass('has-error');
                            element.addClass('has-error');
                        });
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            });
        });

        function clear_input(){
            $('.info_form').html('');
            $('.help').html(" ");
            $('.form-group').removeClass('has-error');
            $(':input').removeClass('has-error');
        }
    </script>
@endpush
</body>
</html>
