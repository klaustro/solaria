'use strict';
import BaseService from './base.service'

const SERVICE_API_URL = '/api/admin/gallery_sliders';

export const galleryService = new BaseService(SERVICE_API_URL);
