'use strict';
import BaseService from './base.service'

const SERVICE_API_URL = '/api/admin/general_informations';

export const generalInformationService = new BaseService(SERVICE_API_URL);
