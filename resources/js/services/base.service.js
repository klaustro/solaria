'use strict';
import request from '../util/request'

export default class BaseService {

    constructor(url) {
        this.url = url
    }

    get(id, params) {
        return request({
            url: `${this.url}/${id}`,
            method: 'get',
            params: params
        })
    }

    add(data) {
        return request({
            url: this.url,
            method: 'post',
            data
        })
    }

    update(data) {
        return request({
            url: `${this.url}/${data.id}`,
            method: 'put',
            data
        })
    }

    remove(id) {
        return request({
            url: `${this.url}/${id}`,
            method: 'delete'
        })
    }

    getAll(q) {
        return request({
            url: this.url,
            method: 'get',
            params: q
        })
    }

}