
window.Vue = require('vue');

import VueSweetalert2 from 'vue-sweetalert2';
import VeeValidate, { Validator } from 'vee-validate';
import it from 'vee-validate/dist/locale/it';

Vue.use(VeeValidate);
Validator.localize('it', it);

Vue.use(VueSweetalert2);

/**
 * Importación de Vistas
 */
import roomSeasonForm from './views/rooms/rooms-seasons/Create';
import roomSeasonEditForm from './views/rooms/rooms-seasons/Edit';
import roomCategoryForm from './views/rooms/room_categories/Edit';
import gallerySliderForm from './views/gallery_sliders/Edit';
import rateSearchForm from './views/rates/SearchForm';
import bookingForm from './views/bookings/Edit';
import roomForm from './views/rooms/rooms/Edit';
import RoomGeneralInfo from './views/rooms/general_information/Edit';
import experienceForm from './views/experience/Edit';

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
Vue.component('room-season-form', roomSeasonForm);
Vue.component('room-season-edit-form', roomSeasonEditForm);
Vue.component('room-category-form', roomCategoryForm);
Vue.component('rate-search-form', rateSearchForm);
Vue.component('booking-form', bookingForm);
Vue.component('room-form', roomForm);
Vue.component('vue-ctk-date-time-picker', window['vue-ctk-date-time-picker']);
Vue.component('gallery-slider-form', gallerySliderForm);
Vue.component('room-general-info', RoomGeneralInfo);
Vue.component('experience-form', experienceForm);

/*vuex*/
import store from '../vuex';


const moment = require('moment-timezone');
require('moment/locale/it');
Vue.use(require('vue-moment'), { moment });

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import CustomCalendar from './components/Calendar'
Vue.component('custom-calendar', CustomCalendar);

const app = new Vue({
    el: '#page-content',
    store,
});

