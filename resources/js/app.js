require('./bootstrap');

window.Vue = require('vue')

const moment = require('moment-timezone')
require('moment/locale/it')
require('moment/locale/de')
require('moment/locale/en-gb')

Vue.use(require('vue-moment'), { moment })

/*vuex*/
import store from './vuex';

/*Filters*/
var numeral = require('numeral');
numeral.register('locale', 'it', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal : function (number) {
        return number === 1 ? 'er' : 'ème';
    },
    currency: {
        symbol: '€'
    }
});

Vue.filter('money', value => {
    numeral.locale('it');
    return numeral(value).format('0,000.00')
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import router from './router';

const app = new Vue({
    el: '#app',
    router,
    store
});
