import Vue from 'vue';

//youtube
import VueYoutube from 'vue-youtube';
Vue.use(VueYoutube);

// Router Vue
import Router from 'vue-router';
Vue.use(Router);

// Swiper slider
import VueAwesomeSwiper from 'vue-awesome-swiper';
import 'swiper/dist/css/swiper.css';
Vue.use(VueAwesomeSwiper, {});

// Vee-Validate
import VeeValidate from 'vee-validate';

import IT from 'vee-validate/dist/locale/it';
import EN from 'vee-validate/dist/locale/en';
import DE from 'vee-validate/dist/locale/de';
import ES from 'vee-validate/dist/locale/es';

Vue.use(VeeValidate, {
    locale: 'it',
    fieldsBagName: 'veeFields',
     dictionary: {
        it: IT,
        en: EN,
        de: DE,
        es: ES,
    }
});

// Sweetalert2
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

// Paypal-Checkout
import PayPal from 'vue-paypal-checkout';
Vue.component('paypal-checkout', PayPal);

import VueTimers from 'vue-timers';
Vue.use(VueTimers);

// component vue ScrolTo
import VueScrollTo from 'vue-scrollto'
Vue.use(VueScrollTo, {
    container: "body",
    duration: 600,
    easing: "ease",
    offset: 0,
    force: true,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true
})

/*
 * Imports
 */

//Componente Padre
import index from '../components/';

// Landing
import AppContent from '../components/landing/AppContent';
import headerLanding from '../components/landing/header';
import sliderOlbia from '../components/landing/sliderOlbia';
import services from '../components/landing/services';
import esperience from '../components/landing/esperience';
import galleryHorizontal from '../components/landing/galleryHorizontal';
import galleryFullWidth from '../components/landing/galleryFullWidth';
import mapa from '../components/landing/mapa';
import newsletter from '../components/landing/newsletter';
import unsubscribeNewsletter from '../components/landing/unsubscribenewsletter';
import aboutUs from '../components/landing/aboutUs';
import homeCamere from '../components/landing/homeCamere';
import contactUs from '../components/landing/contactUs';
import formBooking from '../components/booking/form';

// general
import termini from '../components/general/termini';
import floatCount from '../components/general/float-count';
import modalShare from '../components/general/modalShare.vue';
import information from '../components/general/information.vue';
import btnRegresar from '../components/general/btnRegresar.vue';

// Suites
import headerSuites from '../components/suites/headerSuites';
import navSuites from '../components/suites/navSuites';
import gallerySuites from '../components/suites/gallerySuites';
import infoSuites from '../components/suites/infoSuites';
import iconsServices from '../components/suites/iconsServices';
import generalServices from '../components/suites/generalServices';
import listServices from '../components/suites/listServices';

// user
import createAccount from '../components/user/createAccount';
import clientLogin from '../components/user/clientLogin';
import setPassword from '../components/user/setPassword';
import resetPassword from '../components/user/resetPassword';

// Booking
import headerBooking from '../components/booking/headerBooking';
import BookingStepOne from '../components/booking/step-one/';
import BookingStepTwo from '../components/booking/step-two/';
import BookingSteptree  from '../components/booking/step-three/';
import wizard from '../components/booking/wizard'
import video_youtube from '../components/booking/video_youtube';
import counter from '../components/booking/Counter';
import orderBooking from '../components/booking/step-three/Order.vue';

// My account
import MyAccount from '../components/myaccount';
import ListOrders from '../components/myaccount/ListOrders';
import user_detail from '../components/myaccount/user_detail';
import DeleteAccount from '../components/myaccount/DeleteAccount';
import myOrderDetail from '../components/myaccount/myOrderDetail.vue';
import ChangePassword from '../components/myaccount/ChangePassword';

// Error pages
import error404 from '../components/general/error404.vue';
import error500 from '../components/general/error500.vue';

//experience filter
import indexEsperience from '../components/esperience/indexEsperience.vue';
import formAttivita from '../components/esperience/formAttivita.vue';
import itemFilter from '../components/esperience/itemFilter.vue';
import indexDetails from '../components/esperience/details-esperience/index-details.vue';
import contentDetails from '../components/esperience/details-esperience/content-details.vue';
import galleryFull from '../components/esperience/details-esperience/galleryFull.vue';

// Village
import Village from '../components/village/';
import ListVillages from '../components/village/ListVillages';

// Blog
import indexBlogList    from '../components/blog/indexBlogList.vue';
import blogList         from '../components/blog/blogList.vue';
import indexBlogDetails from '../components/blog/blog-details/indexBlogDetails.vue';
import blogDetails from '../components/blog/blog-details/blogDetails.vue';
import invisibleRecaptcha from '../components/general/InvisibleRecaptcha';


/*
 * Components
 */
Vue.component('index',index);
Vue.component('header_landing', headerLanding);
Vue.component('form_booking',formBooking);
Vue.component('slider_olbia', sliderOlbia);
Vue.component('services', services);
Vue.component('esperience', esperience);
Vue.component('gallery_horizontal', galleryHorizontal);
Vue.component('mapa', mapa);
Vue.component('newsletter', newsletter);
Vue.component('aboutus', aboutUs);
Vue.component('home_camere', homeCamere);
Vue.component('headerSuites', headerSuites);
Vue.component('navSuites', navSuites);
Vue.component('gallerySuites', gallerySuites);
Vue.component('infoSuites', infoSuites);
Vue.component('iconsServices', iconsServices);
Vue.component('generalServices', generalServices);
Vue.component('listServices', listServices);
Vue.component('header_booking', headerBooking);
Vue.component('user_detail', user_detail);
Vue.component('float-count', floatCount);
Vue.component('counter', counter);
Vue.component('orderBooking', orderBooking);
Vue.component('gallery_full_width', galleryFullWidth);
Vue.component('form_attivita', formAttivita);
Vue.component('modal_share', modalShare);
Vue.component('item_filter', itemFilter);
Vue.component('information', information);
Vue.component('wizard', wizard);
Vue.component('video_youtube', video_youtube);
Vue.component('btn_regresar',btnRegresar);
Vue.component('contentDetails',contentDetails);
Vue.component('galleryFull',galleryFull);
Vue.component('invisible-recaptcha', invisibleRecaptcha);
Vue.component('blogList',blogList);
Vue.component('blogDetails',blogDetails);



// Routes
let router = new Router({
    mode:'history',
    routes: [
        {
            path: '*',
            meta: {
                public: true,
            },
            redirect: {
                path: '/404'
            }
        },
        {
            path: '/404',
            meta: {
                public: true,
            },
            name: 'NotFound',
            component: error404
        },
        {
            path: '/500',
            meta: {
                public: true,
            },
            name: 'ServerError',
            component: error500
        },
        {
            path: '/',
            name: 'AppContent',
            component: AppContent
        },
        {
            path: '/my-account',
            name: 'MyAccount',
            component: MyAccount
        },
        {
            path: '/user_detail',
            name: 'user_detail',
            component: user_detail
        },
        {
            path: '/change-password',
            name: 'ChangePassword',
            component: ChangePassword
        },
        {
            path: '/delete-account',
            name: 'DeleteAccount',
            component: DeleteAccount
        },
        {
            path: '/my-orders',
            name: 'ListOrders',
            component: ListOrders
        },
        {
            path: '/my-order-detail/:id',
            name: 'myOrderDetail',
            component: myOrderDetail
        },
        {
            path: '/contactUs',
            name: 'contactUs',
            component: contactUs
        },
        {
            path: '/booking',
            name: 'booking-step-1',
            component: BookingStepOne
        },
        {
            path: '/booking2',
            name: 'booking-step-2',
            component: BookingStepTwo
        },
        {
            path: '/booking2/:token',
            name: 'booking-step-2-with-token',
            component: BookingStepTwo
        },
        {
            path: '/booking3',
            name: 'booking-step-3',
            component: BookingSteptree
        },
        {
            path: '/clientLogin',
            name: 'clientLogin',
            component: clientLogin
        },
        {
            path: '/createAccount',
            name: 'createAccount',
            component: createAccount
        },
        {
            path: '/setPassword',
            name: 'setPassword',
            component: setPassword
        },
        {
            path: '/password/reseted/:hash',
            name: 'resetPassword',
            component: resetPassword
        },
        {
            path: '/unsubscribeNewsletter/:email',
            name: 'unsubscribeNewsletter',
            component: unsubscribeNewsletter
        },
        {
            path: '/termini',
            name: 'termini',
            component: termini
        },
        {
            path: '/esperienze',
            name: 'indexEsperience',
            component: indexEsperience
        },
        {
            path: '/esperienze/:slug',
            name: 'indexEsperienceByCategory',
            component: indexEsperience
        },
        {
            path: '/village/:slug',
            name: 'VillageBySlug',
            component: Village
        },
        {
            path: '/villages',
            name: 'ListVillages',
            component: ListVillages
        },
        {
            path: '/attivita/:slug',
            name: 'indexDetails',
            component: indexDetails
        },
        {
            path: '/promozioni',
            name: 'indexBlogList',
            component: indexBlogList
        },
        {
            path: '/promozioni/:slug',
            name: 'indexBlogDetails',
            component: indexBlogDetails
        }
    ],
    scrollBehavior(to, from) { 
        if (to.hash) {
            if (from.path !== '/')  window.location.href = to.fullPath
        } else return { x: 0, y: 0 }
    },
});
export default router;
