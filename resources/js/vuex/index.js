import Vue from 'vue'
import Vuex from 'vuex'
import Session from './modules/Session'
import Booking from './modules/Booking'
import Language from './modules/Language'
import Activity from './modules/Activity'
import Setting from './modules/Setting'
import Blog from './modules/Blog'
import Calendar from './modules/Calendar'
import createPersistedState from 'vuex-persistedstate'
import * as Cookie from 'js-cookie'

Vue.use(Vuex);

let store = new Vuex.Store({
    modules: {
        Session,
        Booking,
        Language,
        Activity,
        Setting,
        Blog,
        Calendar,
    },
    plugins: [
         createPersistedState({
             paths: ['Session.user', 'Session.authenticated', 'Session.enviroment', 'Booking.booking', 'Booking.filter', 'Booking.counter', 'Booking.locations', 'Booking.categories', 'Booking.orderDetail', 'Language.languageActive', 'Language.languages', 'Setting.data', 'Blog.posts'],
             getItem: key => Cookies.get(key),
             setItem: (key, value) => Cookies.set(key, value, { expires: 3, secure: true }),
             removeItem: key => Cookies.remove(key)
         })
      ]
});
export default store;
