const moment = require('moment');
export default {
    state: {
        booking: {
            code: null,
            personResponsible: {
                userId: null,
                name: null,
                email: null,
                phone: null,
                fiscalCode: null,
            },
            rooms: [],
            totalAmount: 0,
            comment: null,
            created_at: null,
            quantityPersons: 2,
            token: null,
        },
        filter: {},
        locations: [],
        categories: [],
        loading: false,
        counter: null,
        totalRows: null,
        perPage: null,
        order: null,
        orderDetail: null,
        validForm: false,
        all: [],
        rooms: []
    },
    actions: {
        setFilterDates({commit}, item) {
            commit('setFilterDates', {list: item});
        },
        setRoom(context, item) {
            return new Promise((resolve, reject) => {
                axios.post('/api/admin/rooms/lock', {
                    roomId: item.roomId,
                    checkinDate: item.bookingDate.checkin,
                    checkoutDate: item.bookingDate.checkout,
                    datetime: context.state.counter,
                }).then(response => {
                    if (response.data.data == null) {
                        reject(context.getters.getTags({tag:'booking_1_room_not_available_message'}))
                    } else {
                        context.commit('setRoom', {list: item})
                        resolve()
                    }
                }).catch(error => {
                    reject(error)
                })
            })
        },
        deleteRoom({commit}, item) {
            return new Promise((resolve, reject) => {
                axios.post('/api/admin/rooms/unlock', {
                    roomId: item.roomId,
                    checkinDate: item.bookingDate.checkin,
                    checkoutDate: item.bookingDate.checkout
                })
                    .then(response => {
                        commit('deleteRoom', {list: item});
                        resolve(response);
                    })
                    .catch(error => {
                        console.log(error);
                        reject(error);
                    })
            })
        },
        deleteBooking({commit}, item) {
            commit('deleteBooking', {list: item});
        },
        setPersonResponsible({commit}, item) {
            commit('setPersonResponsible', {list: item});
        },
        setRoomsWithResponsible({commit}, item) {
            commit('setRoomsWithResponsible', {list: item});
        },
        APIGetCategories(context) {
            context.state.loading = true;
            return new Promise((resolve, reject) => {
                axios.get('/api/admin/room_categories')
                    .then(response => {
                        context.commit('setCategories', response.data.data);
                        resolve(response);
                        context.state.loading = false
                    })
                    .catch(error => {
                        console.log(error);
                        context.state.loading = false;
                        reject()
                    })
            })
        },
        APIGetLocations(context) {
            context.state.loading = true;
            return new Promise((resolve, reject) => {
                axios.get('/api/admin/room_locations')
                    .then(response => {
                        context.commit('setLocations', response.data.data);
                        resolve(response);
                        context.state.loading = false
                    })
                    .catch(error => {
                        console.log(error);
                        context.state.loading = false;
                        reject()
                    })
            })
        },
        storeBooking(context, payload) {
            context.state.loading = true;
            return new Promise((resolve, reject) => {
                axios.post('/api/admin/bookings', payload)
                    .then(response => {
                        payload.code = response.data.data.code;
                        payload.created_at = moment().tz('Europe/Rome').format('YYYY-MM-DD HH:mm:ss'); //response.data.data.created_at
                        context.commit('storeBooking', payload);
                        resolve(response);
                        context.state.loading = false
                    })
                    .catch(error => {
                        console.log(error);
                        context.state.loading = false;
                        reject()
                    })

            })
        },

        setRoomsResponsibles(context, payload) {
            context.commit('setRoomsResponsibles', payload)
        },
        setCounter(context) {
            context.commit('setCounter')
        },
        resetCounter(context) {
            context.commit('resetCounter')
        },
        stopCounter(context) {
            context.commit('stopCounter')
        },
        setBookingComment(context, payload) {
            context.commit('setBookingComment', payload)
        },
        getBooking(context, booking_id){
            context.state.loading = true
            axios.get('/api/admin/bookings/' + booking_id)
                .then(response => {
                    context.commit('getBooking', response.data.data)
                })
                .catch(error => {
                    console.log(error)
                    context.state.loading = false
                })
        },
        getBookingToken(context, payload){
            context.state.loading = true;
            return new Promise((resolve, reject) => {
                axios.post('/api/admin/rates/get_rate/'+payload)
                .then(res=>{
                    context.commit('getBookingToken', res.data);
                    context.state.loading = false
                    resolve(res.data)
                })
                .catch(er=>{
                    reject(er.response)
                    context.state.loading = false

                })
            });

        },
        setBookingValidForm(context, payload){
            context.commit('setBookingValidForm', payload)
        },
        setQuantityPersons(context, payload){
            context.commit('setQuantityPersons', payload)
        },
        setUserId(context, payload){
            context.commit('setUserId', payload)
        },
        setBooking(context, payload){
            context.commit('setBooking', payload)
        }
    },
    getters: {
        getFilter: state => {
            return state.filter;
        },
        getRooms: state => {
            return state.booking.rooms;
        },
        getTotal: state => {
            return state.booking.totalAmount;
        },
        getBooking: state => {
            return state.booking;
        },
        getOrder: state => {
            return state.order;
        },
        getBookingLoading: state => {
            return state.loading;
        },
        getLocations: state => {
            return state.locations;
        },
        getCategories: state => {
            return state.categories;
        },
        getOrderDetail: state => {
            return state.orderDetail;
        },
        getLocationNameById: (state) => (id) => {
            let data = state.locations;

            const RESPONSE = data.find(key => key.id === parseInt(id));

            if (RESPONSE !== null || RESPONSE !== undefined) return RESPONSE.name;
        },
        getCategoryById: (state) => (id) => {
            const RESPONSE = state.categories.find(key => key.id === parseInt(id));

            if (RESPONSE !== null || RESPONSE !== undefined) return RESPONSE;
        },
        getCategoryBySlug: (state) => (slug) => {
            const RESPONSE = state.categories.find(key => key.slug === slug);

            if (RESPONSE !== null || RESPONSE !== undefined) return RESPONSE;
        },
        getCounter(state) {
            return state.counter
        },
        getBookingValidForm(state){
            return state.validForm
        },
        getQuantityPersons(state){
            return state.booking.quantityPersons
        },
    },
    mutations: {
        getBookingToken(state,res) {
            //Vue.set(state.booking, 'api', res.data);
            //Vue.set(state.booking, 'rooms', res.data.rooms);

        },
        setFilterDates(state, { list }) {
            Vue.set(state, 'filter', list)
        },
        clearFilterDates(state) {
            Vue.set(state, 'filter', {})
        },
        setRoom(state, { list }) {
            const ADD_ROOM = () => {
                let total = state.booking.totalAmount;
                total += parseFloat(list.totalItem);

                state.booking.rooms.push(list);
                Vue.set(state.booking, 'totalAmount', total);
                Vue.set(state.booking, 'token', null);
            };

            let data = state.booking.rooms;

            if (data.length > 0) {
                const RESPONSE = data.find(key => key.roomId === list.roomId);

                (RESPONSE === null || RESPONSE === undefined) ? ADD_ROOM() : 'already added';

            } else ADD_ROOM();
        },
        deleteRoom(state, { list }) {
            let data = state.booking.rooms;

            for (let i in data) {
                if (data[i].roomId === list.roomId) {
                    Vue.delete(state.booking.rooms, i);
                    let total = Math.abs(state.booking.totalAmount - list.totalItem);
                    Vue.set(state.booking, 'totalAmount', total);

                    break;
                }
            }
        },
        deleteBooking(state) {
            Vue.set(state.booking.personResponsible, 'userId', null);
            Vue.set(state.booking.personResponsible, 'name', null);
            Vue.set(state.booking.personResponsible, 'email', null);
            Vue.set(state.booking.personResponsible, 'phone', null);
            Vue.set(state.booking.personResponsible, 'fiscalCode', null);

            Vue.set(state.booking, 'rooms', []);
            Vue.set(state.booking, 'totalAmount', 0);

            Vue.set(state.booking, 'comment', null);
        },
        setPersonResponsible(state, { list }) {
            if (list.userId !== null) Vue.set(state.booking.personResponsible, 'userId', list.userId);


            Vue.set(state.booking.personResponsible, 'name', list.first_name + ' ' + list.last_name);
            Vue.set(state.booking.personResponsible, 'lastname', list.last_name);
            Vue.set(state.booking.personResponsible, 'email', list.email);
            Vue.set(state.booking.personResponsible, 'phone', list.phone);
            Vue.set(state.booking.personResponsible, 'fiscalCode', list.fiscalCode);
            Vue.set(state.booking.personResponsible, 'register', list.register);
            Vue.set(state.booking.personResponsible, 'password', list.password);
            Vue.set(state.booking.personResponsible, 'password_confirmation', list.password_confirmation);
        },
        setRoomsWithResponsible(state, { list }) {
            Vue.set(state.booking, 'personResponsible', list.personResponsible);
            Vue.set(state.booking, 'rooms', list.rooms);
            Vue.set(state.booking, 'comment', list.comment);
        },
        setCategories(state, payload) {
            Vue.set(state, 'categories', payload);
        },
        setLocations(state, payload) {
            Vue.set(state, 'locations', payload);
        },
        storeBooking(state, payload) {
            Vue.set(state, 'booking', payload);
        },
        setRoomsResponsibles(state, payload) {
            for (let i in payload) {
                let room = state.booking.rooms.find(key => key.roomId === payload[i].roomId)
                Vue.set(room, 'personResponsible', payload[i].personResponsible);
                Vue.set(room, 'adults_quantity', payload[i].adults_quantity);
                Vue.set(room, 'children_quantity', payload[i].children_quantity);
            }
        },
        setCounter(state) {
            state.counter = moment().tz('Europe/Rome').format('YYYY-MM-DD HH:mm:ss')
        },
        resetCounter(state) {
            Vue.set(state, 'counter', null);
            Vue.set(state.booking, 'rooms', []);
            Vue.set(state.booking, 'totalAmount', 0);
        },
        stopCounter(state) {
            Vue.set(state, 'counter', null);
        },
        setBookingComment(state, payload) {
            Vue.set(state.booking, 'comment', payload)
        },
        getBooking(state, payload){
            Vue.set(state, 'order', payload.booking)
        },
        setOrderDetail(state, { list }) {
            Vue.set(state, 'orderDetail', list);
        },
        setBookingValidForm(state, payload) {
            Vue.set(state, 'validForm', payload)
        },
        setQuantityPersons(state, payload) {
            Vue.set(state.booking, 'quantityPersons', payload)
        },
        setUserId(state, payload){
            Vue.set(state.booking.personResponsible, 'userId', payload)
        },
        setBooking(state, payload){
            Vue.set(state, 'booking', payload)
        },
    }
}
