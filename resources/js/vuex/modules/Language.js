export default {
    state: {
        languageActive: {
            flag: 'it',
            languageTagsActive: []
        },
        languages: {
            flags: [],
            languagesTags: []
        }
    },
    actions: {
        APIGetAllLanguagesTags({commit}) {
            axios.get('/api/tag_translations').then((res) => {
                commit('setLanguagesTags', { list: res.data.data })

            }).catch((error) => console.log(error))
        },
        changeLanguageByFlagActive({commit}, item) {
            commit('changeLanguageByFlagActive', { list: item })
        },
    },
    getters: {
        getLanguagesFlagActive: state => {
            return state.languageActive.flag;
        },
        getLanguagesTagActive: state => {
            return state.languageActive.languageTagsActive;
        },
        getLanguagesFlags: state => {
            return state.languages.flags;
        },
        getTags: state => item => {
            let data = state.languageActive.languageTagsActive;
            let tag = data.find(key => key.tag === item.tag);

            if (!tag) {
                let defaultLang = state.languages.languagesTags.find(index => index.lang === 'it');
                let it = defaultLang.screens.find(key => key.tag === item.tag);

                if (!it) return { value: 'error no tags by default in tag: ' + item.tag };
                return it.value;

            }
            return tag.value;
        }
    },
    mutations: {
        setLanguagesTags(state, {list}) {
            let flags = [];

            list.forEach(index => {
                if (index.screens.length > 0) {
                    flags.push({ code: index.lang });
                }
            });

            Vue.set(state.languages, 'flags', flags);
            Vue.set(state.languages, 'languagesTags', list);

            let lang = state.languageActive.flag;

            const RESPONSE = list.find(key => key.lang === lang);

            Vue.set(state.languageActive, 'languageTagsActive', RESPONSE.screens)
        },
        changeLanguageByFlagActive(state, {list}) {
            let data = state.languages.languagesTags;

            const RESPONSE = data.find(key => key.lang === list);

            Vue.set(state.languageActive, 'flag', list);
            Vue.set(state.languageActive, 'languageTagsActive', RESPONSE.screens)
        }
    }
};
