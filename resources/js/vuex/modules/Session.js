export default {
    state: {
        user: [],
        authenticated: false,
        enviroment: {},
        loading: false
    },

    actions: {
        setSession({ commit }, item) {
            return new Promise((resolve, reject) => {
                axios.post('/api/auth/login', item)
                    .then(response => {
                        if (response.status === 200) {
                            commit('setSession', { list: response.data });
                            resolve(response);
                        }
                    })
                    .catch(error => {
                        console.log('vuex', error);
                        reject(error);
                    })
            });
        },
        deleteSession({commit}, item) {
            commit('deleteSession', {list: item});
        },
        getEnviroment({commit}) {
            axios.post('/api/get_env').then((response) => {
                commit('setEnviroment', {list: response.data})
            }).catch((error) => {
                console.log(error.data);
            })
        },
        editUser(context, payload) {
            context.state.loading = true;

            return new Promise((resolve, reject) => {
                axios.post('/api/update/user', payload)
                    .then(({ data }) => {
                        context.commit('editUser', data);
                        resolve(data);

                    }).catch((e) => {
                    console.log(e);
                    reject(e);
                })
                    .finally(() => context.state.loading = false)
            });
        },
        createAccount(context, payload){
            return new Promise((resolve, reject) => {
                axios.post('/api/auth/signup', payload)
                    .then(response => {
                        resolve(response)
                    }).catch((error) => {
                        reject(error)
                    })
            })
        }
    },
    getters: {
        getSession: state => {
            return state.user;
        },
        getauthenticated: state => {
            return state.authenticated;
        },
        getEnviroment: state => {
            return state.enviroment;
        },
        getLoading: state => {
            return state.loading;
        }

    },
    mutations: {
        setSession(state, { list }) {
            let user = {
                admin: list.admin,
                id: list.user.id,
                token: list.token,
                name: list.user.name,
                lastname: list.user.lastname,
                email: list.user.email,
                phone: list.user.user_details.phone,
                optionalPhone: list.user.user_details.empresa,
                fiscalCode: list.user.user_details.fiscal_code,
                roleUser: list.roles,
            };

            Vue.set(state, 'authenticated', true);
            Vue.set(state, 'user', [user]);
        },
        deleteSession(state) {
            Vue.set(state, 'authenticated', false);
            Vue.set(state, 'user', []);
        },
        setEnviroment(state, {list}) {
            Vue.set(state, "enviroment", list)
        },
        editUser(state, payload) {

            let obj = {};
            obj.id = payload.user.id;
            obj.token = state.user[0].token;
            obj.name = payload.user.name;
            obj.lastname = payload.user.lastname;
            obj.email = payload.user.email;
            obj.phone = payload.user.user_details.phone;
            obj.optionalPhone = payload.user.user_details.empresa;
            obj.fiscalCode = payload.user.user_details.fiscal_code;
            obj.roleUser = payload.roles;
            state.user.splice(0, 1, obj)
        },
    }
}
