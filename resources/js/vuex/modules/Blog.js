export default {
    state: {
        posts: null,
        loading: false,
    },
    actions: {
        getPosts(context) {
            context.state.loading = true
            axios.get('/api/admin/blogs')
                .then(response => {
                    if (response.status === 200) context.commit('getPosts', response.data)
                })
                .catch(() => context.state.loading = false)
        }
    },
    getters: {
        getPosts: state => {
            return state.posts
        },
        getPost: state => slug => {
            let post = state.posts.find(post => post.slug === slug);
            return post;
        },
        loading: state => {
            return state.loading;
        }
    },
    mutations: {
        getPosts(state, payload){
            Vue.set(state, 'posts', payload.data)
            state.loading = false
        }
    },
}