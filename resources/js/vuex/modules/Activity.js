export default {
    state: {
        data: [],
        solo: null,
        activityCategories: [],
        loading: false,
    },
    getters: {
        getActivityCategories(state){
            return state.activityCategories
        },
        getActivities(state){
            return state.data
        },
        getActivity(state){
            return state.solo[0]
        },
    },
    actions: {
        getActivityCategories(context, payload){
            context.state.loading = true
            return new Promise((resolve, reject) => {
                axios.get('/api/admin/activity_categories')
                    .then(response => {
                        context.commit('getActivityCategories', response.data.data)
                        context.state.loading = false
                    })
                    .catch(error => {
                        console.log(error)
                        context.state.loading = false
                    })
            })
        },
        getActivities(context, payload){
            context.state.loading = true
            return new Promise((resolve, reject) => {
                axios.get('/api/admin/activities')
                    .then(response => {
                        context.commit('getActivities', response.data.data)
                        context.state.loading = false
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error)
                        context.state.loading = false
                        reject(error)
                    })
            })
        },
        getActivity(context, slug){
            context.state.loading = true
            return new Promise((resolve, reject) => {
                axios.get('/api/admin/activity/' + slug)
                    .then(response => {
                        console.log(response.data.data)
                        context.commit('getActivity', response.data.data)
                        context.state.loading = false
                        resolve(response)
                    })
                    .catch(error => {
                        console.log(error)
                        context.state.loading = false
                        reject(error)
                    })
            })
        },
    },
    mutations: {
        getActivityCategories(state, payload){
            Vue.set(state, 'activityCategories', payload);
        },
        getActivities(state, payload){
            Vue.set(state, 'data', payload);
        },
        getActivity(state, payload){
            Vue.set(state, 'solo', payload);
        },
    }
}
