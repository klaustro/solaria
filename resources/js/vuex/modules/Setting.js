export default {
    state: {
        data: [],
    },
    actions: {
        getSettings(context) {
            context.state.loading = true
            axios.get('/api/admin/settings')
                .then(response => {
                    context.commit('getSettings', response.data.data)
                })
                .catch(error => {
                    console.log(error)
                    context.state.loading = false
                })
        }
    },
    getters: {
        getSettings: state => {
            return state.data
        },
        getSetting: state => key => {
            return state.data.find(setting => setting.key === key)
        },
    },
    mutations: {
        getSettings(state, payload) {
            Vue.set(state, 'data', payload)
            state.loading = false
        }
    },
}
