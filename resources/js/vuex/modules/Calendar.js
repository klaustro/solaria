export default {
    state: {
        rooms: [],
        bookings: [],
        seasons: [],
        rates: [],
        loading: false,
    },
    getters: {
        getAllRooms(state){
            return state.rooms
        },
        getBookings(state){
            return state.bookings
        },
        getSeasons(state){
            return state.seasons
        },
        getRates(state){
            return state.rates
        },
    },
    actions: {
        getAllRooms(context){
            context.state.loading = true
            axios.get('/api/admin/rooms')
                .then(response => {
                    context.commit('getAllRooms', response.data)
                })
                .catch(error => {
                    console.log(error)
                    context.state.loading = false
                })
        },
        getBookings(context, payload){
            context.state.loading = true
            axios.post('/api/admin/bookings_calendar', payload)
                .then(response => {
                    context.commit('getBookings', response.data)
                })
                .catch(error => {
                    console.log(error)
                    context.state.loading = false
                })
        },
        getSeasons(context, payload){
            context.state.loading = true
            axios.post('/api/admin/room_seasons_calendar', payload)
                .then(response => {
                    console.log(response.data)
                    context.commit('getSeasons', response.data)
                })
                .catch(error => {
                    console.log(error)
                    context.state.loading = false
                })
        },
        getRates(context, payload){
            context.state.loading = true
            axios.post('/api/admin/rates', payload)
                .then(response => {
                    console.log(response.data)
                    context.commit('getRates', response.data)
                })
                .catch(error => {
                    console.log(error)
                    context.state.loading = false
                })
        },
    },
    mutations: {
        getAllRooms(state, payload){
            Vue.set(state, 'rooms', payload.data.rooms)
        },
        getBookings(state, payload){
            Vue.set(state, 'bookings', payload.data)
        },
        getSeasons(state, payload){
            Vue.set(state, 'seasons', payload.data)
        },
        getRates(state, payload){
            Vue.set(state, 'rates', payload.data)
        },
    }
}